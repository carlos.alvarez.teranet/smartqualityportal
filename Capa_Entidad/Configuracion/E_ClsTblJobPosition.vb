Public Class E_ClsTblJobPosition

	Private _IdJobPosition As Integer
	Private _JobPosition As String
	Private _LineManeger As String
	Private _UltAct As String
	Private _Estado As Boolean

	Private _LastTransactionUser As String
	Private _LastTransactionDate As Date

	Public Property IdJobPosition() As Integer
		Get
			Return _IdJobPosition
		End Get
		Set(value As Integer)
			_IdJobPosition = value
		End Set
	End Property

	Public Property JobPosition() As String
		Get
			Return _JobPosition
		End Get
		Set(value As String)
			_JobPosition = value
		End Set
	End Property

	Public Property UltAct() As String
		Get
			Return _UltAct
		End Get
		Set(value As String)
			_UltAct = value
		End Set
	End Property



	Public Property LineManeger() As String
		Get
			Return _LineManeger
		End Get
		Set(value As String)
			_LineManeger = value
		End Set
	End Property

	Public Property Estado() As Boolean
		Get
			Return _Estado
		End Get
		Set(value As Boolean)
			_Estado = value
		End Set
	End Property

	Public Property LastTransactionUser() As String
		Get
			Return _LastTransactionUser
		End Get
		Set(value As String)
			_LastTransactionUser = value
		End Set
	End Property

	Public Property LastTransactionDate() As Date
		Get
			Return _LastTransactionDate
		End Get
		Set(value As Date)
			_LastTransactionDate = value
		End Set
	End Property

End Class

