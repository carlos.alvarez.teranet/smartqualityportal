Public Class E_ClsTblEjecucion

	Private _IdEjecucion As Integer
	Private _IdEjecucionPadre As Integer
	Private _Owner As String
	Private _FechaCreacion As Date
	Private _IdEstado As Integer
	Private _Titulo As String
	Private _TipoEjecucion As String
	Private _FechaHoraProgramada As Date
	Private _FechaHoraTermino As Date
	Private _FechaHoraReal As Date
	Private _LastTransactionUser As String
	Private _UltAct As String


	Public Property IdEjecucion() As Integer
		Get
			Return _IdEjecucion
		End Get
		Set(value As Integer)
			_IdEjecucion = value
		End Set
	End Property

	Public Property IdEjecucionPadre() As Integer
		Get
			Return _IdEjecucionPadre
		End Get
		Set(value As Integer)
			_IdEjecucionPadre = value
		End Set
	End Property

	Public Property UltAct() As String
		Get
			Return _UltAct
		End Get
		Set(value As String)
			_UltAct = value
		End Set
	End Property

	Public Property Owner() As String
		Get
			Return _Owner
		End Get
		Set(value As String)
			_Owner = value
		End Set
	End Property

	Public Property FechaCreacion() As DateTime
		Get
			Return _FechaCreacion
		End Get
		Set(value As Date)
			_FechaCreacion = value
		End Set
	End Property

	Public Property IdEstado() As Integer
		Get
			Return _IdEstado
		End Get
		Set(value As Integer)
			_IdEstado = value
		End Set
	End Property

	Public Property Titulo() As String
		Get
			Return _Titulo
		End Get
		Set(value As String)
			_Titulo = value
		End Set
	End Property

	Public Property TipoEjecucion() As String
		Get
			Return _TipoEjecucion
		End Get
		Set(value As String)
			_TipoEjecucion = value
		End Set
	End Property

	Public Property FechaHoraProgramada() As DateTime
		Get
			Return _FechaHoraProgramada
		End Get
		Set(value As Date)
			_FechaHoraProgramada = value
		End Set
	End Property

	Public Property FechaHoraTermino() As DateTime

		Get
			Return _FechaHoraTermino
		End Get
		Set(value As Date)
			_FechaHoraTermino = value
		End Set
	End Property



	Public Property FechaHoraReal() As DateTime
		Get
			Return _FechaHoraReal
		End Get
		Set(value As Date)
			_FechaHoraReal = value
		End Set
	End Property

	Public Property LastTransactionUser() As String
		Get
			Return _LastTransactionUser
		End Get
		Set(value As String)
			_LastTransactionUser = value
		End Set
	End Property

End Class

