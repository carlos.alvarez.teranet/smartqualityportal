Public Class E_ClsTblNubWorkflowJobPosition

	Private _IdWorkflow As Integer
	Private _IdJobPosition As Integer
	Private _PosicionRuta As Integer
	Private _LastTransactionUser As String
	Private _LastTransactionDate As String

	Public Property IdWorkflow() As Integer
		Get
			Return _IdWorkflow
		End Get
		Set(value As Integer)
			_IdWorkflow = value
		End Set
	End Property

	Public Property IdJobPosition() As Integer
		Get
			Return _IdJobPosition
		End Get
		Set(value As Integer)
			_IdJobPosition = value
		End Set
	End Property

	Public Property PosicionRuta() As Integer
		Get
			Return _PosicionRuta
		End Get
		Set(value As Integer)
			_PosicionRuta = value
		End Set
	End Property

	Public Property LastTransactionUser() As String
		Get
			Return _LastTransactionUser
		End Get
		Set(value As String)
			_LastTransactionUser = value
		End Set
	End Property

	Public Property LastTransactionDate() As String
		Get
			Return _LastTransactionDate
		End Get
		Set(value As String)
			_LastTransactionDate = value
		End Set
	End Property

End Class

