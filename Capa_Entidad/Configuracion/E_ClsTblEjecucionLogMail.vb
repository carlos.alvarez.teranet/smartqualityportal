Public Class E_ClsTblEjecucionLogMail

	Private _IdEjecucionLogMail As Integer
	Private _IdEjecucion As Integer
	Private _Fecha As Date
	Private _Remitente As String
	Private _Destinatario As String
	Private _Asunto As String
	Private _Cuerpo As String
	Private _Adjunto As String
	Private _LastTransactionUser As String
	Private _LastTransactionDate As Date

	Public Property IdEjecucionLogMail() As Integer
		Get
			Return _IdEjecucionLogMail
		End Get
		Set(value As Integer)
			_IdEjecucionLogMail = value
		End Set
	End Property

	Public Property IdEjecucion() As Integer
		Get
			Return _IdEjecucion
		End Get
		Set(value As Integer)
			_IdEjecucion = value
		End Set
	End Property

	Public Property Fecha() As Date
		Get
			Return _Fecha
		End Get
		Set(value As Date)
			_Fecha = value
		End Set
	End Property

	Public Property Remitente() As String
		Get
			Return _Remitente
		End Get
		Set(value As String)
			_Remitente = value
		End Set
	End Property

	Public Property Destinatario() As String
		Get
			Return _Destinatario
		End Get
		Set(value As String)
			_Destinatario = value
		End Set
	End Property

	Public Property Asunto() As String
		Get
			Return _Asunto
		End Get
		Set(value As String)
			_Asunto = value
		End Set
	End Property

	Public Property Cuerpo() As String
		Get
			Return _Cuerpo
		End Get
		Set(value As String)
			_Cuerpo = value
		End Set
	End Property

	Public Property Adjunto() As String
		Get
			Return _Adjunto
		End Get
		Set(value As String)
			_Adjunto = value
		End Set
	End Property

	Public Property LastTransactionUser() As String
		Get
			Return _LastTransactionUser
		End Get
		Set(value As String)
			_LastTransactionUser = value
		End Set
	End Property

	Public Property LastTransactionDate() As Date
		Get
			Return _LastTransactionDate
		End Get
		Set(value As Date)
			_LastTransactionDate = value
		End Set
	End Property

End Class

