Public Class E_ClsTblComentarioEjecucion

	Private _IdComentario As Integer
	Private _IdEjecucion As Integer
	Private _Fecha As Date
	Private _Comentario As String
	Private _LastTransactionUser As String
	Private _LastTransactionDate As Date

	Public Property IdComentario() As Integer
		Get
			Return _IdComentario
		End Get
		Set(value As Integer)
			_IdComentario = value
		End Set
	End Property

	Public Property IdEjecucion() As Integer
		Get
			Return _IdEjecucion
		End Get
		Set(value As Integer)
			_IdEjecucion = value
		End Set
	End Property

	Public Property Fecha() As Date
		Get
			Return _Fecha
		End Get
		Set(value As Date)
			_Fecha = value
		End Set
	End Property

	Public Property Comentario() As String
		Get
			Return _Comentario
		End Get
		Set(value As String)
			_Comentario = value
		End Set
	End Property

	Public Property LastTransactionUser() As String
		Get
			Return _LastTransactionUser
		End Get
		Set(value As String)
			_LastTransactionUser = value
		End Set
	End Property

	Public Property LastTransactionDate() As Date
		Get
			Return _LastTransactionDate
		End Get
		Set(value As Date)
			_LastTransactionDate = value
		End Set
	End Property

End Class

