Public Class E_ClsTblCarpetaAutorizado

	Private _IdCarpeta As Integer
	Private _IdAutorizado As String
	Private _LastTransactionUser As String
	Private _LastTransactionDate As Date

	Public Property IdCarpeta() As Integer
		Get
			Return _IdCarpeta
		End Get
		Set(value As Integer)
			_IdCarpeta = value
		End Set
	End Property

	Public Property IdAutorizado() As String
		Get
			Return _IdAutorizado
		End Get
		Set(value As String)
			_IdAutorizado = value
		End Set
	End Property

	Public Property LastTransactionUser() As String
		Get
			Return _LastTransactionUser
		End Get
		Set(value As String)
			_LastTransactionUser = value
		End Set
	End Property

	Public Property LastTransactionDate() As Date
		Get
			Return _LastTransactionDate
		End Get
		Set(value As Date)
			_LastTransactionDate = value
		End Set
	End Property

End Class 

