Public Class E_ClsTblOperadorLogico

	Private _IdOperadorLogico As Integer
	Private _IdVariableCondicion As Integer
	Private _OperadorLogico As String
	Private _NombreOperadorLogico As String

	Public Property IdVariableCondicion() As Integer
		Get
			Return _IdVariableCondicion
		End Get
		Set(value As Integer)
			_IdVariableCondicion = value
		End Set
	End Property

	Public Property IdOperadorLogico() As Integer
		Get
			Return _IdOperadorLogico
		End Get
		Set(value As Integer)
			_IdOperadorLogico = value
		End Set
	End Property

	Public Property OperadorLogico() As String
		Get
			Return _OperadorLogico
		End Get
		Set(value As String)
			_OperadorLogico = value
		End Set
	End Property

	Public Property NombreOperadorLogico() As String
		Get
			Return _NombreOperadorLogico
		End Get
		Set(value As String)
			_NombreOperadorLogico = value
		End Set
	End Property

End Class 

