﻿Public Class E_ClsTblAdjuntoEjecucion
    Private _IdAdjunto As Integer
    Private _IdTemp As String
    Private _IdEjecucion As Integer
    Private _Comentario As String
    Private _AdjName As String
    Private _AdjFile As Byte()
    Private _AdjSize As String
    Private _AdjExt As String
    Private _LastTransactionUser As String
    Public Property IdAdjunto() As Integer
        Get
            Return _IdAdjunto
        End Get
        Set(value As Integer)
            _IdAdjunto = value
        End Set
    End Property
    Public Property IdTemp() As String
        Get
            Return _IdTemp
        End Get
        Set(value As String)
            _IdTemp = value
        End Set
    End Property
    Public Property IdEjecucion() As Integer
        Get
            Return _IdEjecucion
        End Get
        Set(value As Integer)
            _IdEjecucion = value
        End Set
    End Property
    Public Property Comentario() As String
        Get
            Return _Comentario
        End Get
        Set(value As String)
            _Comentario = value
        End Set
    End Property
    Public Property AdjName() As String
        Get
            Return _AdjName
        End Get
        Set(value As String)
            _AdjName = value
        End Set
    End Property
    Public Property AdjFile() As Byte()
        Get
            Return _AdjFile
        End Get
        Set(value As Byte())
            _AdjFile = value
        End Set
    End Property
    Public Property AdjSize() As String
        Get
            Return _AdjSize
        End Get
        Set(value As String)
            _AdjSize = value
        End Set
    End Property
    Public Property AdjExt() As String
        Get
            Return _AdjExt
        End Get
        Set(value As String)
            _AdjExt = value
        End Set
    End Property
    Public Property LastTransactionUser() As String
        Get
            Return _LastTransactionUser
        End Get
        Set(value As String)
            _LastTransactionUser = value
        End Set
    End Property
End Class
