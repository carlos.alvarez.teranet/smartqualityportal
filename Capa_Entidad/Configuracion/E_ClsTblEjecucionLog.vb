Public Class E_ClsTblEjecucionLog

	Private _IdEjecucionLog As Integer
	Private _IdEjecucion As Integer
	Private _Fecha As Date
	Private _IdAccion As Integer
	Private _Comentario As String
	Private _LastTransactionUser As String
	Private _LastTransactionDate As Date

	Public Property IdEjecucionLog() As Integer
		Get
			Return _IdEjecucionLog
		End Get
		Set(value As Integer)
			_IdEjecucionLog = value
		End Set
	End Property

	Public Property IdEjecucion() As Integer
		Get
			Return _IdEjecucion
		End Get
		Set(value As Integer)
			_IdEjecucion = value
		End Set
	End Property

	Public Property Fecha() As Date
		Get
			Return _Fecha
		End Get
		Set(value As Date)
			_Fecha = value
		End Set
	End Property

	Public Property IdAccion() As Integer
		Get
			Return _IdAccion
		End Get
		Set(value As Integer)
			_IdAccion = value
		End Set
	End Property

	Public Property Comentario() As String
		Get
			Return _Comentario
		End Get
		Set(value As String)
			_Comentario = value
		End Set
	End Property

	Public Property LastTransactionUser() As String
		Get
			Return _LastTransactionUser
		End Get
		Set(value As String)
			_LastTransactionUser = value
		End Set
	End Property

	Public Property LastTransactionDate() As Date
		Get
			Return _LastTransactionDate
		End Get
		Set(value As Date)
			_LastTransactionDate = value
		End Set
	End Property

End Class

