Public Class E_ClsTblEjecucionCritica

	Private _IdEjecucionCritica As Integer
	Private _IdEjecucion As Integer
	Private _KeyCritica As String
	Private _TxtCritica As String

	Public Property IdEjecucionCritica() As Integer
		Get
			Return _IdEjecucionCritica
		End Get
		Set(value As Integer)
			_IdEjecucionCritica = value
		End Set
	End Property

	Public Property IdEjecucion() As Integer
		Get
			Return _IdEjecucion
		End Get
		Set(value As Integer)
			_IdEjecucion = value
		End Set
	End Property

	Public Property KeyCritica() As String
		Get
			Return _KeyCritica
		End Get
		Set(value As String)
			_KeyCritica = value
		End Set
	End Property

	Public Property TxtCritica() As String
		Get
			Return _TxtCritica
		End Get
		Set(value As String)
			_TxtCritica = value
		End Set
	End Property

End Class

