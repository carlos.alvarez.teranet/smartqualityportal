Public Class E_ClsTblNubCarpetaListaDistribucion

	Private _IdCarpeta As Integer
	Private _IdListaDistribucion As Integer
	Private _LastTransactionUser As String
	Private _LastTransactionDate As Date

	Public Property IdCarpeta() As Integer
		Get
			Return _IdCarpeta
		End Get
		Set(value As Integer)
			_IdCarpeta = value
		End Set
	End Property

	Public Property IdListaDistribucion() As Integer
		Get
			Return _IdListaDistribucion
		End Get
		Set(value As Integer)
			_IdListaDistribucion = value
		End Set
	End Property

	Public Property LastTransactionUser() As String
		Get
			Return _LastTransactionUser
		End Get
		Set(value As String)
			_LastTransactionUser = value
		End Set
	End Property

	Public Property LastTransactionDate() As Date
		Get
			Return _LastTransactionDate
		End Get
		Set(value As Date)
			_LastTransactionDate = value
		End Set
	End Property

End Class

