Public Class E_ClsTblListaDistribucionUsuario

	Private _IdListaDistribucion As Integer
	Private _IdUsuario As String
	Private _LastTransactionUser As String
	Private _LastTransactionDate As Date

	Public Property IdListaDistribucion() As Integer
		Get
			Return _IdListaDistribucion
		End Get
		Set(value As Integer)
			_IdListaDistribucion = value
		End Set
	End Property

	Public Property IdUsuario() As String
		Get
			Return _IdUsuario
		End Get
		Set(value As String)
			_IdUsuario = value
		End Set
	End Property

	Public Property LastTransactionUser() As String
		Get
			Return _LastTransactionUser
		End Get
		Set(value As String)
			_LastTransactionUser = value
		End Set
	End Property

	Public Property LastTransactionDate() As Date
		Get
			Return _LastTransactionDate
		End Get
		Set(value As Date)
			_LastTransactionDate = value
		End Set
	End Property

End Class

