Public Class E_ClsTblListaDistribucion

	Private _IdListaDistribucion As Integer
	Private _ListaDistribucion As String
	Private _Estado As Boolean
	Private _LastTransactionUser As String
	Private _UltAct As String
	Private _LastTransactionDate As Date

	Public Property IdListaDistribucion() As Integer
		Get
			Return _IdListaDistribucion
		End Get
		Set(value As Integer)
			_IdListaDistribucion = value
		End Set
	End Property

	Public Property ListaDistribucion() As String
		Get
			Return _ListaDistribucion
		End Get
		Set(value As String)
			_ListaDistribucion = value
		End Set
	End Property

	Public Property UltAct() As String
		Get
			Return _UltAct
		End Get
		Set(value As String)
			_UltAct = value
		End Set
	End Property



	Public Property Estado() As Boolean
		Get
			Return _Estado
		End Get
		Set(value As Boolean)
			_Estado = value
		End Set
	End Property

	Public Property LastTransactionUser() As String
		Get
			Return _LastTransactionUser
		End Get
		Set(value As String)
			_LastTransactionUser = value
		End Set
	End Property

	Public Property LastTransactionDate() As Date
		Get
			Return _LastTransactionDate
		End Get
		Set(value As Date)
			_LastTransactionDate = value
		End Set
	End Property

End Class

