Public Class E_ClsTblCriticaCondicion

	Private _IdCritica As Integer
	Private _Orden As Integer
	Private _IdVariableCondicion As Integer
	Private _OperadorLogico As String
	Private _Valor1 As String
	Private _Valor2 As String
	Private _AndOr As String
	Private _LastTransactionUser As String
	Private _LastTransactionDate As Date

	Public Property IdCritica() As Integer
		Get
			Return _IdCritica
		End Get
		Set(value As Integer)
			_IdCritica = value
		End Set
	End Property

	Public Property Orden() As Integer
		Get
			Return _Orden
		End Get
		Set(value As Integer)
			_Orden = value
		End Set
	End Property

	Public Property IdVariableCondicion() As Integer
		Get
			Return _IdVariableCondicion
		End Get
		Set(value As Integer)
			_IdVariableCondicion = value
		End Set
	End Property

	Public Property OperadorLogico() As String
		Get
			Return _OperadorLogico
		End Get
		Set(value As String)
			_OperadorLogico = value
		End Set
	End Property

	Public Property Valor1() As String
		Get
			Return _Valor1
		End Get
		Set(value As String)
			_Valor1 = value
		End Set
	End Property

	Public Property Valor2() As String
		Get
			Return _Valor2
		End Get
		Set(value As String)
			_Valor2 = value
		End Set
	End Property

	Public Property AndOr() As String
		Get
			Return _AndOr
		End Get
		Set(value As String)
			_AndOr = value
		End Set
	End Property

	Public Property LastTransactionUser() As String
		Get
			Return _LastTransactionUser
		End Get
		Set(value As String)
			_LastTransactionUser = value
		End Set
	End Property

	Public Property LastTransactionDate() As Date
		Get
			Return _LastTransactionDate
		End Get
		Set(value As Date)
			_LastTransactionDate = value
		End Set
	End Property

End Class

