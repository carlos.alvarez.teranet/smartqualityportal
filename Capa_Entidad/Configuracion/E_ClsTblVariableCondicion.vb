Public Class E_ClsTblVariableCondicion

	Private _IdVariableCondicion As Integer
	Private _VariableCondicion As String
	Private _IdTipoDato As Integer
	Private _Largo As String
	Private _Tabla As String
	Private _Estado As Boolean
	Private _LastTransactionUser As String
	Private _LastTransactionDate As Date
	Private _isComilla As Boolean


	Public Property IdVariableCondicion() As Integer
		Get
			Return _IdVariableCondicion
		End Get
		Set(value As Integer)
			_IdVariableCondicion = value
		End Set
	End Property

	Public Property isComilla() As Boolean
		Get
			Return _isComilla
		End Get
		Set(value As Boolean)
			_isComilla = value
		End Set
	End Property

	Public Property VariableCondicion() As String
		Get
			Return _VariableCondicion
		End Get
		Set(value As String)
			_VariableCondicion = value
		End Set
	End Property

	Public Property IdTipoDato() As Integer
		Get
			Return _IdTipoDato
		End Get
		Set(value As Integer)
			_IdTipoDato = value
		End Set
	End Property

	Public Property Largo() As String
		Get
			Return _Largo
		End Get
		Set(value As String)
			_Largo = value
		End Set
	End Property

	Public Property Tabla() As String
		Get
			Return _Tabla
		End Get
		Set(value As String)
			_Tabla = value
		End Set
	End Property

	Public Property Estado() As Boolean
		Get
			Return _Estado
		End Get
		Set(value As Boolean)
			_Estado = value
		End Set
	End Property

	Public Property LastTransactionUser() As String
		Get
			Return _LastTransactionUser
		End Get
		Set(value As String)
			_LastTransactionUser = value
		End Set
	End Property

	Public Property LastTransactionDate() As Date
		Get
			Return _LastTransactionDate
		End Get
		Set(value As Date)
			_LastTransactionDate = value
		End Set
	End Property

End Class 

