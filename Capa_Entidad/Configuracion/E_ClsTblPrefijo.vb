Public Class E_ClsTblPrefijo

	Private _IdPrefijo As Integer
	Private _IdCarpeta As Integer
	Private _Prefijo As String
	Private _Estado As Boolean
	Private _LastTransactionUser As String
	Private _LastTransactionDate As Date

	Public Property IdPrefijo() As Integer
		Get
			Return _IdPrefijo
		End Get
		Set(value As Integer)
			_IdPrefijo = value
		End Set
	End Property

	Public Property IdCarpeta() As Integer
		Get
			Return _IdCarpeta
		End Get
		Set(value As Integer)
			_IdCarpeta = value
		End Set
	End Property

	Public Property Prefijo() As String
		Get
			Return _Prefijo
		End Get
		Set(value As String)
			_Prefijo = value
		End Set
	End Property

	Public Property Estado() As Boolean
		Get
			Return _Estado
		End Get
		Set(value As Boolean)
			_Estado = value
		End Set
	End Property

	Public Property LastTransactionUser() As String
		Get
			Return _LastTransactionUser
		End Get
		Set(value As String)
			_LastTransactionUser = value
		End Set
	End Property

	Public Property LastTransactionDate() As Date
		Get
			Return _LastTransactionDate
		End Get
		Set(value As Date)
			_LastTransactionDate = value
		End Set
	End Property

End Class 

