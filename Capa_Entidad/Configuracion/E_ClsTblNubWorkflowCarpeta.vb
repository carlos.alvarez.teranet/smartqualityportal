Public Class E_ClsTblNubWorkflowCarpeta

	Private _IdWorkflow As Integer
	Private _IdCarpeta As Integer
	Private _LastTransactionUser As String
	Private _LastTransactionDate As String

	Public Property IdWorkflow() As Integer
		Get
			Return _IdWorkflow
		End Get
		Set(value As Integer)
			_IdWorkflow = value
		End Set
	End Property

	Public Property IdCarpeta() As Integer
		Get
			Return _IdCarpeta
		End Get
		Set(value As Integer)
			_IdCarpeta = value
		End Set
	End Property

	Public Property LastTransactionUser() As String
		Get
			Return _LastTransactionUser
		End Get
		Set(value As String)
			_LastTransactionUser = value
		End Set
	End Property

	Public Property LastTransactionDate() As String
		Get
			Return _LastTransactionDate
		End Get
		Set(value As String)
			_LastTransactionDate = value
		End Set
	End Property

End Class

