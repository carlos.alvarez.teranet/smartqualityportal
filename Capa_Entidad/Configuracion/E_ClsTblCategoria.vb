﻿Public Class E_ClsTblCategoria
    Private _IdCategoria As Integer
    Private _Categoria As String
    Private _LastTransactionUser As String
    Private _Out_UltAct As String
    Public Property IdCategoria() As Integer
        Get
            Return _IdCategoria
        End Get
        Set(value As Integer)
            _IdCategoria = value
        End Set
    End Property
    Public Property Categoria() As String
        Get
            Return _Categoria
        End Get
        Set(value As String)
            _Categoria = value
        End Set
    End Property
    Public Property LastTransactionUser() As String
        Get
            Return _LastTransactionUser
        End Get
        Set(value As String)
            _LastTransactionUser = value
        End Set
    End Property
    Public Property Out_UltAct() As String
        Get
            Return _Out_UltAct
        End Get
        Set(value As String)
            _Out_UltAct = value
        End Set
    End Property
End Class
