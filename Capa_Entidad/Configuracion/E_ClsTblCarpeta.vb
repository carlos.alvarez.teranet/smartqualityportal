﻿Public Class E_ClsTblCarpeta
    Private _IdCarpeta As Integer
    Private _IdCarpetaPadre As Integer
    Private _Carpeta As String
    Private _StoreProcedure As String
    Private _UltAct As String


    Private _Nivel As Integer
    Private _Confidencial As Boolean
    Private _Estado As Boolean
    Private _Contenedora As Boolean
    Private _LastTransactionUser As String
    Private _LastTransactionDate As Date


    Private _cantHijos As Integer


    Public Property IdCarpeta() As Integer
        Get
            Return _IdCarpeta
        End Get
        Set(value As Integer)
            _IdCarpeta = value
        End Set
    End Property
    Public Property IdCarpetaPadre() As Integer
        Get
            Return _IdCarpetaPadre
        End Get
        Set(value As Integer)
            _IdCarpetaPadre = value
        End Set
    End Property
    Public Property Carpeta() As String
        Get
            Return _Carpeta
        End Get
        Set(value As String)
            _Carpeta = value
        End Set
    End Property

    Public Property UltAct() As String
        Get
            Return _UltAct
        End Get
        Set(value As String)
            _UltAct = value
        End Set
    End Property



    Public Property StoreProcedure() As String
        Get
            Return _StoreProcedure
        End Get
        Set(value As String)
            _StoreProcedure = value
        End Set
    End Property


    Public Property Nivel() As Integer
        Get
            Return _Nivel
        End Get
        Set(value As Integer)
            _Nivel = value
        End Set
    End Property
    Public Property Confidencial() As Boolean
        Get
            Return _Confidencial
        End Get
        Set(value As Boolean)
            _Confidencial = value
        End Set
    End Property
    Public Property Estado() As Boolean
        Get
            Return _Estado
        End Get
        Set(value As Boolean)
            _Estado = value
        End Set
    End Property
    Public Property Contenedora() As Boolean
        Get
            Return _Contenedora
        End Get
        Set(value As Boolean)
            _Contenedora = value
        End Set
    End Property
    Public Property LastTransactionUser() As String
        Get
            Return _LastTransactionUser
        End Get
        Set(value As String)
            _LastTransactionUser = value
        End Set
    End Property
    Public Property LastTransactionDate() As Date
        Get
            Return _LastTransactionDate
        End Get
        Set(value As Date)
            _LastTransactionDate = value
        End Set
    End Property


    Public Property cantHijos() As Integer
        Get
            Return _cantHijos
        End Get
        Set(value As Integer)
            _cantHijos = value
        End Set
    End Property



End Class
