Public Class E_ClsTblCritica

	Private _IdCritica As Integer
	Private _IdCarpeta As Integer
	Private _IdPrefijo As Integer
	Private _Correlativo As Integer
	Private _Version As Integer
	Private _IdEstado As Integer
	Private _Owner As String
	Private _FechaCreacion As Date
	Private _FechaRevision As Date
	Private _IdWorkflow As Integer
	Private _Justificacion As String
	Private _IdVariableAccion As Integer
	Private _VariableAccionValor As String
	Private _CorrelativoAux As String

	Private _EstadooAux As String

	Private _LastTransactionUser As String
	Private _LastTransactionDate As Date

	Public Property IdCritica() As Integer
		Get
			Return _IdCritica
		End Get
		Set(value As Integer)
			_IdCritica = value
		End Set
	End Property
	Public Property CorrelativoAux() As String
		Get
			Return _CorrelativoAux
		End Get
		Set(value As String)
			_CorrelativoAux = value
		End Set
	End Property

	Public Property EstadoAux() As String
		Get
			Return _EstadooAux
		End Get
		Set(value As String)
			_EstadooAux = value
		End Set
	End Property



	Public Property IdCarpeta() As Integer
		Get
			Return _IdCarpeta
		End Get
		Set(value As Integer)
			_IdCarpeta = value
		End Set
	End Property

	Public Property IdPrefijo() As Integer
		Get
			Return _IdPrefijo
		End Get
		Set(value As Integer)
			_IdPrefijo = value
		End Set
	End Property

	Public Property Correlativo() As Integer
		Get
			Return _Correlativo
		End Get
		Set(value As Integer)
			_Correlativo = value
		End Set
	End Property

	Public Property Version() As Integer
		Get
			Return _Version
		End Get
		Set(value As Integer)
			_Version = value
		End Set
	End Property

	Public Property IdEstado() As Integer
		Get
			Return _IdEstado
		End Get
		Set(value As Integer)
			_IdEstado = value
		End Set
	End Property

	Public Property Owner() As String
		Get
			Return _Owner
		End Get
		Set(value As String)
			_Owner = value
		End Set
	End Property

	Public Property FechaCreacion() As Date
		Get
			Return _FechaCreacion
		End Get
		Set(value As Date)
			_FechaCreacion = value
		End Set
	End Property

	Public Property FechaRevision() As Date
		Get
			Return _FechaRevision
		End Get
		Set(value As Date)
			_FechaRevision = value
		End Set
	End Property

	Public Property IdWorkflow() As Integer
		Get
			Return _IdWorkflow
		End Get
		Set(value As Integer)
			_IdWorkflow = value
		End Set
	End Property

	Public Property Justificacion() As String
		Get
			Return _Justificacion
		End Get
		Set(value As String)
			_Justificacion = value
		End Set
	End Property

	Public Property IdVariableAccion() As Integer
		Get
			Return _IdVariableAccion
		End Get
		Set(value As Integer)
			_IdVariableAccion = value
		End Set
	End Property

	Public Property VariableAccionValor() As String
		Get
			Return _VariableAccionValor
		End Get
		Set(value As String)
			_VariableAccionValor = value
		End Set
	End Property

	Public Property LastTransactionUser() As String
		Get
			Return _LastTransactionUser
		End Get
		Set(value As String)
			_LastTransactionUser = value
		End Set
	End Property

	Public Property LastTransactionDate() As Date
		Get
			Return _LastTransactionDate
		End Get
		Set(value As Date)
			_LastTransactionDate = value
		End Set
	End Property

End Class

