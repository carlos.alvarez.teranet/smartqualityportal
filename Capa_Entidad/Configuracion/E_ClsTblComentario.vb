Public Class E_ClsTblComentario
    Private _IdComentario As Integer
    Private _IdTemp As String
    Private _IdCritica As Integer
    Private _Comentario As String
    Private _LastTransactionUser As String
    Private _LastTransactionDate As Date
    Public Property IdComentario() As Integer
        Get
            Return _IdComentario
        End Get
        Set(value As Integer)
            _IdComentario = value
        End Set
    End Property
    Public Property IdTemp() As String
        Get
            Return _IdTemp
        End Get
        Set(value As String)
            _IdTemp = value
        End Set
    End Property
    Public Property IdCritica() As Integer
        Get
            Return _IdCritica
        End Get
        Set(value As Integer)
            _IdCritica = value
        End Set
    End Property
    Public Property Comentario() As String
        Get
            Return _Comentario
        End Get
        Set(value As String)
            _Comentario = value
        End Set
    End Property
    Public Property LastTransactionUser() As String
        Get
            Return _LastTransactionUser
        End Get
        Set(value As String)
            _LastTransactionUser = value
        End Set
    End Property
    Public Property LastTransactionDate() As Date
        Get
            Return _LastTransactionDate
        End Get
        Set(value As Date)
            _LastTransactionDate = value
        End Set
    End Property
End Class
