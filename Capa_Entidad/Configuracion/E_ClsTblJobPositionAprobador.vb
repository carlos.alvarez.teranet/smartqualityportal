Public Class E_ClsTblJobPositionAprobador

	Private _IdJobPosition As Integer
	Private _IdAprobador As String
	Private _LastTransactionUser As String
	Private _LastTransactionDate As Date

	Public Property IdJobPosition() As Integer
		Get
			Return _IdJobPosition
		End Get
		Set(value As Integer)
			_IdJobPosition = value
		End Set
	End Property

	Public Property IdAprobador() As String
		Get
			Return _IdAprobador
		End Get
		Set(value As String)
			_IdAprobador = value
		End Set
	End Property

	Public Property LastTransactionUser() As String
		Get
			Return _LastTransactionUser
		End Get
		Set(value As String)
			_LastTransactionUser = value
		End Set
	End Property

	Public Property LastTransactionDate() As Date
		Get
			Return _LastTransactionDate
		End Get
		Set(value As Date)
			_LastTransactionDate = value
		End Set
	End Property

End Class 

