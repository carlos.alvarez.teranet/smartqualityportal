Public Class E_ClsTblWorkflow

	Private _IdWorkflow As Integer
	Private _Workflow As String
	Private _Descripcion As String
	Private _UltAct As String

	Private _Estado As Boolean
	Private _LastTransactionUser As String
	Private _LastTransactionDate As Date

	Public Property IdWorkflow() As Integer
		Get
			Return _IdWorkflow
		End Get
		Set(value As Integer)
			_IdWorkflow = value
		End Set
	End Property

	Public Property Workflow() As String
		Get
			Return _Workflow
		End Get
		Set(value As String)
			_Workflow = value
		End Set
	End Property

	Public Property UltAct() As String
		Get
			Return _UltAct
		End Get
		Set(value As String)
			_UltAct = value
		End Set
	End Property

	Public Property Descripcion() As String
		Get
			Return _Descripcion
		End Get
		Set(value As String)
			_Descripcion = value
		End Set
	End Property

	Public Property Estado() As Boolean
		Get
			Return _Estado
		End Get
		Set(value As Boolean)
			_Estado = value
		End Set
	End Property

	Public Property LastTransactionUser() As String
		Get
			Return _LastTransactionUser
		End Get
		Set(value As String)
			_LastTransactionUser = value
		End Set
	End Property

	Public Property LastTransactionDate() As Date
		Get
			Return _LastTransactionDate
		End Get
		Set(value As Date)
			_LastTransactionDate = value
		End Set
	End Property

End Class 

