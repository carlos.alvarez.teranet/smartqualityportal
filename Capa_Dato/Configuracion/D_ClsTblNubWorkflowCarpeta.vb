Imports Capa_Entidad
Imports System.Data.SqlClient

Public Class D_ClsTblNubWorkflowCarpeta

	Public Function SelectRecord(ByVal E_ObjNubWorkflowCarpeta As E_ClsTblNubWorkflowCarpeta) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblNubWorkflowCarpeta")

			Proc.Param("TipoQuery", "SELECT")
			Proc.Param("IdWorkflow", E_ObjNubWorkflowCarpeta.IdWorkflow)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function SelectAll(ByVal IdWorkFlow As String) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblNubWorkflowCarpeta")

			Proc.Param("TipoQuery", "SELECTALL")
			Proc.Param("IdWorkflow", IdWorkFlow)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function Insert(ByVal E_ObjNubWorkflowCarpeta As E_ClsTblNubWorkflowCarpeta) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblNubWorkflowCarpeta")

			Proc.Param("TipoQuery", "INSERT")
			Proc.Param("IdWorkflow", E_ObjNubWorkflowCarpeta.IdWorkflow)
			Proc.Param("IdCarpeta", E_ObjNubWorkflowCarpeta.IdCarpeta)
			Proc.Param("LastTransactionUser", E_ObjNubWorkflowCarpeta.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Update(ByVal E_ObjNubWorkflowCarpeta As E_ClsTblNubWorkflowCarpeta) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblNubWorkflowCarpeta")

			Proc.Param("TipoQuery", "UPDATE")
			Proc.Param("IdWorkflow", E_ObjNubWorkflowCarpeta.IdWorkflow)
			Proc.Param("IdCarpeta", E_ObjNubWorkflowCarpeta.IdCarpeta)
			Proc.Param("LastTransactionUser", E_ObjNubWorkflowCarpeta.LastTransactionUser.Trim())
			Proc.Param("LastTransactionDate", E_ObjNubWorkflowCarpeta.LastTransactionDate)
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Delete(ByVal E_ObjNubWorkflowCarpeta As E_ClsTblNubWorkflowCarpeta) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblNubWorkflowCarpeta")

			Proc.Param("TipoQuery", "DELETE")
			Proc.Param("IdWorkflow", E_ObjNubWorkflowCarpeta.IdWorkflow)
			Proc.Param("IdCarpeta", E_ObjNubWorkflowCarpeta.IdCarpeta)
			Proc.Param("LastTransactionUser", E_ObjNubWorkflowCarpeta.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

End Class

