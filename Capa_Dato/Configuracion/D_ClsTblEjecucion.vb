Imports Capa_Entidad
Imports System.Data.SqlClient

Public Class D_ClsTblEjecucion

	Public Function SelectRecord(ByVal E_ObjEjecucion As E_ClsTblEjecucion) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblEjecucion")

			Proc.Param("TipoQuery", "SELECT")
			Proc.Param("IdEjecucion", E_ObjEjecucion.IdEjecucion)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function SelectAllChild(ByVal IdEjecucion As Integer) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblEjecucion")

			Proc.Param("TipoQuery", "SELECTALLCHILD")
			Proc.Param("IdEjecucion", IdEjecucion)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function EjecutaCritica(ByVal IdEjecucion As Integer) As String
		Try
			Dim Proc As New StoredProcedure("SP_JobEjecucion")

			Proc.Param("TipoQuery", "Inmediato")
			Proc.Param("IdEjecucion", IdEjecucion)
			Proc.ExecSP_ds()
			Return "OK"
		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function SelectAll(ByVal TxtBsc As String) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblEjecucion")

			Proc.Param("TipoQuery", "SELECTALL")
			Proc.Param("TxtBsc", TxtBsc)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function Insert(ByVal E_ObjEjecucion As E_ClsTblEjecucion) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblEjecucion")

			Proc.Param("TipoQuery", "INSERT")
			Proc.Param("IdEjecucion", E_ObjEjecucion.IdEjecucion)
			Proc.Param("IdEjecucionPadre", E_ObjEjecucion.IdEjecucionPadre)
			Proc.Param("Owner", E_ObjEjecucion.Owner.Trim())
			Proc.Param("FechaCreacion", E_ObjEjecucion.FechaCreacion)
			Proc.Param("IdEstado", E_ObjEjecucion.IdEstado)
			Proc.Param("Titulo", E_ObjEjecucion.Titulo.Trim())
			Proc.Param("TipoEjecucion", E_ObjEjecucion.TipoEjecucion.Trim())
			Proc.Param("FechaHoraProgramada", E_ObjEjecucion.FechaHoraProgramada)
			Proc.Param("LastTransactionUser", E_ObjEjecucion.LastTransactionUser.Trim())

			Dim ds As DataSet = Proc.ExecSP_ds()
			Dim dt As DataTable = ds.Tables(0)

			If dt.Rows.Count > 0 Then
				Return "OK" & CStr(dt.Rows(0).Item("IdEjecucion"))
			Else
				Throw New Exception("Error")
			End If
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Update(ByVal E_ObjEjecucion As E_ClsTblEjecucion) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblEjecucion")

			Proc.Param("TipoQuery", "UPDATE")
			Proc.Param("IdEjecucion", E_ObjEjecucion.IdEjecucion)
			Proc.Param("IdEjecucionPadre", E_ObjEjecucion.IdEjecucionPadre)
			Proc.Param("Owner", E_ObjEjecucion.Owner.Trim())
			Proc.Param("FechaCreacion", E_ObjEjecucion.FechaCreacion)
			Proc.Param("IdEstado", E_ObjEjecucion.IdEstado)
			Proc.Param("Titulo", E_ObjEjecucion.Titulo.Trim())
			Proc.Param("TipoEjecucion", E_ObjEjecucion.TipoEjecucion.Trim())
			Proc.Param("FechaHoraProgramada", E_ObjEjecucion.FechaHoraProgramada)
			Proc.Param("LastTransactionUser", E_ObjEjecucion.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK" & CStr(E_ObjEjecucion.IdEjecucion)

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Delete(ByVal E_ObjEjecucion As E_ClsTblEjecucion) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblEjecucion")

			Proc.Param("TipoQuery", "DELETE")
			Proc.Param("IdEjecucion", E_ObjEjecucion.IdEjecucion)
			Proc.Param("LastTransactionUser", E_ObjEjecucion.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

End Class

