Imports Capa_Entidad
Imports System.Data.SqlClient

Public Class D_ClsTblNubEjecucionListaDistribucion

	Public Function SelectRecord(ByVal E_ObjNubEjecucionListaDistribucion As E_ClsTblNubEjecucionListaDistribucion) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblNubEjecucionListaDistribucion")

			Proc.Param("TipoQuery", "SELECT")
			Proc.Param("IdEjecucion", E_ObjNubEjecucionListaDistribucion.IdEjecucion)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function SelectAll(ByVal IdEjecucion As Integer) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblNubEjecucionListaDistribucion")

			Proc.Param("TipoQuery", "SELECTALL")
			Proc.Param("IdEjecucion", IdEjecucion)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function Insert(ByVal E_ObjNubEjecucionListaDistribucion As E_ClsTblNubEjecucionListaDistribucion) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblNubEjecucionListaDistribucion")

			Proc.Param("TipoQuery", "INSERT")
			Proc.Param("IdEjecucion", E_ObjNubEjecucionListaDistribucion.IdEjecucion)
			Proc.Param("IdListaDistribucion", E_ObjNubEjecucionListaDistribucion.IdListaDistribucion)
			Proc.Param("LastTransactionUser", E_ObjNubEjecucionListaDistribucion.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Update(ByVal E_ObjNubEjecucionListaDistribucion As E_ClsTblNubEjecucionListaDistribucion) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblNubEjecucionListaDistribucion")

			Proc.Param("TipoQuery", "UPDATE")
			Proc.Param("IdEjecucion", E_ObjNubEjecucionListaDistribucion.IdEjecucion)
			Proc.Param("IdListaDistribucion", E_ObjNubEjecucionListaDistribucion.IdListaDistribucion)
			Proc.Param("LastTransactionUser", E_ObjNubEjecucionListaDistribucion.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Delete(ByVal E_ObjNubEjecucionListaDistribucion As E_ClsTblNubEjecucionListaDistribucion) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblNubEjecucionListaDistribucion")

			Proc.Param("TipoQuery", "DELETE")
			Proc.Param("IdEjecucion", E_ObjNubEjecucionListaDistribucion.IdEjecucion)
			Proc.Param("IdListaDistribucion", E_ObjNubEjecucionListaDistribucion.IdListaDistribucion)
			Proc.Param("LastTransactionUser", E_ObjNubEjecucionListaDistribucion.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

End Class

