Imports Capa_Entidad
Imports System.Data.SqlClient

Public Class D_ClsTblOperadorLogico

	Public Function SelectRecord(ByVal E_ObjOperadorLogico As E_ClsTblOperadorLogico) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblOperadorLogico")

			Proc.Param("TipoQuery", "SELECT")
			Proc.Param("IdOperadorLogico", E_ObjOperadorLogico.IdOperadorLogico)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function SelectAll(ByVal TxtBsc As String) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblOperadorLogico")

			Proc.Param("TipoQuery", "SELECTALL")
			Proc.Param("TxtBsc", TxtBsc)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function Insert(ByVal E_ObjOperadorLogico As E_ClsTblOperadorLogico) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblOperadorLogico")

			Proc.Param("TipoQuery", "INSERT")
			Proc.Param("IdOperadorLogico", E_ObjOperadorLogico.IdOperadorLogico)
			Proc.Param("OperadorLogico", E_ObjOperadorLogico.OperadorLogico.Trim())
			Proc.Param("NombreOperadorLogico", E_ObjOperadorLogico.NombreOperadorLogico.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Update(ByVal E_ObjOperadorLogico As E_ClsTblOperadorLogico) As String
		Try
			Dim Proc As New StoredProcedure("_SP_TblOperadorLogico")

			Proc.Param("TipoQuery", "UPDATE")
			Proc.Param("IdOperadorLogico", E_ObjOperadorLogico.IdOperadorLogico)
			Proc.Param("OperadorLogico", E_ObjOperadorLogico.OperadorLogico.Trim())
			Proc.Param("NombreOperadorLogico", E_ObjOperadorLogico.NombreOperadorLogico.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function LoadCbo(ByVal IdVariableCondicion As Integer) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblOperadorLogico")
			Proc.Param("IdVariableCondicion", IdVariableCondicion)
			Proc.Param("TipoQuery", "LOADCBO")
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function Delete(ByVal E_ObjOperadorLogico As E_ClsTblOperadorLogico) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblOperadorLogico")

			Proc.Param("TipoQuery", "DELETE")
			Proc.Param("IdOperadorLogico", E_ObjOperadorLogico.IdOperadorLogico)
			'Proc.Param("LastTransactionUser", E_ObjOperadorLogico.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

End Class

