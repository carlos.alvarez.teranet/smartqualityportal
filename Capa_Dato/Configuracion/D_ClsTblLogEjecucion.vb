Imports Capa_Entidad
Imports System.Data.SqlClient

Public Class D_ClsTblLogEjecucion

	Public Function SelectRecord(ByVal E_ObjLogEjecucion As E_ClsTblLogEjecucion) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblLogEjecucion")

			Proc.Param("TipoQuery", "SELECT")
			Proc.Param("IdLogEjecucion", E_ObjLogEjecucion.IdLogEjecucion)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function SelectAll(ByVal IdEjecucion As Integer) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblLogEjecucion")

			Proc.Param("QueryType", "SELECTALL")
			Proc.Param("IdEjecucion", IdEjecucion)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function changeStatusEjecucion(ByVal IdEjecucion As Integer) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblLogEjecucion")

			Proc.Param("QueryType", "GetAprobadoresEjecucionsListadoCambiaEstado")
			Proc.Param("IdEjecucion", IdEjecucion)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function Insert(ByVal E_ObjLogEjecucion As E_ClsTblLogEjecucion) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblLogEjecucion")

			Proc.Param("QueryType", "INSERT")
			Proc.Param("IdLogEjecucion", E_ObjLogEjecucion.IdLogEjecucion)
			Proc.Param("IdEjecucion", E_ObjLogEjecucion.IdEjecucion)
			Proc.Param("Fecha", E_ObjLogEjecucion.Fecha)
			Proc.Param("IdAccion", E_ObjLogEjecucion.IdAccion)
			Proc.Param("Comentario", E_ObjLogEjecucion.Comentario.Trim())
			Proc.Param("LastTransactionUser", E_ObjLogEjecucion.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Update(ByVal E_ObjLogEjecucion As E_ClsTblLogEjecucion) As String
		Try
			Dim Proc As New StoredProcedure("_SP_TblLogEjecucion")

			Proc.Param("TipoQuery", "UPDATE")
			Proc.Param("IdLogEjecucion", E_ObjLogEjecucion.IdLogEjecucion)
			Proc.Param("IdEjecucion", E_ObjLogEjecucion.IdEjecucion)
			Proc.Param("Fecha", E_ObjLogEjecucion.Fecha)
			Proc.Param("IdAccion", E_ObjLogEjecucion.IdAccion)
			Proc.Param("Comentario", E_ObjLogEjecucion.Comentario.Trim())
			Proc.Param("LastTransactionUser", E_ObjLogEjecucion.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Delete(ByVal E_ObjLogEjecucion As E_ClsTblLogEjecucion) As String
		Try
			Dim Proc As New StoredProcedure("_SP_TblLogEjecucion")

			Proc.Param("TipoQuery", "DELETE")
			Proc.Param("IdLogEjecucion", E_ObjLogEjecucion.IdLogEjecucion)
			Proc.Param("IdEjecucion", E_ObjLogEjecucion.IdEjecucion)
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

End Class

