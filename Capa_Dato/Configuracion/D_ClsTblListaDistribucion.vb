Imports Capa_Entidad
Imports System.Data.SqlClient

Public Class D_ClsTblListaDistribucion

	Public Function SelectRecord(ByVal E_ObjListaDistribucion As E_ClsTblListaDistribucion) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblListaDistribucion")

			Proc.Param("QueryType", "SELECT")
			Proc.Param("IdListaDistribucion", E_ObjListaDistribucion.IdListaDistribucion)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function



	Public Function SelectAll(ByVal TxtBsc As String) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblListaDistribucion")

			Proc.Param("QueryType", "SELECTALL")
			Proc.Param("TxtBsc", TxtBsc)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function Insert(ByVal E_ObjListaDistribucion As E_ClsTblListaDistribucion) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblListaDistribucion")

			Proc.Param("QueryType", "INSERT")
			Proc.Param("ListaDistribucion", E_ObjListaDistribucion.ListaDistribucion.Trim())
			Proc.Param("Estado", E_ObjListaDistribucion.Estado)
			Proc.Param("LastTransactionUser", E_ObjListaDistribucion.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Update(ByVal E_ObjListaDistribucion As E_ClsTblListaDistribucion) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblListaDistribucion")

			Proc.Param("QueryType", "UPDATE")
			Proc.Param("ListaDistribucion", E_ObjListaDistribucion.ListaDistribucion.Trim())
			Proc.Param("IdListaDistribucion", E_ObjListaDistribucion.IdListaDistribucion)
			Proc.Param("Estado", E_ObjListaDistribucion.Estado)
			Proc.Param("LastTransactionUser", E_ObjListaDistribucion.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Delete(ByVal E_ObjListaDistribucion As E_ClsTblListaDistribucion) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblListaDistribucion")

			Proc.Param("QueryType", "UPDATE")
			Proc.Param("IdListaDistribucion", E_ObjListaDistribucion.IdListaDistribucion)
			Proc.Param("Estado", E_ObjListaDistribucion.Estado)
			Proc.Param("LastTransactionUser", E_ObjListaDistribucion.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function



	Public Function LoadCbo() As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblListaDistribucion")

			Proc.Param("QueryType", "LOADCBO")
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

End Class

