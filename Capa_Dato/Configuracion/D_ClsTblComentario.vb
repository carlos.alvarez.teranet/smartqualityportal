Imports Capa_Entidad
Imports System.Data.SqlClient
Public Class D_ClsTblComentario
    Public Function SelectAll(ByVal E_ObjComentario As E_ClsTblComentario) As DataSet
        Try
            Dim Proc As New StoredProcedure("SP_TblComentario")

            Proc.Param("Querytype", "SELECTALL")
            Proc.Param("IdCritica", E_ObjComentario.IdCritica)

            Return Proc.ExecSP_ds()

        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        Finally

        End Try
    End Function
    Public Function Insert(ByVal E_ObjComentario As E_ClsTblComentario) As String
        Try
            Dim Proc As New StoredProcedure("SP_TblComentario")

            Proc.Param("Querytype", "INSERT")
            Proc.Param("IdCritica", E_ObjComentario.IdCritica)
            Proc.Param("Comentario", E_ObjComentario.Comentario.Trim())
            Proc.Param("LastTransactionUser", E_ObjComentario.LastTransactionUser.Trim())

            Proc.ExecSP()
            Return "OK"

        Catch ex As Exception
            Return ex.Message
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Delete(ByVal E_ObjComentario As E_ClsTblComentario) As String
        Try
            Dim Proc As New StoredProcedure("SP_TblComentario")

            Proc.Param("Querytype", "DELETE")
            Proc.Param("IdComentario", E_ObjComentario.IdComentario)
            Proc.Param("LastTransactionUser", E_ObjComentario.LastTransactionUser.Trim())

            Proc.ExecSP()
            Return "OK"

        Catch ex As Exception
            Return ex.Message
            Throw New Exception(ex.Message)
        End Try
    End Function

    Public Function SelectAll_Temp(ByVal E_ObjComentario As E_ClsTblComentario) As DataSet
        Try
            Dim Proc As New StoredProcedure("SP_TblComentario")

            Proc.Param("Querytype", "SELECTALL_TEMP")
            Proc.Param("IdTemp", E_ObjComentario.IdTemp.Trim())

            Return Proc.ExecSP_ds()

        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        Finally

        End Try
    End Function
    Public Function Insert_Temp(ByVal E_ObjComentario As E_ClsTblComentario) As String
        Try
            Dim Proc As New StoredProcedure("SP_TblComentario")

            Proc.Param("Querytype", "INSERT_TEMP")
            Proc.Param("IdTemp", E_ObjComentario.IdTemp.Trim())
            Proc.Param("Comentario", E_ObjComentario.Comentario.Trim())
            Proc.Param("LastTransactionUser", E_ObjComentario.LastTransactionUser.Trim())

            Proc.ExecSP()
            Return "OK"

        Catch ex As Exception
            Return ex.Message
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Delete_Temp(ByVal E_ObjComentario As E_ClsTblComentario) As String
        Try
            Dim Proc As New StoredProcedure("SP_TblComentario")

            Proc.Param("Querytype", "DELETE_TEMP")
            Proc.Param("IdComentario", E_ObjComentario.IdComentario)

            Proc.ExecSP()
            Return "OK"

        Catch ex As Exception
            Return ex.Message
            Throw New Exception(ex.Message)
        End Try
    End Function
End Class
