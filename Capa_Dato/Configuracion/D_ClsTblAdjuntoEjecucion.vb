﻿Imports Capa_Entidad
Imports System.Data.SqlClient
Public Class D_ClsTblAdjuntoEjecucion
    Public Function SelectAll(ByVal E_ObjAdjunto As E_ClsTblAdjuntoEjecucion) As DataSet
        Try
            Dim Proc As New StoredProcedure("SP_TblAdjuntoEjecucion")

            Proc.Param("Querytype", "SELECTALL")
            Proc.Param("IdEjecucion", E_ObjAdjunto.IdEjecucion)

            Return Proc.ExecSP_ds()

        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        Finally

        End Try
    End Function
    Public Function Insert(ByVal E_ObjAdjunto As E_ClsTblAdjuntoEjecucion) As String
        Try
            Dim Proc As New StoredProcedure("SP_TblAdjuntoEjecucion")

            Proc.Param("Querytype", "INSERT")
            Proc.Param("IdEjecucion", E_ObjAdjunto.IdEjecucion)
            If E_ObjAdjunto.Comentario.Trim() <> "" Then Proc.Param("Comentario", E_ObjAdjunto.Comentario.Trim())
            Proc.Param("AdjName", E_ObjAdjunto.AdjName.Trim())
            Proc.Param("AdjFile", E_ObjAdjunto.AdjFile)
            Proc.Param("AdjSize", E_ObjAdjunto.AdjSize.Trim())
            Proc.Param("AdjExt", E_ObjAdjunto.AdjExt.Trim())
            Proc.Param("LastTransactionUser", E_ObjAdjunto.LastTransactionUser.Trim())

            Proc.ExecSP()
            Return "OK"

        Catch ex As Exception
            Return ex.Message
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Delete(ByVal E_ObjAdjunto As E_ClsTblAdjuntoEjecucion) As String
        Try
            Dim Proc As New StoredProcedure("SP_TblAdjuntoEjecucion")

            Proc.Param("Querytype", "DELETE")
            Proc.Param("IdAdjunto", E_ObjAdjunto.IdAdjunto)
            Proc.Param("LastTransactionUser", E_ObjAdjunto.LastTransactionUser.Trim())

            Proc.ExecSP()
            Return "OK"

        Catch ex As Exception
            Return ex.Message
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function GetAdjunto(ByVal E_ObjAdjunto As E_ClsTblAdjuntoEjecucion) As Byte()
        Try
            Dim Proc As New StoredProcedure("SP_TblAdjuntoEjecucion")

            Proc.Param("Querytype", "GET_DCTO")
            Proc.Param("IdAdjunto", E_ObjAdjunto.IdAdjunto)

            Dim ds As DataSet = Proc.ExecSP_ds()
            Dim dt As DataTable = ds.Tables(0)
            If dt.Rows.Count > 0 Then
                Return dt.Rows(0).Item("AdjFile")
            Else
                Throw New Exception("No fue posible encontrar el archivo adjunto")
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        Finally

        End Try
    End Function

    Public Function SelectAll_Temp(ByVal E_ObjAdjunto As E_ClsTblAdjuntoEjecucion) As DataSet
        Try
            Dim Proc As New StoredProcedure("SP_TblAdjuntoEjecucion")

            Proc.Param("Querytype", "SELECTALL_TEMP")
            Proc.Param("IdTemp", E_ObjAdjunto.IdTemp.Trim())

            Return Proc.ExecSP_ds()

        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        Finally

        End Try
    End Function
    Public Function Insert_Temp(ByVal E_ObjAdjunto As E_ClsTblAdjuntoEjecucion) As String
        Try
            Dim Proc As New StoredProcedure("SP_TblAdjuntoEjecucion")

            Proc.Param("Querytype", "INSERT_TEMP")
            Proc.Param("IdTemp", E_ObjAdjunto.IdTemp.Trim())
            If E_ObjAdjunto.Comentario.Trim() <> "" Then Proc.Param("Comentario", E_ObjAdjunto.Comentario.Trim())
            Proc.Param("AdjName", E_ObjAdjunto.AdjName.Trim())
            Proc.Param("AdjFile", E_ObjAdjunto.AdjFile)
            Proc.Param("AdjSize", E_ObjAdjunto.AdjSize.Trim())
            Proc.Param("AdjExt", E_ObjAdjunto.AdjExt.Trim())
            Proc.Param("LastTransactionUser", E_ObjAdjunto.LastTransactionUser.Trim())

            Proc.ExecSP()
            Return "OK"

        Catch ex As Exception
            Return ex.Message
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Delete_Temp(ByVal E_ObjAdjunto As E_ClsTblAdjuntoEjecucion) As String
        Try
            Dim Proc As New StoredProcedure("SP_TblAdjuntoEjecucion")

            Proc.Param("Querytype", "DELETE_TEMP")
            Proc.Param("IdAdjunto", E_ObjAdjunto.IdAdjunto)

            Proc.ExecSP()
            Return "OK"

        Catch ex As Exception
            Return ex.Message
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function GetAdjuntoTemp(ByVal E_ObjAdjunto As E_ClsTblAdjuntoEjecucion) As Byte()
        Try
            Dim Proc As New StoredProcedure("SP_TblAdjuntoEjecucion")

            Proc.Param("Querytype", "GET_DCTO_TEMP")
            Proc.Param("IdAdjunto", E_ObjAdjunto.IdAdjunto)

            Dim ds As DataSet = Proc.ExecSP_ds()
            Dim dt As DataTable = ds.Tables(0)
            If dt.Rows.Count > 0 Then
                Return dt.Rows(0).Item("AdjFile")
            Else
                Throw New Exception("No fue posible encontrar el archivo adjunto")
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        Finally

        End Try
    End Function
End Class
