Imports Capa_Entidad
Imports System.Data.SqlClient

Public Class D_ClsTblEjecucionLogMail

	Public Function SelectRecord(ByVal E_ObjEjecucionLogMail As E_ClsTblEjecucionLogMail) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblEjecucionLogMail")

			Proc.Param("TipoQuery", "SELECT")
			Proc.Param("IdEjecucionLogMail", E_ObjEjecucionLogMail.IdEjecucionLogMail)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function SelectAll(ByVal TxtBsc As String) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblEjecucionLogMail")

			Proc.Param("TipoQuery", "SELECTALL")
			Proc.Param("TxtBsc", TxtBsc)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function Insert(ByVal E_ObjEjecucionLogMail As E_ClsTblEjecucionLogMail) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblEjecucionLogMail")

			Proc.Param("TipoQuery", "INSERT")
			Proc.Param("IdEjecucionLogMail", E_ObjEjecucionLogMail.IdEjecucionLogMail)
			Proc.Param("IdEjecucion", E_ObjEjecucionLogMail.IdEjecucion)
			Proc.Param("Fecha", E_ObjEjecucionLogMail.Fecha)
			Proc.Param("Remitente", E_ObjEjecucionLogMail.Remitente.Trim())
			Proc.Param("Destinatario", E_ObjEjecucionLogMail.Destinatario.Trim())
			Proc.Param("Asunto", E_ObjEjecucionLogMail.Asunto.Trim())
			Proc.Param("Cuerpo", E_ObjEjecucionLogMail.Cuerpo.Trim())
			Proc.Param("Adjunto", E_ObjEjecucionLogMail.Adjunto.Trim())
			Proc.Param("LastTransactionUser", E_ObjEjecucionLogMail.LastTransactionUser.Trim())
			Proc.Param("LastTransactionDate", E_ObjEjecucionLogMail.LastTransactionDate)
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Update(ByVal E_ObjEjecucionLogMail As E_ClsTblEjecucionLogMail) As String
		Try
			Dim Proc As New StoredProcedure("_SP_TblEjecucionLogMail")

			Proc.Param("TipoQuery", "UPDATE")
			Proc.Param("IdEjecucionLogMail", E_ObjEjecucionLogMail.IdEjecucionLogMail)
			Proc.Param("IdEjecucion", E_ObjEjecucionLogMail.IdEjecucion)
			Proc.Param("Fecha", E_ObjEjecucionLogMail.Fecha)
			Proc.Param("Remitente", E_ObjEjecucionLogMail.Remitente.Trim())
			Proc.Param("Destinatario", E_ObjEjecucionLogMail.Destinatario.Trim())
			Proc.Param("Asunto", E_ObjEjecucionLogMail.Asunto.Trim())
			Proc.Param("Cuerpo", E_ObjEjecucionLogMail.Cuerpo.Trim())
			Proc.Param("Adjunto", E_ObjEjecucionLogMail.Adjunto.Trim())
			Proc.Param("LastTransactionUser", E_ObjEjecucionLogMail.LastTransactionUser.Trim())
			Proc.Param("LastTransactionDate", E_ObjEjecucionLogMail.LastTransactionDate)
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Delete(ByVal E_ObjEjecucionLogMail As E_ClsTblEjecucionLogMail) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblEjecucionLogMail")

			Proc.Param("TipoQuery", "DELETE")
			Proc.Param("IdEjecucionLogMail", E_ObjEjecucionLogMail.IdEjecucionLogMail)
			Proc.Param("IdEjecucion", E_ObjEjecucionLogMail.IdEjecucion)
			Proc.Param("LastTransactionUser", E_ObjEjecucionLogMail.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

End Class

