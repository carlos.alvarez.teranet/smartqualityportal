Imports Capa_Entidad
Imports System.Data.SqlClient

Public Class D_ClsTblLogCritica

	Public Function SelectRecord(ByVal E_ObjLogCritica As E_ClsTblLogCritica) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblLogCritica")

			Proc.Param("TipoQuery", "SELECT")
			Proc.Param("IdLogCritica", E_ObjLogCritica.IdLogCritica)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function SelectAll(ByVal IdCritica As Integer) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblLogCritica")

			Proc.Param("QueryType", "SELECTALL")
			Proc.Param("IdCritica", IdCritica)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function changeStatusCritica(ByVal IdCritica As Integer) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblLogCritica")

			Proc.Param("QueryType", "GetAprobadoresCriticasListadoCambiaEstado")
			Proc.Param("IdCritica", IdCritica)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function Insert(ByVal E_ObjLogCritica As E_ClsTblLogCritica) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblLogCritica")

			Proc.Param("QueryType", "INSERT")
			Proc.Param("IdLogCritica", E_ObjLogCritica.IdLogCritica)
			Proc.Param("IdCritica", E_ObjLogCritica.IdCritica)
			Proc.Param("Fecha", E_ObjLogCritica.Fecha)
			Proc.Param("IdAccion", E_ObjLogCritica.IdAccion)
			Proc.Param("Comentario", E_ObjLogCritica.Comentario.Trim())
			Proc.Param("LastTransactionUser", E_ObjLogCritica.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Update(ByVal E_ObjLogCritica As E_ClsTblLogCritica) As String
		Try
			Dim Proc As New StoredProcedure("_SP_TblLogCritica")

			Proc.Param("TipoQuery", "UPDATE")
			Proc.Param("IdLogCritica", E_ObjLogCritica.IdLogCritica)
			Proc.Param("IdCritica", E_ObjLogCritica.IdCritica)
			Proc.Param("Fecha", E_ObjLogCritica.Fecha)
			Proc.Param("IdAccion", E_ObjLogCritica.IdAccion)
			Proc.Param("Comentario", E_ObjLogCritica.Comentario.Trim())
			Proc.Param("LastTransactionUser", E_ObjLogCritica.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Delete(ByVal E_ObjLogCritica As E_ClsTblLogCritica) As String
		Try
			Dim Proc As New StoredProcedure("_SP_TblLogCritica")

			Proc.Param("TipoQuery", "DELETE")
			Proc.Param("IdLogCritica", E_ObjLogCritica.IdLogCritica)
			Proc.Param("IdCritica", E_ObjLogCritica.IdCritica)
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

End Class

