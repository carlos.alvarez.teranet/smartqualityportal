Imports Capa_Entidad
Imports System.Data.SqlClient

Public Class D_ClsTblJobPositionAprobador

	Public Function SelectRecord(ByVal E_ObjJobPositionAprobador As E_ClsTblJobPositionAprobador) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblJobPositionAprobador")

			Proc.Param("TipoQuery", "SELECT")
			Proc.Param("IdJobPosition", E_ObjJobPositionAprobador.IdJobPosition)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function



	Public Function getAprobadoresJobPosition(ByVal IdJobPosition As Integer) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblJobPositionAprobador")

			Proc.Param("QueryType", "getAprobadoresJobPosition")
			Proc.Param("IdJobPosition", IdJobPosition)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function
	Public Function SelectAll(ByVal IdJobPosition As Integer) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblJobPositionAprobador")

			Proc.Param("QueryType", "SELECTALL")
			Proc.Param("IdJobPosition", IdJobPosition)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function Insert(ByVal E_ObjJobPositionAprobador As E_ClsTblJobPositionAprobador) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblJobPositionAprobador")

			Proc.Param("QueryType", "INSERT")
			Proc.Param("IdJobPosition", E_ObjJobPositionAprobador.IdJobPosition)
			Proc.Param("IdAprobador", E_ObjJobPositionAprobador.IdAprobador.Trim())
			Proc.Param("LastTransactionUser", E_ObjJobPositionAprobador.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Update(ByVal E_ObjJobPositionAprobador As E_ClsTblJobPositionAprobador) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblJobPositionAprobador")

			Proc.Param("TipoQuery", "UPDATE")
			Proc.Param("IdJobPosition", E_ObjJobPositionAprobador.IdJobPosition)
			Proc.Param("IdAprobador", E_ObjJobPositionAprobador.IdAprobador.Trim())
			Proc.Param("LastTransactionUser", E_ObjJobPositionAprobador.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Delete(ByVal E_ObjJobPositionAprobador As E_ClsTblJobPositionAprobador) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblJobPositionAprobador")

			Proc.Param("QueryType", "DELETE")
			Proc.Param("IdJobPosition", E_ObjJobPositionAprobador.IdJobPosition)
			Proc.Param("IdAprobador", E_ObjJobPositionAprobador.IdAprobador.Trim())
			Proc.Param("LastTransactionUser", E_ObjJobPositionAprobador.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

End Class

