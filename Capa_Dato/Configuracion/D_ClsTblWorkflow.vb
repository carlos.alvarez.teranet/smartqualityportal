Imports Capa_Entidad
Imports System.Data.SqlClient

Public Class D_ClsTblWorkflow

	Public Function SelectRecord(ByVal E_ObjWorkflow As E_ClsTblWorkflow) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblWorkflow")

			Proc.Param("QueryType", "SELECT")
			Proc.Param("IdWorkflow", E_ObjWorkflow.IdWorkflow)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function SelectAll(ByVal TxtBsc As String) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblWorkflow")

			Proc.Param("QueryType", "SELECTALL")
			Proc.Param("TxtBsc", TxtBsc)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function Insert(ByVal E_ObjWorkflow As E_ClsTblWorkflow) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblWorkflow")

			Proc.Param("QueryType", "INSERT")
			Proc.Param("Workflow", E_ObjWorkflow.Workflow.Trim())
			Proc.Param("Estado", E_ObjWorkflow.Estado)
			Proc.Param("LastTransactionUser", E_ObjWorkflow.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Update(ByVal E_ObjWorkflow As E_ClsTblWorkflow) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblWorkflow")

			Proc.Param("QueryType", "UPDATE")
			Proc.Param("IdWorkflow", E_ObjWorkflow.IdWorkflow)
			Proc.Param("Workflow", E_ObjWorkflow.Workflow.Trim())
			Proc.Param("Estado", E_ObjWorkflow.Estado)
			Proc.Param("LastTransactionUser", E_ObjWorkflow.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Delete(ByVal E_ObjWorkflow As E_ClsTblWorkflow) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblWorkflow")

			Proc.Param("QueryType", "DELETE")
			Proc.Param("IdWorkflow", E_ObjWorkflow.IdWorkflow)
			Proc.Param("LastTransactionUser", E_ObjWorkflow.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function LoadCbo() As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblWorkflow")

			Proc.Param("QueryType", "LOADCBO")
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

End Class

