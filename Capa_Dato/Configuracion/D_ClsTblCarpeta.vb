﻿Public Class D_ClsTblCarpeta
    Public Function GetCarpeta(ByVal E_ObjCarpeta As Capa_Entidad.E_ClsTblCarpeta) As DataSet
        Try
            Dim Proc As New StoredProcedure("SP_TblCarpeta")

            Proc.Param("TipoQuery", "GET_CARPETA")
            Proc.Param("IdCarpeta", E_ObjCarpeta.IdCarpeta)

            Return Proc.ExecSP_ds()

        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        Finally

        End Try
    End Function


    Public Function getSubCarpeta(ByVal IdCarpeta As Integer) As DataSet
        Try
            Dim Proc As New StoredProcedure("SP_TblCarpeta")

            Proc.Param("TipoQuery", "BreadcrumbsFolderSimple")
            Proc.Param("IdCarpeta", IdCarpeta)

            Return Proc.ExecSP_ds()

        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        Finally

        End Try
    End Function


    Public Function GetCarpetasContenedoras() As DataSet
        Try
            Dim Proc As New StoredProcedure("SP_TblCarpeta")

            Proc.Param("TipoQuery", "GETCONTENEDORES")

            Return Proc.ExecSP_ds()

        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        Finally

        End Try
    End Function

    Public Function LoadCarpetasCriticas() As DataSet
        Try
            Dim Proc As New StoredProcedure("SP_TblCarpeta")

            Proc.Param("TipoQuery", "CKECKCARPETASCONTENEDORAS")

            Return Proc.ExecSP_ds()

        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        Finally

        End Try
    End Function







    Public Function CarpetasTreeViewContenedor() As DataSet
        Try
            Dim Proc As New StoredProcedure("SP_TblCarpeta")

            Proc.Param("TipoQuery", "TreeViewContenedor")

            Return Proc.ExecSP_ds()

        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        Finally

        End Try
    End Function

    Public Function CarpetasTreeView() As DataSet
        Try
            Dim Proc As New StoredProcedure("SP_TblCarpeta")

            Proc.Param("TipoQuery", "TreeView")

            Return Proc.ExecSP_ds()

        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        Finally

        End Try
    End Function

    Public Function SelectLoadCmb(ByVal IdWorkFlow As Integer) As DataSet
        Try
            Dim Proc As New StoredProcedure("SP_TblCarpeta")
            Proc.Param("TipoQuery", "LOADCBO")
            Proc.Param("IdWorkFlowExtraData", IdWorkFlow)

            Return Proc.ExecSP_ds()
        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        Finally

        End Try
    End Function


    Public Function InsertCarpeta(ByVal E_ObjCarpeta As Capa_Entidad.E_ClsTblCarpeta) As String
        Try
            Dim Proc As New StoredProcedure("SP_TblCarpeta")

            Proc.Param("TipoQuery", "INSERT")

            Proc.Param("Carpeta", E_ObjCarpeta.Carpeta.Trim())
            Proc.Param("Nivel", 1)
            Proc.Param("LastTransactionUser", E_ObjCarpeta.LastTransactionUser)

            Dim ds As DataSet = Proc.ExecSP_ds()
            Dim dt As DataTable = ds.Tables(0)

            If dt.Rows.Count > 0 Then
                Return "OK" & CStr(dt.Rows(0).Item("IdCarpeta"))
            Else
                Throw New Exception("Error")
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        Finally

        End Try
    End Function
    Public Function InsertSubCarpeta(ByVal E_ObjCarpeta As Capa_Entidad.E_ClsTblCarpeta) As String
        Try
            Dim Proc As New StoredProcedure("SP_TblCarpeta")

            Proc.Param("TipoQuery", "INSERT")
            Proc.Param("IdCarpetaPadre", E_ObjCarpeta.IdCarpetaPadre)
            Proc.Param("Carpeta", E_ObjCarpeta.Carpeta.Trim())
            'Proc.Param("StoreProcedure", E_ObjCarpeta.StoreProcedure.Trim())

            Proc.Param("Nivel", E_ObjCarpeta.Nivel)
            Proc.Param("Confidencial", E_ObjCarpeta.Confidencial)
            Proc.Param("Estado", E_ObjCarpeta.Estado)
            Proc.Param("Contenedora", E_ObjCarpeta.Contenedora)
            Proc.Param("LastTransactionUser", E_ObjCarpeta.LastTransactionUser)

            Dim ds As DataSet = Proc.ExecSP_ds()
            Dim dt As DataTable = ds.Tables(0)

            If dt.Rows.Count > 0 Then
                Return "OK" & CStr(dt.Rows(0).Item("IdCarpeta"))
            Else
                Throw New Exception("Error")
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        Finally

        End Try
    End Function
    Public Function Update(ByVal E_ObjCarpeta As Capa_Entidad.E_ClsTblCarpeta) As String
        Try
            Dim Proc As New StoredProcedure("SP_TblCarpeta")

            Proc.Param("TipoQuery", "UPDATE")
            Proc.Param("IdCarpeta", E_ObjCarpeta.IdCarpeta)
            Proc.Param("Carpeta", E_ObjCarpeta.Carpeta.Trim())
            Proc.Param("StoreProcedure", E_ObjCarpeta.StoreProcedure.Trim())

            Proc.Param("Nivel", E_ObjCarpeta.Nivel)
            Proc.Param("Confidencial", E_ObjCarpeta.Confidencial)
            Proc.Param("Estado", E_ObjCarpeta.Estado)
            Proc.Param("Contenedora", E_ObjCarpeta.Contenedora)

            Proc.Param("LastTransactionUser", E_ObjCarpeta.LastTransactionUser)

            Proc.ExecSP()
            Return "OK"

        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        Finally

        End Try
    End Function
    Public Function Delete(ByVal E_ObjCarpeta As Capa_Entidad.E_ClsTblCarpeta) As String
        Try
            Dim Proc As New StoredProcedure("SP_TblCarpeta")

            Proc.Param("TipoQuery", "DELETE")
            Proc.Param("IdCarpeta", E_ObjCarpeta.IdCarpeta)
            Proc.Param("LastTransactionUser", E_ObjCarpeta.LastTransactionUser)

            Proc.ExecSP()
            Return "OK"

        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        Finally

        End Try
    End Function
    Public Function BreadcrumbsFolder(ByVal E_ObjCarpeta As Capa_Entidad.E_ClsTblCarpeta) As String
        Try
            Dim Proc As New StoredProcedure("SP_TblCarpeta")

            Proc.Param("TipoQuery", "BreadcrumbsFolder")
            Proc.Param("IdCarpeta", E_ObjCarpeta.IdCarpeta)

            Dim ds As DataSet = Proc.ExecSP_ds()
            Dim dt As DataTable = ds.Tables(0)

            If dt.Rows.Count > 0 Then
                Return dt.Rows(0).Item("BreadcrumbsFolder").ToString.Trim()
            Else
                Throw New Exception("Error")
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        Finally

        End Try
    End Function


    Public Function getSubCarpetaRuta(ByVal E_ObjCarpeta As Capa_Entidad.E_ClsTblCarpeta) As String
        Try
            Dim Proc As New StoredProcedure("SP_TblCarpeta")

            Proc.Param("TipoQuery", "getSubCarpetaRuta")
            Proc.Param("IdCarpeta", E_ObjCarpeta.IdCarpeta)

            Dim ds As DataSet = Proc.ExecSP_ds()
            Dim dt As DataTable = ds.Tables(0)

            If dt.Rows.Count > 0 Then
                Return dt.Rows(0).Item("getSubCarpetaRuta").ToString.Trim()
            Else
                Throw New Exception("Error")
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        Finally

        End Try
    End Function

    Public Function BreadcrumbsFolderSimple(ByVal E_ObjCarpeta As Capa_Entidad.E_ClsTblCarpeta) As String
        Try
            Dim Proc As New StoredProcedure("SP_TblCarpeta")

            Proc.Param("TipoQuery", "BreadcrumbsFolderSimple")
            Proc.Param("IdCarpeta", E_ObjCarpeta.IdCarpeta)

            Dim ds As DataSet = Proc.ExecSP_ds()
            Dim dt As DataTable = ds.Tables(0)

            If dt.Rows.Count > 0 Then
                Return dt.Rows(0).Item("BreadcrumbsFolder").ToString.Trim()
            Else
                Throw New Exception("Error")
            End If

        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        Finally

        End Try
    End Function

End Class
