Imports Capa_Entidad
Imports System.Data.SqlClient

Public Class D_ClsTblListaDistribucionUsuario

	Public Function SelectRecord(ByVal E_ObjListaDistribucionUsuario As E_ClsTblListaDistribucionUsuario) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblListaDistribucionUsuario")

			Proc.Param("TipoQuery", "SELECT")
			Proc.Param("IdListaDistribucion", E_ObjListaDistribucionUsuario.IdListaDistribucion)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function SelectAll(ByVal IdListadistribucion As String) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblListaDistribucionUsuario")

			Proc.Param("TipoQuery", "SELECTALL")
			Proc.Param("IdListaDistribucion", IdListadistribucion)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function getUsuariosNoSeleccionados(ByVal IdListaDistribucion As Integer) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblListaDistribucionUsuario")

			Proc.Param("TipoQuery", "getUsuariosNoSeleccionados")
			Proc.Param("IdListaDistribucion", IdListaDistribucion)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function SelectAllbyListaDistribucion(ByVal IdListaDistribucion As Integer) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblListaDistribucionUsuario")

			Proc.Param("TipoQuery", "SELECTALL")
			Proc.Param("IdListaDistribucion", IdListaDistribucion)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function Insert(ByVal E_ObjListaDistribucionUsuario As E_ClsTblListaDistribucionUsuario) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblListaDistribucionUsuario")

			Proc.Param("TipoQuery", "INSERT")
			Proc.Param("IdListaDistribucion", E_ObjListaDistribucionUsuario.IdListaDistribucion)
			Proc.Param("IdUsuario", E_ObjListaDistribucionUsuario.IdUsuario.Trim())
			Proc.Param("LastTransactionUser", E_ObjListaDistribucionUsuario.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Update(ByVal E_ObjListaDistribucionUsuario As E_ClsTblListaDistribucionUsuario) As String
		Try
			Dim Proc As New StoredProcedure("_SP_TblListaDistribucionUsuario")

			Proc.Param("TipoQuery", "UPDATE")
			Proc.Param("IdListaDistribucion", E_ObjListaDistribucionUsuario.IdListaDistribucion)
			Proc.Param("IdUsuario", E_ObjListaDistribucionUsuario.IdUsuario.Trim())
			Proc.Param("LastTransactionUser", E_ObjListaDistribucionUsuario.LastTransactionUser.Trim())
			Proc.Param("LastTransactionDate", E_ObjListaDistribucionUsuario.LastTransactionDate)
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Delete(ByVal E_ObjListaDistribucionUsuario As E_ClsTblListaDistribucionUsuario) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblListaDistribucionUsuario")

			Proc.Param("TipoQuery", "DELETE")
			Proc.Param("IdListaDistribucion", E_ObjListaDistribucionUsuario.IdListaDistribucion)
			Proc.Param("IdUsuario", E_ObjListaDistribucionUsuario.IdUsuario.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

End Class

