Imports Capa_Entidad
Imports System.Data.SqlClient

Public Class D_ClsTblEjecucionCritica

	Public Function SelectRecord(ByVal E_ObjEjecucionCritica As E_ClsTblEjecucionCritica) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblEjecucionCritica")

			Proc.Param("TipoQuery", "SELECT")
			Proc.Param("IdEjecucionCritica", E_ObjEjecucionCritica.IdEjecucionCritica)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function SelectAll(ByVal TxtBsc As String) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblEjecucionCritica")

			Proc.Param("TipoQuery", "SELECTALL")
			Proc.Param("TxtBsc", TxtBsc)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function Insert(ByVal E_ObjEjecucionCritica As E_ClsTblEjecucionCritica) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblEjecucionCritica")

			Proc.Param("TipoQuery", "INSERT")
			Proc.Param("IdEjecucionCritica", E_ObjEjecucionCritica.IdEjecucionCritica)
			Proc.Param("IdEjecucion", E_ObjEjecucionCritica.IdEjecucion)
			Proc.Param("KeyCritica", E_ObjEjecucionCritica.KeyCritica.Trim())
			Proc.Param("TxtCritica", E_ObjEjecucionCritica.TxtCritica.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Update(ByVal E_ObjEjecucionCritica As E_ClsTblEjecucionCritica) As String
		Try
			Dim Proc As New StoredProcedure("_SP_TblEjecucionCritica")

			Proc.Param("TipoQuery", "UPDATE")
			Proc.Param("IdEjecucionCritica", E_ObjEjecucionCritica.IdEjecucionCritica)
			Proc.Param("IdEjecucion", E_ObjEjecucionCritica.IdEjecucion)
			Proc.Param("KeyCritica", E_ObjEjecucionCritica.KeyCritica.Trim())
			Proc.Param("TxtCritica", E_ObjEjecucionCritica.TxtCritica.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Delete(ByVal E_ObjEjecucionCritica As E_ClsTblEjecucionCritica) As String
		Try
			Dim Proc As New StoredProcedure("_SP_TblEjecucionCritica")

			Proc.Param("TipoQuery", "DELETE")
			Proc.Param("IdEjecucionCritica", E_ObjEjecucionCritica.IdEjecucionCritica)
			Proc.Param("IdEjecucion", E_ObjEjecucionCritica.IdEjecucion)
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

End Class

