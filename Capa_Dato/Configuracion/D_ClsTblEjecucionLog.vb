Imports Capa_Entidad
Imports System.Data.SqlClient

Public Class D_ClsTblEjecucionLog

	Public Function SelectRecord(ByVal E_ObjEjecucionLog As E_ClsTblEjecucionLog) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblEjecucionLog")

			Proc.Param("TipoQuery", "SELECT")
			Proc.Param("IdEjecucionLog", E_ObjEjecucionLog.IdEjecucionLog)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function SelectAll(ByVal TxtBsc As String) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblEjecucionLog")

			Proc.Param("TipoQuery", "SELECTALL")
			Proc.Param("TxtBsc", TxtBsc)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function Insert(ByVal E_ObjEjecucionLog As E_ClsTblEjecucionLog) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblEjecucionLog")

			Proc.Param("TipoQuery", "INSERT")
			Proc.Param("IdEjecucionLog", E_ObjEjecucionLog.IdEjecucionLog)
			Proc.Param("IdEjecucion", E_ObjEjecucionLog.IdEjecucion)
			Proc.Param("Fecha", E_ObjEjecucionLog.Fecha)
			Proc.Param("IdAccion", E_ObjEjecucionLog.IdAccion)
			Proc.Param("Comentario", E_ObjEjecucionLog.Comentario.Trim())
			Proc.Param("LastTransactionUser", E_ObjEjecucionLog.LastTransactionUser.Trim())
			Proc.Param("LastTransactionDate", E_ObjEjecucionLog.LastTransactionDate)
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Update(ByVal E_ObjEjecucionLog As E_ClsTblEjecucionLog) As String
		Try
			Dim Proc As New StoredProcedure("_SP_TblEjecucionLog")

			Proc.Param("TipoQuery", "UPDATE")
			Proc.Param("IdEjecucionLog", E_ObjEjecucionLog.IdEjecucionLog)
			Proc.Param("IdEjecucion", E_ObjEjecucionLog.IdEjecucion)
			Proc.Param("Fecha", E_ObjEjecucionLog.Fecha)
			Proc.Param("IdAccion", E_ObjEjecucionLog.IdAccion)
			Proc.Param("Comentario", E_ObjEjecucionLog.Comentario.Trim())
			Proc.Param("LastTransactionUser", E_ObjEjecucionLog.LastTransactionUser.Trim())
			Proc.Param("LastTransactionDate", E_ObjEjecucionLog.LastTransactionDate)
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Delete(ByVal E_ObjEjecucionLog As E_ClsTblEjecucionLog) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblEjecucionLog")

			Proc.Param("TipoQuery", "DELETE")
			Proc.Param("IdEjecucionLog", E_ObjEjecucionLog.IdEjecucionLog)
			Proc.Param("IdEjecucion", E_ObjEjecucionLog.IdEjecucion)
			Proc.Param("LastTransactionUser", E_ObjEjecucionLog.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

End Class

