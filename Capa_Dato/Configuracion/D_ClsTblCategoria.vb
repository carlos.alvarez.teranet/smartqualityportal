﻿Imports Capa_Entidad
Imports System.Data.SqlClient
Public Class D_ClsTblCategoria
    Public Function SelectAll(ByVal TxtBsc As String) As DataSet
        Try
            Dim Proc As New StoredProcedure("SP_TblCategoria")

            Proc.Param("Querytype", "SELECTALL")
            Proc.Param("TxtBsc", TxtBsc)

            Return Proc.ExecSP_ds()

        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        Finally

        End Try
    End Function





    Public Function GetRegistro(ByVal E_ObjCategoria As E_ClsTblCategoria) As DataSet
        Try
            Dim Proc As New StoredProcedure("SP_TblCategoria")

            Proc.Param("Querytype", "GETREGISTRO")
            Proc.Param("IdCategoria", E_ObjCategoria.IdCategoria)

            Return Proc.ExecSP_ds()

        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        Finally

        End Try
    End Function
    Public Function Insert(ByVal E_ObjCategoria As E_ClsTblCategoria) As String
        Try
            Dim Proc As New StoredProcedure("SP_TblCategoria")

            Proc.Param("Querytype", "INSERT")
            Proc.Param("Categoria", E_ObjCategoria.Categoria.Trim())
            Proc.Param("LastTransactionUser", E_ObjCategoria.LastTransactionUser.Trim())

            Proc.ExecSP()
            Return "OK"

        Catch ex As Exception
            Return ex.Message
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Update(ByVal E_ObjCategoria As E_ClsTblCategoria) As String
        Try
            Dim Proc As New StoredProcedure("SP_TblCategoria")

            Proc.Param("Querytype", "UPDATE")
            Proc.Param("IdCategoria", E_ObjCategoria.IdCategoria)
            Proc.Param("Categoria", E_ObjCategoria.Categoria.Trim())
            Proc.Param("LastTransactionUser", E_ObjCategoria.LastTransactionUser.Trim())

            Proc.ExecSP()
            Return "OK"

        Catch ex As Exception
            Return ex.Message
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function Delete(ByVal E_ObjCategoria As E_ClsTblCategoria) As String
        Try
            Dim Proc As New StoredProcedure("SP_TblCategoria")

            Proc.Param("Querytype", "DELETE")
            Proc.Param("IdCategoria", E_ObjCategoria.IdCategoria)
            Proc.Param("LastTransactionUser", E_ObjCategoria.LastTransactionUser.Trim())

            Proc.ExecSP()
            Return "OK"

        Catch ex As Exception
            Return ex.Message
            Throw New Exception(ex.Message)
        End Try
    End Function
    Public Function LoadCbo() As DataSet
        Try
            Dim Proc As New StoredProcedure("SP_TblCategoria")
            Proc.Param("Querytype", "LOADCBO")

            Return Proc.ExecSP_ds()

        Catch ex As Exception
            Throw New Exception(ex.Message)
            Return Nothing
        Finally

        End Try
    End Function
End Class
