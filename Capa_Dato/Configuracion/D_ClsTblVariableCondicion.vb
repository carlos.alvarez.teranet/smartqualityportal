Imports Capa_Entidad
Imports System.Data.SqlClient

Public Class D_ClsTblVariableCondicion

	Public Function SelectRecord(ByVal E_ObjVariableCondicion As E_ClsTblVariableCondicion) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblVariableCondicion")

			Proc.Param("TipoQuery", "SELECT")
			Proc.Param("IdVariableCondicion", E_ObjVariableCondicion.IdVariableCondicion)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function SelectAll(ByVal TxtBsc As String) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblVariableCondicion")

			Proc.Param("TipoQuery", "SELECTALL")
			Proc.Param("TxtBsc", TxtBsc)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function Insert(ByVal E_ObjVariableCondicion As E_ClsTblVariableCondicion) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblVariableCondicion")

			Proc.Param("TipoQuery", "INSERT")
			Proc.Param("IdVariableCondicion", E_ObjVariableCondicion.IdVariableCondicion)
			Proc.Param("VariableCondicion", E_ObjVariableCondicion.VariableCondicion.Trim())
			Proc.Param("IdTipoDato", E_ObjVariableCondicion.IdTipoDato)
			Proc.Param("Largo", E_ObjVariableCondicion.Largo.Trim())
			Proc.Param("Tabla", E_ObjVariableCondicion.Tabla.Trim())
			Proc.Param("Estado", E_ObjVariableCondicion.Estado)
			Proc.Param("LastTransactionUser", E_ObjVariableCondicion.LastTransactionUser.Trim())
			Proc.Param("LastTransactionDate", E_ObjVariableCondicion.LastTransactionDate)
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Update(ByVal E_ObjVariableCondicion As E_ClsTblVariableCondicion) As String
		Try
			Dim Proc As New StoredProcedure("_SP_TblVariableCondicion")

			Proc.Param("TipoQuery", "UPDATE")
			Proc.Param("IdVariableCondicion", E_ObjVariableCondicion.IdVariableCondicion)
			Proc.Param("VariableCondicion", E_ObjVariableCondicion.VariableCondicion.Trim())
			Proc.Param("IdTipoDato", E_ObjVariableCondicion.IdTipoDato)
			Proc.Param("Largo", E_ObjVariableCondicion.Largo.Trim())
			Proc.Param("Tabla", E_ObjVariableCondicion.Tabla.Trim())
			Proc.Param("Estado", E_ObjVariableCondicion.Estado)
			Proc.Param("LastTransactionUser", E_ObjVariableCondicion.LastTransactionUser.Trim())
			Proc.Param("LastTransactionDate", E_ObjVariableCondicion.LastTransactionDate)
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Delete(ByVal E_ObjVariableCondicion As E_ClsTblVariableCondicion) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblVariableCondicion")

			Proc.Param("TipoQuery", "DELETE")
			Proc.Param("IdVariableCondicion", E_ObjVariableCondicion.IdVariableCondicion)
			Proc.Param("LastTransactionUser", E_ObjVariableCondicion.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function


	Public Function LoadCbo() As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblVariableCondicion")

			Proc.Param("TipoQuery", "LOADCBO")
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

End Class

