Imports Capa_Entidad
Imports System.Data.SqlClient

Public Class D_ClsTblCarpetaAutorizado

	Public Function SelectRecord(ByVal E_ObjCarpetaAutorizado As E_ClsTblCarpetaAutorizado) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblCarpetaAutorizado")

			Proc.Param("TipoQuery", "SELECT")
			Proc.Param("IdCarpeta", E_ObjCarpetaAutorizado.IdCarpeta)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function SelectAll(ByVal TxtBsc As String) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblCarpetaAutorizado")

			Proc.Param("TipoQuery", "SELECTALL")
			Proc.Param("TxtBsc", TxtBsc)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function SelectAllbyCarpeta(ByVal IdCarpeta As Int16) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblCarpetaAutorizado")

			Proc.Param("TipoQuery", "SELECTALL")
			Proc.Param("IdCarpeta", IdCarpeta)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function



	Public Function Insert(ByVal E_ObjCarpetaAutorizado As E_ClsTblCarpetaAutorizado) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblCarpetaAutorizado")

			Proc.Param("TipoQuery", "INSERT")
			Proc.Param("IdCarpeta", E_ObjCarpetaAutorizado.IdCarpeta)
			Proc.Param("IdAutorizado", E_ObjCarpetaAutorizado.IdAutorizado.Trim())
			Proc.Param("LastTransactionUser", E_ObjCarpetaAutorizado.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Update(ByVal E_ObjCarpetaAutorizado As E_ClsTblCarpetaAutorizado) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblCarpetaAutorizado")

			Proc.Param("TipoQuery", "UPDATE")
			Proc.Param("IdCarpeta", E_ObjCarpetaAutorizado.IdCarpeta)
			Proc.Param("IdAutorizado", E_ObjCarpetaAutorizado.IdAutorizado.Trim())
			Proc.Param("LastTransactionUser", E_ObjCarpetaAutorizado.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Delete(ByVal E_ObjCarpetaAutorizado As E_ClsTblCarpetaAutorizado) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblCarpetaAutorizado")

			Proc.Param("TipoQuery", "DELETE")
			Proc.Param("IdCarpeta", E_ObjCarpetaAutorizado.IdCarpeta)
			Proc.Param("IdAutorizado", E_ObjCarpetaAutorizado.IdAutorizado)

			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function DeleteAllCarpeta(ByVal IdCarpeta As Integer) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblCarpetaAutorizado")

			Proc.Param("TipoQuery", "DELETECARPETA")
			Proc.Param("IdCarpeta", IdCarpeta)

			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

End Class

