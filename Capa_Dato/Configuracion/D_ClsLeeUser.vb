﻿Public Class D_ClsLeeUser
	Public Function LoadCbo() As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_LeeUser")

			Proc.Param("QueryType", "LOADCBO")
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function
End Class
