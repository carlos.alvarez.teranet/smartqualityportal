Imports Capa_Entidad
Imports System.Data.SqlClient

Public Class D_ClsTblNubWorkflowJobPosition

	Public Function SelectRecord(ByVal E_ObjNubWorkflowJobPosition As E_ClsTblNubWorkflowJobPosition) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblNubWorkflowJobPosition")

			Proc.Param("TipoQuery", "SELECT")
			Proc.Param("IdWorkflow", E_ObjNubWorkflowJobPosition.IdWorkflow)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function SelectAll(ByVal IdWorkFlow As Integer) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblNubWorkflowJobPosition")

			Proc.Param("TipoQuery", "SELECTALL")
			Proc.Param("IdWorkflow", IdWorkFlow)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function Insert(ByVal E_ObjNubWorkflowJobPosition As E_ClsTblNubWorkflowJobPosition) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblNubWorkflowJobPosition")

			Proc.Param("TipoQuery", "INSERT")
			Proc.Param("IdWorkflow", E_ObjNubWorkflowJobPosition.IdWorkflow)
			Proc.Param("IdJobPosition", E_ObjNubWorkflowJobPosition.IdJobPosition)
			Proc.Param("PosicionRuta", E_ObjNubWorkflowJobPosition.PosicionRuta)
			Proc.Param("LastTransactionUser", E_ObjNubWorkflowJobPosition.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Update(ByVal E_ObjNubWorkflowJobPosition As E_ClsTblNubWorkflowJobPosition) As String
		Try
			Dim Proc As New StoredProcedure("_SP_TblNubWorkflowJobPosition")

			Proc.Param("TipoQuery", "UPDATE")
			Proc.Param("IdWorkflow", E_ObjNubWorkflowJobPosition.IdWorkflow)
			Proc.Param("IdJobPosition", E_ObjNubWorkflowJobPosition.IdJobPosition)
			Proc.Param("PosicionRuta", E_ObjNubWorkflowJobPosition.PosicionRuta)
			Proc.Param("LastTransactionUser", E_ObjNubWorkflowJobPosition.LastTransactionUser.Trim())
			Proc.Param("LastTransactionDate", E_ObjNubWorkflowJobPosition.LastTransactionDate)
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Delete(ByVal E_ObjNubWorkflowJobPosition As E_ClsTblNubWorkflowJobPosition) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblNubWorkflowJobPosition")

			Proc.Param("TipoQuery", "DELETE")
			Proc.Param("IdWorkflow", E_ObjNubWorkflowJobPosition.IdWorkflow)
			Proc.Param("IdJobPosition", E_ObjNubWorkflowJobPosition.IdJobPosition)
			Proc.Param("LastTransactionUser", E_ObjNubWorkflowJobPosition.LastTransactionUser)

			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

End Class

