Imports Capa_Entidad
Imports System.Data.SqlClient

Public Class D_ClsTblComentarioEjecucion

	Public Function SelectRecord(ByVal E_ObjComentarioEjecucion As E_ClsTblComentarioEjecucion) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblComentarioEjecucion")

			Proc.Param("TipoQuery", "SELECT")
			Proc.Param("IdComentario", E_ObjComentarioEjecucion.IdComentario)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function SelectAll(ByVal IdEjecucion As Integer) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblComentarioEjecucion")

			Proc.Param("TipoQuery", "SELECTALL")
			Proc.Param("IdEjecucion", IdEjecucion)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function Insert(ByVal E_ObjComentarioEjecucion As E_ClsTblComentarioEjecucion) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblComentarioEjecucion")

			Proc.Param("TipoQuery", "INSERT")
			Proc.Param("IdEjecucion", E_ObjComentarioEjecucion.IdEjecucion)
			Proc.Param("Comentario", E_ObjComentarioEjecucion.Comentario.Trim())
			Proc.Param("LastTransactionUser", E_ObjComentarioEjecucion.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Update(ByVal E_ObjComentarioEjecucion As E_ClsTblComentarioEjecucion) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblComentarioEjecucion")

			Proc.Param("TipoQuery", "UPDATE")
			Proc.Param("IdComentario", E_ObjComentarioEjecucion.IdComentario)
			Proc.Param("IdEjecucion", E_ObjComentarioEjecucion.IdEjecucion)
			Proc.Param("Fecha", E_ObjComentarioEjecucion.Fecha)
			Proc.Param("Comentario", E_ObjComentarioEjecucion.Comentario.Trim())
			Proc.Param("LastTransactionUser", E_ObjComentarioEjecucion.LastTransactionUser.Trim())
			Proc.Param("LastTransactionDate", E_ObjComentarioEjecucion.LastTransactionDate)
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Delete(ByVal E_ObjComentarioEjecucion As E_ClsTblComentarioEjecucion) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblComentarioEjecucion")

			Proc.Param("TipoQuery", "DELETE")
			Proc.Param("IdComentario", E_ObjComentarioEjecucion.IdComentario)
			Proc.Param("IdEjecucion", E_ObjComentarioEjecucion.IdEjecucion)
			Proc.Param("LastTransactionUser", E_ObjComentarioEjecucion.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

End Class

