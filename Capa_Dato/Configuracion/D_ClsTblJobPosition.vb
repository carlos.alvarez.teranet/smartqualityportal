Imports Capa_Entidad
Imports System.Data.SqlClient

Public Class D_ClsTblJobPosition

	Public Function SelectRecord(ByVal E_ObjJobPosition As E_ClsTblJobPosition) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblJobPosition")

			Proc.Param("QueryType", "SELECT")
			Proc.Param("IdJobPosition", E_ObjJobPosition.IdJobPosition)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function SelecAllCboWorkFlow(ByVal IdWorkFlow As String) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblJobPosition")

			Proc.Param("QueryType", "SELECTALLCBOWORKFLOW")
			Proc.Param("IdWorkFlow", IdWorkFlow)

			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function SelectAll(ByVal TxtBsc As String) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblJobPosition")

			Proc.Param("QueryType", "SELECTALL")
			Proc.Param("TxtBsc", TxtBsc)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function Insert(ByVal E_ObjJobPosition As E_ClsTblJobPosition) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblJobPosition")

			Proc.Param("QueryType", "INSERT")
			Proc.Param("JobPosition", E_ObjJobPosition.JobPosition.Trim())
			Proc.Param("LineManeger", E_ObjJobPosition.LineManeger.Trim())
			Proc.Param("Estado", E_ObjJobPosition.Estado)
			Proc.Param("LastTransactionUser", E_ObjJobPosition.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Update(ByVal E_ObjJobPosition As E_ClsTblJobPosition) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblJobPosition")

			Proc.Param("QueryType", "UPDATE")
			Proc.Param("IdJobPosition", E_ObjJobPosition.IdJobPosition)
			Proc.Param("JobPosition", E_ObjJobPosition.JobPosition.Trim())
			Proc.Param("LineManeger", E_ObjJobPosition.LineManeger.Trim())
			Proc.Param("Estado", E_ObjJobPosition.Estado)
			Proc.Param("LastTransactionUser", E_ObjJobPosition.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Delete(ByVal E_ObjJobPosition As E_ClsTblJobPosition) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblJobPosition")

			Proc.Param("QueryType", "DELETE")
			Proc.Param("IdJobPosition", E_ObjJobPosition.IdJobPosition)
			Proc.Param("LastTransactionUser", E_ObjJobPosition.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

End Class

