Imports Capa_Entidad
Imports System.Data.SqlClient

Public Class D_ClsTblNubEjecucionCarpeta

	Public Function SelectRecord(ByVal E_ObjNubEjecucionCarpeta As E_ClsTblNubEjecucionCarpeta) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblNubEjecucionCarpeta")

			Proc.Param("TipoQuery", "SELECT")
			Proc.Param("IdEjecucion", E_ObjNubEjecucionCarpeta.IdEjecucion)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function SelectAll(ByVal IdEjecucion As Integer) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblNubEjecucionCarpeta")

			Proc.Param("TipoQuery", "SELECTALL")
			Proc.Param("IdEjecucion", IdEjecucion)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function Insert(ByVal E_ObjNubEjecucionCarpeta As E_ClsTblNubEjecucionCarpeta) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblNubEjecucionCarpeta")

			Proc.Param("TipoQuery", "INSERT")
			Proc.Param("IdEjecucion", E_ObjNubEjecucionCarpeta.IdEjecucion)
			Proc.Param("IdCarpeta", E_ObjNubEjecucionCarpeta.IdCarpeta)
			Proc.Param("LastTransactionUser", E_ObjNubEjecucionCarpeta.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Update(ByVal E_ObjNubEjecucionCarpeta As E_ClsTblNubEjecucionCarpeta) As String
		Try
			Dim Proc As New StoredProcedure("_SP_TblNubEjecucionCarpeta")

			Proc.Param("TipoQuery", "UPDATE")
			Proc.Param("IdEjecucion", E_ObjNubEjecucionCarpeta.IdEjecucion)
			Proc.Param("IdCarpeta", E_ObjNubEjecucionCarpeta.IdCarpeta)
			Proc.Param("LastTransactionUser", E_ObjNubEjecucionCarpeta.LastTransactionUser.Trim())
			Proc.Param("LastTransactionDate", E_ObjNubEjecucionCarpeta.LastTransactionDate)
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Delete(ByVal E_ObjNubEjecucionCarpeta As E_ClsTblNubEjecucionCarpeta) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblNubEjecucionCarpeta")

			Proc.Param("TipoQuery", "DELETE")
			Proc.Param("IdEjecucion", E_ObjNubEjecucionCarpeta.IdEjecucion)
			Proc.Param("IdCarpeta", E_ObjNubEjecucionCarpeta.IdCarpeta)
			Proc.Param("LastTransactionUser", E_ObjNubEjecucionCarpeta.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

End Class

