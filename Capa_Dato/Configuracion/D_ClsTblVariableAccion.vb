Imports Capa_Entidad
Imports System.Data.SqlClient

Public Class D_ClsTblVariableAccion

	Public Function SelectRecord(ByVal E_ObjVariableAccion As E_ClsTblVariableAccion) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblVariableAccion")

			Proc.Param("TipoQuery", "SELECT")
			Proc.Param("IdVariableAccion", E_ObjVariableAccion.IdVariableAccion)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function SelectAll(ByVal TxtBsc As String) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblVariableAccion")

			Proc.Param("TipoQuery", "SELECTALL")
			Proc.Param("TxtBsc", TxtBsc)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function LoadCbo() As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblVariableAccion")

			Proc.Param("TipoQuery", "LOADCBO")
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function
	Public Function Insert(ByVal E_ObjVariableAccion As E_ClsTblVariableAccion) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblVariableAccion")

			Proc.Param("TipoQuery", "INSERT")
			Proc.Param("IdVariableAccion", E_ObjVariableAccion.IdVariableAccion)
			Proc.Param("VariableAccion", E_ObjVariableAccion.VariableAccion.Trim())
			Proc.Param("IdTipoDato", E_ObjVariableAccion.IdTipoDato)
			Proc.Param("Largo", E_ObjVariableAccion.Largo.Trim())
			Proc.Param("Tabla", E_ObjVariableAccion.Tabla.Trim())
			Proc.Param("Estado", E_ObjVariableAccion.Estado)
			Proc.Param("LastTransactionUser", E_ObjVariableAccion.LastTransactionUser.Trim())
			Proc.Param("LastTransactionDate", E_ObjVariableAccion.LastTransactionDate)
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Update(ByVal E_ObjVariableAccion As E_ClsTblVariableAccion) As String
		Try
			Dim Proc As New StoredProcedure("_SP_TblVariableAccion")

			Proc.Param("TipoQuery", "UPDATE")
			Proc.Param("IdVariableAccion", E_ObjVariableAccion.IdVariableAccion)
			Proc.Param("VariableAccion", E_ObjVariableAccion.VariableAccion.Trim())
			Proc.Param("IdTipoDato", E_ObjVariableAccion.IdTipoDato)
			Proc.Param("Largo", E_ObjVariableAccion.Largo.Trim())
			Proc.Param("Tabla", E_ObjVariableAccion.Tabla.Trim())
			Proc.Param("Estado", E_ObjVariableAccion.Estado)
			Proc.Param("LastTransactionUser", E_ObjVariableAccion.LastTransactionUser.Trim())
			Proc.Param("LastTransactionDate", E_ObjVariableAccion.LastTransactionDate)
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Delete(ByVal E_ObjVariableAccion As E_ClsTblVariableAccion) As String
		Try
			Dim Proc As New StoredProcedure("_SP_TblVariableAccion")

			Proc.Param("TipoQuery", "DELETE")
			Proc.Param("IdVariableAccion", E_ObjVariableAccion.IdVariableAccion)
			Proc.Param("LastTransactionUser", E_ObjVariableAccion.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

End Class

