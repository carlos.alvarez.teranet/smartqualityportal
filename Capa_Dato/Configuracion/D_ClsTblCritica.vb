Imports Capa_Entidad
Imports System.Data.SqlClient

Public Class D_ClsTblCritica

	Public Function SelectRecord(ByVal E_ObjCritica As E_ClsTblCritica) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblCritica")

			Proc.Param("TipoQuery", "SELECT")
			Proc.Param("IdCritica", E_ObjCritica.IdCritica)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function SelectAll(ByVal TxtBsc As String, ByVal IdCarpeta As Integer) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblCritica")

			Proc.Param("TipoQuery", "SELECTALL")
			'			Proc.Param("TxtBsc", TxtBsc)
			Proc.Param("IdCarpeta", IdCarpeta)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function
	Public Function getPendientesAprobacion(ByVal IdCritica As Integer) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblLogCritica")

			Proc.Param("QueryType", "GetAprobadoresCriticasListado")
			Proc.Param("IdCritica", IdCritica)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try


	End Function

	Public Function SelectAllPenditeAprobacion(ByVal IdUsuario As String) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblCritica")

			Proc.Param("TipoQuery", "SelectAllPenditeAprobacion")
			Proc.Param("LastTransactionUser", IdUsuario)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try


	End Function



	Public Function getCorrelativoCarpeta(ByVal IdCarpeta As Integer) As String

		Try
			Dim Proc As New StoredProcedure("SP_TblCritica")

			Proc.Param("TipoQuery", "getCorrelativoCarpeta")
			Proc.Param("IdCarpeta", IdCarpeta)
			Dim ds As DataSet = Proc.ExecSP_ds()
			Dim dt As DataTable = ds.Tables(0)

			If dt.Rows.Count > 0 Then
				Return dt.Rows(0).Item("correlativo")
			Else
				Throw New Exception("Error")
			End If



		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function



	Public Function Insert(ByVal E_ObjCritica As E_ClsTblCritica) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblCritica")

			Proc.Param("TipoQuery", "INSERT")
			Proc.Param("IdCritica", E_ObjCritica.IdCritica)
			Proc.Param("IdCarpeta", E_ObjCritica.IdCarpeta)
			Proc.Param("IdPrefijo", E_ObjCritica.IdPrefijo)
			Proc.Param("Correlativo", E_ObjCritica.Correlativo)
			Proc.Param("Version", E_ObjCritica.Version)
			Proc.Param("IdEstado", E_ObjCritica.IdEstado)
			Proc.Param("Owner", E_ObjCritica.Owner.Trim())
			Proc.Param("FechaCreacion", E_ObjCritica.FechaCreacion)
			Proc.Param("FechaRevision", E_ObjCritica.FechaRevision)
			Proc.Param("IdWorkflow", E_ObjCritica.IdWorkflow)
			Proc.Param("Justificacion", E_ObjCritica.Justificacion.Trim())
			Proc.Param("IdVariableAccion", E_ObjCritica.IdVariableAccion)
			Proc.Param("VariableAccionValor", E_ObjCritica.VariableAccionValor.Trim())
			Proc.Param("LastTransactionUser", E_ObjCritica.LastTransactionUser.Trim())
			Dim ds As DataSet = Proc.ExecSP_ds()
			Dim dt As DataTable = ds.Tables(0)

			If dt.Rows.Count > 0 Then
				Return "OK" & CStr(dt.Rows(0).Item("IdCritica"))
			Else
				Throw New Exception("Error")
			End If
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function



	Public Function UpdateEstado(ByVal E_ObjCritica As E_ClsTblCritica) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblCritica")

			Proc.Param("TipoQuery", "UpdateEstado")
			Proc.Param("IdCritica", E_ObjCritica.IdCritica)
			Proc.Param("IdEstado", E_ObjCritica.IdEstado)
			Proc.Param("LastTransactionUser", E_ObjCritica.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK" & CStr(E_ObjCritica.IdCritica)

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function
	Public Function Update(ByVal E_ObjCritica As E_ClsTblCritica) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblCritica")

			Proc.Param("TipoQuery", "UPDATE")
			Proc.Param("IdCritica", E_ObjCritica.IdCritica)
			Proc.Param("IdCarpeta", E_ObjCritica.IdCarpeta)
			Proc.Param("IdPrefijo", E_ObjCritica.IdPrefijo)
			Proc.Param("Correlativo", E_ObjCritica.Correlativo)
			Proc.Param("Version", E_ObjCritica.Version)
			Proc.Param("IdEstado", E_ObjCritica.IdEstado)
			Proc.Param("Owner", E_ObjCritica.Owner.Trim())
			Proc.Param("FechaCreacion", E_ObjCritica.FechaCreacion)
			Proc.Param("FechaRevision", E_ObjCritica.FechaRevision)
			Proc.Param("IdWorkflow", E_ObjCritica.IdWorkflow)
			Proc.Param("Justificacion", E_ObjCritica.Justificacion.Trim())
			Proc.Param("IdVariableAccion", E_ObjCritica.IdVariableAccion)
			Proc.Param("VariableAccionValor", E_ObjCritica.VariableAccionValor.Trim())
			Proc.Param("LastTransactionUser", E_ObjCritica.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK" & CStr(E_ObjCritica.IdCritica)

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Delete(ByVal E_ObjCritica As E_ClsTblCritica) As String
		Try
			Dim Proc As New StoredProcedure("_SP_TblCritica")

			Proc.Param("TipoQuery", "DELETE")
			Proc.Param("IdCritica", E_ObjCritica.IdCritica)
			Proc.Param("LastTransactionUser", E_ObjCritica.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

End Class

