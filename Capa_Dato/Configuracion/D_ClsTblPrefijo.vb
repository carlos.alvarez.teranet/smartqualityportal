Imports Capa_Entidad
Imports System.Data.SqlClient

Public Class D_ClsTblPrefijo

	Public Function SelectRecord(ByVal E_ObjPrefijo As E_ClsTblPrefijo) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblPrefijo")

			Proc.Param("TipoQuery", "SELECT")
			Proc.Param("IdPrefijo", E_ObjPrefijo.IdPrefijo)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function SelectAllbyCarpeta(ByVal IdCarpeta As Int16) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblPrefijo")

			Proc.Param("TipoQuery", "SELECTALL")
			Proc.Param("IdCarpeta", IdCarpeta)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function LoadCbo(ByVal IdCarpeta As Int16) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblPrefijo")

			Proc.Param("TipoQuery", "LOADCBO")
			Proc.Param("IdCarpeta", IdCarpeta)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function




	Public Function SelectAll(ByVal TxtBsc As String) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblPrefijo")

			Proc.Param("TipoQuery", "SELECTALL")
			Proc.Param("IdCarpeta", "TxtBsc")

			Proc.Param("TxtBsc", TxtBsc)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function Insert(ByVal E_ObjPrefijo As E_ClsTblPrefijo) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblPrefijo")

			Proc.Param("TipoQuery", "INSERT")
			Proc.Param("IdPrefijo", E_ObjPrefijo.IdPrefijo)
			Proc.Param("IdCarpeta", E_ObjPrefijo.IdCarpeta)
			Proc.Param("Prefijo", E_ObjPrefijo.Prefijo.Trim())
			Proc.Param("Estado", E_ObjPrefijo.Estado)
			Proc.Param("LastTransactionUser", E_ObjPrefijo.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Update(ByVal E_ObjPrefijo As E_ClsTblPrefijo) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblPrefijo")

			Proc.Param("TipoQuery", "UPDATE")
			Proc.Param("IdPrefijo", E_ObjPrefijo.IdPrefijo)
			Proc.Param("IdCarpeta", E_ObjPrefijo.IdCarpeta)
			Proc.Param("Prefijo", E_ObjPrefijo.Prefijo.Trim())
			Proc.Param("Estado", E_ObjPrefijo.Estado)
			Proc.Param("LastTransactionUser", E_ObjPrefijo.LastTransactionUser.Trim())
			Proc.Param("LastTransactionDate", E_ObjPrefijo.LastTransactionDate)
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Delete(ByVal E_ObjPrefijo As E_ClsTblPrefijo) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblPrefijo")

			Proc.Param("TipoQuery", "DELETE")
			Proc.Param("IdPrefijo", E_ObjPrefijo.IdPrefijo)
			Proc.Param("IdCarpeta", E_ObjPrefijo.IdCarpeta)
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

End Class

