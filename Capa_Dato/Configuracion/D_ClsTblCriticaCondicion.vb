Imports Capa_Entidad
Imports System.Data.SqlClient

Public Class D_ClsTblCriticaCondicion

	Public Function SelectRecord(ByVal E_ObjCriticaCondicion As E_ClsTblCriticaCondicion) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblCriticaCondicion")

			Proc.Param("TipoQuery", "SELECT")
			Proc.Param("IdCritica", E_ObjCriticaCondicion.IdCritica)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function SelectByCritica(ByVal IdCritica As Integer) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblCriticaCondicion")

			Proc.Param("TipoQuery", "SelectByCritica")
			Proc.Param("IdCritica", IdCritica)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function



	Public Function SelectAll(ByVal TxtBsc As String) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblCriticaCondicion")

			Proc.Param("TipoQuery", "SELECTALL")
			Proc.Param("TxtBsc", TxtBsc)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function Insert(ByVal E_ObjCriticaCondicion As E_ClsTblCriticaCondicion) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblCriticaCondicion")

			Proc.Param("TipoQuery", "INSERT")
			Proc.Param("IdCritica", E_ObjCriticaCondicion.IdCritica)
			Proc.Param("Orden", E_ObjCriticaCondicion.Orden)
			Proc.Param("IdVariableCondicion", E_ObjCriticaCondicion.IdVariableCondicion)
			Proc.Param("OperadorLogico", E_ObjCriticaCondicion.OperadorLogico.Trim())
			Proc.Param("Valor1", E_ObjCriticaCondicion.Valor1.Trim())
			Proc.Param("Valor2", E_ObjCriticaCondicion.Valor2.Trim())
			Proc.Param("AndOr", E_ObjCriticaCondicion.AndOr.Trim())
			Proc.Param("LastTransactionUser", E_ObjCriticaCondicion.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Update(ByVal E_ObjCriticaCondicion As E_ClsTblCriticaCondicion) As String
		Try
			Dim Proc As New StoredProcedure("_SP_TblCriticaCondicion")

			Proc.Param("TipoQuery", "UPDATE")
			Proc.Param("IdCritica", E_ObjCriticaCondicion.IdCritica)
			Proc.Param("Orden", E_ObjCriticaCondicion.Orden)
			Proc.Param("IdVariableCondicion", E_ObjCriticaCondicion.IdVariableCondicion)
			Proc.Param("OperadorLogico", E_ObjCriticaCondicion.OperadorLogico.Trim())
			Proc.Param("Valor1", E_ObjCriticaCondicion.Valor1.Trim())
			Proc.Param("Valor2", E_ObjCriticaCondicion.Valor2.Trim())
			Proc.Param("AndOr", E_ObjCriticaCondicion.AndOr.Trim())
			Proc.Param("LastTransactionUser", E_ObjCriticaCondicion.LastTransactionUser.Trim())
			Proc.Param("LastTransactionDate", E_ObjCriticaCondicion.LastTransactionDate)
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Delete(ByVal E_ObjCriticaCondicion As E_ClsTblCriticaCondicion) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblCriticaCondicion")

			Proc.Param("TipoQuery", "DELETE")
			Proc.Param("IdCritica", E_ObjCriticaCondicion.IdCritica)
			Proc.Param("Orden", E_ObjCriticaCondicion.Orden)
			Proc.Param("LastTransactionUser", E_ObjCriticaCondicion.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

End Class

