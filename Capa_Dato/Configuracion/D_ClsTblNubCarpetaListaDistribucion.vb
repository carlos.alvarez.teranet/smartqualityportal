Imports Capa_Entidad
Imports System.Data.SqlClient

Public Class D_ClsTblNubCarpetaListaDistribucion

	Public Function SelectRecord(ByVal E_ObjNubCarpetaListaDistribucion As E_ClsTblNubCarpetaListaDistribucion) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblNubCarpetaListaDistribucion")

			Proc.Param("TipoQuery", "SELECT")
			Proc.Param("IdCarpeta", E_ObjNubCarpetaListaDistribucion.IdCarpeta)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function SelectAll(ByVal IdCarpeta As Integer) As DataSet
		Try
			Dim Proc As New StoredProcedure("SP_TblNubCarpetaListaDistribucion")

			Proc.Param("TipoQuery", "SELECTALL")
			Proc.Param("IdCarpeta", IdCarpeta)
			Return Proc.ExecSP_ds()

		Catch ex As Exception
			Throw New Exception(ex.Message)
			Return Nothing
		Finally

		End Try
	End Function

	Public Function Insert(ByVal E_ObjNubCarpetaListaDistribucion As E_ClsTblNubCarpetaListaDistribucion) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblNubCarpetaListaDistribucion")

			Proc.Param("TipoQuery", "INSERT")
			Proc.Param("IdCarpeta", E_ObjNubCarpetaListaDistribucion.IdCarpeta)
			Proc.Param("IdListaDistribucion", E_ObjNubCarpetaListaDistribucion.IdListaDistribucion)
			Proc.Param("LastTransactionUser", E_ObjNubCarpetaListaDistribucion.LastTransactionUser.Trim())
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Update(ByVal E_ObjNubCarpetaListaDistribucion As E_ClsTblNubCarpetaListaDistribucion) As String
		Try
			Dim Proc As New StoredProcedure("_SP_TblNubCarpetaListaDistribucion")

			Proc.Param("TipoQuery", "UPDATE")
			Proc.Param("IdCarpeta", E_ObjNubCarpetaListaDistribucion.IdCarpeta)
			Proc.Param("IdListaDistribucion", E_ObjNubCarpetaListaDistribucion.IdListaDistribucion)
			Proc.Param("LastTransactionUser", E_ObjNubCarpetaListaDistribucion.LastTransactionUser.Trim())
			Proc.Param("LastTransactionDate", E_ObjNubCarpetaListaDistribucion.LastTransactionDate)
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

	Public Function Delete(ByVal E_ObjNubCarpetaListaDistribucion As E_ClsTblNubCarpetaListaDistribucion) As String
		Try
			Dim Proc As New StoredProcedure("SP_TblNubCarpetaListaDistribucion")

			Proc.Param("TipoQuery", "DELETE")
			Proc.Param("IdCarpeta", E_ObjNubCarpetaListaDistribucion.IdCarpeta)
			Proc.ExecSP()
			Return "OK"

		Catch ex As Exception
			Return ex.Message
			Throw New Exception(ex.Message)
		End Try
	End Function

End Class

