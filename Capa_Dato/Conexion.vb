﻿Imports System.Data.SqlClient
Public Class Conexion

#Region " Variables "

    Private mUsuario As String
    Private mPassword As String
    Private mConSSPI As Boolean = False
    Private mServidor As String
    Private mBaseDatos As String
    Private mSqlConn As SqlConnection
#End Region

#Region " Propiedades "

    Public Property Usuario() As String
        Get
            Return mUsuario
        End Get
        Set(ByVal Value As String)
            mUsuario = Value
        End Set
    End Property

    Public Property Password() As String
        Get
            Return mPassword
        End Get
        Set(ByVal Value As String)
            mPassword = Value
        End Set
    End Property

    Public Property ConSSPI() As Boolean
        Get
            Return mConSSPI
        End Get
        Set(ByVal Value As Boolean)
            mConSSPI = Value
        End Set
    End Property

    Public Property Servidor() As String
        Get
            Return mServidor
        End Get
        Set(ByVal Value As String)
            mServidor = Value
        End Set
    End Property

    Public Property BaseDatos() As String
        Get
            Return mBaseDatos
        End Get
        Set(ByVal Value As String)
            mBaseDatos = Value
        End Set
    End Property

    Public Property SQLConn() As SqlConnection
        Get
            Return mSqlConn
        End Get
        Set(ByVal Value As SqlConnection)
            mSqlConn = Value
        End Set
    End Property

#End Region

    'Crea el string de conexion a la base de datos.
    Private Function StrConexion() As String
        Try
            Dim strConn As String
            strConn = "Server=" & Servidor & "; " & "DataBase=" & BaseDatos & "; "
            If Not ConSSPI Then
                strConn &= "user id=" & Usuario & ";password=" & Password
            Else
                strConn &= "Integrated Security=SSPI"
            End If

            Return strConn
        Catch ex As Exception
            Throw ex
        End Try
    End Function

    Public Sub New()
        Try
            'WEBCONFIG
            Me.Servidor = System.Configuration.ConfigurationManager.AppSettings("BD_Servidor")
            Me.BaseDatos = System.Configuration.ConfigurationManager.AppSettings("BD_Nombre")
            Me.Usuario = System.Configuration.ConfigurationManager.AppSettings("BD_Usuario")
            Me.Password = System.Configuration.ConfigurationManager.AppSettings("BD_Password")
            SQLConn = New SqlConnection(Me.StrConexion)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub
End Class

Public Class StoredProcedure

#Region " Variables "
    Private mNombreProcedimiento As String
    Private mParametros As Collection
#End Region

#Region " Propiedades "
    Public Property Nombre() As String
        Get
            Return mNombreProcedimiento
        End Get
        Set(ByVal Value As String)
            mNombreProcedimiento = Value
        End Set
    End Property

    Public Property Parametros() As Collection
        Get
            Return mParametros
        End Get
        Set(ByVal Value As Collection)
            mParametros = Value
        End Set
    End Property
#End Region

#Region " Constructor "

    'Solo recibe el nombre del procedimiento e inicializa la colección.
    Public Sub New(ByVal nNombre As String)
        Try
            Nombre = nNombre
            Parametros = New Collection
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

#End Region

#Region " Propiedades y fn_generales "
    Public Overloads Sub Param(ByVal pVariable As String, ByVal pValor As Object)
        Try
            Dim iParametro As New clsParameter("@" & pVariable, pValor, ParameterDirection.Input)
            Me.Parametros.Add(iParametro)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    'Agrega los parametros del procedimiento y su respectivo valor.
    Public Overloads Sub Param(ByVal pVariable As String, ByRef pValor As Object, ByVal pDireccion As ParameterDirection)
        Try
            Dim iParametro As New clsParameter("@" & pVariable, pValor, pDireccion)
            Me.Parametros.Add(iParametro)
        Catch ex As Exception
            Throw ex
        End Try
    End Sub

    Public Function ParamOut(ByVal pVariable As String) As String
        Dim MiValor As String = ""
        Dim mParametro As clsParameter
        For Each mParametro In Me.Parametros
            If mParametro.Direccion = ParameterDirection.ReturnValue Or mParametro.Direccion = ParameterDirection.InputOutput Or mParametro.Direccion = ParameterDirection.Output Then
                If mParametro.Variable = "@" & pVariable Then
                    Try
                        MiValor = mParametro.Valor
                    Catch ex As Exception
                        MiValor = ""
                    End Try
                    Exit For
                End If
            End If
        Next
        Return MiValor
    End Function


    'Ejecuta el procedimiento almacenado.
    Public Function ExecSP_ds() As DataSet
        'Try
        Dim Conn As New Conexion

        Dim sqlCmd As New SqlCommand(Me.Nombre, Conn.SQLConn)
        sqlCmd.CommandType = CommandType.StoredProcedure
        sqlCmd.CommandTimeout = 0
        Dim MisParametrosSon As String = ""
        Dim mParametro As clsParameter
        'Agrega las variables al procedimiento almacenado
        For Each mParametro In Me.Parametros

            Dim pParam As New SqlParameter(mParametro.Variable, mParametro.GetTypeProperty)
            pParam.Direction = mParametro.Direccion  ' ParameterDirection.Input
            If mParametro.GetTypeProperty <> SqlDbType.Image And mParametro.GetTypeProperty <> SqlDbType.Bit Then
                If IsNumeric(mParametro.Valor) And Mid(mParametro.Valor, 1, 1) <> "0" And mParametro.Variable <> "@Ip" Then
                    If mParametro.Valor.ToString.Contains(",") Then ' es con decimales
                        mParametro.Valor = CDbl(mParametro.Valor)
                    Else
                        mParametro.Valor = CLng(mParametro.Valor) ' es Un Entero
                    End If
                Else
                    If IsDate(mParametro.Valor) And mParametro.GetTypeProperty = SqlDbType.VarChar Then
                        If mParametro.Valor.ToString.Length > 7 Then
                            mParametro.Valor = mParametro.Valor.ToString.Substring(6, 4) & mParametro.Valor.ToString.Substring(3, 2) & mParametro.Valor.ToString.Substring(0, 2)
                        End If
                    End If
                End If
            End If
            pParam.Value = mParametro.Valor
            Dim Largo As Integer = 300
            If mParametro.GetTypeProperty <> SqlDbType.Image And mParametro.GetTypeProperty <> SqlDbType.Bit Then
                If mParametro.Valor <> Nothing Then
                    If mParametro.Valor.ToString.Length > 0 Then
                        Largo = mParametro.Valor.ToString.Length
                    End If
                End If
                pParam.Size = Largo
            End If
            'MisParametrosSon = MisParametrosSon & "'" & mParametro.Valor & "',"

            sqlCmd.Parameters.Add(pParam)
        Next
        'SqlAdapter utiliza el SqlCommand para llenar el Dataset
        Dim sda As New SqlDataAdapter(sqlCmd)
        'Se llena el dataset
        Dim ds As New DataSet
        sda.Fill(ds)

        For Each mParametro In Me.Parametros
            If mParametro.Direccion = ParameterDirection.ReturnValue Or mParametro.Direccion = ParameterDirection.InputOutput Or mParametro.Direccion = ParameterDirection.Output Then
                mParametro.Valor = sqlCmd.Parameters(mParametro.Variable).Value
            End If
        Next


        Conn.SQLConn.Close()
        Conn.SQLConn.Dispose()
        Return ds

    End Function

    Public Sub ExecSP()
        'Try
        Dim Conn As New Conexion
        Dim sqlCmd As New SqlCommand(Me.Nombre, Conn.SQLConn)
        sqlCmd.CommandType = CommandType.StoredProcedure
        sqlCmd.CommandTimeout = 0

        Dim MisParametrosSon As String = ""
        Dim mParametro As clsParameter
        'Agrega las variables al procedimiento almacenado
        For Each mParametro In Me.Parametros
            Dim pParam As New SqlParameter(mParametro.Variable, mParametro.GetTypeProperty)
            pParam.Direction = mParametro.Direccion  ' ParameterDirection.Input


            If mParametro.GetTypeProperty <> SqlDbType.Image And mParametro.GetTypeProperty <> SqlDbType.Bit Then
                If IsNumeric(mParametro.Valor) And Mid(mParametro.Valor, 1, 1) <> "0" And (mParametro.Variable <> "@IP") Then
                    If mParametro.Valor.ToString.Contains(",") Then ' es con decimales
                        mParametro.Valor = CDbl(mParametro.Valor)
                    Else
                        'If mParametro.Valor.ToString.Length >= 10 Then
                        '    mParametro.Valor = mParametro.Valor.ToString
                        'Else
                        Try
                            mParametro.Valor = CLng(mParametro.Valor) ' es Un Entero
                        Catch ex As Exception
                            mParametro.Valor = Trim(mParametro.Valor) ' es Un Entero
                        End Try
                        'End If
                    End If
                Else
                    If IsDate(mParametro.Valor) And mParametro.GetTypeProperty = SqlDbType.VarChar Then
                        If mParametro.Valor.ToString.Length > 7 Then
                            If mParametro.Valor.ToString.Length <= 10 Then 'Es Fecha Sin Horas
                                mParametro.Valor = mParametro.Valor.ToString.Substring(6, 4) & mParametro.Valor.ToString.Substring(3, 2) & mParametro.Valor.ToString.Substring(0, 2)
                            Else
                                'Es Fecha Con Horas

                                '" 2:54:"
                                Dim Hora As String = mParametro.Valor.ToString.Substring(11, 5)
                                If IsNumeric(Hora.ToString.Substring(0, 2)) = False Then
                                    Hora = "0" & Hora.ToString.Substring(0, 4)
                                End If
                                mParametro.Valor = mParametro.Valor.ToString.Substring(6, 4) & mParametro.Valor.ToString.Substring(3, 2) & mParametro.Valor.ToString.Substring(0, 2) & " " & Hora
                                ' mParametro.Valor = mParametro.Valor.ToString.Substring(6, 4) & mParametro.Valor.ToString.Substring(3, 2) & mParametro.Valor.ToString.Substring(0, 2) & mParametro.Valor.ToString.Substring(10, 6)
                            End If
                        End If
                    End If
                End If
                MisParametrosSon = MisParametrosSon & "'" & mParametro.Valor & "',"
            End If
            pParam.Value = mParametro.Valor

            Dim Largo As Integer = 300

            If mParametro.GetTypeProperty <> SqlDbType.Image And mParametro.GetTypeProperty <> SqlDbType.Bit Then
                If mParametro.Valor <> Nothing Then
                    If mParametro.Valor.ToString.Length > 0 Then
                        Largo = mParametro.Valor.ToString.Length
                    End If
                End If
                pParam.Size = Largo
            End If
            sqlCmd.Parameters.Add(pParam)
        Next


        Conn.SQLConn.Open()
        sqlCmd.ExecuteNonQuery()

        For Each mParametro In Me.Parametros
            If mParametro.Direccion = ParameterDirection.ReturnValue Or mParametro.Direccion = ParameterDirection.InputOutput Or mParametro.Direccion = ParameterDirection.Output Then
                mParametro.Valor = sqlCmd.Parameters(mParametro.Variable).Value
            End If
        Next

        Conn.SQLConn.Close()
        Conn.SQLConn.Dispose()
    End Sub
#End Region

End Class

Public Class clsParameter

    Private mVariable As String
    Private mValor As Object
    Private mDireccion As ParameterDirection = ParameterDirection.Input ' pParam.Direction = ParameterDirection.Input


    'Nombre de la variable, debe ser igual a la declarada en el procedimiento almacenado
    Public Property Variable() As String
        Get
            Return mVariable
        End Get
        Set(ByVal Value As String)
            mVariable = Value
        End Set
    End Property

    'Valor de la variable, puede ser de cualquier tipo de dato. preferible que 
    'coincida con las variables declaradas en GetTypeProperty
    Public Property Valor()
        Get
            Return mValor
        End Get
        Set(ByVal Value)
            mValor = Value
        End Set
    End Property

    Public Property Direccion() As ParameterDirection
        Get
            Return mDireccion
        End Get
        Set(ByVal Value As ParameterDirection)
            mDireccion = Value
        End Set
    End Property


    'Se definen los posibles tipos de datos que se le van a enviar al procedimiento almacenado
    'Esta lista podria aumentar conforme se usen otro tipo de variable.
    Public ReadOnly Property GetTypeProperty() As SqlDbType
        Get
            Try

                If mValor.GetType.FullName = "System.String" Then
                    Return SqlDbType.VarChar
                ElseIf mValor.GetType.FullName = "System.Int16" Then
                    Return SqlDbType.Int
                ElseIf mValor.GetType.FullName = "System.Int32" Then
                    Return SqlDbType.Int
                ElseIf mValor.GetType.FullName = "System.Int64" Then
                    Return SqlDbType.Int
                ElseIf mValor.GetType.FullName = "System.Decimal" Then
                    Return SqlDbType.Decimal
                ElseIf mValor.GetType.FullName = "System.Double" Then
                    Return SqlDbType.BigInt
                ElseIf mValor.GetType.FullName = "System.DateTime" Then
                    Return SqlDbType.DateTime
                ElseIf mValor.GetType.FullName = "System.Byte" Then
                    Return SqlDbType.Image
                ElseIf mValor.GetType.FullName = "System.Boolean" Then
                    Return SqlDbType.Bit
                ElseIf mValor.GetType.FullName = "System.Byte[]" Then
                    Return SqlDbType.Image
                Else
                    Return SqlDbType.VarChar 'Esta linea la agregue para que no aparesca la alerta de que no se esta devolviendo algo en la funcion
                End If
            Catch ex As Exception
                Return SqlDbType.VarChar
            End Try
        End Get
    End Property

    'Procedimiento de creacion de la variable.
    Public Sub New(ByVal pVariable As String, ByRef pValor As Object, ByVal pDireccion As ParameterDirection)
        Try
            Me.Variable = pVariable
            Me.Valor = pValor
            Me.Direccion = pDireccion
        Catch ex As Exception
            Throw New Exception("Error en la creacion del Parametro" & vbCrLf & ex.Message)
        End Try
    End Sub

End Class
