﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/GlbMaster.Master" CodeBehind="FrmEjecucion.aspx.vb" Inherits="Capa_Presentacion.FrmEjecucion" %>
<%@ Register Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../assets/js/jsUpdateProgress.js"></script>

    <!-- Font Lato + css -->
    <link href="../assets/css/lato.css?v=v1" rel="stylesheet" />
    <link href="../assets/css/complementos.css?v=v1.2" rel="stylesheet" />
    
    <link href="../assets/css/FrmCriticas.css?v=v4" rel="stylesheet" />
    <link href="../assets/css/bootstrap5-toggle.min.css" rel="stylesheet">


    <script type="text/javascript">
        var ModalProgress = '<%= ModalProgress.ClientID %>';
    </script>  

    <asp:Panel ID="PnProgress" runat="server">
        <asp:UpdateProgress ID="UpProgress" runat="server">
            <ProgressTemplate>
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center text-primary">
                                <img src='../assets/images/loadingLogin.gif' style='max-width:50px;'>
                            </div>
                        </div>
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </asp:Panel>
    <asp:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="PnProgress" BackgroundCssClass="TeraModalBackground" PopupControlID="PnProgress"  />
    

    <!-- MOSTRAR DATOS EN EL GRIDVIEW -->
    <asp:UpdatePanel runat="server" ID="UpdatePanelIndex">
        <ContentTemplate>
            <asp:Panel runat="server" ID="PanelIndex" CssClass="center-block">
                <div class="container-fluid">
                    <br />
                    <div class="row shadow-sm rounded">
                        <div class="col-11">                       
                            <h2 style="color:#26BD75; font-family:'Lato'; font-weight:900 !important;">Programación de  <span class="badge text-bg-warning">EJECUCIÓN</span></h2>
                        </div>
                        <div class="col-1">
                            <span class="float-end">
                                <asp:LinkButton 
                                    ID="BtnAddRegistro" 
                                    runat="server" 
                                    visible="true"
                                    CssClass="btn btn-success btn-addButton" 
                                    OnClick="BtnAddRegistro_Click">
                                       <i class="fa-solid fa-plus"></i>
                                </asp:LinkButton>
                            </span>
                        </div>
                        
                    </div>
                    

                    <div class="row">
                        

                            <div class="col-12 col-md-12">
                                <asp:TextBox ID="FrmPpal_TxtIdCarpeta"      Visible="false" runat="server" />
                                
                                <div class="row " runat="server" ID="BtnFilter_row" visible="true">
                                    <div class="col-6">
                                        <div class="input-group mb-3">
                                            <label class="input-group-text border-secondary text-white bg-dark" for="TxtBsc"><i class="fa-solid fa-magnifying-glass"></i>&nbsp;Buscar&nbsp;&nbsp;</label>
                                            <asp:TextBox ID="TxtBsc" CssClass="form-control border-secondary form-control-sm"
                                                placeholder="para buscar valores múltiples utilice el separador ;" style="font-size: 1.1rem !important;"
                                                onkeypress="if ((event || window.event).keyCode == 13) { var node = (event || window.event).target || (event || window.event).srcElement; if (node.type == 'text' && node.id == 'ContentPlaceHolder1_TxtBsc') { return false; } }"
                                                autocomplete="off" runat="server"></asp:TextBox>
                                            <asp:LinkButton ID="BtnBsc" CssClass="btn btn-dark"  OnClick="BtnBsc_Click" runat="server">
                                                &nbsp;&nbsp;<i class="fa-brands fa-golang" style="height:1.8em !important; vertical-align: inherit !important;"></i>&nbsp;&nbsp;
                                            </asp:LinkButton>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <div class="gradientLineKantar" style="float:left;"></div>
                                        <asp:GridView 
                                            ID="GrillaPpal" 
                                            runat="server" 
                                            CssClass="TeraGridview table table-bordered bg-light"
                                            Style="text-align:center; vertical-align:middle; font-size:1rem; max-width:1600px; margin-left: 0px; width:98% !important;"
                                                                    
                                            PageSize="50" AllowPaging="true" AutoGenerateColumns="false" 
                                            HorizontalAlign="Center" 
                                            AllowSorting="true"
                                            OnRowDataBound="GrillaPpal_RowDataBound">
                                            <HeaderStyle 
                                                CssClass="bg-dark text-center text-white" 
                                                Font-Size="Medium"  Font-Bold="false" />                                                             
                            
                                            <Columns>                                    
                                                <%-- 0 --%>
                                                <asp:TemplateField HeaderText="" Visible="false" ControlStyle-Font-Size ="Small" >
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblIdEjecucion" runat="server" Text='<%# Eval("IdEjecucion") %>'></asp:Label>
                                                                        
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%-- 1 --%>
                                                <asp:TemplateField HeaderText="<span class='link-light'></span>">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblNroFila" runat="server" Text='<%# Eval("NroFila") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                 <%-- 2 --%>
                                                <asp:TemplateField HeaderText="<span class='link-light'>Titulo</span>">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblTitulo" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                 <%-- 3 --%>
                                                <asp:TemplateField HeaderText="<span class='link-light'>Creación</span>" ItemStyle-Width="50">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblFechaCreacion" runat="server" Text='<%# Eval("FechaCreacion") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%-- 4 --%>
                                                <asp:TemplateField HeaderText="<span class='link-light'>Owner</span>">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblOwner" runat="server" Text='<%# Eval("Owner") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%-- 5 --%>
                                                <asp:TemplateField HeaderText="<span class='link-light'>Programado para </span>"  ItemStyle-Width="50"> 
                                                    <ItemTemplate>
                                                       <asp:Label ID="LblFechaHoraProgramada" runat="server" Text='<%# Eval("FechaHoraProgramada") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                 <%-- 6 --%>
                                                <asp:TemplateField HeaderText="<span class='link-light'>Estado</span>" > 
                                                    <ItemTemplate>
                                                       <asp:Label ID="LblEstado" runat="server" Text='<%# Eval("Estado") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                 <%-- 7 --%>
                                                <asp:TemplateField HeaderText="<span class='link-light'>Inicio</span>" > 
                                                    <ItemTemplate>
                                                       <asp:Label ID="LblInicio" runat="server" Text='<%# Eval("FechaHoraReal") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%-- 8 --%>
                                                <asp:TemplateField HeaderText="<span class='link-light'>Termino</span>" > 
                                                    <ItemTemplate>

                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                 <%-- 9 --%>
                                                <asp:TemplateField HeaderText="<span class='link-light'>Duración</span>" > 
                                                    <ItemTemplate>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                


                                                <%-- 10 --%>
                                                <asp:TemplateField HeaderText="<i class='fa-solid fa-pen-to-square'></i>" >
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" OnClick ="GrillaPpal_BtnEditar_Click" CssClass="btn btn-sm btn-warning"><i class='fa-solid fa-pen-to-square'></i></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>

                                            <PagerTemplate>                                    
                                                <nav style="float:right;">
                                                    <div class="btn-group" role="group">
                                                        <asp:LinkButton ID="GrillaPpal_BtnFirst"    runat="server" CssClass="btn btn-outline-light btn-sm" Text="Primera Página"      ToolTip="Primera Página"    OnClick="GrillaPpal_BtnFirst_Click"><i class="fa-solid fa-backward-fast"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="GrillaPpal_BtnPrev"     runat="server" CssClass="btn btn-outline-light btn-sm" Text="Página Anterior"     ToolTip="Página Anterior"   OnClick="GrillaPpal_BtnPrev_Click"><i class="fa-solid fa-arrow-left"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="GrillaPpal_BtnNext"     runat="server" CssClass="btn btn-outline-light btn-sm" Text="Página Siguiente"    ToolTip="Página Siguiente"  OnClick="GrillaPpal_BtnNext_Click"><i class="fa-solid fa-arrow-right"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="GrillaPpal_BtnLast"     runat="server" CssClass="btn btn-outline-light btn-sm" Text="Última Página"       ToolTip="Última Página"     OnClick="GrillaPpal_BtnLast_Click"><i class="fa-solid fa-forward-fast"></i></asp:LinkButton>
                                                    </div>
                                                    <div class="justify-content-center text-white" style="font-size:0.8rem">
                                                        <asp:Label ID="GrillaPpal_CurrentPageLabel" runat="server" />                            
                                                    </div>
                                                </nav>
                                            </PagerTemplate>


                                            
                                            <EmptyDataTemplate>
                                                <div class="alert alert-warning align-items-center" role="alert">
                                                    <h1><i class="fa-solid fa-triangle-exclamation"></i></h1>
                                                    <div>No existen registros para mostrar.</div>
                                                </div>
                                            </EmptyDataTemplate>                                
                                        </asp:GridView>
                                    </div>
                                </div>
                                       
                            </div>
                        </div>
                    </div> 

                    <br />
                    
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
<!-- FIN MOSTRAR DATOS EN EL GRIDVIEW -->


<!-- FORMULARIO -->
<asp:Button ID="FrmPpal_BtnVer" runat="server" Style="display:none" />
<asp:ModalPopupExtender ID="FrmPpal" runat="server" TargetControlID="FrmPpal_BtnVer" PopupControlID="FrmPpal_Panel" BackgroundCssClass="TeraModalBackground"></asp:ModalPopupExtender>
<asp:Panel ID="FrmPpal_Panel" runat="server">
    <asp:UpdatePanel ID="FrmPpal_UpdatePanel" runat="server">
        <ContentTemplate>
            <div class="modal" id="modal-FrmPrincipal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div id="FrmPpal_dialog" class=" modal-dialog modal-lg  modal-dialog-scrollable" style="display:none;" runat="server">
                    <div class="modal-content">
                        <div class="modal-header text-center bg-dark text-white">
                             <div class="container-fluid ">
                                <div class="row" style ="width :100%">
                                    <div class="col-12 text-center">
                                        <h4 class="modal-title fs-5" ID="FrmPpalTitulo" runat="server"></h4>
                                    </div>
                                </div> 
                            </div>
                            

                            
                        </div>
                
                        <div class="modal-body">
                            <asp:TextBox ID="FrmPpal_TxtIdEjecucion" style="display:none;" runat="server" />

                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-12">
                                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link active" runat="server" ID="Ffrm_principal_TabOpc1" data-bs-toggle="tab" data-bs-target="#ContentPlaceHolder1_CriticaPanelTab" type="button" role="tab" aria-controls="ContentPlaceHolder1_CriticaPanelTab" aria-selected="true">Ejecución</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" runat="server" ID="Ffrm_principal_TabOpc2"  data-bs-toggle="tab" data-bs-target="#ContentPlaceHolder1_comentarioPanelTab" type="button" role="tab" aria-controls="ContentPlaceHolder1_comentarioPanelTab" aria-selected="false">Comentarios</button>
                                            </li>
                                            <li class="nav-item" role="presentation">
                                                <button class="nav-link" runat="server" ID="Ffrm_principal_TabOpc3" data-bs-toggle="tab" data-bs-target="#ContentPlaceHolder1_LogPanelTab" type="button" role="tab" aria-controls="ContentPlaceHolder1_LogPanelTab" aria-selected="false">Log</button>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" runat="server" id="CriticaPanelTab" role="tabpanel" aria-labelledby="Crítica-tab">
                                            <div class ="container-fluid mt-3" style="min-height:400px;">
                                                <!--div class="row mt-2" style="border:1px solid #535362; background-color:beige; padding:10px">
                                                    <div class="col-6">
                                                        <strong>Autor:</strong> 
                                                    </div>

                                                    <div class="col-6">
                                                        <strong>Fecha Creación:</strong> 
                                                    </div>
                                                
                                                    <div class="col-6 mt-2">
                                                        <strong>Estado:</strong> 
                                                    </div>

                                                    <div class="col-6 mt-2">
                                                        <button class="btn btn-success btn-sm">
                                                            Ver Log de Ejecución
                                                        </button>      
                                                        <button class="btn btn-success btn-sm">
                                                            Ver Mail Distribución
                                                        </button>      
                                                    </div>
                                                </!-div-->
                                                <div class="row mt-2">
                                                    <div class="col-7">
                                                        <label class="" for="FrmPpal_TxtTituloEjecucion" style="color:#000 !important; font-weight:bold">
                                                            Titulo
                                                        </label>
                                                        <asp:TextBox ID="FrmPpal_TxtTituloEjecucion" class="form-control" MaxLength="30" autocomplete="off" runat="server" />
                                                        <div ID="FrmPpal_TxtTituloEjecucion_Msj" class="" runat="server"></div>
                                                    </div>

                                                    <div class="col-5">
                                                        <label class="" for="FrmPpal_CboTipoEjecucion" style="color:#000 !important;font-weight:bold">
                                                            Tipo Ejecución
                                                        </label>
                                        
                                                        <asp:DropDownList 
                                                            CssClass=" form-select" 
                                                            ID="FrmPpal_CboTipoEjecucion"  
                                                            runat="server" 
                                                             AutoPostBack="True" 
                                                             onselectedindexchanged="getDetailTipoEjecucion_change"
                                                            >
                                                            <asp:ListItem Text="<< Seleccionar >>" Value="0" />
                                                            <asp:ListItem Text="Inmediata" Value="1" />
                                                            <asp:ListItem Text="Programada" Value="2" />

                                                        </asp:DropDownList>
                                                    </div>
                                                </div>
                                                
                                                <div class="row mt-3">
                                                    <div class="col-7">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <table>
                                                                    <tr runat="server" id="FrmPpal_CellDistribuirFechaHora">
                                                                        <td><div style="font-weight:bold">Fecha y hora Planificada</div></td>
                                                                        <td>
                                                                            <asp:TextBox ID="FrmPpal_txtFechaHoraPlanificacion" CssClass="form-control input-sm" TextMode="DateTimeLocal" MaxLength="16" runat="server" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr><td height="20"></td></tr>
                                                                    <tr runat="server" id="FrmPpal_CellDistribuirResultado">
                                                                        <td><div style="font-weight:bold">Distribuir los resultados por Mail</div></td>
                                                                        <td valign="top">
                                                                            <div class="form-check form-switch">
                                                                                <input runat="server" class="form-check-input FrmPpal_RdDistribucionResultadoMail" type="checkbox" role="switch" id="FrmPpal_RdDistribucionResultadoMail" >
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                
                                                            </div>
                                                        </div>
                                                        <div class="row mt-2 div-ListaDistribucion" style="display:<%=ViewState("ListaDistribucion") %>" id="divListaDistribucion">
                                                            <div class="col-12">
                                                                <hr  class="hr"/>
                                                                <label class="" for="FrmPpal_LstListaDistribucion" style="color:#000 !important;font-weight:bold">
                                                                    Lista Distribución
                                                                </label>
                                                                
                                                                <input id="searchListaDistribucion" class=" form-control" placeholder="Filtrar Lista de distribuciones" />
                                                                
                                                            
                                                                <div class="form-check" style="max-height:200px;overflow-y: auto; padding-left:20px;">
                                                                    <asp:CheckBoxList ID="FrmPpal_LstListaDistribucion" CssClass="checkbox LstBoxListaDistribucion  " RepeatDirection="Vertical" RepeatColumns="1" runat="server"></asp:CheckBoxList>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>

                                                    <div class="col-5">
                                                        <label class="" for="FrmPpal_ChkCarpetaCriticas" style="color:#000 !important;font-weight:bold">
                                                            Criticas a ejecutar
                                                        </label> 
                                                        <div class="form-check" style="max-height:200px;overflow-y: auto; padding-left:10px;">
                                                            <asp:CheckBoxList ID="FrmPpal_ChkCarpetaCriticas" CssClass="checkbox LstBox  " RepeatDirection="Vertical" RepeatColumns="1" runat="server"></asp:CheckBoxList>
                                                        </div>

                                                    </div>

                                                    
                                                </div>


                                                <div class="row mt-1">
                                                    <div class="col-12">
                                                        <div id="FrmPpal_DivUltAct" style="font-size:0.8rem !important; color:#8fc6ab !important; text-align:right !important" runat="server"></div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="tab-pane fade" runat="server" id="comentarioPanelTab" role="tabpanel" aria-labelledby="comentario-tab">
                                            <div class="row mt-2">
                                                <div class="col-12">
                                                    <div class="float-end" style="margin-right:30px">
                                                        <asp:LinkButton CssClass="btn btn-success btn-sm" runat="server" OnClick="FrmPpal_Comentario_click">
                                                            <i class="fa-solid fa-comment"></i>
                                                            add comentario
                                                        </asp:LinkButton>
                                                    </div>
                                                    <h6>Comentarios</h6>

                                                    <div class="row mt-4"  style="min-height:120px; ">
                                                        <%=ViewState("Comentarios") %>
                                                    </div>
                                                </div>

                                            </div>
                            
                                        </div>
                                    
                                        <div class="tab-pane fade" runat="server" id="LogPanelTab" role="tabpanel" aria-labelledby="Log-tab">
                                            <div class ="container-fluid" style="min-height:400px;">
                                                <div class="row mt-2">
                                                    <div class="col-8 text-center" style="margin:auto">
                                                         <%=ViewState("Logs") %>
                                                        LOG
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>




                                
                                </div>

                           
                            </div>
                        </div>
                        <div class="modal-footer bg-light"  style="border-top:2px solid #535362 !important;">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-6">
                                        <asp:LinkButton ID="FrmPpal_BtnRepetirEjecucion" OnClick="FrmPpal_BtnRepetirEjecucion_Click" runat="server" CssClass="btn btn-success" ToolTip="Almacenar"><i class="fa-solid fa-calendar-days"></i>&nbsp;&nbsp;&nbsp;Repetir ejecuciones</asp:LinkButton>
                                        &nbsp;&nbsp;&nbsp;
                                    </div>
                                    <div class="col-6 text-end">

                                        <asp:LinkButton ID="FrmPpal_BtnCerrar" runat="server" CssClass="btn btn-secondary" ToolTip="Cerrar" OnClick="FrmPpal_BtnCerrar_Click"><i class="fa-regular fa-share-from-square"></i>&nbsp;&nbsp;&nbsp;Cerrar</asp:LinkButton>
                                        &nbsp;&nbsp;&nbsp;
                                        <asp:LinkButton ID="FrmPpal_BtnAlmacenar" OnClick="FrmPpal_BtnAlmacenar_Click" runat="server" CssClass="btn btn-primary" ToolTip="Almacenar"><i class="fa-regular fa-save"></i>&nbsp;&nbsp;&nbsp;Crear</asp:LinkButton>
                                        &nbsp;&nbsp;&nbsp;
                                       
                       
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<!-- FIN FORMULARIO -->


<!-- FORMULARIO COMENTARIO -->
<asp:Button ID="FrmComentarios_BtnVer" runat="server" Style="display:none" />
<asp:ModalPopupExtender ID="FrmComentarios" runat="server" TargetControlID="FrmComentarios_BtnVer" PopupControlID="FrmComentarios_Panel" BackgroundCssClass="TeraModalBackground"></asp:ModalPopupExtender>
<asp:Panel ID="FrmComentarios_Panel" runat="server">
    <asp:UpdatePanel ID="FrmComentarios_UpdatePanel" runat="server">
        <ContentTemplate>
            <div class="modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div id="FrmComentarios_dialog" class="modal-dialog modal-dialog-centered" style="display:none;" runat="server">
                    <div class="modal-content">
                        <div class="modal-header bg-success text-white">
                            <h4 class="modal-title fs-5">Comentar</h4>
                        </div>                
                        <div class="modal-body">
                                    
                            <asp:TextBox style="width:100%; height:143px;" runat="server" TextMode="MultiLine"  ID="FrmlPpal_traspasaComentario"></asp:TextBox>
                                    
                            
                        </div> 
                
                        <div class="modal-footer bg-light" style="border-top:1px solid #535362 !important;">
                            <asp:LinkButton ID="FrmComentarios_BtnCerrar"    runat="server" CssClass="btn btn-secondary btn-sm" ToolTip="Cerrar"        OnClick="FrmComentarios_BtnCerrar_Click"><i class="fa-regular fa-share-from-square"></i>&nbsp;&nbsp;&nbsp;Cerrar</asp:LinkButton>
                            &nbsp;&nbsp;&nbsp;
                            <asp:LinkButton ID="FrmComentarios_BtnComentar"    runat="server" CssClass="btn btn-success btn-sm"   ToolTip="Comentar"    OnClick="FrmComentarios_BtnComentar_Click"><i class='fa-solid fa-pen-to-square'></i>&nbsp;&nbsp;&nbsp;Comentar</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<!-- FIN FORMULARIO -->


<!-- FORMULARIO Planificacion -->
<asp:Button ID="FrmCalendarioEjecucion_BtnVer" runat="server" Style="display:none" />
<asp:ModalPopupExtender ID="FrmCalendarioEjecucion" runat="server" TargetControlID="FrmCalendarioEjecucion_BtnVer" PopupControlID="FrmCalendarioEjecucion_Panel" BackgroundCssClass="TeraModalBackground"></asp:ModalPopupExtender>
<asp:Panel ID="FrmCalendarioEjecucion_Panel" runat="server">
    <asp:UpdatePanel ID="FrmCalendarioEjecucion_UpdatePanel" runat="server">
        <ContentTemplate>
            <div class="modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div id="FrmCalendarioEjecucion_dialog" class="modal-dialog modal-lg modal-dialog-centered" style="display:none;" runat="server">
                    <div class="modal-content">
                        <div class="modal-header bg-success text-white">
                            <h4 class="modal-title fs-5">Planificación de ejecución</h4>
                        </div>                
                        <div class="modal-body">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="accordion" id="accordionExample">
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingUnaVez">
      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseUnaVez" aria-expanded="true" aria-controls="collapseUnaVez">
        Una vez
      </button>
    </h2>
    <div id="collapseUnaVez" class="accordion-collapse collapse show" aria-labelledby="headingUnaVez" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        
          <div class="container">
              <div class="row">
                  <div class="col-5" style="height:200px; margin:auto;">
                    <b>Inicio:</b>
                    <asp:TextBox ID="FrmPpal_TxtUnaVezFecha" CssClass="form-control input-sm" TextMode="DateTimeLocal" MaxLength="16" runat="server" />

                  </div>
              </div>
          </div>
      
      </div>
    </div>
  </div>

  <div class="accordion-item">
    <h2 class="accordion-header" id="headingDiariamente">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseDiariamente" aria-expanded="false" aria-controls="collapseDiariamente">
        Diariamente
      </button>
    </h2>
    <div id="collapseDiariamente" class="accordion-collapse collapse" aria-labelledby="headingDiariamente" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        <div class="container">
              <div class="row">
                  <div class="col-5" style="height:200px; margin:auto;">
                    <b>Inicio:</b>
                    <asp:TextBox ID="FrmPpal_TxtDiariamenteFecha" CssClass="form-control input-sm" TextMode="DateTimeLocal" MaxLength="16" runat="server" />

                  </div>
              </div>
          </div>
      </div>
    </div>
  </div>
  
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingSemanalmente">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseSemanalmente" aria-expanded="false" aria-controls="collapseSemanalmente">
        Semanalmente
      </button>
    </h2>
    <div id="collapseSemanalmente" class="accordion-collapse collapse" aria-labelledby="headingSemanalmente" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        <strong>This is the third item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
      </div>
    </div>
  </div>


  <div class="accordion-item">
    <h2 class="accordion-header" id="headingMensualmente">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseMensualmente" aria-expanded="false" aria-controls="collapseMensualmente">
        Mensualmente
      </button>
    </h2>
    <div id="collapseMensualmente" class="accordion-collapse collapse" aria-labelledby="headingMensualmente" data-bs-parent="#accordionExample">
      <div class="accordion-body">
        <strong>This is the third item's accordion body.</strong> It is hidden by default, until the collapse plugin adds the appropriate classes that we use to style each element. These classes control the overall appearance, as well as the showing and hiding via CSS transitions. You can modify any of this with custom CSS or overriding our default variables. It's also worth noting that just about any HTML can go within the <code>.accordion-body</code>, though the transition does limit overflow.
      </div>
    </div>
  </div>


</div>
                                        
                                    </div>
                                </div>
                            </div>
                                    
                                    
                            
                        </div> 
                
                        <div class="modal-footer bg-light" style="border-top:1px solid #535362 !important;">
                            <asp:LinkButton ID="FrmCalendarioEjecucion_BtnCerrar"    runat="server" CssClass="btn btn-secondary btn-sm" ToolTip="Cerrar"        OnClick="FrmCalendarioEjecucion_BtnCerrar_Click"><i class="fa-regular fa-share-from-square"></i>&nbsp;&nbsp;&nbsp;Cerrar</asp:LinkButton>
                            &nbsp;&nbsp;&nbsp;
                            <asp:LinkButton ID="FrmCalendarioEjecucion_BtnComentar"    runat="server" CssClass="btn btn-success btn-sm"   ToolTip="Comentar" OnClick="FrmCalendarioEjecucion_BtnComentar_Click"><i class='fa-solid fa-pen-to-square'></i>&nbsp;&nbsp;&nbsp;Comentar</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<!-- FIN Planificacion -->


<!-- MENSAJES -->
<asp:Button ID="FrmMsg_BtnVer" runat="server" Text="" style="display:none;" />
<asp:ModalPopupExtender ID="FrmMsg" runat="server" TargetControlID="FrmMsg_BtnVer" PopupControlID="FrmMsg_UpdatePanel" BackgroundCssClass="TeraModalBackground"></asp:ModalPopupExtender>
<asp:Panel ID="FrmMsg_Panel" runat="server">
    <asp:UpdatePanel ID="FrmMsg_UpdatePanel" runat="server">
        <ContentTemplate>
            <div class="modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div id="FrmMsg_dialog" class="modal-dialog modal-lg modal-dialog-centered" style="display:none;" runat="server">
                    <div class="modal-content">
                        <div id="FrmMsg_TipoMensaje" class="" style="color:#FFF !important" runat="server">                            
                            <h4 class="modal-title fs-5" style="color:#FFF !important;">
                                    <i ID="FrmMsg_TipoMensajeIcono" class="" runat="server"></i>&nbsp;
                                    <asp:Label ID="FrmMsg_LblTitulo" runat="server" Text=""></asp:Label>
                            </h4>
                        </div>

                        <div class="modal-body">
                            <asp:Label ID="FrmMsg_LblCuerpo" runat="server" Text=""></asp:Label>
                        </div> <%--modal-body--%>

                        <div class="modal-footer bg-light" style="border-top:1px solid #535362 !important;">
                            <asp:Button ID="FrmMsg_BtnAceptar" Width="50px" runat="server" Text="OK" CssClass="" OnClick="FrmMsg_BtnAceptar_Click" data-bs-dismiss="modal" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<!-- FIN MENSAJES -->


<script src="https://cdn.jsdelivr.net/npm/bootstrap5-toggle@5.0.4/js/bootstrap5-toggle.jquery.min.js"></script>

<script>
    function init() {
        $(document).ready(function () {
            
            $('input[data-toggle="toggle"]').bootstrapToggle();
            $(".FrmPpal_RdDistribucionResultadoMail").change(function () {
                if ($(this).is(":checked")) {
                    $(".div-ListaDistribucion").fadeIn()
                } else {
                    $(".div-ListaDistribucion").hide()
                }
                
            })

            $("#searchListaDistribucion").on("keyup", function () {
                var value = $(this).val().toLowerCase();
                $(".LstBoxListaDistribucion  tr").filter(function () {
                    $(this).toggle($(this).find("td:first").text().toLowerCase().indexOf(value) > -1)
                });
            });
        })
    }

    
    init()
</script>


</asp:Content>
