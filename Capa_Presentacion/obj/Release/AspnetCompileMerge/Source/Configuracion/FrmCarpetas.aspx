﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/GlbMaster.Master" CodeBehind="FrmCarpetas.aspx.vb" Inherits="Capa_Presentacion.FrmCarpetas" %>

<%@ Register Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../assets/js/jsUpdateProgress.js"></script>

    <!-- Font Lato + css -->
    <link href="../assets/css/lato.css?v=v1" rel="stylesheet" />
    <link href="../assets/css/complementos.css?v=v1.2" rel="stylesheet" />
    <link href="../assets/css/FrmCarpetas.css?v=v2" rel="stylesheet" />
    <link href="../assets/css/bootstrap5-toggle.min.css" rel="stylesheet">

    <script type="text/javascript">
        var ModalProgress = '<%= ModalProgress.ClientID %>';
    </script>

    <asp:Panel ID="PnProgress" runat="server">
        <asp:UpdateProgress ID="UpProgress" runat="server">
            <ProgressTemplate>
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center text-primary">
                                <img src='../assets/images/loadingLogin.gif' style='max-width: 50px;'>
                            </div>
                        </div>
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </asp:Panel>
    <asp:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="PnProgress" BackgroundCssClass="TeraModalBackground" PopupControlID="PnProgress" />


    <!-- MOSTRAR DATOS EN EL GRIDVIEW -->
    <asp:UpdatePanel runat="server" ID="UpdatePanelIndex">
        <ContentTemplate>
            <asp:Panel runat="server" ID="PanelIndex" CssClass="center-block">
                <div class="container-fluid">
                    <br />
                    <div class="row shadow-sm rounded">
                        <div class="col-11">
                            <h2 style="color: #26BD75; font-family: 'Lato'; font-weight: 900 !important;">Mantenedor <span class="badge text-bg-warning">Carpetas</span></h2>
                        </div>
                        <div class="col-1">
                            <span class="float-end">
                                <asp:LinkButton
                                    ID="BtnAddRegistro"
                                    runat="server"
                                    CssClass="btn btn-success btn-addButton"
                                    OnClick="FrmCarpetaRaiz_BtnAdd_click">
                                       <i class="fa-solid fa-plus"></i>
                                </asp:LinkButton>
                            </span>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-12 col-md-2 bg-color-folder">
                            <h6 class=" mt-3"><i class="fa-solid fa-wrench"></i>Todo</h6>
                            <div class="mt-4" style="min-height: 85vh">
                                <asp:TreeView ID="CarpetasTreeView" runat="server"
                                    ExpandImageUrl="~/assets/images/folder-solid.png"
                                    CollapseImageUrl="~/assets/images/folder-open-solid.png"
                                    LeafNodeStyle-ImageUrl="~/assets/images/folder-solid.png"
                                    CssClass="ForTreeView"
                                    NodeWrap="true"
                                    ShowLines="false"
                                    Font-Size="Small"
                                    BorderStyle="None"
                                    BackColor="Transparent"
                                    ForeColor="#555555"
                                    NodeStyle-HorizontalPadding="2px"
                                    NodeStyle-VerticalPadding="2px"
                                    NodeStyle-ChildNodesPadding="2px"
                                    SelectedNodeStyle-Font-Bold="false"
                                    SelectedNodeStyle-ForeColor="#000000"
                                    SelectedNodeStyle-BackColor="#FDFDEA"
                                    SelectedNodeStyle-Font-Italic="true">
                                </asp:TreeView>

                                <asp:TextBox ID="FrmPpal_TxtIdCarpeta" Visible="false" runat="server" />
                                <asp:TextBox ID="FrmPpal_TxtIdCarpetaPadre" Visible="false" runat="server" />
                                <asp:TextBox ID="FrmPpal_TxtNivel" Visible="false" runat="server" />
                                <asp:TextBox ID="FrmPpal_TxtSwTipo" Visible="false" runat="server" />
                                <asp:TextBox ID="FrmPpal_LastOptionSelected" Visible="false" runat="server" />

                            </div>
                        </div>

                        <div class="col-12 col-md-10">
                            <asp:Panel runat="server" ID="Ffrm_principal_contenedorCriticas" Visible="false">
                                <div class="row">
                                    <div class="col-12" style="padding-top: 8px; padding-bottom: -10px;">
                                        <div id="DivBreadcrumbsFolder" runat="server">
                                            <ol class="breadcrumb">
                                                <li>
                                                    <img src="../assets/images/folder-solid.png" alt="" style="border-width: 0; width: 16px; margin-right: 4px; margin-top: -3px;"></li>


                                            </ol>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12 col-md-10">
                                        <%--<div class="form-group" id="FrmPpal_DivCarpeta" runat="server">
                                            <label class="control-label" for="FrmPpal_TxtCarpeta">Nombre Carpeta:</label>
                                            <asp:TextBox ID="FrmPpal_TxtCarpeta" CssClass="form-control" placeholder="" MaxLength="50" runat="server" />
                                            <span id="FrmPpal_TxtCarpeta_Msj" class="invalid-feedback" aria-hidden="true" runat="server"></span>
                                        </div>--%>
                                        <%--<div class="input-group mt-1 mb-5">
                                            <span class="input-group-text specialform" id="FrmCarpetaRaiz_TxtCarpetaUpdate_Addons"><i class="far fa-folder"></i></span>
                                            <asp:TextBox ID="FrmPpal_TxtCarpeta" class="form-control specialform" placeholder="Carpeta" aria-describedby="FrmCarpetaRaiz_TxtCarpetaUpdate_Addonss" runat="server" />
                                            <div ID="FrmPpal_TxtCarpeta_Msj" class="" runat="server"></div>
                                        </div>--%>

                                        <label class="" for="FrmPpal_TxtCarpeta" style="color:#000 !important;"><h6 style="margin-bottom:0.7rem !important; font-size:1.2rem !important;">Nombre Carpeta</h6></label> 
                                        <asp:TextBox ID="FrmPpal_TxtCarpeta" class="form-control" MaxLength="30" autocomplete="off" runat="server" />
                                        <div ID="FrmPpal_TxtCarpeta_Msj" class="" runat="server"></div>
                                
                                        <div id="FrmPpal_DivUltAct" style="font-size:0.8rem !important; color:#8fc6ab !important; text-align:right !important" runat="server"></div>

                                        <hr class="hr" />

                                    </div>
                                </div>

                                <div class="row mt-3">
                                    <div class="col-md-3 col-12">
                                        <div class="row">
                                            <div class="col-12  ">
                                                <label class="control-label text-sm" for="FrmPpal_RbtConfidencial">Carpeta Confidencial:</label>
                                            </div>
                                            <div class="col-6 ">
                                                <asp:TextBox type="checkbox" runat="server" style="width:100%" ID="FrmPpal_ChkPrivada" data-onstyle="success" value="1"  data-toggle="toggle" data-size="sm" data-onlabel=" <i class='fa-solid fa-check'></i> Si" data-offlabel=" <i class='fa-solid fa-xmark'></i> No"/>
                                            </div>
                                        </div>


                                        <div class="row mt-2">
                                            <div class="col-12  ">
                                                <label class="control-label text-sm" for="FrmPpal_RbtConfidencial">Estado:</label>
                                            </div>
                                            <div class="col-6 ">
                                                <asp:TextBox style="width:100px;"   type="checkbox" runat="server" ID="FrmPpal_ChkEstado" value="1" data-onstyle="success"  data-toggle="toggle" data-size="sm" data-onlabel=" Habilitada" data-offlabel=" Deshabilitada"/>
                                            </div>
                                        </div>


                                        <div class="row mt-2">
                                            <div class="col-12  ">
                                                <label class="control-label text-sm" for="FrmPpal_RbtConfidencial">Contenedora de Críticas:</label>
                                            </div>
                                            <div class="col-6 ">
                                                <asp:TextBox type="checkbox" CssClass="toggleContenedorCriticas"  runat="server" value="1" ID="FrmPpal_ChkContenedorCriticas" data-onstyle="success"  data-toggle="toggle" data-size="sm" data-onlabel=" <i class='fa-solid fa-check'></i> Si" data-offlabel=" <i class='fa-solid fa-xmark'></i> No"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-7  col-12" style=" padding:0 !important;min-height:300px; ">

                                            <div class="row">
                                                <div class="col-10 mb-4 panel-contedora-criticas">
                                                    <div class="form-group" id="FrmPpal_DivStoreProcedure"  runat="server">
                                                        <label class="control-label" for="FrmPpal_TxtStoreProcedure">Store Procedure:</label>
                                                        <asp:TextBox ID="FrmPpal_TxtStoreProcedure" CssClass="form-control" placeholder="" MaxLength="50" runat="server" />
                                                        <span ID="FrmPpal_TxtStoreProcedure_Msj" class="invalid-feedback" aria-hidden="true" runat="server"></span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="accordion panel-contedora-criticas " id="accordionTareas" >
                                                        
                                                <div class="accordion-item" >
                                                    <h2 class="accordion-header" id="headingOne">
                                                        <button class="accordion-button" style="background-color:#8fc6ab !important; color:#2c3034;" id="Ffrm_principal_BtnCollapse1" type="button" data-bs-toggle="collapse" data-bs-target="#ContentPlaceHolder1_Ffrm_principal_LabelAcordeonOpc1" aria-expanded="true" aria-controls="collapseOne">
                                                            Lista de Distribución
                                                        </button>
                                                    </h2>
                                                    <div ID="Ffrm_principal_LabelAcordeonOpc1" runat="server" class="accordion-collapse collapse " aria-labelledby="headingOne" data-bs-parent="#accordionTareas">
                                                        <div class="accordion-body" >
                                                            <small>Buscar</small>
                                                            <input id="searchListaDistribucion" class=" form-control" placeholder="Filtrar Lista de distribuciones" />
                                                            <hr  class="hr"/>
                                                            <div class="form-check" style="height:200px;overflow-y: auto; padding-left:20px;">
                                                                <asp:CheckBoxList ID="FrmPpal_LstListaDistribucion" CssClass="checkbox LstBox  " RepeatDirection="Vertical" RepeatColumns="1" runat="server"></asp:CheckBoxList>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                        


                                                <div class="accordion-item" >
                                                    <h2 class="accordion-header" id="headingThree">
                                                        <button class="accordion-button collapsed" style="background-color:#8fc6ab !important; color:#2c3034;" id="Ffrm_principal_BtnCollapse2" type="button" data-bs-toggle="collapse" data-bs-target="#ContentPlaceHolder1_Ffrm_principal_LabelAcordeonOpc2" aria-expanded="false" aria-controls="collapseThree">
                                                            Autorizado a Crear / Actualizar / Desactivar Críticas
                                                        </button>
                                                    </h2>
                                                    <div runat="server" ID="Ffrm_principal_LabelAcordeonOpc2" class="accordion-collapse collapse " aria-labelledby="headingThree" data-bs-parent="#accordionTareas">
                                                        <div class="accordion-body"> 
                                                                    
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <div class="input-group">
                                                                        <asp:DropDownList  CssClass="form-control select2 select2-container--bootstrap" placeholder="<<Seleccionar>>" style="width:90%"  ID="FrmPpal_CboUserGestionSearch" runat="server" ></asp:DropDownList>
                                                                    
                                                                        <div class="input-group-append" >
                                                                            <asp:LinkButton runat="server" class="btn-sm btn-addon btn-add" ID="FrmPpal_BtnGrabar_personaAutorizada"  OnClick="FrmPpal_BtnGrabar_personaAutorizada_click"  type="button"> <i class="fa-solid fa-plus"></i> Add</asp:LinkButton>
                                                                        </div>
                                                                            
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row mt-4">
                                                                <asp:GridView 
                                                                    ID="FrmPpal_GrillaAutoriza" 
                                                                    runat="server" border="0" 
                                                                     CssClass="TeraGridview table table-bordered bg-light"
                                                                    Style="text-align:center; vertical-align:middle; font-size:1rem; max-width:1600px; margin-left: 0px; width:98% !important;"
                                                                    PageSize="50" AllowPaging="true" 
                                                                    AutoGenerateColumns="false" 
                                                                    HorizontalAlign="Center"  
                                                                    OnRowDataBound="FrmPpal_GrillaAutoriza_RowDataBound" 
                                                                    AllowSorting="true"
                                                                >
                                                                    <HeaderStyle 
                                                                        CssClass="bg-dark text-center text-white" 
                                                                        Font-Size="Medium" Font-Bold="false" />                                                             
                                                                        <Columns>

                                                                            <%-- 0 --%>
                                                                            <asp:TemplateField HeaderText="<span class='link-light'></span>" ItemStyle-Width="50" Visible="false">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="LblIdAutorizado" runat="server" Text='<%# Eval("IdAutorizado") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <%-- 1 --%>
                                                                            <asp:TemplateField HeaderText="<span class='link-light'>Nº</span>" ItemStyle-Width="50">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="LblNroFila" runat="server" Text='<%# Eval("NroFila") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <%-- 2 --%>
                                                                            <asp:TemplateField HeaderStyle-CssClass="text-center" SortExpression="Usuario" HeaderText="Usuario" Visible="true">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="LblIdAutorizado" runat="server" Text='<%# Eval("Persona") %>'> </asp:Label>
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                            <%-- 3 --%>
                                                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="<i class='fa-solid fa-trash'></i>" Visible="true">
                                                                                <ItemTemplate>
                                                                                    <asp:LinkButton CssClass="btn btn-sm btn-danger" runat="server" ID="FfrmPrincipal_btnElimina" OnClick="FfrmPrincipal_btnElimina_click"><i class='fa-solid fa-trash'></i></asp:LinkButton>

                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>

                                                                               
                                                                    </Columns>
                                                                    <PagerTemplate>                                    
                                                                        <nav style="float:right;">
                                                                            <div class="btn-group" role="group">
                                                                                <asp:LinkButton ID="FrmPpal_GrillaAutoriza_BtnFirst" runat="server" CssClass="btn btn-outline-light btn-sm" Text="Primera Página" ToolTip="Primera Página" OnClick="FrmPpal_GrillaAutoriza_BtnFirst_Click"><i class="fa-solid fa-backward-fast"></i></asp:LinkButton>
                                                                                <asp:LinkButton ID="FrmPpal_GrillaAutoriza_BtnPrev" runat="server" CssClass="btn btn-outline-light btn-sm" Text="Página Anterior" ToolTip="Página Anterior" OnClick="FrmPpal_GrillaAutoriza_BtnPrev_Click"><i class="fa-solid fa-arrow-left"></i></asp:LinkButton>
                                                                                <asp:LinkButton ID="FrmPpal_GrillaAutoriza_BtnNext" runat="server" CssClass="btn btn-outline-light btn-sm" Text="Página Siguiente" ToolTip="Página Siguiente" OnClick="FrmPpal_GrillaAutoriza_BtnNext_Click"><i class="fa-solid fa-arrow-right"></i></asp:LinkButton>
                                                                                <asp:LinkButton ID="FrmPpal_GrillaAutoriza_BtnLast" runat="server" CssClass="btn btn-outline-light btn-sm" Text="Última Página" ToolTip="Última Página" OnClick="FrmPpal_GrillaAutoriza_BtnLast_Click"><i class="fa-solid fa-forward-fast"></i></asp:LinkButton>
                                                                            </div>
                                                                            <div class="justify-content-center text-white" style="font-size:0.8rem">
                                                                                <asp:Label ID="FrmPpal_GrillaAutoriza_CurrentPageLabel" runat="server" CssClass="label label-warning" Font-Size="Smaller" />
                                                                            </div>
                                                                        </nav>
                                                                    </PagerTemplate>
                                                                    
                                                                    <EmptyDataTemplate>
                                                                        <div class="alert alert-warning align-items-center" role="alert">
                                                                            <h1><i class="fa-solid fa-triangle-exclamation"></i></h1>
                                                                            <div>No existen registros para mostrar.</div>
                                                                        </div>
                                                                    </EmptyDataTemplate>    
                                                                </asp:GridView>

                                                            </div>
                                                            <!--small>Seleccione los usuarios que podran  <b class="text-success">Crear</b>/<b class="text-primary">Actualizar</b>/<b class="text-danger">Desactivar</b> Críticas</small-->
                                                            <asp:HiddenField ID="hfSelected" runat="server" />
                                                                
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="accordion-item" >
                                                    <h2 class="accordion-header" id="headingTwo">
                                                        <button class="accordion-button collapsed" style="background-color:#8fc6ab !important; color:#2c3034;" id="Ffrm_principal_BtnCollapse3" type="button" data-bs-toggle="collapse" data-bs-target="#ContentPlaceHolder1_Ffrm_principal_LabelAcordeonOpc3" aria-expanded="false" aria-controls="collapseTwo">
                                                            Prefijos
                                                        </button>
                                                    </h2>
                                                    <div runat="server" ID="Ffrm_principal_LabelAcordeonOpc3" class="accordion-collapse collapse " aria-labelledby="headingTwo" data-bs-parent="#accordionTareas">
                                                        <div class="accordion-body">
                                                                
                                                            <div class="row ">

                                                                <div class="col-6  ">
                                                                    <div class="input-group mb-3">
                                                                        <span class="input-group-text">Prefijo</span>
    <asp:TextBox ID="FrmPpal_TxtPrefijo" CssClass="form-control" placeholder="" MaxLength="5" runat="server" />
    <asp:LinkButton ID="LinkButton2" runat="server" OnClick="FrmPpal_BtnAddPrefijo_Click" CssClass="btn btn-success  active" ToolTip="Agregar prefijo"><i class="fa-solid fa-plus"></i> Add</asp:LinkButton></div>
                                                                           
                                                                </div>
                                                                        

                                                                <div class="col-3  ">
                                                                            
                                                        
                                                                </div>
                                                            </div>

                                                            <div class="row mt-3">
                                                                <div class="col-12">
                                                                     <asp:GridView 
                                                                    ID="FrmPpal_GrillaPrefijo" 
                                                                    runat="server" border="0" 
                                                                     CssClass="TeraGridview table table-bordered bg-light"
                                                                    Style="text-align:center; vertical-align:middle; font-size:1rem; max-width:1600px; margin-left: 0px; width:98% !important;"
                                                                    PageSize="50" AllowPaging="true" 
                                                                    AutoGenerateColumns="false" 
                                                                    HorizontalAlign="Center"  
                                                                    OnRowDataBound="FrmPpal_GrillaPrefijo_RowDataBound" 
                                                                    AllowSorting="true"
                                                                >
                                                                    <HeaderStyle 
                                                                        CssClass="bg-dark text-center text-white" 
                                                                        Font-Size="Medium" Font-Bold="false" />                                                             
                                                                        <Columns>

                                                                           <%-- 0 --%>         
                                                                        <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="#" Visible="false">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="LblIdPrefijo" runat="server" Text='<%# Eval("IdPrefijo") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>


                                                                         <%-- 1 --%>    
                                                                         <asp:TemplateField HeaderText="<span class='link-light'>Nº</span>" ItemStyle-Width="50">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="LblNroFila" runat="server" Text='<%# Eval("NroFila") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                         <%-- 2 --%>
                                                                        <asp:TemplateField  HeaderStyle-CssClass="text-center" HeaderStyle-Width="90%" HeaderText="Prefijo" SortExpression="Prefijo" Visible="true">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="LblPrefijo" runat="server" Text='<%# Eval("Prefijo") %>'></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                         <%-- 3 --%>
                                                                        <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderStyle-Width="10%" HeaderText="<i class='fa-solid fa-trash'></i>" Visible="true">
                                                                            <ItemTemplate>
                                                                                <div class="text-center" style="width:100%">
                                                                                    <asp:LinkButton CssClass="btn btn-sm btn-danger" runat="server" ID="FfrmPrincipal_btnElimina_prefijo" OnClick="FfrmPrincipal_btnElimina_prefijo_click"><i class='fa-solid fa-trash'></i></asp:LinkButton>
                                                                                </div>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>

                                                                               
                                                                    </Columns>
                                                                    <PagerTemplate>                                    
                                                                        <nav style="float:right;">
                                                                            <div class="btn-group" role="group">
                                                                                <asp:LinkButton ID="FrmPpal_GrillaPrefijo_BtnFirst" runat="server" CssClass="btn btn-outline-light btn-sm" Text="Primera Página" ToolTip="Primera Página" OnClick="FrmPpal_GrillaPrefijo_BtnFirst_Click"><i class="fa-solid fa-backward-fast"></i></asp:LinkButton>
                                                                                <asp:LinkButton ID="FrmPpal_GrillaPrefijo_BtnPrev" runat="server" CssClass="btn btn-outline-light btn-sm" Text="Página Anterior" ToolTip="Página Anterior" OnClick="FrmPpal_GrillaPrefijo_BtnPrev_Click"><i class="fa-solid fa-arrow-left"></i></asp:LinkButton>
                                                                                <asp:LinkButton ID="FrmPpal_GrillaPrefijo_BtnNext" runat="server" CssClass="btn btn-outline-light btn-sm" Text="Página Siguiente" ToolTip="Página Siguiente" OnClick="FrmPpal_GrillaPrefijo_BtnNext_Click"><i class="fa-solid fa-arrow-right"></i></asp:LinkButton>
                                                                                <asp:LinkButton ID="FrmPpal_GrillaPrefijo_BtnLast" runat="server" CssClass="btn btn-outline-light btn-sm" Text="Última Página" ToolTip="Última Página" OnClick="FrmPpal_GrillaPrefijo_BtnLast_Click"><i class="fa-solid fa-forward-fast"></i></asp:LinkButton>
                                                                            </div>
                                                                            <div class="justify-content-center text-white" style="font-size:0.8rem">
                                                                                <asp:Label ID="FrmPpal_GrillaPrefijo_CurrentPageLabel" runat="server" CssClass="label label-warning" Font-Size="Smaller" />
                                                                            </div>
                                                                        </nav>
                                                                    </PagerTemplate>
                                                                    
                                                                    <EmptyDataTemplate>
                                                                        <div class="alert alert-warning align-items-center" role="alert">
                                                                            <h1><i class="fa-solid fa-triangle-exclamation"></i></h1>
                                                                            <div>No existen registros para mostrar.</div>
                                                                        </div>
                                                                    </EmptyDataTemplate>    
                                                                </asp:GridView>

                                                                </div>
                                                                        
                                                            </div>
                                                                
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                           

                                </div>

                                 <div class="row mt-4" runat="server" ID="FrmPpal_botonera" visible ="true">
                                    <div class="col-10  bg-light text-end" style=" padding-top:8px;padding-bottom:10px;;">
                                        <div class="float-end">
                                            <asp:LinkButton  class="btn btn-success btn-sm FrmPpal_BtnGrabar"   ID="FrmPpal_BtnGrabar" runat="server" OnClick="FrmPpal_BtnGrabar_Click">
                                                <i class='fas fa-save'></i> Crear    
                                            </asp:LinkButton>
                                            <asp:LinkButton ID="FrmPpal_BtnAddSubCarpeta" visible="false"  CssClass="btn btn-sm btn-info addSubCarpeta" Text="Add SubCarpeta"  ToolTip="Add SubCarpeta" runat="server"  OnClick ="FrmPpal_BtnAddSubCarpeta_click">
                                                <i class="fa-solid fa-folder-tree"></i> Add SubCarpeta
                                            </asp:LinkButton>

                                        </div>

                                       
                                                       
                                    </div>
                                </div>
                                            
                            </asp:Panel>



                        </div>
                    </div>
                </div>

                <br />

                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!-- FIN MOSTRAR DATOS EN EL GRIDVIEW -->


    <!-- FORMULARIO -->
    <asp:Button ID="FrmPpal_BtnVer" runat="server" Style="display: none" />
    <asp:ModalPopupExtender ID="FrmPpal" runat="server" TargetControlID="FrmPpal_BtnVer" PopupControlID="FrmPpal_Panel" BackgroundCssClass="TeraModalBackground"></asp:ModalPopupExtender>
    <asp:Panel ID="FrmPpal_Panel" runat="server">
        <asp:UpdatePanel ID="FrmPpal_UpdatePanel" runat="server">
            <ContentTemplate>
                <div class="modal" id="modal-FrmPrincipal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                   <div class="modal-dialog modal-md" style="display:none" runat="server" id="FrmPpal_dialog">
                        <div class="modal-content">
                            
                            <div class="modal-header bg-dark text-white">
                                <h4 ID="FrmPpalTitulo" class="modal-title fs-5" runat="server"></h4>
                            </div>
                            <div class="modal-body">
                                 <label class="" for="FrmCarpetaRaiz_TxtCarpeta" style="color:#333 !important;"><h6 style="margin-bottom:0rem !important; font-size:1.1rem !important;">Nombre de Carpeta</h6></label> 
                                <div class="input-group mt-1 mb-5">
                                    <span class="input-group-text specialform" id="FrmCarpetaRaiz_TxtCarpeta_Addon"><i class="far fa-folder"></i></span>
                                    <asp:TextBox ID="FrmCarpetaRaiz_TxtCarpeta" class="form-control specialform" placeholder="Carpeta" aria-describedby="FrmCarpetaRaiz_TxtCarpeta_Addon" runat="server" />
                                    <div ID="FrmCarpetaRaiz_TxtCarpeta_Msj" class="" runat="server"></div>
                                </div>

                                <div runat="server" visible="false" ID="pathBrath" class="styleRemoveHipe"></div>

                            </div>
                            <div class="modal-footer bg-light" style="border-top:2px solid #535362 !important;">
                                <asp:LinkButton ID="FrmPpal_BtnCerrar"    runat="server" CssClass="btn btn-secondary" ToolTip="Cerrar"        OnClick="FrmPpal_BtnCerrar_Click"><i class="fa-regular fa-share-from-square"></i>&nbsp;&nbsp;&nbsp;Cerrar</asp:LinkButton>
                                &nbsp;&nbsp;&nbsp;
                                <asp:LinkButton ID="FrmPpal_BtnGuardar" runat="server" CssClass="btn btn-success " ToolTip="Almacenar" OnClick="FrmPpal_BtnGuardar_Click"></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <!-- FIN FORMULARIO -->




    <!-- MENSAJES -->
    <asp:Button ID="FrmMsg_BtnVer" runat="server" Text="" Style="display: none;" />
    <asp:ModalPopupExtender ID="FrmMsg" runat="server" TargetControlID="FrmMsg_BtnVer" PopupControlID="FrmMsg_UpdatePanel" BackgroundCssClass="TeraModalBackground"></asp:ModalPopupExtender>
    <asp:Panel ID="FrmMsg_Panel" runat="server">
        <asp:UpdatePanel ID="FrmMsg_UpdatePanel" runat="server">
            <ContentTemplate>
                <div class="modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                    <div id="FrmMsg_dialog" class="modal-dialog modal-lg modal-dialog-centered" style="display: none;" runat="server">
                        <div class="modal-content">
                            <div id="FrmMsg_TipoMensaje" class="" style="color: #FFF !important" runat="server">
                                <h4 class="modal-title fs-5" style="color: #FFF !important;">
                                    <i id="FrmMsg_TipoMensajeIcono" class="" runat="server"></i>&nbsp;
                                    <asp:Label ID="FrmMsg_LblTitulo" runat="server" Text=""></asp:Label>
                                </h4>
                            </div>

                            <div class="modal-body">
                                <asp:Label ID="FrmMsg_LblCuerpo" runat="server" Text=""></asp:Label>
                            </div>
                            <%--modal-body--%>

                            <div class="modal-footer bg-light" style="border-top: 1px solid #535362 !important;">
                                <asp:Button ID="FrmMsg_BtnAceptar" Width="50px" runat="server" Text="OK" CssClass="" OnClick="FrmMsg_BtnAceptar_Click" data-bs-dismiss="modal" />
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <!-- FIN MENSAJES -->



    <script src="https://cdn.jsdelivr.net/npm/bootstrap5-toggle@5.0.4/js/bootstrap5-toggle.jquery.min.js"></script>
<script>
    function init() {
        $(document).ready(function () {
            $('input[data-toggle="toggle"]').bootstrapToggle();
            $('[data-toggle="tooltip"]').tooltip()

            var select2 = $('.select2').select2({
                placeholder: $(this).data('placeholder'),
            });
            //select2.data('select2').$selection.css('height', '31px');

            $(".accordion-button").addClass("collapsed")

            for (let i = 1; i <= 3; i++) {
                let isClase = $("#ContentPlaceHolder1_Ffrm_principal_LabelAcordeonOpc" + i).attr("class")
                if (isClase) {
                    if (isClase.includes("show")) {
                        $("#Ffrm_principal_BtnCollapse" + i).removeClass("collapsed")
                    }
                }
            }


            $(".LstBox input:checkbox").each(function () {
                $(this).addClass("form-check-input");
            })

            $("#searchListaDistribucion").on("keyup", function () {
                var value = $(this).val().toLowerCase();
                $(".LstBox  tr").filter(function () {
                    $(this).toggle($(this).find("td:first").text().toLowerCase().indexOf(value) > -1)
                });
            });

            $('.toggleContenedorCriticas').change(function () {
                let istoggleContenedorCriticas = ($(this).prop('checked'))
                if (istoggleContenedorCriticas) {
                    $(".panel-contedora-criticas").fadeIn()

                    $(".addSubCarpeta").hide()
                } else {
                    $(".panel-contedora-criticas").hide()

                    $(".addSubCarpeta").show()
                }
            })

            console.log($('.toggleContenedorCriticas').is(":checked"))
            if (!$('.toggleContenedorCriticas').is(":checked")) {
                $(".panel-contedora-criticas").hide()
            } else {
                $(".panel-contedora-criticas").fadeIn()
            }

        })
    }

    function eliminaHtml() {
        $(".styleRemoveHipe *").prop('disabled', true);
    }
    init()
</script>


</asp:Content>



