﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/GlbMaster.Master" CodeBehind="FrmListaDistribucion.aspx.vb" Inherits="Capa_Presentacion.FrmListaDistribucion" %>
<%@ Register Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../assets/js/jsUpdateProgress.js?v=v1.2"></script>

    <!-- Font Lato + css -->
    <link href="../assets/css/lato.css?v=v1.2" rel="stylesheet" />
    <%--<link href="../assets/css/FrmListaDistribucion.css?v=v1" rel="stylesheet" />--%>
    <link href="../assets/css/complementos.css?v=v1.2" rel="stylesheet" />

    <script type="text/javascript">
        var ModalProgress = '<%= ModalProgress.ClientID %>';
    </script>  

    <asp:Panel ID="PnProgress" runat="server">
        <asp:UpdateProgress ID="UpProgress" runat="server">
            <ProgressTemplate>
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center text-primary">
                                <img src='../assets/images/loadingLogin.gif' style='max-width:50px;'>
                            </div>
                        </div>
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </asp:Panel>
    <asp:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="PnProgress" BackgroundCssClass="TeraModalBackground" PopupControlID="PnProgress"  />
    

<!-- MOSTRAR DATOS EN EL GRIDVIEW -->
    <asp:UpdatePanel runat="server" ID="UpdatePanelIndex">
        <ContentTemplate>
            <asp:Panel runat="server" ID="PanelIndex" CssClass="center-block">
                <div class="container-fluid">
                    <br />
                    <div class="row shadow-sm rounded">
                        <div class="col-11">                       
                            <h2 style="color:#26BD75; font-family:'Lato'; font-weight:900 !important;">Mantenedor de <span class="badge text-bg-warning">Lista Distribución </span></h2>
                        </div>
                        <div class="col-1">
                            <span class="float-end">
                                <asp:LinkButton 
                                    ID="BtnAddRegistro" 
                                    runat="server" 
                                    CssClass="btn btn-success btn-addButton" 
                                    OnClick="BtnAddRegistro_Click">
                                       <i class="fa-solid fa-plus"></i>
                                </asp:LinkButton>
                            </span>
                        </div>
                    </div>

                    <br />
                    <div class="row">
                        <div class="col-5">
                            <div class="input-group mb-3">
                                <label class="input-group-text border-secondary text-white bg-dark" for="TxtBsc"><i class="fa-solid fa-magnifying-glass"></i>&nbsp;Buscar&nbsp;&nbsp;</label>
                                <asp:TextBox ID="TxtBsc" CssClass="form-control border-secondary form-control-sm"
                                    placeholder="para buscar valores múltiples utilice el separador ;" style="font-size: 1.1rem !important;"
                                    onkeypress="if ((event || window.event).keyCode == 13) { var node = (event || window.event).target || (event || window.event).srcElement; if (node.type == 'text' && node.id == 'ContentPlaceHolder1_TxtBsc') { return false; } }"
                                    autocomplete="off" runat="server"></asp:TextBox>
                                <asp:LinkButton ID="BtnBsc" CssClass="btn btn-dark"  OnClick="BtnBsc_Click" runat="server">
                                    &nbsp;&nbsp;<i class="fa-brands fa-golang" style="height:1.8em !important; vertical-align: inherit !important;"></i>&nbsp;&nbsp;
                                </asp:LinkButton>
                            </div>
                        </div>
                    </div> 

                    <br />
                    <div class="row">
                        <div class="col-12">
                            <div class="gradientLineKantar" style="float:left;"></div>
                            <asp:GridView ID="GrillaPpal" runat="server" CssClass="TeraGridview table table-bordered bg-light"
                                Style="text-align:center; vertical-align:middle; font-size:1rem; max-width:1600px; margin-left: 0px; width:98% !important;"
                                PageSize="50" AllowPaging="true" AutoGenerateColumns="false" 
                                HorizontalAlign="Center" AllowSorting="true"
                                OnRowDataBound="GrillaPpal_RowDataBound">
                            <HeaderStyle CssClass="bg-dark text-center text-white" Font-Size="Medium" Font-Bold="false" />                                                             
                                <Columns>                                    
                                    <%-- 0 --%>
                                    <asp:TemplateField HeaderText="" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="LblIdListaDistribucion" runat="server" Text='<%# Eval("IdListaDistribucion") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- 1 --%>
                                    <asp:TemplateField HeaderText="<span class='link-light'>Nº</span>" ItemStyle-Width="50">
                                        <ItemTemplate>
                                            <asp:Label ID="LblNroFila" runat="server" Text='<%# Eval("NroFila") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- 2 --%>
                                    <asp:TemplateField HeaderText="<span class='link-light'>Lista Distribución  <i class='fa-solid fa-ellipsis-vertical'></i></span>" SortExpression="ListaDistribucion">
                                        <ItemTemplate>
                                            <asp:Label ID="LblListaDistribucion" runat="server" Text='<%# Eval("ListaDistribucion") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                   


                                    <%-- 3 --%>
                                    <asp:TemplateField HeaderText="<span class='link-light'>Usuarios " >
                                        <ItemTemplate>
                                             <div class="list-user">
                                                <%# Eval("Usuarios") %>
                                             </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>



                                    <%-- 4 --%>
                                    <asp:TemplateField HeaderText="<span class='link-light'><i class='fa-solid fa-user-plus'></i></span>" ItemStyle-Width="50">
                                        <ItemTemplate>
                                            <asp:LinkButton CssClass="btn btn-success btn-sm" ID="GrillaPpal_BtnEditar" OnClick="GrillaPpal_BtnAddUsuarios_Click" runat="server"><i class="fa-solid fa-user-plus"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                     <%-- 5 --%>
                                    <asp:TemplateField HeaderText="<span class='link-light'><i class='fa-solid fa-pen-to-square'></i></span>" ItemStyle-Width="50">
                                        <ItemTemplate>
                                            <asp:LinkButton CssClass="btn btn-info btn-sm" ID="GrillaPpal_BtnAddUser" OnClick="GrillaPpal_BtnEditar_Click" runat="server"><i class="fa-solid fa-pen-to-square"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- 6 --%>
                                    <asp:TemplateField HeaderText="<span class='link-light'><i class='fa-solid fa-trash-can'></i></span>" ItemStyle-Width="50">
                                        <ItemTemplate>
                                            <asp:LinkButton CssClass="btn btn-danger btn-sm" ID="GrillaPpal_BtnEliminar" OnClick="GrillaPpal_BtnEliminar_Click" runat="server"><i class="fa-solid fa-trash-can"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerTemplate>                                    
                                    <nav style="float:right;">
                                        <div class="btn-group" role="group">
                                            <asp:LinkButton ID="GrillaPpal_BtnFirst"    runat="server" CssClass="btn btn-outline-light btn-sm" Text="Primera Página"      ToolTip="Primera Página"    OnClick="GrillaPpal_BtnFirst_Click"><i class="fa-solid fa-backward-fast"></i></asp:LinkButton>
                                            <asp:LinkButton ID="GrillaPpal_BtnPrev"     runat="server" CssClass="btn btn-outline-light btn-sm" Text="Página Anterior"     ToolTip="Página Anterior"   OnClick="GrillaPpal_BtnPrev_Click"><i class="fa-solid fa-arrow-left"></i></asp:LinkButton>
                                            <asp:LinkButton ID="GrillaPpal_BtnNext"     runat="server" CssClass="btn btn-outline-light btn-sm" Text="Página Siguiente"    ToolTip="Página Siguiente"  OnClick="GrillaPpal_BtnNext_Click"><i class="fa-solid fa-arrow-right"></i></asp:LinkButton>
                                            <asp:LinkButton ID="GrillaPpal_BtnLast"     runat="server" CssClass="btn btn-outline-light btn-sm" Text="Última Página"       ToolTip="Última Página"     OnClick="GrillaPpal_BtnLast_Click"><i class="fa-solid fa-forward-fast"></i></asp:LinkButton>
                                        </div>
                                        <div class="justify-content-center text-white" style="font-size:0.8rem">
                                            <asp:Label ID="GrillaPpal_CurrentPageLabel" runat="server" />                            
                                        </div>
                                    </nav>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    <div class="alert alert-warning align-items-center" role="alert">
                                        <h1><i class="fa-solid fa-triangle-exclamation"></i></h1>
                                        <div>No existen registros para mostrar.</div>
                                    </div>
                                </EmptyDataTemplate>                                
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
<!-- FIN MOSTRAR DATOS EN EL GRIDVIEW -->


<!-- FORMULARIO -->
<asp:Button ID="FrmPpal_BtnVer" runat="server" Style="display:none" />
<asp:ModalPopupExtender ID="FrmPpal" runat="server" TargetControlID="FrmPpal_BtnVer" PopupControlID="FrmPpal_Panel" BackgroundCssClass="TeraModalBackground"></asp:ModalPopupExtender>
<asp:Panel ID="FrmPpal_Panel" runat="server">
    <asp:UpdatePanel ID="FrmPpal_UpdatePanel" runat="server">
        <ContentTemplate>
            <div class="modal" id="modal-FrmPrincipal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div id="FrmPpal_dialog" class="modal-dialog modal-lg" style="display:none;" runat="server">
                    <div class="modal-content">
                        <div class="modal-header bg-dark text-white">
                            <h4 ID="FrmPpalTitulo" class="modal-title fs-5" runat="server"></h4>
                        </div>
                
                        <div class="modal-body">
                            <asp:TextBox ID="FrmPpal_TxtIdListaDistribucion" style="display:none;" runat="server" />

                            <div class="row gx-5">
                                <div class="col-8">
                                    <label class="" for="FrmPpal_TxtListaDistribucion" style="color:#000 !important;"><h6 style="margin-bottom:0.7rem !important; font-size:1.2rem !important;">Lista Distribución </h6></label> 
                                    <asp:TextBox ID="FrmPpal_TxtListaDistribucion" class="form-control" MaxLength="30" autocomplete="off" runat="server" />
                                    <div ID="FrmPpal_TxtListaDistribucion_Msj" class="" runat="server"></div>
                                </div>
                            </div>
                            
                            <br />
                        </div>

                        <div id="FrmPpal_DivUltAct" style="font-size:0.8rem !important; color:#26BD75 !important; text-align:right !important" runat="server"></div>

                        <div class="modal-footer bg-light" style="border-top:2px solid #535362 !important;">
                            <asp:LinkButton ID="FrmPpal_BtnCerrar"    runat="server" CssClass="btn btn-secondary" ToolTip="Cerrar"        OnClick="FrmPpal_BtnCerrar_Click"><i class="fa-regular fa-share-from-square"></i>&nbsp;&nbsp;&nbsp;Cerrar</asp:LinkButton>
                            &nbsp;&nbsp;&nbsp;
                            <asp:LinkButton ID="FrmPpal_BtnGuardar"    runat="server" CssClass="btn btn-success"   ToolTip="Actualizar"    OnClick="FrmPpal_BtnGuardar_Click"></asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<!-- FIN FORMULARIO -->


<!-- FORMULARIO Usuarios -->
<asp:Button ID="FrmUsuarios_BtnVer" runat="server" Style="display:none" />
<asp:ModalPopupExtender ID="FrmUsuarios" runat="server" TargetControlID="FrmUsuarios_BtnVer" PopupControlID="FrmUsuarios_Panel" BackgroundCssClass="TeraModalBackground"></asp:ModalPopupExtender>
<asp:Panel ID="FrmUsuarios_Panel" runat="server">
    <asp:UpdatePanel ID="FrmUsuarios_UpdatePanel" runat="server">
        <ContentTemplate>
            <div class="modal" id="modal-FrmUsuarios" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div id="FrmUsuarios_dialog" class="modal-dialog modal-lg" style="display:none;" runat="server">
                    <div class="modal-content">
                        <div class="modal-header bg-dark text-white">
                            <h4 ID="FrmUsuariosTitulo" class="modal-title fs-5" runat="server"></h4>
                        </div>
                
                        <div class="modal-body">

                            <div class="row">
                                <asp:TextBox ID="FrmUsuarios_LblIdListaDistribucion"      Visible="false" runat="server" />
                                <div class="col-12">
                                    <div class="input-group">
                                        <asp:DropDownList  CssClass="form-control select2-add  select2-container--bootstrap" placeholder="<<Seleccionar>>" style="width:85%;z-index:10000;"  ID="FrmUsuarios_CboUserGestionSearch" runat="server" ></asp:DropDownList>
                                        <div class="input-group-append" >
                                            <asp:LinkButton runat="server" class="btn-sm btn-addon btn-add" ID="FrmUsuarios_BtnGrabar"  OnClick="FrmUsuarios_BtnGrabar_click"  type="button"> <i class="fa-solid fa-plus"></i> Add</asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            

                             <div class="row mt-4">
                                <div class="col-12">
                                    <asp:GridView ID="FrmUsuarios_GrillaUsuarios" runat="server" CssClass="TeraGridview table table-bordered bg-light"
                                    Style="text-align:center; vertical-align:middle; font-size:1rem; max-width:1600px; margin-left: 0px; width:98% !important;"
                                    PageSize="50" AllowPaging="true" AutoGenerateColumns="false" 
                                    HorizontalAlign="Center" AllowSorting="true"
                                    OnRowDataBound="FrmUsuarios_GrillaUsuarios_RowDataBound">
                                    
                                    
                                    <HeaderStyle CssClass="bg-dark text-center text-white" Font-Size="Medium" Font-Bold="false" /> 
                                        <Columns>
                                            <%-- 0 --%>
                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="Usuario" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="LblIdUsuario" runat="server" Text='<%# Eval("IdUsuario") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                             <%-- 1 --%>
                                            <asp:TemplateField HeaderText="<span class='link-light'>Nº</span>" ItemStyle-Width="50">
                                                <ItemTemplate>
                                                    <asp:Label ID="LblNroFila" runat="server" Text='<%# Eval("NroFila") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                             <%-- 2 --%>
                                            <asp:TemplateField 
                                                HeaderStyle-CssClass="text-center" 
                                                HeaderText="<span class='link-light'>Persona  <i class='fa-solid fa-ellipsis-vertical'></i></span>" 
                                                Visible="true" ItemStyle-HorizontalAlign="Left" 
                                                SortExpression="Persona"
                                            >    
                                                <ItemTemplate>
                                                    <asp:Label ID="LblPersona" runat="server" Text='<%# Eval("Persona") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                             <%-- 3 --%>
                                            <asp:TemplateField HeaderStyle-CssClass="text-center" HeaderText="<i class='fa-solid fa-trash'></i>" Visible="true">
                                                <ItemTemplate>
                                                    <asp:LinkButton CssClass="btn btn-sm btn-danger" runat="server" ID="FrmUsuarios_btnElimina" OnClick="FrmUsuarios_btnEliminaUsuario_click"><i class='fa-solid fa-trash'></i></asp:LinkButton>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>

                                        <PagerTemplate>                                    
                                            <nav style="float:right;">
                                                <div class="btn-group" role="group">
                                                    <asp:LinkButton ID="FrmUsuarios_GrillaUsuarios_BtnFirst"    runat="server" CssClass="btn btn-outline-light btn-sm" Text="Primera Página"      ToolTip="Primera Página"    OnClick="FrmUsuarios_GrillaUsuarios_BtnFirst_Click"><i class="fa-solid fa-backward-fast"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="FrmUsuarios_GrillaUsuarios_BtnPrev"     runat="server" CssClass="btn btn-outline-light btn-sm" Text="Página Anterior"     ToolTip="Página Anterior"   OnClick="FrmUsuarios_GrillaUsuarios_BtnPrev_Click"><i class="fa-solid fa-arrow-left"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="FrmUsuarios_GrillaUsuarios_BtnNext"     runat="server" CssClass="btn btn-outline-light btn-sm" Text="Página Siguiente"    ToolTip="Página Siguiente"  OnClick="FrmUsuarios_GrillaUsuarios_BtnNext_Click"><i class="fa-solid fa-arrow-right"></i></asp:LinkButton>
                                                    <asp:LinkButton ID="FrmUsuarios_GrillaUsuarios_BtnLast"     runat="server" CssClass="btn btn-outline-light btn-sm" Text="Última Página"       ToolTip="Última Página"     OnClick="FrmUsuarios_GrillaUsuarios_BtnLast_Click"><i class="fa-solid fa-forward-fast"></i></asp:LinkButton>
                                                </div>
                                                <div class="justify-content-center text-white" style="font-size:0.8rem">
                                                    <asp:Label ID="FrmUsuarios_GrillaUsuarios_CurrentPageLabel" runat="server" />                            
                                                </div>
                                            </nav>
                                        </PagerTemplate>
                                        <EmptyDataTemplate>
                                            <div class="alert alert-warning align-items-center" role="alert">
                                                <h1><i class="fa-solid fa-triangle-exclamation"></i></h1>
                                                <div>No hay Usuarios asociados.</div>
                                            </div>
                                        </EmptyDataTemplate>     
                                        
                                    </asp:GridView>

                                    
                                </div>
                            </div>
                           
                            <br />
                            

                        </div>
                        <div id="FrmUsuariosUltAct" style="font-size:0.8rem !important; color:#26BD75 !important; text-align:right !important" runat="server"></div>

                        <div class="modal-footer bg-light" style="border-top:2px solid #535362 !important;">
                            <asp:LinkButton ID="FrmUsuarios_BtnCerrar"    runat="server" CssClass="btn btn-secondary" ToolTip="Cerrar"        OnClick="FrmPpalUsuarios_BtnCerrar_Click"><i class="fa-regular fa-share-from-square"></i>&nbsp;&nbsp;&nbsp;Cerrar</asp:LinkButton>
                            &nbsp;&nbsp;&nbsp;
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<!-- FIN FORMULARIO -->




<!-- FORMULARIO ELIMINAR-->
<asp:Button ID="FrmDelete_BtnVer" runat="server" Style="display:none" />
<asp:ModalPopupExtender ID="FrmDelete" runat="server" TargetControlID="FrmDelete_BtnVer" PopupControlID="FrmDelete_Panel" BackgroundCssClass="TeraModalBackground"></asp:ModalPopupExtender>
<asp:Panel ID="FrmDelete_Panel" runat="server">
    <asp:UpdatePanel ID="FrmDelete_UpdatePanel" runat="server">
        <ContentTemplate>
            <div class="modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div id="FrmDelete_dialog" class="modal-dialog modal-dialog-centered" style="display:none;" runat="server">
                    <div class="modal-content">
                        <div class="modal-header bg-danger text-white">
                            <h4 ID="FrmDeleteTitulo" class="modal-title fs-5" runat="server"></h4>
                        </div>                
                        <div class="modal-body">
                            <asp:TextBox ID="FrmDelete_TxtId" style="display:none;" runat="server" />
                            <asp:Label ID="FrmDelete_Msg" runat="server"></asp:Label>

                            <br /><br />
                            <p class="text-center"><b>¿DESEA CONTINUAR?</b></p>

                        </div> <%--modal-body--%>
                
                        <div class="modal-footer bg-light" style="border-top:1px solid #535362 !important;">
                            <asp:LinkButton ID="FrmDelete_BtnCerrar"    runat="server" CssClass="btn btn-secondary btn-sm" ToolTip="Cerrar"        OnClick="FrmDelete_BtnCerrar_Click"><i class="fa-regular fa-share-from-square"></i>&nbsp;&nbsp;&nbsp;Cerrar</asp:LinkButton>
                            &nbsp;&nbsp;&nbsp;
                            <asp:LinkButton ID="FrmDelete_BtnEliminar"    runat="server" CssClass="btn btn-danger btn-sm"   ToolTip="Actualizar"    OnClick="FrmDelete_BtnEliminar_Click"><i class='fa-solid fa-pen-to-square'></i>&nbsp;&nbsp;&nbsp;Elimnar</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<!-- FIN FORMULARIO -->

<!-- MENSAJES -->
<asp:Button ID="FrmMsg_BtnVer" runat="server" Text="" style="display:none;" />
<asp:ModalPopupExtender ID="FrmMsg" runat="server" TargetControlID="FrmMsg_BtnVer" PopupControlID="FrmMsg_UpdatePanel" BackgroundCssClass="TeraModalBackground"></asp:ModalPopupExtender>
<asp:Panel ID="FrmMsg_Panel" runat="server">
    <asp:UpdatePanel ID="FrmMsg_UpdatePanel" runat="server">
        <ContentTemplate>
            <div class="modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div id="FrmMsg_dialog" class="modal-dialog modal-lg modal-dialog-centered" style="display:none;" runat="server">
                    <div class="modal-content">
                        <div id="FrmMsg_TipoMensaje" class="" style="color:#FFF !important" runat="server">                            
                            <h4 class="modal-title fs-5" style="color:#FFF !important;">
                                    <i ID="FrmMsg_TipoMensajeIcono" class="" runat="server"></i>&nbsp;
                                    <asp:Label ID="FrmMsg_LblTitulo" runat="server" Text=""></asp:Label>
                            </h4>
                        </div>

                        <div class="modal-body">
                            <asp:Label ID="FrmMsg_LblCuerpo" runat="server" Text=""></asp:Label>
                        </div> <%--modal-body--%>

                        <div class="modal-footer bg-light" style="border-top:1px solid #535362 !important;">
                            <asp:Button ID="FrmMsg_BtnAceptar" Width="50px" runat="server" Text="OK" CssClass="" OnClick="FrmMsg_BtnAceptar_Click" data-bs-dismiss="modal" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<!-- FIN MENSAJES -->




<script>
    function init() {
        $(document).ready(function () {
            var select2 = $('.select2').select2({
                placeholder: $(this).data('placeholder'),
                dropdownParent: $("#modal-FrmPrincipal")

            });
            var select2Add = $('.select2-add').select2({
                placeholder: $(this).data('placeholder'),
                dropdownParent: $("#modal-FrmUsuarios")

            });

        })
    }


    init()
</script>


</asp:Content>
