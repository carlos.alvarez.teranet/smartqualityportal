﻿////V1.0
function loadingModal(IdParam, idPanel) {
    var myModal = new bootstrap.Modal(document.getElementById(IdParam), {
        keyboard: false
    })
    myModal.show()
    ModalPopupJs.push(myModal);
    $(document).ready(function () {
        $("#" + idPanel).fadeIn();
    })

};

function closeModal(IdParam) {
    console.log(ModalPopupJs);
    ModalPopupJs.forEach(function (elemento, indice, array) {
        if (elemento._element.id == IdParam) {
            elemento.hide();
            ModalPopupJs.splice(indice, 1);
        }
    })
}

