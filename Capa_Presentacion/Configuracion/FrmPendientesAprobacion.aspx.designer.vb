﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class FrmPendientesAprobacion

    '''<summary>
    '''Control PnProgress.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents PnProgress As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control UpProgress.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents UpProgress As Global.System.Web.UI.UpdateProgress

    '''<summary>
    '''Control ModalProgress.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ModalProgress As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''Control UpdatePanelIndex.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents UpdatePanelIndex As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control PanelIndex.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents PanelIndex As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control FrmPpal_TxtIdCarpeta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_TxtIdCarpeta As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control BtnFilter_row.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents BtnFilter_row As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control TxtBsc.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents TxtBsc As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control BtnBsc.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents BtnBsc As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control GrillaPpal.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GrillaPpal As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control FrmPpal_BtnVer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_BtnVer As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control FrmPpal.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''Control FrmPpal_Panel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_Panel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control FrmPpal_UpdatePanel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_UpdatePanel As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control FrmPpal_dialog.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_dialog As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpalTitulo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpalTitulo As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control LblCloneBread.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents LblCloneBread As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpal_CboPrefijo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_CboPrefijo As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control FrmPpal_Correlativo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_Correlativo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmPpal_Version.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_Version As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmPpal_TxtIdCritica.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_TxtIdCritica As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control frmPpal_textFechaVencimiento.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents frmPpal_textFechaVencimiento As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control TextBox1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents TextBox1 As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control Ffrm_principal_TabOpc1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Ffrm_principal_TabOpc1 As Global.System.Web.UI.HtmlControls.HtmlButton

    '''<summary>
    '''Control Ffrm_principal_TabOpc2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Ffrm_principal_TabOpc2 As Global.System.Web.UI.HtmlControls.HtmlButton

    '''<summary>
    '''Control Ffrm_principal_TabOpc3.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Ffrm_principal_TabOpc3 As Global.System.Web.UI.HtmlControls.HtmlButton

    '''<summary>
    '''Control CriticaPanelTab.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CriticaPanelTab As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpal_panelEdit.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_panelEdit As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control Frm_CmbVariablesAccion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Frm_CmbVariablesAccion As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control Frm_TxtValor.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Frm_TxtValor As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control Frm_CmbVariablesCondicion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Frm_CmbVariablesCondicion As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control Frm_CmbOperadorLogico.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Frm_CmbOperadorLogico As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control Frm_TxtValorCondicion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Frm_TxtValorCondicion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmlPpal_OpcAndOr.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmlPpal_OpcAndOr As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control frmPpal_lbelTranscripcionCampo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents frmPpal_lbelTranscripcionCampo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control frmPpal_lbelOperadorLogico.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents frmPpal_lbelOperadorLogico As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control frmPpal_lbelValorCondicion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents frmPpal_lbelValorCondicion As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control frmPpal_lbelAndOr.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents frmPpal_lbelAndOr As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control FrmlPpal_IndexUpdate.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmlPpal_IndexUpdate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmPpal_BtntraspasaTranscripcionCondicion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_BtntraspasaTranscripcionCondicion As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control GridViewCondiciones.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GridViewCondiciones As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control FrmPpal_ResultLabelTransaccion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_ResultLabelTransaccion As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control FrmPpal_CmbWorkFlowAprobacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_CmbWorkFlowAprobacion As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control FrmPpal_Justificacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_Justificacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmPpal_panelNoEdit.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_panelNoEdit As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control GridViewCondicionesNoEdit.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GridViewCondicionesNoEdit As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control FrmPpal_ResultLabelTransaccionNoEdit.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_ResultLabelTransaccionNoEdit As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control FrmPpal_CmbWorkFlowAprobacionNoEdit.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_CmbWorkFlowAprobacionNoEdit As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control FrmPpal_TxtJustificacionNoEdit.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_TxtJustificacionNoEdit As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control GrillaRutaAprobacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GrillaRutaAprobacion As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control comentarioPanelTab.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents comentarioPanelTab As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control LogPanelTab.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents LogPanelTab As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control pathBrath.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pathBrath As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpal_LogCritica.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_LogCritica As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control FrmPpal_AutorLog.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_AutorLog As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpal_FechaCreacionLog.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_FechaCreacionLog As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpal_EstadoActualLog.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_EstadoActualLog As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpal_BtnCerrar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_BtnCerrar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmPpal_BtnAprobar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_BtnAprobar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmPpal_BtnRechazar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_BtnRechazar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmComentarios_BtnVer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmComentarios_BtnVer As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control FrmComentarios.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmComentarios As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''Control FrmComentarios_Panel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmComentarios_Panel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control FrmComentarios_UpdatePanel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmComentarios_UpdatePanel As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control FrmComentarios_dialog.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmComentarios_dialog As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmlPpal_traspasaComentario.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmlPpal_traspasaComentario As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmComentarios_BtnCerrar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmComentarios_BtnCerrar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmComentarios_BtnComentar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmComentarios_BtnComentar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmAprueboRechazo_BtnVer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmAprueboRechazo_BtnVer As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control FrmAprueboRechazo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmAprueboRechazo As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''Control FrmAprueboRechazo_Panel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmAprueboRechazo_Panel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control FrmAprueboRechazo_UpdatePanel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmAprueboRechazo_UpdatePanel As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control FrmAprueboRechazo_dialog.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmAprueboRechazo_dialog As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmAprueboRechazo_header.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmAprueboRechazo_header As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmAprueboRechazo_headerTitle.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmAprueboRechazo_headerTitle As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmAprueboRechazo_txtOpcion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmAprueboRechazo_txtOpcion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmAprueboRechazo_txtAprueboRechazo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmAprueboRechazo_txtAprueboRechazo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmAprueboRechazo_BtnCerrar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmAprueboRechazo_BtnCerrar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmAprueboRechazo_BtnGuardar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmAprueboRechazo_BtnGuardar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmDelete_BtnVer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete_BtnVer As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control FrmDelete.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''Control FrmDelete_Panel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete_Panel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control FrmDelete_UpdatePanel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete_UpdatePanel As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control FrmDelete_dialog.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete_dialog As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmDeleteTitulo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDeleteTitulo As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmDelete_TxtId.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete_TxtId As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmDelete_Msg.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete_Msg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control FrmDelete_BtnCerrar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete_BtnCerrar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmDelete_BtnEliminar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete_BtnEliminar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmMsg_BtnVer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_BtnVer As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control FrmMsg.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''Control FrmMsg_Panel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_Panel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control FrmMsg_UpdatePanel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_UpdatePanel As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control FrmMsg_dialog.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_dialog As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmMsg_TipoMensaje.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_TipoMensaje As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmMsg_TipoMensajeIcono.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_TipoMensajeIcono As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmMsg_LblTitulo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_LblTitulo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control FrmMsg_LblCuerpo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_LblCuerpo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control FrmMsg_BtnAceptar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_BtnAceptar As Global.System.Web.UI.WebControls.Button
End Class
