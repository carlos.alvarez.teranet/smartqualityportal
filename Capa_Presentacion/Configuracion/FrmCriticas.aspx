﻿<%@ Page Title="" ValidateRequest="false" Language="vb" AutoEventWireup="false" MasterPageFile="~/GlbMaster.Master" CodeBehind="FrmCriticas.aspx.vb" Inherits="Capa_Presentacion.FrmCriticas" %>
<%@ Register Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" TagPrefix="asp" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../assets/js/jsUpdateProgress.js"></script>

    <!-- Font Lato + css -->
    <link href="../assets/css/lato.css?v=v1" rel="stylesheet" />
    <link href="../assets/css/complementos.css?v=v1.2" rel="stylesheet" />
    
    <link href="../assets/css/FrmCriticas.css?v=v4" rel="stylesheet" />
    <script src='https://code.jquery.com/ui/1.11.4/jquery-ui.min.js'></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.27.3/ui/trumbowyg.min.css"/>


    <script type="text/javascript">
        var ModalProgress = '<%= ModalProgress.ClientID %>';
    </script>  

    <asp:Panel ID="PnProgress" runat="server">
        <asp:UpdateProgress ID="UpProgress" runat="server">
            <ProgressTemplate>
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center text-primary">
                                <img src='../assets/images/loadingLogin.gif' style='max-width:50px;'>
                            </div>
                        </div>
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </asp:Panel>
    <asp:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="PnProgress" BackgroundCssClass="TeraModalBackground" PopupControlID="PnProgress"  />
    

    <!-- MOSTRAR DATOS EN EL GRIDVIEW -->
    <asp:UpdatePanel runat="server" ID="UpdatePanelIndex">
        <ContentTemplate>
            <asp:Panel runat="server" ID="PanelIndex" CssClass="center-block">
                <div class="container-fluid">
                    <br />
                    <div class="row shadow-sm rounded">
                        <div class="col-11">                       
                            <h2 style="color:#26BD75; font-family:'Lato'; font-weight:900 !important;">Mantenedor <span class="badge text-bg-warning">CRITICAS</span></h2>
                        </div>
                        <div class="col-1">
                            <span class="float-end">
                                <asp:LinkButton 
                                    ID="BtnAddRegistro" 
                                    runat="server" 
                                    visible="false"
                                    CssClass="btn btn-success btn-addButton" 
                                    OnClick="BtnAddRegistro_Click">
                                       <i class="fa-solid fa-plus"></i>
                                </asp:LinkButton>
                            </span>
                        </div>
                    </div>
                    

                    <div class="row">
                        <div class="col-12 col-md-2 bg-color-folder">
                            <h6 class=" mt-3"><i class="fa-solid fa-wrench"></i> Todo</h6>
                            <div class="mt-4" style="min-height: 85vh">
                                <asp:TreeView ID="CarpetasTreeView" runat="server"
                                    ExpandImageUrl="~/assets/images/folder-solid.png"  
                                    CollapseImageUrl="~/assets/images/folder-open-solid.png"
                                    LeafNodeStyle-ImageUrl="~/assets/images/folder-solid.png"
                                    CssClass="ForTreeView"
                                    NodeWrap="true"
                                    ShowLines="false"
                                    Font-Size="Small"   
                                    BorderStyle="None"
                                    BackColor="Transparent"
                                    ForeColor="#555555"
                                    NodeStyle-HorizontalPadding="2px" 
                                    NodeStyle-VerticalPadding="2px" 
                                    NodeStyle-ChildNodesPadding="2px"
                                    SelectedNodeStyle-Font-Bold="false" 
                                    SelectedNodeStyle-ForeColor="#000000" 
                                    SelectedNodeStyle-BackColor="#FDFDEA" 
                                    SelectedNodeStyle-Font-Italic="true">
                                </asp:TreeView>

                                <asp:TextBox ID="FrmPpal_TxtIdCarpeta"      Visible="false" runat="server" />
                                <asp:TextBox ID="FrmPpal_TxtIdCarpetaPadre" Visible="false" runat="server" />
                                <asp:TextBox ID="FrmPpal_TxtNivel"          Visible="false" runat="server" />
                                <asp:TextBox ID="FrmPpal_TxtSwTipo"         Visible="false" runat="server" />
                                <asp:TextBox ID="FrmPpal_LastOptionSelected" Visible="false" runat="server" />

                                </div>
                            </div>

                            <div class="col-12 col-md-10">
                                <div class="row">
                                    <div class="col-12" style="padding-top:8px;padding-bottom:-10px;">
                                        <div id="DivBreadcrumbsFolder" runat="server">
                                            <ol class="breadcrumb">
                                                <li><img src="../assets/images/folder-solid.png" alt="" style="border-width:0; width: 16px; margin-right:4px; margin-top:-3px;"></li>
                                                                
                                                        
                                            </ol>
                                        </div>
                                    </div>
                                </div>  
                                
                                <div class="row " runat="server" ID="BtnFilter_row" visible="false">
                                    <div class="col-6">
                                        <div class="input-group mb-3">
                                            <label class="input-group-text border-secondary text-white bg-dark" for="TxtBsc"><i class="fa-solid fa-magnifying-glass"></i>&nbsp;Buscar&nbsp;&nbsp;</label>
                                            <asp:TextBox ID="TxtBsc" CssClass="form-control border-secondary form-control-sm"
                                                placeholder="para buscar valores múltiples utilice el separador ;" style="font-size: 1.1rem !important;"
                                                onkeypress="if ((event || window.event).keyCode == 13) { var node = (event || window.event).target || (event || window.event).srcElement; if (node.type == 'text' && node.id == 'ContentPlaceHolder1_TxtBsc') { return false; } }"
                                                autocomplete="off" runat="server"></asp:TextBox>
                                            <asp:LinkButton ID="BtnBsc" CssClass="btn btn-dark"  OnClick="BtnBsc_Click" runat="server">
                                                &nbsp;&nbsp;<i class="fa-brands fa-golang" style="height:1.8em !important; vertical-align: inherit !important;"></i>&nbsp;&nbsp;
                                            </asp:LinkButton>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <div class="gradientLineKantar" style="float:left;"></div>
                                        <asp:GridView 
                                            ID="GrillaPpal" 
                                            runat="server" 
                                            CssClass="TeraGridview table table-bordered bg-light"
                                            Style="text-align:center; vertical-align:middle; font-size:1rem; max-width:1600px; margin-left: 0px; width:98% !important;"
                                                                    
                                            PageSize="50" AllowPaging="true" AutoGenerateColumns="false" 
                                            HorizontalAlign="Center" 
                                            AllowSorting="true"
                                            OnRowDataBound="GrillaPpal_RowDataBound">
                                            <HeaderStyle 
                                                CssClass="bg-dark text-center text-white" 
                                                Font-Size="Medium"  Font-Bold="false" />                                                             
                            
                                            <Columns>                                    
                                                <%-- 0 --%>
                                                <asp:TemplateField HeaderText="" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblIdCritica" runat="server" Text='<%# Eval("IdCritica") %>'></asp:Label>
                                                        <asp:Label ID="LblIdCarpeta" runat="server" Text='<%# Eval("IdCarpeta") %>'></asp:Label>
                                                                        
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%-- 1 --%>
                                                <asp:TemplateField HeaderText="<span class='link-light'>Código</span>" ItemStyle-Width="150">
                                                    <ItemTemplate>
                                                        <asp:Label ID="LblCodigo" runat="server" Text='<%# Eval("Codigo") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%-- 2 --%>
                                                <asp:TemplateField HeaderText="<span class='link-light'>Estado </span>">
                                                    <ItemTemplate>
                                                        
                                                        <asp:Label ID="LblEstado" runat="server"  Text='<%# Eval("Estado") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%-- 3 --%>
                                                <asp:TemplateField HeaderText="<span class='link-light'>Critica </span>" > 
                                                    <ItemTemplate>
                                                       <asp:Label ID="LblCondicion" runat="server" Text='<%# Eval("Condicion") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>


                                                <%-- 4 --%>
                                                <asp:TemplateField HeaderText="<i class='fa-solid fa-pen-to-square'></i>" >
                                                    <ItemTemplate>
                                                        <asp:LinkButton runat="server" OnClick ="GrillaPpal_BtnEditar_Click" CssClass="btn btn-sm btn-warning"><i class='fa-solid fa-pen-to-square'></i></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>

                                            <PagerTemplate>                                    
                                                <nav style="float:right;">
                                                    <div class="btn-group" role="group">
                                                        <asp:LinkButton ID="GrillaPpal_BtnFirst"    runat="server" CssClass="btn btn-outline-light btn-sm" Text="Primera Página"      ToolTip="Primera Página"    OnClick="GrillaPpal_BtnFirst_Click"><i class="fa-solid fa-backward-fast"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="GrillaPpal_BtnPrev"     runat="server" CssClass="btn btn-outline-light btn-sm" Text="Página Anterior"     ToolTip="Página Anterior"   OnClick="GrillaPpal_BtnPrev_Click"><i class="fa-solid fa-arrow-left"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="GrillaPpal_BtnNext"     runat="server" CssClass="btn btn-outline-light btn-sm" Text="Página Siguiente"    ToolTip="Página Siguiente"  OnClick="GrillaPpal_BtnNext_Click"><i class="fa-solid fa-arrow-right"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="GrillaPpal_BtnLast"     runat="server" CssClass="btn btn-outline-light btn-sm" Text="Última Página"       ToolTip="Última Página"     OnClick="GrillaPpal_BtnLast_Click"><i class="fa-solid fa-forward-fast"></i></asp:LinkButton>
                                                    </div>
                                                    <div class="justify-content-center text-white" style="font-size:0.8rem">
                                                        <asp:Label ID="GrillaPpal_CurrentPageLabel" runat="server" />                            
                                                    </div>
                                                </nav>
                                            </PagerTemplate>


                                            
                                            <EmptyDataTemplate>
                                                <div class="alert alert-warning align-items-center" role="alert">
                                                    <h1><i class="fa-solid fa-triangle-exclamation"></i></h1>
                                                    <div>No existen registros para mostrar.</div>
                                                </div>
                                            </EmptyDataTemplate>                                
                                        </asp:GridView>
                                    </div>
                                </div>
                                       
                            </div>
                        </div>
                    </div> 

                    <br />
                    
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
<!-- FIN MOSTRAR DATOS EN EL GRIDVIEW -->


<!-- FORMULARIO -->
<asp:Button ID="FrmPpal_BtnVer" runat="server" Style="display:none" />
<asp:ModalPopupExtender ID="FrmPpal" runat="server" TargetControlID="FrmPpal_BtnVer" PopupControlID="FrmPpal_Panel" BackgroundCssClass="TeraModalBackground"></asp:ModalPopupExtender>
<asp:Panel ID="FrmPpal_Panel" runat="server">
    <asp:UpdatePanel ID="FrmPpal_UpdatePanel" runat="server">
        <ContentTemplate>
            <div class="modal" id="modal-FrmPrincipal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div id="FrmPpal_dialog" class=" modal-dialog modal-xl  modal-dialog-scrollable" style="display:none;" runat="server">
                    <div class="modal-content">
                        <div class="modal-header text-center bg-dark text-white">
                             <div class="container-fluid ">
                                <div class="row" style ="width :100%">
                                    <div class="col-12 text-center">
                                        <h4 class="modal-title fs-5">
                                           <h4 ID="FrmPpalTitulo" class="modal-title fs-5" runat="server"></h4>
                                        </h4>
                                    </div>
                                </div> 
                                <div class="row" style ="margin-top :-20px;margin-bottom:-10px">
                                    <div class ="col-6 text-start breadcrumb_frm" runat="server" ID="LblCloneBread">
                                        <ol class=" breadcrumb">
                                            <li><img src="../assets/images/folder-solid.png" alt="" style="border-width:0; width: 16px; margin-right:4px; margin-top:-3px;"> Todo</li>
                                        </ol>
                                    </div>
                                    
                                    <div class ="col-2 offset-4 text-end ">
                                        <div class="input-group">
                                            <asp:DropDownList  CssClass="custom-select" style="min-width :100px;" placeholder="<<Seleccionar>>" ID="FrmPpal_CboPrefijo" runat="server" ></asp:DropDownList>
                                            <label class="input-group-text text-success " for="FrmPpal_CboPrefijo"><%=FrmPpal_Correlativo_label%>-V1</label>
                                            <asp:TextBox  runat="server" ID="FrmPpal_Correlativo" Visible="false"></asp:TextBox>
                                            <asp:TextBox  runat="server" ID="FrmPpal_Version" Visible="false"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            

                            
                        </div>
                
                        <div class="modal-body">
                            <asp:TextBox ID="FrmPpal_TxtIdCritica" style="display:none;" runat="server" />
                            <asp:TextBox ID="FrmPpal_TxtIdTemp"   style="display:none;" runat="server" />

                            <div class="row ">
                                <div class="col-12">
                                    <div class="float-end" style="margin-left:-420px; margin-right:20px; width:390px;">
                                        <div class="row g-2 align-items-center">
                                            <div class="col-auto">
                                            <label for="inputPassword6" class="col-form-label"><small>Fecha de Vencimiento o Revisión</small></label>
                                            </div>
                                            <div class="col-auto">
                                                <asp:TextBox ID="frmPpal_textFechaVencimiento" CssClass="form-control input-sm" TextMode="Date" MaxLength="10" runat="server" />
                                            </div>
                                            <asp:TextBox runat="server" ID="TextBox1" Visible="false" ></asp:TextBox>
                                        </div>
                                    </div>
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <button class="nav-link active" runat="server" ID="Ffrm_principal_TabOpc1" data-bs-toggle="tab" data-bs-target="#ContentPlaceHolder1_CriticaPanelTab" type="button" role="tab" aria-controls="ContentPlaceHolder1_CriticaPanelTab" aria-selected="true">Crítica</button>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <button class="nav-link" runat="server" ID="Ffrm_principal_TabOpc2"  data-bs-toggle="tab" data-bs-target="#ContentPlaceHolder1_comentarioPanelTab" type="button" role="tab" aria-controls="ContentPlaceHolder1_comentarioPanelTab" aria-selected="false">Comentarios</button>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <button class="nav-link" runat="server" ID="Ffrm_principal_TabOpc3" data-bs-toggle="tab" data-bs-target="#ContentPlaceHolder1_LogPanelTab" type="button" role="tab" aria-controls="ContentPlaceHolder1_LogPanelTab" aria-selected="false">Log</button>
                                        </li>
                                    </ul>

                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade show active" runat="server" id="CriticaPanelTab" role="tabpanel" aria-labelledby="Crítica-tab">
                                            <div class ="container-fluid">
                                                <asp:Panel runat="server" ID="FrmPpal_panelEdit" Visible="true" >
                                                    <div class="row mt-2">
                                                    
                                                    
                                                        <div class="col-7">
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <h5>Acción:</h5>
                                                                </div>

                                                                <div class="col-6">
                                                                    <label for="Frm_CmbVariables" class="form-label text-success">Variable: </label>
                                                                    <asp:DropDownList 
                                                                        CssClass=" form-select select-Frm_CmbVariables" 
                                                                        AutoPostBack="True" 
                                                                       
                                                                        ID="Frm_CmbVariablesAccion"  
                                                                        onselectedindexchanged="getDetailVariableAccion"
                                                                        runat="server" 
                                                                      
                                                                        >
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class="col-4">
                                                                    <label for="Frm_CmbVariables" class="form-label text-success">Valor: </label>
                                                                        <asp:TextBox  
                                                                            class="form-control campoAccionChange valorAccionClass" 
                                                                            ID="Frm_TxtValor"  
                                                                            OnTextChanged="getDetailVariableAccion"
                                                                            AutoPostBack='true'
                                                                            runat="server"/>
                                                                </div>
                                                                <div class="col-12">
                                                                    <hr />
                                                                </div>

                                                                <div class="col-12">
                                                                    <h5>Condición:</h5>
                                                                </div>

                                                                <div class ="col-3">
                                                                    <label for="Frm_CmbVariablesCondicion" class="form-label text-success">Campo: </label>
                                                                    <asp:DropDownList 
                                                                        onselectedindexchanged="getOperadorLogistico"  
                                                                        AutoPostBack="True" 
                                                                        CssClass=" form-select campoCondicionChange Frm_CmbVariablesCondicion"   
                                                                        ID="Frm_CmbVariablesCondicion" runat="server" >
                                                                    </asp:DropDownList>
                                                                </div>


                                                                <div class ="col-4">
                                                                    <label for="Frm_CmbVariablesCondicion" class="form-label text-success">Operador Lógico: </label>
                                                                    <asp:DropDownList  
                                                                        onselectedindexchanged="setOperadorLogistico"  
                                                                        AutoPostBack="True" 
                                                                        CssClass="form-select Frm_CmbOperadorLogico campoCondicionChange"  
                                                                        ID="Frm_CmbOperadorLogico" 
                                                                        runat="server" >
                                                                    </asp:DropDownList>
                                                                </div>

                                                                <div class ="col-3">
                                                                    <label for="Frm_TxtValor" class="form-label text-warning">Valor</label>
                                                                    <asp:TextBox  
                                                                        class="form-control campoCondicionChange ValorCondicion" 
                                                                         OnTextChanged="setValorCondicion"
                                                                         AutoPostBack='true'
                                                                        ID="Frm_TxtValorCondicion"  runat="server"/>
                                                                </div>

                                                                <div class="col-2 align-bottom">
                                                                    <div class="btn-group" style="margin-top:30px" role="group" aria-label="Basic mixed styles example">
                                                                      <asp:LinkButton runat="server" CssClass="btn btn-outline-secondary" OnClick="setAND">And</asp:LinkButton>
                                                                      <asp:LinkButton runat="server" CssClass="btn btn-outline-secondary" OnClick="setOR">Or</asp:LinkButton>
                                                                      <asp:TextBox runat="server" style="display:none" AutoPostBack="true"  ID="FrmlPpal_OpcAndOr" CssClass="FrmlPpal_OpcAndOr"></asp:TextBox>
                                                                    </div>                
                                                                </div>

                                                                <div class="col-12 mt-4">
                                                                    <div class="input-group">
                                                                        <label class="input-group-text" style="width:90%" id="transcripcionCondicionSet" for="inputGroupSelect01">
                                                                            <asp:Label runat="server" ID="frmPpal_lbelTranscripcionCampo" style="color:blue;font-weight:bold;"></asp:Label>&nbsp;
                                                                            <asp:Label runat="server" ID="frmPpal_lbelOperadorLogico" style="color:red;font-weight:bold;"></asp:Label>&nbsp;
                                                                            <asp:Label runat="server" ID="frmPpal_lbelValorCondicion" style="color:green;font-weight:bold;"></asp:Label>&nbsp;
                                                                            <asp:Label runat="server" ID="frmPpal_lbelAndOr" CssClass ="resultAndOr" style="color:black;font-weight:bold;"></asp:Label>
                                                                            <asp:TextBox value="" Visible="false" ID="FrmlPpal_IndexUpdate" runat="server" ></asp:TextBox>
                                                                        </label>
                                                                        <asp:LinkButton runat="server" style="width:10%" Visible="false" CssClass="btn btn-success" ID="FrmPpal_BtntraspasaTranscripcionCondicion" onclick="FrmPpal_BtntraspasaTranscripcionCondicion_click" type="button"> <i class="fa-solid fa-plus"></i> Add</asp:LinkButton>
                                                                    </div>
                                                                </div>


                                                                <div class="col-12 mt-4">
                                                                    <div class="card">
                                                                        <div class="card-header">
                                                                            Transcripción Sentencia SQL:
                                                                        </div>

                                                                        <div class="card-body" style ="min-height :100px">
                                                                            <table class="no-style" style="width:100%">
                                                                                <tr>
                                                                                    <td>IF (</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:GridView ID="GridViewCondiciones" runat="server" CssClass=" table table-bordered table-order-criticas"
                                                                                            Style="width:100% !important;"
                                                                                            PageSize="50" AllowPaging="true" AutoGenerateColumns="false" 
                                                                                            HorizontalAlign="Center" AllowSorting="true"
                                                                                            OnRowDataBound="GridViewCondiciones_RowDataBound">
                                                                                        <HeaderStyle CssClass="" Font-Size="Medium"   />
                                                                                            <Columns>                                    
                                                                                                <%-- 0 --%>
                                                                                                <asp:TemplateField   HeaderText="" Visible="true">
                                                                                                    <ItemTemplate  >
                                                                                                        <div style="display:none">
                                                                                                            <asp:Label ID="LblIdFilaCondicion"  runat="server" Text='<%# Eval("IdCondicion") %>'></asp:Label>
                                                                                                        </div>
                                                                                                        <div class="bg-icon-order"></div>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>

                                                                                                 <%-- 1 --%>
                                                                                                <asp:TemplateField HeaderText="Condición" Visible="true">
                                                                                                    <ItemTemplate>
                                                                                                        <div style="width:350px;">
                                                                                                            <%# Eval("CadenaCondicion") %>
                                                                                                        </div>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>

                                                                                                 <%-- 2 --%>
                                                                                                <asp:TemplateField HeaderText="" Visible="true" >
                                                                                                    <ItemTemplate>
                                                                                                        <asp:LinkButton runat ="server" OnClick="FrmPpal_BtnEditar_Click" CssClass="btn-sm btn btn-warning btn-edit-disabled" type="button"><i class="fa-solid fa-pen-to-square"></i></asp:LinkButton>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>

                                                                                                 <%-- 3 --%>
                                                                                                <asp:TemplateField HeaderText="" Visible="true">
                                                                                                    <ItemTemplate>
                                                                                                        <asp:LinkButton runat ="server"  OnClick="FrmPpal_BtnEliminar_Click"  CssClass="btn-sm btn btn-danger btn-edit-disabled"><i class="fa-solid fa-trash-can"></i></asp:LinkButton>

                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>

               

                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>)</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Begin</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label runat="server" ID ="FrmPpal_ResultLabelTransaccion"></asp:Label>&nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>End</td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                        

                                                            </div>
                                                        </div>
                                                        <div class="col-5">
                                                        
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <h5>Workflow de Aprobación:</h5>
                                                            </div>
                                                            <div class="col-8">
                                                                <label for="FrmPpal_CmbWorkFlowAprobacion" class="form-label text-success">Ruta: </label>
                                                                <asp:DropDownList 
                                                                    CssClass=" form-select select-Frm_CmbVariables" 
                                                                    ID="FrmPpal_CmbWorkFlowAprobacion"  
                                                                    runat="server" 
                                                                    style="width:100%">
                                                                </asp:DropDownList>
                                                            </div>
                                                        </div>



                                                        <div class="row">
                                                            <div class="col-12">
                                                                <hr />
                                                            </div>
                                                            <div class="col-12">
                                                                <h5>Justificación:</h5>
                                                            </div>
                                                            <div class="col-12">
                                                                <textarea class="FrmPpal_TxtJustificacion"></textarea>
                                                                <asp:TextBox runat="server" ID="FrmPpal_Justificacion" style="display:none;" CssClass="FrmPpal_TxtJustificacion_texto"></asp:TextBox>

                                                            </div>
                                                        </div>
                                                        
                                                       

                                                    </div>
                                                    </div>
                                                </asp:Panel>
                                                 
                                                <asp:Panel runat="server" ID="FrmPpal_panelNoEdit" Visible="false" >
                                                    <div class="row mt-2">
                                                        <div class="col-7">
                                                            <div class="row">
                                                                
                                                                


                                                                <div class="col-12 mt-4">
                                                                    <div class="card">
                                                                        <div class="card-header">
                                                                            Transcripción Sentencia SQL:
                                                                        </div>

                                                                        <div class="card-body" style ="min-height :100px">
                                                                            <table class="no-style" style="width:100%">
                                                                                <tr>
                                                                                    <td>IF (</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:GridView ID="GridViewCondicionesNoEdit" runat="server" CssClass="" 
                                                                                            Style="width:100% !important; border:0 !important; margin-top:-20px"
                                                                                            PageSize="50" AllowPaging="true" AutoGenerateColumns="false" 
                                                                                            HorizontalAlign="Center" AllowSorting="true"
                                                                                            >
                                                                                        <HeaderStyle CssClass=""  />
                                                                                            <Columns>                                    
                                                                                                <%-- 0 --%>
                                                                                                <asp:TemplateField HeaderText="" Visible="true">
                                                                                                    <ItemTemplate>
                                                                                                        <div style="width:350px; margin-left:30px">
                                                                                                            <%# Eval("CadenaCondicion") %>
                                                                                                        </div>
                                                                                                    </ItemTemplate>
                                                                                                </asp:TemplateField>

                                                                                            </Columns>
                                                                                        </asp:GridView>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>)</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>Begin</td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>
                                                                                        <asp:Label runat="server" ID ="FrmPpal_ResultLabelTransaccionNoEdit" style="margin-left:30px"></asp:Label>&nbsp;
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td>End</td>
                                                                                </tr>
                                                                            </table>
                                                                        </div>
                                                                    </div>

                                                                 
                                                                    
                                                                </div>
                                                        

                                                            </div>
                                                        </div>
                                                        <div class="col-5">
                                                        
                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <h5>Workflow de Aprobación:</h5>
                                                                </div>
                                                                <div class="col-8">
                                                                    <label for="FrmPpal_CmbWorkFlowAprobacionNoEdit" class="form-label text-success">Ruta: </label>
                                                                    <asp:label 
                                                                        ID="FrmPpal_CmbWorkFlowAprobacionNoEdit"  
                                                                        runat="server" 
                                                                        style="width:100%">
                                                                    </asp:label>
                                                                </div>
                                                            </div>


                                                            <div class="row">
                                                                <div class="col-12">
                                                                    <hr />
                                                                </div>
                                                                <div class="col-12">
                                                                    <h5>Justificación:</h5>
                                                                </div>
                                                                <div class="col-12">
                                                                    <asp:label runat="server" ID="FrmPpal_TxtJustificacionNoEdit"></asp:label>
                                                                
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-6 mt-3">
                                                            <hr />
                                                            <h5>Ruta de Aprobación:</h5>
                                                            <asp:GridView 
                                                                ID="GrillaRutaAprobacion" runat="server" 
                                                                CssClass="table table-bordered bg-light" 
                                                                Style="text-align:center; vertical-align:middle; font-size:1rem; max-width:1600px; margin-left: 0px; width:98% !important;"
                                                                PageSize="50" AllowPaging="true" AutoGenerateColumns="false" 
                                                                HorizontalAlign="Center" AllowSorting="true"
                                                            >
                                                                <HeaderStyle 
                                                                    CssClass="bg-dark text-center text-white" 
                                                                    Font-Size="Medium" Font-Bold="false"  
                                                                />
                                                                <Columns>                                    
                                                                    <%-- 0 --%>
                                                                    <asp:TemplateField HeaderText="<span class='link-light'>Posición</span>" Visible="false"  ItemStyle-Width="50">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="LblPosicionRuta" runat="server" Text='<%# Eval("PosicionRuta") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <%-- 1 --%>
                                                                    <asp:TemplateField HeaderText="<span class='link-light'>JobPosition</span>">
                                                                        <ItemTemplate>
                                                                            <asp:Label ID="LblPosicionRuta" runat="server" style="font-size:small" Text='<%# Eval("JobPosition") %>'></asp:Label>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>

                                                                    <%-- 2 --%>
                                                                    <asp:TemplateField HeaderText="<span class='link-light'>Aprobadores</span>">
                                                                        <ItemTemplate>
                                                                            <div class="list-user">
                                                                                <ol>
                                                                                    <%# Eval("Aprobadores").replace("()", "") %>
                                                                                </ol>
                                                                            </div>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </asp:Panel>
                                            </div>
                                        </div>

                                        <div class="tab-pane fade" runat="server" id="comentarioPanelTab" role="tabpanel" aria-labelledby="comentario-tab">
                                            <div class ="container-fluid">
                                                <div class="row mt-3">
                                                    <div class="col-6">

                                                        <div class="float-end" style="margin-right:30px">
                                                            <asp:LinkButton CssClass="btn btn-success btn-sm" runat="server" OnClick="FrmPpal_Comentario_click">
                                                                <i class="fa-solid fa-comment"></i>
                                                                add comentario
                                                            </asp:LinkButton>
                                                        </div>
                                                        <h5>Comentarios</h5>
                                                        <!--div class="row mt-4"  style="min-height:300px; ">
                                                             <%=ViewState("Comentarios") %>
                                                        </!--div-->
                                                            <div id="GrillaComentario_RegCount" class="TeraGridviewRegCount mw1200" runat="server" style="display:none;"></div>
                                                            <asp:GridView ID="GrillaComentario" runat="server" CssClass="table table-borderless mw1200"
                                            Style="font-size:1rem !important; border-color: transparent;"
                                            PageSize="1000" AllowPaging="true" AutoGenerateColumns="false" 
                                            HorizontalAlign="Center" AllowSorting="true"
                                            ShowHeader="false" ShowFooter="false" PagerSettings-Visible="false"
                                            OnRowDataBound="GrillaComentario_RowDataBound">
                                        <HeaderStyle CssClass="bg-dark text-center text-white" Font-Size="Medium" Font-Bold="false" />                                                             
                                            <Columns>                                    
                                                <%-- 0 --%>
                                                <asp:TemplateField HeaderText="" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="GrillaComentario_LblNroFila"                 runat="server" Text='<%# Eval("NroFila") %>'></asp:Label>
                                                        <asp:Label ID="GrillaComentario_LblIdComentario"            runat="server" Text='<%# Eval("IdComentario") %>'></asp:Label>
                                                        <asp:Label ID="GrillaComentario_LblIdCritica"                runat="server" Text='<%# Eval("IdCritica") %>'></asp:Label>
                                                        <asp:Label ID="GrillaComentario_LblNombre"                  runat="server" Text='<%# Eval("Nombre") %>'></asp:Label>
                                                         <asp:Label ID="GrillaComentario_LblLastTransactionUser"    runat="server" Text='<%# Eval("LastTransactionUser") %>'></asp:Label>
                                                        <asp:Label ID="GrillaComentario_LblFecha"                   runat="server" Text='<%# Eval("Fecha", "{0:dd/MM/yyyy HH:mm}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%-- 1 --%>
                                                <asp:TemplateField HeaderText="<span class='link-light'>Nº</span>" ItemStyle-Width="80">
                                                    <ItemTemplate>
                                                        <div class="bd-callout bd-callout-warning fs-4" style="margin-top:0px !important; margin-bottom: 0px !important;">
                                                            <%# Eval("NroFila") %>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%-- 2 --%>
                                                <asp:TemplateField HeaderText="<span class='link-light'>Adjunto</span>">
                                                    <ItemTemplate>
                                                        <asp:Label ID="GrillaAdjunto_LblInfo" runat="server" Text='<%# Eval("Info") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%-- 3 --%>
                                                <asp:TemplateField HeaderText="<span class='link-light'><i class='fa-solid fa-trash-can'></i></span>" ItemStyle-Width="50" ItemStyle-CssClass="col-grid-action">
                                                    <ItemTemplate>
                                                        <asp:LinkButton CssClass="btn btn-danger btn-sm btn-grid-action"    ID="GrillaComentario_BtnEliminar" OnClick="GrillaComentario_BtnEliminar_Click" runat="server"><i class="fa-solid fa-trash-can"></i></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%-- 1 -%>
                                                <asp:TemplateField HeaderText="<span class='link-light'>Nº</span>" ItemStyle-Width="80">
                                                    <ItemTemplate>
                                                        <div class="bd-callout bd-callout-warning fs-4" style="margin-top:0px !important; margin-bottom: 0px !important;">
                                                            <%# Eval("NroFila") %>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%-- 2 -%>
                                                <asp:TemplateField HeaderText="<span class='link-light'>Comentario</span>">
                                                    <ItemTemplate>
                                                        <asp:Label ID="GrillaComentario_LblComentario" runat="server" Text='<%# Eval("Comentario") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%-- 3 -%>
                                                <asp:TemplateField HeaderText="<span class='link-light'><i class='fa-solid fa-trash-can'></i></span>" ItemStyle-Width="50" ItemStyle-CssClass="col-grid-action">
                                                    <ItemTemplate>
                                                        <asp:LinkButton CssClass="btn btn-danger btn-sm btn-grid-action" ID="GrillaComentario_BtnEliminar" OnClick="GrillaComentario_BtnEliminar_Click" runat="server"><i class="fa-solid fa-trash-can"></i></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField--%>
                                            </Columns>
                                            <PagerTemplate>                                    
                                                <nav style="float:right;">
                                                    <div class="justify-content-center text-white" style="font-size:0.8rem">
                                                        <asp:Label ID="GrillaComentario_CurrentPageLabel" runat="server" />                            
                                                    </div>
                                                </nav>
                                            </PagerTemplate>
                                            <EmptyDataTemplate>
                                                <div class="alert alert-warning align-items-center" role="alert">
                                                    <h1><i class="fa-solid fa-triangle-exclamation"></i></h1>
                                                    <div>No existen comentarios.</div>
                                                </div>
                                            </EmptyDataTemplate>                                
                                        </asp:GridView>
                                    
                                                       
                                                    </div>


                                                    <div class="col-6">
                                                        <div class="float-end" style="margin-right:30px">
                                                            <asp:LinkButton ID="FrmPpal_AddAdjunto"      CssClass="btn btn-success btn-sm" runat="server" OnClick="FrmPpal_AddAdjunto_Click">
                                                                <i class="fa-solid fa-paperclip"></i>
                                                                add archivo
                                                            </asp:LinkButton>
                                                        </div>
                                                        <div id="GrillaAdjunto_RegCount" class="TeraGridviewRegCount mw1200" runat="server" style="display:none;"></div>
                                        <asp:GridView ID="GrillaAdjunto" runat="server" CssClass="table table-borderless mw1200"
                                            Style="font-size:1rem !important; border-color: transparent;"
                                            PageSize="1000" AllowPaging="true" AutoGenerateColumns="false" 
                                            HorizontalAlign="Center" AllowSorting="true"
                                            ShowHeader="false" ShowFooter="false" PagerSettings-Visible="false"
                                            OnRowDataBound="GrillaAdjunto_RowDataBound">
                                        <HeaderStyle CssClass="bg-dark text-center text-white" Font-Size="Medium" Font-Bold="false" />                                                             
                                            <Columns>                                    
                                                <%-- 0 --%>
                                                <asp:TemplateField HeaderText="" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="GrillaAdjunto_LblNroFila"                runat="server" Text='<%# Eval("NroFila") %>'></asp:Label>
                                                        <asp:Label ID="GrillaAdjunto_LblIdAdjunto"              runat="server" Text='<%# Eval("IdAdjunto") %>'></asp:Label>
                                                        <asp:Label ID="GrillaAdjunto_LblIdCritica"              runat="server" Text='<%# Eval("IdCritica") %>'></asp:Label>
                                                        <asp:Label ID="GrillaAdjunto_LblNombre"                 runat="server" Text='<%# Eval("Nombre") %>'></asp:Label>
                                                        <asp:Label ID="GrillaAdjunto_LblAdjName"                runat="server" Text='<%# Eval("AdjName") %>'></asp:Label>
                                                        <asp:Label ID="GrillaAdjunto_LblLastTransactionUser"    runat="server" Text='<%# Eval("LastTransactionUser") %>'></asp:Label>
                                                        <asp:Label ID="GrillaAdjunto_LblFecha"                  runat="server" Text='<%# Eval("Fecha", "{0:dd/MM/yyyy HH:mm}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%-- 1 --%>
                                                <asp:TemplateField HeaderText="<span class='link-light'>Nº</span>" ItemStyle-Width="80">
                                                    <ItemTemplate>
                                                        <div class="bd-callout bd-callout-warning fs-4" style="margin-top:0px !important; margin-bottom: 0px !important;">
                                                            <%# Eval("NroFila") %>
                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%-- 2 --%>
                                                <asp:TemplateField HeaderText="<span class='link-light'>Adjunto</span>">
                                                    <ItemTemplate>
                                                        <asp:Label ID="GrillaAdjunto_LblInfo" runat="server" Text='<%# Eval("Info") %>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

                                                <%-- 3 --%>
                                                <asp:TemplateField HeaderText="<span class='link-light'><i class='fa-solid fa-trash-can'></i></span>" ItemStyle-Width="50" ItemStyle-CssClass="col-grid-action">
                                                    <ItemTemplate>
                                                        <asp:LinkButton CssClass="btn btn-danger btn-sm btn-grid-action"    ID="GrillaAdjunto_BtnEliminar" OnClick="GrillaAdjunto_BtnEliminar_Click" runat="server"><i class="fa-solid fa-trash-can"></i></asp:LinkButton>
                                                        <br /><br />
                                                        <asp:LinkButton CssClass="btn btn-success btn-sm btn-grid-action"   ID="GrillaAdjunto_BtnDescargar" OnClick="GrillaAdjunto_BtnDescargar_Click" runat="server"><i class="fa-solid fa-cloud-arrow-down"></i></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <PagerTemplate>                                    
                                                <nav style="float:right;">
                                                    <div class="justify-content-center text-white" style="font-size:0.8rem">
                                                        <asp:Label ID="GrillaAdjunto_CurrentPageLabel" runat="server" />                            
                                                    </div>
                                                </nav>
                                            </PagerTemplate>
                                            <EmptyDataTemplate>
                                                <div class="alert alert-warning align-items-center" role="alert">
                                                    <h1><i class="fa-solid fa-triangle-exclamation"></i></h1>
                                                    <div>No existen adjuntos.</div>
                                                </div>
                                            </EmptyDataTemplate>                                
                                        </asp:GridView>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                         <div class="tab-pane fade" runat="server" id="LogPanelTab" role="tabpanel" aria-labelledby="Log-tab">
                                            <div class ="container-fluid">
                                                <div class="row mt-2">
                                                    <div class="col-8 text-center" style="margin:auto">
                                                        <div class="row">
                                                            <%=ViewState("Logs") %>
                                                        </div>
                                                         
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    
                                    </div>
            


                                </div>
                            
                                <div runat="server" visible="false" ID="pathBrath" class="styleRemoveHipe"></div>

                            </div>
                        </div>
                        <div class="modal-footer bg-light"  style="border-top:2px solid #535362 !important;">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-6">
                                        <asp:Panel runat="server" ID="FrmPpal_LogCritica" Visible="false">
                                            <table id="cuadro-log">
                                                <tr>
                                                    <td><b>Autor</b></td><td colspan="5">: <span runat="server" ID="FrmPpal_AutorLog"></span></td>
                                                </tr>
                                                <tr>
                                                    <td><b>Creación</b></td><td>: <span runat="server" ID="FrmPpal_FechaCreacionLog"></span></td>
                                                </tr>
                                                <tr>
                                                   <td><b>Estado</b></td><td>: <span runat="server" ID="FrmPpal_EstadoActualLog"></span></td>
                                                </tr>
                                               
                                            </table>
                                        </asp:Panel>
                                    </div>
                                    <div class="col-6 text-end">

                                        <asp:LinkButton ID="FrmPpal_BtnCerrar" runat="server" CssClass="btn btn-secondary" ToolTip="Cerrar" OnClick="FrmPpal_BtnCerrar_Click"><i class="fa-regular fa-share-from-square"></i>&nbsp;&nbsp;&nbsp;Cerrar</asp:LinkButton>
                                        &nbsp;&nbsp;&nbsp;
                                        <asp:LinkButton ID="FrmPpal_BtnGuardar" runat="server" CssClass="btn btn-warning row-botonera" ToolTip="Actualizar" OnClick="FrmPpal_BtnGuardar_Click"></asp:LinkButton>
                                        &nbsp;&nbsp;
                                        <asp:LinkButton ID="FrmPpal_BtnEnviarAprobacion" OnClick="FrmPpal_BtnEnviarAprobacion_Click" runat="server" CssClass="btn btn-success row-botonera" ToolTip="Enviar a Aprobación"><i class="fa-regular fa-paper-plane"></i> Enviar a Apobación</asp:LinkButton>
                       

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<!-- FIN FORMULARIO -->

<!-- FORMULARIO COMENTARIO -->
<asp:Button ID="FrmComentarios_BtnVer" runat="server" Style="display:none" />
<asp:ModalPopupExtender ID="FrmComentarios" runat="server" TargetControlID="FrmComentarios_BtnVer" PopupControlID="FrmComentarios_Panel" BackgroundCssClass="TeraModalBackground"></asp:ModalPopupExtender>
<asp:Panel ID="FrmComentarios_Panel" runat="server">
    <asp:UpdatePanel ID="FrmComentarios_UpdatePanel" runat="server">
        <ContentTemplate>
            <div class="modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div id="FrmComentarios_dialog" class="modal-dialog modal-dialog-centered" style="display:none;" runat="server">
                    <div class="modal-content">
                       <div class="modal-header bg-dark text-white" style="padding:0.9rem 1rem 0.9rem 1rem !important">
                            <h4 class="modal-title fs-5"><i class='fa-solid fa-comment'></i>&nbsp;&nbsp;Agregar <span Class='badge text-bg-warning'>Comentario</span></h4>
                        </div>            
                        <div class="modal-body">
                                    
                            <asp:TextBox style="width:100%; height:143px;" runat="server" TextMode="MultiLine"  ID="FrmlPpal_traspasaComentario"></asp:TextBox>
                                    
                            
                        </div> 
                
                        <div class="modal-footer bg-light" style="border-top:1px solid #535362 !important;">
                            <asp:LinkButton ID="FrmComentarios_BtnCerrar"    runat="server" CssClass="btn btn-secondary btn-sm" ToolTip="Cerrar"        OnClick="FrmComentarios_BtnCerrar_Click"><i class="fa-regular fa-share-from-square"></i>&nbsp;&nbsp;&nbsp;Cerrar</asp:LinkButton>
                            &nbsp;&nbsp;&nbsp;
                            <asp:LinkButton ID="FrmComentarios_BtnComentar"    runat="server" CssClass="btn btn-success btn-sm"   ToolTip="Comentar"    OnClick="FrmComentarios_BtnComentar_Click"><i class='fa-solid fa-pen-to-square'></i>&nbsp;&nbsp;&nbsp;Comentar</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<!-- FIN FORMULARIO -->






<!-- FORMULARIO ELIMINAR-->
<asp:Button ID="FrmDelete_BtnVer" runat="server" Style="display:none" />
<asp:ModalPopupExtender ID="FrmDelete" runat="server" TargetControlID="FrmDelete_BtnVer" PopupControlID="FrmDelete_Panel" BackgroundCssClass="TeraModalBackground"></asp:ModalPopupExtender>
<asp:Panel ID="FrmDelete_Panel" runat="server">
    <asp:UpdatePanel ID="FrmDelete_UpdatePanel" runat="server">
        <ContentTemplate>
            <div class="modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div id="FrmDelete_dialog" class="modal-dialog modal-dialog-centered" style="display:none;" runat="server">
                    <div class="modal-content">
                        <div class="modal-header bg-danger text-white">
                            <h4 ID="FrmDelete_Titulo" class="modal-title fs-5" runat="server"></h4>
                        </div>                
                        <div class="modal-body">
                            <asp:TextBox ID="FrmDelete_TxtOpt"  style="display:none;" runat="server" />
                            <asp:TextBox ID="FrmDelete_TxtId"   style="display:none;" runat="server" />
                            <asp:Label ID="FrmDelete_Msg" runat="server"></asp:Label>

                            <br /><br />
                            <p class="text-center"><b>¿DESEA CONTINUAR?</b></p>

                        </div> <%--modal-body--%>
                
                        <div class="modal-footer bg-light" style="border-top:1px solid #535362 !important;">
                            <asp:LinkButton ID="FrmDelete_BtnCerrar"    runat="server" CssClass="btn btn-secondary btn-sm"  ToolTip="Cerrar"    OnClick="FrmDelete_BtnCerrar_Click"><i class="fa-regular fa-share-from-square"></i>&nbsp;&nbsp;&nbsp;Cerrar</asp:LinkButton>
                            &nbsp;&nbsp;&nbsp;
                            <asp:LinkButton ID="FrmDelete_BtnEliminar"  runat="server" CssClass="btn btn-danger btn-sm"     ToolTip="Eliminar"  OnClick="FrmDelete_BtnEliminar_Click"><i class='fa-solid fa-pen-to-square'></i>&nbsp;&nbsp;&nbsp;Elimnar</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<!-- FIN FORMULARIO -->

<!-- FORMULARIO ADJUNTO -->
<asp:Button ID="FrmAdjunto_BtnVer" runat="server" Style="display:none" />
<asp:ModalPopupExtender ID="FrmAdjunto" runat="server" TargetControlID="FrmAdjunto_BtnVer" PopupControlID="FrmAdjunto_Panel" BackgroundCssClass="TeraModalBackground"></asp:ModalPopupExtender>
<asp:Panel ID="FrmAdjunto_Panel" runat="server">
    <asp:UpdatePanel ID="FrmAdjunto_UpdatePanel" runat="server">
        <ContentTemplate>
            <div class="modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div id="FrmAdjunto_dialog" class="modal-dialog" style="display:none;" runat="server">
                    <div class="modal-content">
                        <div class="modal-header bg-dark text-white" style="padding:0.9rem 1rem 0.9rem 1rem !important">
                            <h4 class="modal-title fs-5"><i class='fa-solid fa-paperclip'></i>&nbsp;&nbsp;Agregar <span Class='badge text-bg-warning'>Adjunto</span></h4>
                        </div>
                        
                        <div class="modal-body">
                            <asp:TextBox ID="FrmAdjunto_TxtIdTemp"   style="display:none;" runat="server" />
                            <asp:TextBox ID="FrmAdjunto_TxtIdCritica" style="display:none;" runat="server" />

                            <div class="row">
                                <div class="col-12">
                                    <label class="form-label" for="FrmAdjunto_TxtComentario" style="color:#000 !important;"><h6 style="margin-bottom:0.1rem !important; font-size:1rem !important;">Comentario</h6></label>
                                    <asp:TextBox ID="FrmAdjunto_TxtComentario" Class="form-control form-control-sm" TextMode="MultiLine" Rows="4" runat="server" />
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-12">
                                    <asp:FileUpload ID="FrmAdjunto_Upload" Class="form-control form-control-sm" runat="server" />
                                    <div ID="FrmAdjunto_Upload_Msj" class="" runat="server"></div>
                                </div>
                            </div>
                            <br />
                        </div>

                        <div class="modal-footer bg-light" style="border-top:1px solid #535362 !important;">
                            <asp:LinkButton ID="FrmAdjunto_BtnCerrar"    runat="server" Class="btn btn-secondary btn-sm" ToolTip="Cerrar"    OnClick="FrmAdjunto_BtnCerrar_Click"><i class="fa-regular fa-share-from-square"></i>&nbsp;&nbsp;&nbsp;Cerrar</asp:LinkButton>
                            &nbsp;&nbsp;&nbsp;
                            <asp:LinkButton ID="FrmAdjunto_BtnGuardar"  runat="server" Class="btn btn-success btn-sm"   ToolTip="Adjuntar"  OnClick="FrmAdjunto_BtnGuardar_Click"><i class="fa-regular fa-floppy-disk"></i>&nbsp;&nbsp;&nbsp;Adjuntar</asp:LinkButton>  
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="FrmAdjunto_BtnGuardar" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Panel>
<!-- FIN FORMULARIO ADJUNTOS -->


<!-- MENSAJES -->
<asp:Button ID="FrmMsg_BtnVer" runat="server" Text="" style="display:none;" />
<asp:ModalPopupExtender ID="FrmMsg" runat="server" TargetControlID="FrmMsg_BtnVer" PopupControlID="FrmMsg_UpdatePanel" BackgroundCssClass="TeraModalBackground"></asp:ModalPopupExtender>
<asp:Panel ID="FrmMsg_Panel" runat="server">
    <asp:UpdatePanel ID="FrmMsg_UpdatePanel" runat="server">
        <ContentTemplate>
            <div class="modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div id="FrmMsg_dialog" class="modal-dialog modal-lg modal-dialog-centered" style="display:none;" runat="server">
                    <div class="modal-content">
                        <div id="FrmMsg_TipoMensaje" class="" style="color:#FFF !important" runat="server">                            
                            <h4 class="modal-title fs-5" style="color:#FFF !important;">
                                    <i ID="FrmMsg_TipoMensajeIcono" class="" runat="server"></i>&nbsp;
                                    <asp:Label ID="FrmMsg_LblTitulo" runat="server" Text=""></asp:Label>
                            </h4>
                        </div>

                        <div class="modal-body">
                            <asp:Label ID="FrmMsg_LblCuerpo" runat="server" Text=""></asp:Label>
                        </div> <%--modal-body--%>

                        <div class="modal-footer bg-light" style="border-top:1px solid #535362 !important;">
                            <asp:Button ID="FrmMsg_BtnAceptar" Width="50px" runat="server" Text="OK" CssClass="" OnClick="FrmMsg_BtnAceptar_Click" data-bs-dismiss="modal" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<!-- FIN MENSAJES -->



<script src="https://cdnjs.cloudflare.com/ajax/libs/Trumbowyg/2.27.3/trumbowyg.min.js"></script>
<script>
    function init() {
        $(document).ready(function () {
            var select2 = $('.select2').select2({
                placeholder: $(this).data('placeholder'),
                dropdownParent: $("#modal-FrmRutaAprobacion")

            });

            $(".nav-link").click(function () {
                //opcionTab($(this).attr('data-id'))
            })
            reloadOrder()
        })
    }

    function reloadOrder() {
        $(document).ready(function () {
            
            $('.table-order-criticas tbody').sortable({
                update: function (event, ui) {
                    $(this).children().each(function (index) {
                        //$(this).find('td').last().html(index + 1);
                    });
                }
            });

            $('.FrmPpal_TxtJustificacion')
                .trumbowyg()
                .on('tbwchange', function () {
                    $(".FrmPpal_TxtJustificacion_texto").val($(this).trumbowyg('html'));
                }); 


            const texto = $(".FrmPpal_TxtJustificacion_texto").val()
                $(".FrmPpal_TxtJustificacion").trumbowyg('html', texto)
            
            //$(".FrmPpal_TxtJustificacion_texto").val()
            //$(".table-order-criticas tbody").sortable({
            //    cursor: 'row-resize',
            //    placeholder: 'ui-state-highlight',
            //    opacity: '0.55',
            //    items: '.ui-sortable-handle'
            //}).disableSelection();

            
        })
    }

                                                       

    function setNuevoComentario() {
        $(".add-new-comentario").fadeIn()
    }

    init()
</script>


</asp:Content>
