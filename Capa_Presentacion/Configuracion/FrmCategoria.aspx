﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/GlbMaster.Master" CodeBehind="FrmCategoria.aspx.vb" Inherits="Capa_Presentacion.FrmCategoria" %>
<%@ Register Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../assets/js/jsUpdateProgress.js"></script>

    <!-- Font Lato -->
    <link href="../assets/css/lato.css?v=v1" rel="stylesheet" />

    <script type="text/javascript">
        var ModalProgress = '<%= ModalProgress.ClientID %>';
    </script>  

    <asp:Panel ID="PnProgress" runat="server">
        <asp:UpdateProgress ID="UpProgress" runat="server">
            <ProgressTemplate>
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center text-primary">
                                <img src='../assets/images/loadingLogin.gif' style='max-width:50px;'>
                            </div>
                        </div>
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </asp:Panel>
    <asp:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="PnProgress" BackgroundCssClass="TeraModalBackground" PopupControlID="PnProgress"  />
    


<style type="text/css">
/*Esto se utliza para el hover del gridview sin marcar el titulo y el pie*/
tr:not(:first-child):not(:last-child):hover{
    background-color:#4DFF7A;
}

tr:first-child{
     border-top:2px solid #26BD75;
     border-left:1px solid #000;
     border-bottom:3px solid #535362;
}


tr:last-child{
     background-color:#535362;
     border-top:2px solid #000;
     border-left:1px solid #535362;
     border-right:1px solid #000;
     border-bottom:3px solid #26BD75;
}

tr:first-child th:last-child {
   border-right:1px solid #000;
}

tr a{
    text-decoration:none;
    color:#FFF;
}

tr a:hover{
    text-decoration:none;
    color:#FFF;
}

.gradientLineKantar {
    display: block;
    width: 6px;
    height: 97.5%;
    z-index: 1;
    background: repeating-linear-gradient(0deg,#e8bb3d,#ffed6a 32%,#b28300 80%,#e8bb3d);
    background-size: 100% 70%;
    transition: background .5s;
    background-position-y: var(--gymo-pos);
    will-change: opacity;
    /* Chrome 10-25, Safari 5.1-6 */
    background: -webkit-repeating-linear-gradient(0deg,#e8bb3d,#ffed6a 32%,#b28300 80%,#e8bb3d);
    /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
    background: -moz-repeating-linear-gradient(0deg,#e8bb3d,#ffed6a 32%,#b28300 80%,#e8bb3d);
}
</style>

<%--bloquea el enter en toda la pagina--%>
<%--      <script type="text/javascript">
          function stopEnterKey(evt) {
              alert('1');
              var evt = (evt) ? evt : ((event) ? event : null);
              var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
              if ((evt.keyCode == 13) && (node.type == "text")) {
                  return false;
              }
          }
          document.onkeypress = stopEnterKey;
      </script>--%>

<!-- MOSTRAR DATOS EN EL GRIDVIEW -->
    <asp:UpdatePanel runat="server" ID="UpdatePanelIndex">
        <ContentTemplate>
            <asp:Panel runat="server" ID="PanelIndex" CssClass="center-block">
                <div class="container-fluid">
                    <br />
                    <div class="row shadow-sm rounded">
                        <div class="col-11">                       
                            <h2 style="color:#26BD75; font-family:'Lato'; font-weight:900 !important;">Mantenedor de <span class="badge text-bg-warning">CATEGORÍA</span></h2>
                        </div>
                        <div class="col-1">
                            <span class="float-end">
                                <asp:LinkButton ID="BtnAddRegistro" runat="server" CssClass="btn btn-success" style="background-color:#26BD75 !important;" OnClick="BtnAddRegistro_Click">&nbsp;&nbsp;<i class="fa-solid fa-plus"></i>&nbsp;&nbsp;</asp:LinkButton>
                            </span>
                        </div>
                    </div>

                    <br />
                    <div class="row">
                        <div class="col-5">
                            <div class="input-group mb-3">
                                <label class="input-group-text border-secondary text-white bg-dark" for="TxtBsc"><i class="fa-solid fa-magnifying-glass"></i>&nbsp;Buscar&nbsp;&nbsp;</label>
                                <asp:TextBox ID="TxtBsc" CssClass="form-control border-secondary form-control-sm"
                                    placeholder="para buscar valores múltiples utilice el separador ;" style="font-size: 1.1rem !important;"
                                    onkeypress="if ((event || window.event).keyCode == 13) { var node = (event || window.event).target || (event || window.event).srcElement; if (node.type == 'text' && node.id == 'ContentPlaceHolder1_TxtBsc') { return false; } }"
                                    autocomplete="off" runat="server"></asp:TextBox>
                                <asp:LinkButton ID="BtnBsc" CssClass="btn btn-dark"  OnClick="BtnBsc_Click" runat="server">
                                    &nbsp;&nbsp;<i class="fa-brands fa-golang" style="height:1.8em !important; vertical-align: inherit !important;"></i>&nbsp;&nbsp;
                                </asp:LinkButton>
                            </div>
                        </div>
                    </div> 

                    <br />
                    <div class="row">
                        <div class="col-12">
                            <div class="gradientLineKantar" style="float:left;"></div>
                            <asp:GridView ID="GrillaPpal" runat="server" CssClass="table table-bordered bg-light"
                                Style="text-align:center; vertical-align:middle; font-size:1rem; max-width:1600px; margin-left: 0px; width:98% !important;"
                                PageSize="50" AllowPaging="true" AutoGenerateColumns="false" 
                                HorizontalAlign="Center" AllowSorting="true"
                                OnRowDataBound="GrillaPpal_RowDataBound">
                            <HeaderStyle CssClass="bg-dark text-center text-white" Font-Size="Larger" Font-Bold="false" />                                                             
                                <Columns>                                    
                                    <%-- 0 --%>
                                    <asp:TemplateField HeaderText="" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="LblIdCategoria" runat="server" Text='<%# Eval("IdCategoria") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- 1 --%>
                                    <asp:TemplateField HeaderText="<span class='link-light'>Nº</span>" ItemStyle-Width="50">
                                        <ItemTemplate>
                                            <asp:Label ID="LblNroFila" runat="server" Text='<%# Eval("NroFila") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- 2 --%>
                                    <asp:TemplateField HeaderText="<span class='link-light'>Categoría <i class='fa-solid fa-ellipsis-vertical'></i></span>" SortExpression="Categoria">
                                        <ItemTemplate>
                                            <asp:Label ID="LblCategoria" runat="server" Text='<%# Eval("Categoria") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- 3 --%>
                                    <asp:TemplateField HeaderText="<span class='link-light'><i class='fa-solid fa-pen-to-square'></i></span>" ItemStyle-Width="50">
                                        <ItemTemplate>
                                            <asp:LinkButton CssClass="btn btn-info btn-sm" ID="GrillaPpal_BtnEditar" OnClick="GrillaPpal_BtnEditar_Click" runat="server"><i class="fa-solid fa-pen-to-square"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- 4 --%>
                                    <asp:TemplateField HeaderText="<span class='link-light'><i class='fa-solid fa-trash-can'></i></span>" ItemStyle-Width="50">
                                        <ItemTemplate>
                                            <asp:LinkButton CssClass="btn btn-danger btn-sm" ID="GrillaPpal_BtnEliminar" OnClick="GrillaPpal_BtnEliminar_Click" runat="server"><i class="fa-solid fa-trash-can"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerTemplate>                                    
                                    <nav style="float:right;">
                                        <div class="btn-group" role="group">
                                            <asp:LinkButton ID="GrillaPpal_BtnFirst"    runat="server" CssClass="btn btn-outline-light btn-sm" Text="Primera Página"      ToolTip="Primera Página"    OnClick="GrillaPpal_BtnFirst_Click"><i class="fa-solid fa-backward-fast"></i></asp:LinkButton>
                                            <asp:LinkButton ID="GrillaPpal_BtnPrev"     runat="server" CssClass="btn btn-outline-light btn-sm" Text="Página Anterior"     ToolTip="Página Anterior"   OnClick="GrillaPpal_BtnPrev_Click"><i class="fa-solid fa-arrow-left"></i></asp:LinkButton>
                                            <asp:LinkButton ID="GrillaPpal_BtnNext"     runat="server" CssClass="btn btn-outline-light btn-sm" Text="Página Siguiente"    ToolTip="Página Siguiente"  OnClick="GrillaPpal_BtnNext_Click"><i class="fa-solid fa-arrow-right"></i></asp:LinkButton>
                                            <asp:LinkButton ID="GrillaPpal_BtnLast"     runat="server" CssClass="btn btn-outline-light btn-sm" Text="Última Página"       ToolTip="Última Página"     OnClick="GrillaPpal_BtnLast_Click"><i class="fa-solid fa-forward-fast"></i></asp:LinkButton>
                                        </div>
                                        <div class="justify-content-center text-white" style="font-size:0.8rem">
                                            <asp:Label ID="GrillaPpal_CurrentPageLabel" runat="server" />                            
                                        </div>
                                    </nav>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    <div class="alert alert-warning align-items-center" role="alert">
                                        <h1><i class="fa-solid fa-triangle-exclamation"></i></h1>
                                        <div>No existen registros para mostrar.</div>
                                    </div>
                                </EmptyDataTemplate>                                
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
<!-- FIN MOSTRAR DATOS EN EL GRIDVIEW -->

<!-- FORMULARIO -->
<asp:Button ID="FrmPpal_BtnVer" runat="server" Style="display:none" />
<asp:ModalPopupExtender ID="FrmPpal" runat="server" TargetControlID="FrmPpal_BtnVer" PopupControlID="FrmPpal_Panel" BackgroundCssClass="TeraModalBackground"></asp:ModalPopupExtender>
<asp:Panel ID="FrmPpal_Panel" runat="server">
    <asp:UpdatePanel ID="FrmPpal_UpdatePanel" runat="server">
        <ContentTemplate>
            <div class="modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div id="FrmPpal_dialog" class="modal-dialog modal-lg" style="display:none;" runat="server">
                    <div class="modal-content">
                        <div class="modal-header bg-dark text-white">
                            <h4 ID="FrmPpalTitulo" class="modal-title fs-5" runat="server"></h4>
                        </div>
                
                        <div class="modal-body">
                            <asp:TextBox ID="FrmPpal_TxtIdCategoria" style="display:none;" runat="server" />

                            <div class="row gx-5">
                                <div class="col-8">
                                    <label class="" for="FrmPpal_TxtCategoria" style="color:#000 !important;"><h6 style="margin-bottom:0.7rem !important; font-size:1.2rem !important;">Categoría</h6></label> 
                                    <asp:TextBox ID="FrmPpal_TxtCategoria" class="form-control" MaxLength="30" autocomplete="off" runat="server" />
                                    <div ID="FrmPpal_TxtCategoria_Msj" class="" runat="server"></div>
                                </div>
                            </div>
                            <br />
                        </div>

                        <div id="FrmPpal_DivUltAct" style="font-size:0.8rem !important; color:#26BD75 !important; text-align:right !important" runat="server"></div>

                        <div class="modal-footer bg-light" style="border-top:2px solid #535362 !important;">
                            <asp:LinkButton ID="FrmPpal_BtnCerrar"    runat="server" CssClass="btn btn-secondary" ToolTip="Cerrar"        OnClick="FrmPpal_BtnCerrar_Click"><i class="fa-regular fa-share-from-square"></i>&nbsp;&nbsp;&nbsp;Cerrar</asp:LinkButton>
                            &nbsp;&nbsp;&nbsp;
                            <asp:LinkButton ID="FrmPpal_BtnGuardar"    runat="server" CssClass="btn btn-success"   ToolTip="Actualizar"    OnClick="FrmPpal_BtnGuardar_Click"></asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<!-- FIN FORMULARIO -->

<!-- FORMULARIO ELIMINAR-->
<asp:Button ID="FrmDelete_BtnVer" runat="server" Style="display:none" />
<asp:ModalPopupExtender ID="FrmDelete" runat="server" TargetControlID="FrmDelete_BtnVer" PopupControlID="FrmDelete_Panel" BackgroundCssClass="TeraModalBackground"></asp:ModalPopupExtender>
<asp:Panel ID="FrmDelete_Panel" runat="server">
    <asp:UpdatePanel ID="FrmDelete_UpdatePanel" runat="server">
        <ContentTemplate>
            <div class="modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div id="FrmDelete_dialog" class="modal-dialog modal-dialog-centered" style="display:none;" runat="server">
                    <div class="modal-content">
                        <div class="modal-header bg-danger text-white">
                            <h4 ID="FrmDeleteTitulo" class="modal-title fs-5" runat="server"></h4>
                        </div>                
                        <div class="modal-body">
                            <asp:TextBox ID="FrmDelete_TxtId" style="display:none;" runat="server" />
                            <asp:Label ID="FrmDelete_Msg" runat="server"></asp:Label>

                            <br /><br />
                            <p class="text-center"><b>¿DESEA CONTINUAR?</b></p>

                        </div> <%--modal-body--%>
                
                        <div class="modal-footer bg-light" style="border-top:1px solid #535362 !important;">
                            <asp:LinkButton ID="FrmDelete_BtnCerrar"    runat="server" CssClass="btn btn-secondary btn-sm" ToolTip="Cerrar"        OnClick="FrmDelete_BtnCerrar_Click"><i class="fa-regular fa-share-from-square"></i>&nbsp;&nbsp;&nbsp;Cerrar</asp:LinkButton>
                            &nbsp;&nbsp;&nbsp;
                            <asp:LinkButton ID="FrmDelete_BtnEliminar"    runat="server" CssClass="btn btn-danger btn-sm"   ToolTip="Actualizar"    OnClick="FrmDelete_BtnEliminar_Click"><i class='fa-solid fa-pen-to-square'></i>&nbsp;&nbsp;&nbsp;Elimnar</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<!-- FIN FORMULARIO -->

<!-- MENSAJES -->
<asp:Button ID="FrmMsg_BtnVer" runat="server" Text="" style="display:none;" />
<asp:ModalPopupExtender ID="FrmMsg" runat="server" TargetControlID="FrmMsg_BtnVer" PopupControlID="FrmMsg_UpdatePanel" BackgroundCssClass="TeraModalBackground"></asp:ModalPopupExtender>
<asp:Panel ID="FrmMsg_Panel" runat="server">
    <asp:UpdatePanel ID="FrmMsg_UpdatePanel" runat="server">
        <ContentTemplate>
            <div class="modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div id="FrmMsg_dialog" class="modal-dialog modal-lg modal-dialog-centered" style="display:none;" runat="server">
                    <div class="modal-content">
                        <div id="FrmMsg_TipoMensaje" class="" style="color:#FFF !important" runat="server">                            
                            <h4 class="modal-title fs-5" style="color:#FFF !important;">
                                    <i ID="FrmMsg_TipoMensajeIcono" class="" runat="server"></i>&nbsp;
                                    <asp:Label ID="FrmMsg_LblTitulo" runat="server" Text=""></asp:Label>
                            </h4>
                        </div>

                        <div class="modal-body">
                            <asp:Label ID="FrmMsg_LblCuerpo" runat="server" Text=""></asp:Label>
                        </div> <%--modal-body--%>

                        <div class="modal-footer bg-light" style="border-top:1px solid #535362 !important;">
                            <asp:Button ID="FrmMsg_BtnAceptar" Width="50px" runat="server" Text="OK" CssClass="" OnClick="FrmMsg_BtnAceptar_Click" data-bs-dismiss="modal" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<!-- FIN MENSAJES -->

</asp:Content>