﻿Imports Capa_Entidad
Imports Capa_Negocio

Public Class FrmWorkflow
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Dim URL As String = Page.Request.UrlReferrer.AbsolutePath
        Catch ex As Exception
            'Response.Redirect("../PageError.aspx")
        End Try
        If Request.Form("__EVENTTARGET") <> "" Then
            If Request.Form("__EVENTTARGET") = "UploadEvent" Then
                GrillaPpal_Llenar()
            End If
        End If
        If Not Page.IsPostBack Then
            Dim var_krypton As String = OwnDecryption(Page.Request("krypton"))
            ViewState("MyIdUser") = OwnPageRequest(var_krypton, "IdUser")


            GrillaPpal_Llenar()
            GrillaPpal.Attributes("Workflow") = SortDirection.Descending
        End If
    End Sub

#Region "Index"
    Protected Sub BtnBsc_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
    End Sub
    Protected Sub BtnAddRegistro_Click(sender As Object, e As EventArgs)
        FrmPpal_Limpiar()

        FrmPpalTitulo.InnerHtml = "<i class='far fa-edit'></i>&nbsp;&nbsp;Agregar <span Class='badge text-bg-warning'>JOB POSITION</span>"
        FrmPpal_BtnGuardar.Text = "<i class='fa-regular fa-floppy-disk'></i>&nbsp;&nbsp;&nbsp;Guardar"
        ' FrmPpal_CboLineManeger_Llenar()

        FrmPpal_TxtWorkflow.Focus()

        ModalShow(FrmPpal_dialog, FrmPpal)
        execJS("init()")
    End Sub


    'Protected Sub FrmPpal_CboLineManeger_Llenar()

    '    Dim N_ObjLeeUser As New N_ClsLeeUser

    '    FrmPpal_CboLineManeger.Items.Clear()
    '    FrmPpal_CboLineManeger.DataTextField = "TextField"
    '    FrmPpal_CboLineManeger.DataValueField = "ValueField"
    '    FrmPpal_CboLineManeger.DataSource = N_ObjLeeUser.LoadCbo()
    '    FrmPpal_CboLineManeger.DataBind()

    '    'FrmPpal_CboLineManeger.Items.Insert(0, New ListItem("<< Seleccionar >>", "0"))
    '    'FrmPpal_CboLineManeger.Items.Item(0).Selected = True


    'End Sub
#End Region

#Region "Grilla Principal"
    Protected Sub GrillaPpal_Llenar()
        Try
            Dim N_ObjWorkflow As New N_ClsTblWorkflow
            GrillaPpal.DataSource = SortingPageIndex(N_ObjWorkflow.SelectAll(TxtBsc.Text.Trim()), ViewState("GrillaPpal_ColumnSorting"), ViewState("GrillaPpal_TypeSorting"))
            GrillaPpal.DataBind()

            GrillaPpal.Columns(1).ItemStyle.HorizontalAlign = HorizontalAlign.Center
            GrillaPpal.Columns(2).ItemStyle.HorizontalAlign = HorizontalAlign.Left
            GrillaPpal.Columns(3).ItemStyle.HorizontalAlign = HorizontalAlign.Left

            TxtBsc.Focus()
        Catch ex As Exception
            GrillaPpal.DataSource = Nothing
            GrillaPpal.DataBind()
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub GrillaPpal_BtnEditar_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
        Dim IdWorkflow As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblIdWorkflow"), Label)

        GetRegistro(IdWorkflow.Text)

        FrmPpalTitulo.InnerHtml = "<i class='far fa-edit'></i>&nbsp;&nbsp;Modificar <span Class='badge text-bg-warning'>WorkFlow</span>"
        FrmPpal_BtnGuardar.Text = "<i class='fa-regular fa-floppy-disk'></i>&nbsp;&nbsp;&nbsp;Actualizar"
        FrmPpal_TxtWorkflow.Focus()

        ModalShow(FrmPpal_dialog, FrmPpal)
    End Sub
    Protected Sub GrillaPpal_BtnEliminar_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
        Dim IdWorkflow As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblIdWorkflow"), Label)
        Dim Workflow As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblWorkflow"), Label)

        FrmDeleteTitulo.InnerHtml = "<i class='fa-solid fa-pen-to-square'></i>&nbsp;&nbsp;Eliminar Job Position"

        FrmDelete_TxtId.Text = IdWorkflow.Text
        FrmDelete_Msg.Text = "Se eliminará el job position <b>" & Workflow.Text & "</b>."

        ModalShow(FrmDelete_dialog, FrmDelete)
    End Sub
    Protected Sub GrillaPpal_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Pager Then
            Dim PiePagina As Label = e.Row.FindControl("GrillaPpal_CurrentPageLabel")
            Dim TotalPaginas As Integer = Convert.ToString(GrillaPpal.PageCount).Trim()
            PiePagina.Text = "Página " & Convert.ToString(GrillaPpal.PageIndex + 1).Trim() & " de " & TotalPaginas.ToString()
        End If
    End Sub
    Protected Sub GrillaPpal_BtnFirst_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        GrillaPpal.PageIndex = 0
        GrillaPpal.DataBind()
    End Sub
    Protected Sub GrillaPpal_BtnPrev_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        If (GrillaPpal.PageIndex <> 0) Then
            GrillaPpal.PageIndex = GrillaPpal.PageIndex - 1
            GrillaPpal.DataBind()
        End If
    End Sub
    Protected Sub GrillaPpal_BtnNext_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        If (GrillaPpal.PageIndex <> GrillaPpal.PageCount - 1) Then
            GrillaPpal.PageIndex = GrillaPpal.PageIndex + 1
            GrillaPpal.DataBind()
        End If
    End Sub
    Protected Sub GrillaPpal_BtnLast_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        GrillaPpal.PageIndex = GrillaPpal.PageCount - 1
        GrillaPpal.DataBind()
    End Sub
    Private Sub GrillaPpal_PreRender(sender As Object, e As EventArgs) Handles GrillaPpal.PreRender
        'Esto es para que la grilla muestre el paginador aunque tenga menos de 10 registros
        If GrillaPpal.BottomPagerRow IsNot Nothing Then
            If GrillaPpal.BottomPagerRow.Visible = False Then
                GrillaPpal.BottomPagerRow.Visible = True
            End If
        End If
    End Sub
    Private Sub GrillaPpal_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles GrillaPpal.Sorting
        ViewState("GrillaPpal_ColumnSorting") = e.SortExpression

        If GrillaPpal.Attributes(e.SortExpression) = SortDirection.Ascending Then
            ViewState("GrillaPpal_TypeSorting") = "DESC"
            GrillaPpal_ChangeHeader()
        Else
            ViewState("GrillaPpal_TypeSorting") = "ASC"
            GrillaPpal_ChangeHeader()
        End If

        GrillaPpal_Llenar()
    End Sub
    Private Sub GrillaPpal_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GrillaPpal.RowCommand
        If Trim(e.CommandArgument) = "Workflow" Then
            ViewState("GrillaPpal_ColumnIndex") = 2
        End If
    End Sub
    Private Sub GrillaPpal_ChangeHeader()
        Dim Base As String = "<span class='link-light'>XXXTITULOXXX <i class='fa-solid fa-ellipsis-vertical'></i></span>"
        Dim Type As String

        If ViewState("GrillaPpal_TypeSorting") = "DESC" Then
            Type = "<i class='fa-solid fa-arrow-down-z-a'></i>"
        Else
            Type = "<i class='fa-solid fa-arrow-down-a-z'></i>"
        End If

        Dim GrillaPpal_Indice = New Integer() {2}
        Dim GrillaPpal_Campo() As String = {"Workflow"}
        Dim GrillaPpal_Titulo() As String = {"Job Position"}

        For I = 0 To GrillaPpal_Indice.Length - 1
            If ViewState("GrillaPpal_ColumnIndex") = GrillaPpal_Indice(I) Then
                GrillaPpal.Columns(GrillaPpal_Indice(I)).HeaderText = "<span class='link-light'>" & GrillaPpal_Titulo(I) & " " & Type & "</span>"
                If ViewState("GrillaPpal_TypeSorting") = "DESC" Then
                    GrillaPpal.Attributes(GrillaPpal_Campo(I)) = SortDirection.Descending
                Else
                    GrillaPpal.Attributes(GrillaPpal_Campo(I)) = SortDirection.Ascending
                End If
            Else
                GrillaPpal.Columns(GrillaPpal_Indice(I)).HeaderText = Replace(Base, "XXXTITULOXXX", GrillaPpal_Titulo(I))
                GrillaPpal.Attributes(GrillaPpal_Campo(I)) = SortDirection.Descending
            End If
        Next
    End Sub
#End Region

#Region "Formulario Principal"
    Protected Sub GetRegistro(ByVal IdWorkflow As Integer)
        Try
            FrmPpal_Limpiar()
            'FrmPpal_CboLineManeger_Llenar()
            Dim E_ObjWorkflow As New E_ClsTblWorkflow
            Dim N_ObjWorkflow As New N_ClsTblWorkflow
            E_ObjWorkflow.IdWorkflow = IdWorkflow
            E_ObjWorkflow = N_ObjWorkflow.SelectRecord(E_ObjWorkflow)

            FrmPpal_TxtIdWorkflow.Text = E_ObjWorkflow.IdWorkflow
            FrmPpal_TxtWorkflow.Text = E_ObjWorkflow.Workflow.Trim()
            'FrmPpal_CboLineManeger.SelectedValue = E_ObjWorkflow.LineManeger



            FrmPpal_DivUltAct.InnerHtml = E_ObjWorkflow.UltAct


            execJS("init()")

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub FrmPpal_Limpiar()
        FrmPpal_DivUltAct.InnerHtml = ""
        FrmPpal_TxtIdWorkflow.Text = ""

        FrmPpal_TxtWorkflow.Text = ""
        GetValidador(FrmPpal_TxtWorkflow, "form-control specialform", FrmPpal_TxtWorkflow_Msj, "", "LIMPIAR")

    End Sub
    Function FrmPpal_ValidarCampos() As Boolean
        Dim SW As Boolean = True

        If FrmPpal_TxtWorkflow.Text.Trim() = "" Then
            GetValidador(FrmPpal_TxtWorkflow, "form-control specialform", FrmPpal_TxtWorkflow_Msj, "* Obligatorio", "ERROR")
            SW = False
        Else
            GetValidador(FrmPpal_TxtWorkflow, "form-control specialform", FrmPpal_TxtWorkflow_Msj, "", "OK")
        End If

        Return SW
    End Function
    Protected Sub FrmPpal_BtnGuardar_Click(sender As Object, e As EventArgs)
        Try
            If Not FrmPpal_ValidarCampos() Then
                Exit Sub
            End If

            Dim ResultadoTransaccion As String
            Dim E_ObjWorkflow As New E_ClsTblWorkflow
            Dim N_ObjWorkflow As New N_ClsTblWorkflow

            E_ObjWorkflow.Workflow = FrmPpal_TxtWorkflow.Text.Trim()
            E_ObjWorkflow.LastTransactionUser = ViewState("MyIdUser")
            E_ObjWorkflow.Estado = True

            If FrmPpal_BtnGuardar.Text = "<i class='fa-regular fa-floppy-disk'></i>&nbsp;&nbsp;&nbsp;Guardar" Then
                ResultadoTransaccion = N_ObjWorkflow.Insert(E_ObjWorkflow)
                If ResultadoTransaccion <> "OK" Then
                    GetValidador(FrmPpal_TxtWorkflow, "form-control specialform", FrmPpal_TxtWorkflow_Msj, "", "LIMPIAR")
                    MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
                    ModalShow(FrmMsg_dialog, FrmMsg)
                    Exit Sub
                End If
            Else
                E_ObjWorkflow.IdWorkflow = FrmPpal_TxtIdWorkflow.Text.Trim()
                ResultadoTransaccion = N_ObjWorkflow.Update(E_ObjWorkflow)
                If ResultadoTransaccion <> "OK" Then
                    GetValidador(FrmPpal_TxtWorkflow, "form-control specialform", FrmPpal_TxtWorkflow_Msj, "", "LIMPIAR")
                    MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
                    ModalShow(FrmMsg_dialog, FrmMsg)
                    Exit Sub
                End If
            End If

            GrillaPpal_Llenar()
            ModalHide(FrmPpal_dialog, FrmPpal)

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub FrmPpal_BtnCerrar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmPpal_dialog, FrmPpal)
    End Sub
#End Region


#Region "FormularioRutaAprobacion"
    'Cierra Modal RutaAprobacion
    Protected Sub FrmPpalRutaAprobacion_BtnCerrar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmRutaAprobacion_dialog, FrmRutaAprobacion)
    End Sub

    'Abre modal Ruta Aprobacion
    Protected Sub GrillaPpal_BtnAddRuta_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
        Dim IdWorkflow As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblIdWorkflow"), Label)
        FrmRutaAprobacion_LblIdWorkflow.Text = IdWorkflow.Text

        getJobPositionWorkFlow()
        getCarpetasWorkFlow()

        ModalShow(FrmRutaAprobacion_dialog, FrmRutaAprobacion)
        execJS("init()")
    End Sub


    Protected Sub getCarpetasWorkFlow()
        Try

            Dim N_ObjNubWorkflowCarpeta As New N_ClsTblNubWorkflowCarpeta
            Dim E_ObjNubWorkflowCarpeta As New E_ClsTblNubWorkflowCarpeta

            FrmRutaAprobacion_GrillaCarpetas.DataSource = SortingPageIndex(N_ObjNubWorkflowCarpeta.SelectAll(FrmRutaAprobacion_LblIdWorkflow.Text), ViewState("GrillaPpal_ColumnSorting"), ViewState("GrillaPpal_TypeSorting"))

            FrmRutaAprobacion_GrillaCarpetas.DataBind()

            Dim N_ObjCarpeta As New N_ClsTblCarpeta
            FrmRutaAprobacion_CboCarpetaGestionSearch.Items.Clear()
            FrmRutaAprobacion_CboCarpetaGestionSearch.DataTextField = "TextField"
            FrmRutaAprobacion_CboCarpetaGestionSearch.DataValueField = "ValueField"
            Dim reaultLoadCbo = N_ObjCarpeta.SelectLoadCmb(FrmRutaAprobacion_LblIdWorkflow.Text)

            FrmRutaAprobacion_CboCarpetaGestionSearch.DataSource = reaultLoadCbo
            FrmRutaAprobacion_CboCarpetaGestionSearch.DataBind()

            If FrmRutaAprobacion_GrillaCarpetas.Rows.Count = 0 Then
                FrmRutaAprobacion_GrillaCarpetas.Visible = False
            Else
                FrmRutaAprobacion_GrillaCarpetas.Visible = True
            End If

            If FrmRutaAprobacion_CboCarpetaGestionSearch.Items.Count = 0 Then
                FrmRutaAprobacion_CboCarpetaGestionSearch.Visible = False
                FrmRutaAprobacionCarpeta_BtnGrabar.Visible = False

            Else
                FrmRutaAprobacion_CboCarpetaGestionSearch.Visible = True
                FrmRutaAprobacionCarpeta_BtnGrabar.Visible = True
            End If

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.Message)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try

    End Sub

    Protected Sub getJobPositionWorkFlow()
        Try

            Dim N_ObjNubWorkflowJobPosition As New N_ClsTblNubWorkflowJobPosition
            Dim E_ObjNubWorkflowJobPosition As New E_ClsTblNubWorkflowJobPosition

            'FrmRutaAprobacion_GrillaJobPosition.DataSource = N_ObjNubWorkflowJobPosition.SelectAll(FrmRutaAprobacion_LblIdWorkflow.Text)
            FrmRutaAprobacion_GrillaJobPosition.DataSource = SortingPageIndex(N_ObjNubWorkflowJobPosition.SelectAll(FrmRutaAprobacion_LblIdWorkflow.Text), ViewState("GrillaPpal_ColumnSorting"), ViewState("GrillaPpal_TypeSorting"))

            FrmRutaAprobacion_GrillaJobPosition.DataBind()

            Dim N_ObjJobPosition As New N_ClsTblJobPosition
            FrmRutaAprobacion_CboUserGestionSearch.Items.Clear()
            FrmRutaAprobacion_CboUserGestionSearch.DataTextField = "TextField"
            FrmRutaAprobacion_CboUserGestionSearch.DataValueField = "ValueField"
            Dim reaultLoadCbo = N_ObjJobPosition.SelecAllCboWorkFlow(FrmRutaAprobacion_LblIdWorkflow.Text)

            FrmRutaAprobacion_CboUserGestionSearch.DataSource = reaultLoadCbo
            FrmRutaAprobacion_CboUserGestionSearch.DataBind()

            If FrmRutaAprobacion_GrillaJobPosition.Rows.Count = 0 Then
                'FrmRutaAprobacion_CboUserGestionSearch.Visible = False
                FrmRutaAprobacion_GrillaJobPosition.Visible = False
            Else
                'FrmRutaAprobacion_CboUserGestionSearch.Visible = True
                FrmRutaAprobacion_GrillaJobPosition.Visible = True
            End If

            If FrmRutaAprobacion_CboUserGestionSearch.Items.Count = 0 Then
                FrmRutaAprobacion_CboUserGestionSearch.Visible = False
                FrmRutaAprobacion_BtnGrabar.Visible = False
            Else
                FrmRutaAprobacion_CboUserGestionSearch.Visible = True
                FrmRutaAprobacion_BtnGrabar.Visible = True
            End If

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.Message)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try

        FrmRutaAprobacionTitulo.InnerHtml = "<i class='far fa-edit'></i>&nbsp;&nbsp;Ruta de <span Class='badge text-bg-warning'>Aprobación</span>"
    End Sub

    Protected Sub FrmRutaAprobacion_GrillaCarpetas_BtnFirst_Click(sender As Object, e As EventArgs)
        getCarpetasWorkFlow()
        FrmRutaAprobacion_GrillaCarpetas.PageIndex = 0
        FrmRutaAprobacion_GrillaCarpetas.DataBind()
    End Sub
    Protected Sub FrmRutaAprobacion_GrillaCarpetas_BtnPrev_Click(sender As Object, e As EventArgs)
        getCarpetasWorkFlow()
        If (FrmRutaAprobacion_GrillaCarpetas.PageIndex <> 0) Then
            FrmRutaAprobacion_GrillaCarpetas.PageIndex = FrmRutaAprobacion_GrillaCarpetas.PageIndex - 1
            FrmRutaAprobacion_GrillaCarpetas.DataBind()
        End If
    End Sub
    Protected Sub FrmRutaAprobacion_GrillaCarpetas_BtnNext_Click(sender As Object, e As EventArgs)
        getCarpetasWorkFlow()
        If (FrmRutaAprobacion_GrillaCarpetas.PageIndex <> FrmRutaAprobacion_GrillaCarpetas.PageCount - 1) Then
            FrmRutaAprobacion_GrillaCarpetas.PageIndex = FrmRutaAprobacion_GrillaCarpetas.PageIndex + 1
            FrmRutaAprobacion_GrillaCarpetas.DataBind()
        End If
    End Sub
    Protected Sub FrmRutaAprobacion_GrillaCarpetas_BtnLast_Click(sender As Object, e As EventArgs)
        getCarpetasWorkFlow()
        FrmRutaAprobacion_GrillaCarpetas.PageIndex = FrmRutaAprobacion_GrillaCarpetas.PageCount - 1
        FrmRutaAprobacion_GrillaCarpetas.DataBind()
    End Sub
    Private Sub FrmRutaAprobacion_GrillaCarpetas_PreRender(sender As Object, e As EventArgs) Handles FrmRutaAprobacion_GrillaCarpetas.PreRender
        'Esto es para que la grilla muestre el paginador aunque tenga menos de 10 registros
        If FrmRutaAprobacion_GrillaCarpetas.BottomPagerRow IsNot Nothing Then
            If FrmRutaAprobacion_GrillaCarpetas.BottomPagerRow.Visible = False Then
                FrmRutaAprobacion_GrillaCarpetas.BottomPagerRow.Visible = True
            End If
        End If
    End Sub
    Protected Sub FrmRutaAprobacion_GrillaCarpetas_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Pager Then
            Dim PiePaginaAprobador As Label = e.Row.FindControl("FrmRutaAprobacion_GrillaCarpetas_CurrentPageLabel")
            Dim TotalPaginas As Integer = Convert.ToString(FrmRutaAprobacion_GrillaCarpetas.PageCount).Trim()
            PiePaginaAprobador.Text = "Página " & Convert.ToString(FrmRutaAprobacion_GrillaCarpetas.PageIndex + 1).Trim() & " de " & TotalPaginas.ToString()
        End If
    End Sub
    Protected Sub FrmRutaAprobacion_GrillaJobPosition_BtnFirst_Click(sender As Object, e As EventArgs)
        getJobPositionWorkFlow()
        FrmRutaAprobacion_GrillaJobPosition.PageIndex = 0
        FrmRutaAprobacion_GrillaJobPosition.DataBind()
    End Sub
    Protected Sub FrmRutaAprobacion_GrillaJobPosition_BtnPrev_Click(sender As Object, e As EventArgs)
        getJobPositionWorkFlow()
        If (FrmRutaAprobacion_GrillaJobPosition.PageIndex <> 0) Then
            FrmRutaAprobacion_GrillaJobPosition.PageIndex = FrmRutaAprobacion_GrillaJobPosition.PageIndex - 1
            FrmRutaAprobacion_GrillaJobPosition.DataBind()
        End If
    End Sub
    Protected Sub FrmRutaAprobacion_GrillaJobPosition_BtnNext_Click(sender As Object, e As EventArgs)
        getJobPositionWorkFlow()
        If (FrmRutaAprobacion_GrillaJobPosition.PageIndex <> FrmRutaAprobacion_GrillaJobPosition.PageCount - 1) Then
            FrmRutaAprobacion_GrillaJobPosition.PageIndex = FrmRutaAprobacion_GrillaJobPosition.PageIndex + 1
            FrmRutaAprobacion_GrillaJobPosition.DataBind()
        End If
    End Sub
    Protected Sub FrmRutaAprobacion_GrillaJobPosition_BtnLast_Click(sender As Object, e As EventArgs)
        getJobPositionWorkFlow()
        FrmRutaAprobacion_GrillaJobPosition.PageIndex = FrmRutaAprobacion_GrillaJobPosition.PageCount - 1
        FrmRutaAprobacion_GrillaJobPosition.DataBind()
    End Sub
    Private Sub FrmRutaAprobacion_GrillaJobPosition_PreRender(sender As Object, e As EventArgs) Handles FrmRutaAprobacion_GrillaJobPosition.PreRender
        'Esto es para que la grilla muestre el paginador aunque tenga menos de 10 registros
        If FrmRutaAprobacion_GrillaJobPosition.BottomPagerRow IsNot Nothing Then
            If FrmRutaAprobacion_GrillaJobPosition.BottomPagerRow.Visible = False Then
                FrmRutaAprobacion_GrillaJobPosition.BottomPagerRow.Visible = True
            End If
        End If
    End Sub
    Protected Sub FrmRutaAprobacion_GrillaJobPosition_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Pager Then
            Dim PiePaginaAprobador As Label = e.Row.FindControl("FrmRutaAprobacion_GrillaJobPosition_CurrentPageLabel")
            Dim TotalPaginas As Integer = Convert.ToString(FrmRutaAprobacion_GrillaJobPosition.PageCount).Trim()
            PiePaginaAprobador.Text = "Página " & Convert.ToString(FrmRutaAprobacion_GrillaJobPosition.PageIndex + 1).Trim() & " de " & TotalPaginas.ToString()

        End If
    End Sub
    Protected Sub FrmRutaAprobacion_btnEliminaCarpetas_click(sender As Object, e As EventArgs)
        Try
            Dim btn As LinkButton = sender
            Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
            Dim IdCarpeta As Label = CType(FrmRutaAprobacion_GrillaCarpetas.Rows(row.RowIndex).FindControl("LblIdCarpetas"), Label)

            Dim N_ObjNubWorkflowCarpeta As New N_ClsTblNubWorkflowCarpeta
            Dim E_ObjNubWorkflowCarpeta As New E_ClsTblNubWorkflowCarpeta

            E_ObjNubWorkflowCarpeta.IdCarpeta = IdCarpeta.Text
            E_ObjNubWorkflowCarpeta.IdWorkflow = FrmRutaAprobacion_LblIdWorkflow.Text
            E_ObjNubWorkflowCarpeta.LastTransactionUser = ViewState("MyIdUser")

            Dim ResultadoTransaccion As String = N_ObjNubWorkflowCarpeta.Delete(E_ObjNubWorkflowCarpeta)
            If Mid(ResultadoTransaccion, 1, 2) = "OK" Then
                getCarpetasWorkFlow()
                GrillaPpal_Llenar()
            Else
                MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
                ModalShow(FrmMsg_dialog, FrmMsg)
            End If
            execJS("init()")
        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.Message)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub FrmRutaAprobacion_btnEliminaJobPosition_click(sender As Object, e As EventArgs)
        Try
            Dim btn As LinkButton = sender
            Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
            Dim IdJobPosition As Label = CType(FrmRutaAprobacion_GrillaJobPosition.Rows(row.RowIndex).FindControl("LblIdJobPosition"), Label)

            Dim N_ObjNubWorkflowJobPosition As New N_ClsTblNubWorkflowJobPosition
            Dim E_ObjNubWorkflowJobPosition As New E_ClsTblNubWorkflowJobPosition

            E_ObjNubWorkflowJobPosition.IdJobPosition = IdJobPosition.Text
            E_ObjNubWorkflowJobPosition.IdWorkflow = FrmRutaAprobacion_LblIdWorkflow.Text
            E_ObjNubWorkflowJobPosition.LastTransactionUser = ViewState("MyIdUser")

            Dim ResultadoTransaccion As String = N_ObjNubWorkflowJobPosition.Delete(E_ObjNubWorkflowJobPosition)
            If Mid(ResultadoTransaccion, 1, 2) = "OK" Then
                getJobPositionWorkFlow()
                GrillaPpal_Llenar()
            Else
                MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
                ModalShow(FrmMsg_dialog, FrmMsg)
            End If
            execJS("init()")
        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.Message)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
#End Region

    Protected Sub FrmRutaAprobacionCarpeta_BtnGrabar_click(sender As Object, e As EventArgs)
        Try
            Dim N_ObjNubWorkflowCarpeta As New N_ClsTblNubWorkflowCarpeta
            Dim E_ObjNubWorkflowCarpeta As New E_ClsTblNubWorkflowCarpeta
            Dim respuesta As String
            E_ObjNubWorkflowCarpeta.IdWorkflow = FrmRutaAprobacion_LblIdWorkflow.Text
            E_ObjNubWorkflowCarpeta.IdCarpeta = FrmRutaAprobacion_CboCarpetaGestionSearch.Text
            E_ObjNubWorkflowCarpeta.LastTransactionUser = ViewState("MyIdUser")


            respuesta = N_ObjNubWorkflowCarpeta.Insert(E_ObjNubWorkflowCarpeta)

            If respuesta = "OK" Then
                getCarpetasWorkFlow()
                GrillaPpal_Llenar()
            End If


            execJS("init()")

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub


    Protected Sub FrmRutaAprobacion_BtnGrabar_click(sender As Object, e As EventArgs)
        Try
            Dim N_ObjNubWorkflowJobPosition As New N_ClsTblNubWorkflowJobPosition
            Dim E_ObjNubWorkflowJobPosition As New E_ClsTblNubWorkflowJobPosition
            Dim respuesta As String
            E_ObjNubWorkflowJobPosition.IdWorkflow = FrmRutaAprobacion_LblIdWorkflow.Text
            E_ObjNubWorkflowJobPosition.IdJobPosition = FrmRutaAprobacion_CboUserGestionSearch.Text
            E_ObjNubWorkflowJobPosition.PosicionRuta = 0
            E_ObjNubWorkflowJobPosition.LastTransactionUser = ViewState("MyIdUser")


            respuesta = N_ObjNubWorkflowJobPosition.Insert(E_ObjNubWorkflowJobPosition)

            If respuesta = "OK" Then
                getJobPositionWorkFlow()
                GrillaPpal_Llenar()
            End If


            execJS("init()")

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub



#Region "FrmDelete"
    Protected Sub FrmDelete_BtnCerrar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmDelete_dialog, FrmDelete)
    End Sub
    Protected Sub FrmDelete_BtnEliminar_Click(sender As Object, e As EventArgs)
        Try
            ModalHide(FrmDelete_dialog, FrmDelete)

            Dim E_ObjWorkflow As New E_ClsTblWorkflow
            Dim N_ObjWorkflow As New N_ClsTblWorkflow
            Dim ResultadoTransaccion As String

            E_ObjWorkflow.IdWorkflow = FrmDelete_TxtId.Text.Trim()
            E_ObjWorkflow.LastTransactionUser = ViewState("MyIdUser")

            ResultadoTransaccion = N_ObjWorkflow.Delete(E_ObjWorkflow)
            If ResultadoTransaccion <> "OK" Then
                MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
                ModalShow(FrmMsg_dialog, FrmMsg)
                Exit Sub
            End If

            GrillaPpal_Llenar()

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
#End Region

#Region "FrmMsg"
    Protected Sub FrmMsg_BtnAceptar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmMsg_dialog, FrmMsg)
    End Sub

    Private Sub execJS(ByVal script As String)
        Try
            Dim TxtJavascript As String = "<script type='text/javascript'>" + script + "</script>"
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "alerta", TxtJavascript, False)
        Catch ex As Exception

        End Try
    End Sub
#End Region

End Class