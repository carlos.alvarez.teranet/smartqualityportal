﻿Imports Capa_Entidad
Imports Capa_Negocio

Public Class FrmListaDistribucion
    Inherits System.Web.UI.Page
#Region "Index"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Dim URL As String = Page.Request.UrlReferrer.AbsolutePath
        Catch ex As Exception
            Response.Redirect("../PageError.aspx")
        End Try

        If Not Page.IsPostBack Then
            Dim var_krypton As String = OwnDecryption(Page.Request("krypton"))
            ViewState("MyIdUser") = OwnPageRequest(var_krypton, "IdUser")
            GrillaPpal_Llenar()
            GrillaPpal.Attributes("ListaDistribucion") = SortDirection.Descending
        End If
    End Sub

    Protected Sub BtnBsc_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
    End Sub
    Protected Sub BtnAddRegistro_Click(sender As Object, e As EventArgs)
        FrmPpal_Limpiar()

        FrmPpalTitulo.InnerHtml = "<i class='far fa-edit'></i>&nbsp;&nbsp;Agregar <span Class='badge text-bg-warning'>LISTA DISTRIBUCIÓN</span>"
        FrmPpal_BtnGuardar.Text = "<i class='fa-regular fa-floppy-disk'></i>&nbsp;&nbsp;&nbsp;Guardar"

        FrmPpal_TxtListaDistribucion.Focus()

        ModalShow(FrmPpal_dialog, FrmPpal)
        execJS("init()")
    End Sub


#End Region

#Region "Grilla Principal"
    Protected Sub GrillaPpal_Llenar()
        Try
            Dim N_ObjListaDistribucion As New N_ClsTblListaDistribucion
            GrillaPpal.DataSource = SortingPageIndex(N_ObjListaDistribucion.SelectAll(TxtBsc.Text.Trim()), ViewState("GrillaPpal_ColumnSorting"), ViewState("GrillaPpal_TypeSorting"))
            GrillaPpal.DataBind()

            GrillaPpal.Columns(1).ItemStyle.HorizontalAlign = HorizontalAlign.Center
            GrillaPpal.Columns(2).ItemStyle.HorizontalAlign = HorizontalAlign.Left
            GrillaPpal.Columns(3).ItemStyle.HorizontalAlign = HorizontalAlign.Center

            TxtBsc.Focus()
        Catch ex As Exception
            GrillaPpal.DataSource = Nothing
            GrillaPpal.DataBind()
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub GrillaPpal_BtnEditar_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
        Dim IdListaDistribucion As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblIdListaDistribucion"), Label)

        GetRegistro(IdListaDistribucion.Text)

        FrmPpalTitulo.InnerHtml = "<i class='far fa-edit'></i>&nbsp;&nbsp;Modificar <span Class='badge text-bg-warning'>Lista de Distribución</span>"
        FrmPpal_BtnGuardar.Text = "<i class='fa-regular fa-floppy-disk'></i>&nbsp;&nbsp;&nbsp;Actualizar"
        FrmPpal_TxtListaDistribucion.Focus()

        ModalShow(FrmPpal_dialog, FrmPpal)
    End Sub
    Protected Sub GrillaPpal_BtnEliminar_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
        Dim IdListaDistribucion As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblIdListaDistribucion"), Label)
        Dim ListaDistribucion As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblListaDistribucion"), Label)

        FrmDeleteTitulo.InnerHtml = "<i class='fa-solid fa-pen-to-square'></i>&nbsp;&nbsp;Eliminar Lista Distribución"

        FrmDelete_TxtId.Text = IdListaDistribucion.Text
        FrmDelete_Msg.Text = "Se eliminará el Lista Distribución <b>" & ListaDistribucion.Text & "</b>."

        ModalShow(FrmDelete_dialog, FrmDelete)
    End Sub
    Protected Sub GrillaPpal_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Pager Then
            Dim PiePagina As Label = e.Row.FindControl("GrillaPpal_CurrentPageLabel")
            Dim TotalPaginas As Integer = Convert.ToString(GrillaPpal.PageCount).Trim()
            PiePagina.Text = "Página " & Convert.ToString(GrillaPpal.PageIndex + 1).Trim() & " de " & TotalPaginas.ToString()
        End If
    End Sub
    Protected Sub GrillaPpal_BtnFirst_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        GrillaPpal.PageIndex = 0
        GrillaPpal.DataBind()
    End Sub
    Protected Sub GrillaPpal_BtnPrev_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        If (GrillaPpal.PageIndex <> 0) Then
            GrillaPpal.PageIndex = GrillaPpal.PageIndex - 1
            GrillaPpal.DataBind()
        End If
    End Sub
    Protected Sub GrillaPpal_BtnNext_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        If (GrillaPpal.PageIndex <> GrillaPpal.PageCount - 1) Then
            GrillaPpal.PageIndex = GrillaPpal.PageIndex + 1
            GrillaPpal.DataBind()
        End If
    End Sub
    Protected Sub GrillaPpal_BtnLast_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        GrillaPpal.PageIndex = GrillaPpal.PageCount - 1
        GrillaPpal.DataBind()
    End Sub
    Private Sub GrillaPpal_PreRender(sender As Object, e As EventArgs) Handles GrillaPpal.PreRender
        'Esto es para que la grilla muestre el paginador aunque tenga menos de 10 registros
        If GrillaPpal.BottomPagerRow IsNot Nothing Then
            If GrillaPpal.BottomPagerRow.Visible = False Then
                GrillaPpal.BottomPagerRow.Visible = True
            End If
        End If
    End Sub
    Private Sub GrillaPpal_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles GrillaPpal.Sorting
        ViewState("GrillaPpal_ColumnSorting") = e.SortExpression

        If GrillaPpal.Attributes(e.SortExpression) = SortDirection.Ascending Then
            ViewState("GrillaPpal_TypeSorting") = "DESC"
            GrillaPpal_ChangeHeader()
        Else
            ViewState("GrillaPpal_TypeSorting") = "ASC"
            GrillaPpal_ChangeHeader()
        End If

        GrillaPpal_Llenar()
    End Sub
    Private Sub GrillaPpal_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GrillaPpal.RowCommand
        If Trim(e.CommandArgument) = "ListaDistribucion" Then
            ViewState("GrillaPpal_ColumnIndex") = 2
        End If
    End Sub
    Private Sub GrillaPpal_ChangeHeader()
        Dim Base As String = "<span class='link-light'>XXXTITULOXXX <i class='fa-solid fa-ellipsis-vertical'></i></span>"
        Dim Type As String

        If ViewState("GrillaPpal_TypeSorting") = "DESC" Then
            Type = "<i class='fa-solid fa-arrow-down-z-a'></i>"
        Else
            Type = "<i class='fa-solid fa-arrow-down-a-z'></i>"
        End If

        Dim GrillaPpal_Indice = New Integer() {2}
        Dim GrillaPpal_Campo() As String = {"ListaDistribucion"}
        Dim GrillaPpal_Titulo() As String = {"Lista Distribución"}

        For I = 0 To GrillaPpal_Indice.Length - 1
            If ViewState("GrillaPpal_ColumnIndex") = GrillaPpal_Indice(I) Then
                GrillaPpal.Columns(GrillaPpal_Indice(I)).HeaderText = "<span class='link-light'>" & GrillaPpal_Titulo(I) & " " & Type & "</span>"
                If ViewState("GrillaPpal_TypeSorting") = "DESC" Then
                    GrillaPpal.Attributes(GrillaPpal_Campo(I)) = SortDirection.Descending
                Else
                    GrillaPpal.Attributes(GrillaPpal_Campo(I)) = SortDirection.Ascending
                End If
            Else
                GrillaPpal.Columns(GrillaPpal_Indice(I)).HeaderText = Replace(Base, "XXXTITULOXXX", GrillaPpal_Titulo(I))
                GrillaPpal.Attributes(GrillaPpal_Campo(I)) = SortDirection.Descending
            End If
        Next
    End Sub
#End Region

#Region "Formulario Principal"
    Protected Sub GetRegistro(ByVal IdListaDistribucion As Integer)
        Try
            FrmPpal_Limpiar()
            Dim E_ObjListaDistribucion As New E_ClsTblListaDistribucion
            Dim N_ObjListaDistribucion As New N_ClsTblListaDistribucion
            E_ObjListaDistribucion.IdListaDistribucion = IdListaDistribucion
            E_ObjListaDistribucion = N_ObjListaDistribucion.SelectRecord(E_ObjListaDistribucion)

            FrmPpal_TxtIdListaDistribucion.Text = E_ObjListaDistribucion.IdListaDistribucion
            FrmPpal_TxtListaDistribucion.Text = E_ObjListaDistribucion.ListaDistribucion.Trim()
            FrmPpal_DivUltAct.InnerHtml = E_ObjListaDistribucion.UltAct


            execJS("init()")

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub FrmPpal_Limpiar()
        FrmPpal_DivUltAct.InnerHtml = ""
        FrmPpal_TxtIdListaDistribucion.Text = ""

        FrmPpal_TxtListaDistribucion.Text = ""
        GetValidador(FrmPpal_TxtListaDistribucion, "form-control specialform", FrmPpal_TxtListaDistribucion_Msj, "", "LIMPIAR")

    End Sub
    Function FrmPpal_ValidarCampos() As Boolean
        Dim SW As Boolean = True

        If FrmPpal_TxtListaDistribucion.Text.Trim() = "" Then
            GetValidador(FrmPpal_TxtListaDistribucion, "form-control specialform", FrmPpal_TxtListaDistribucion_Msj, "* Obligatorio", "ERROR")
            SW = False
        Else
            GetValidador(FrmPpal_TxtListaDistribucion, "form-control specialform", FrmPpal_TxtListaDistribucion_Msj, "", "OK")
        End If

        Return SW
    End Function
    Protected Sub FrmPpal_BtnGuardar_Click(sender As Object, e As EventArgs)
        Try
            If Not FrmPpal_ValidarCampos() Then
                Exit Sub
            End If

            Dim ResultadoTransaccion As String
            Dim E_ObjListaDistribucion As New E_ClsTblListaDistribucion
            Dim N_ObjListaDistribucion As New N_ClsTblListaDistribucion

            E_ObjListaDistribucion.ListaDistribucion = FrmPpal_TxtListaDistribucion.Text.Trim()
            E_ObjListaDistribucion.LastTransactionUser = ViewState("MyIdUser")
            E_ObjListaDistribucion.Estado = True

            If FrmPpal_BtnGuardar.Text = "<i class='fa-regular fa-floppy-disk'></i>&nbsp;&nbsp;&nbsp;Guardar" Then
                ResultadoTransaccion = N_ObjListaDistribucion.Insert(E_ObjListaDistribucion)
                If ResultadoTransaccion <> "OK" Then
                    GetValidador(FrmPpal_TxtListaDistribucion, "form-control specialform", FrmPpal_TxtListaDistribucion_Msj, "", "LIMPIAR")
                    MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
                    ModalShow(FrmMsg_dialog, FrmMsg)
                    Exit Sub
                End If
            Else
                E_ObjListaDistribucion.IdListaDistribucion = FrmPpal_TxtIdListaDistribucion.Text.Trim()
                ResultadoTransaccion = N_ObjListaDistribucion.Update(E_ObjListaDistribucion)
                If ResultadoTransaccion <> "OK" Then
                    GetValidador(FrmPpal_TxtListaDistribucion, "form-control specialform", FrmPpal_TxtListaDistribucion_Msj, "", "LIMPIAR")
                    MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
                    ModalShow(FrmMsg_dialog, FrmMsg)
                    Exit Sub
                End If
            End If

            GrillaPpal_Llenar()
            ModalHide(FrmPpal_dialog, FrmPpal)

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub FrmPpal_BtnCerrar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmPpal_dialog, FrmPpal)
    End Sub
#End Region



#Region "FormularioUsuarios"
    Protected Sub FrmPpalUsuarios_BtnCerrar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmUsuarios_dialog, FrmUsuarios)
    End Sub
    Protected Sub GrillaPpal_BtnAddUsuarios_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
        Dim IdListaDistribucion As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblIdListaDistribucion"), Label)
        FrmUsuarios_LblIdListaDistribucion.Text = IdListaDistribucion.Text

        getLoadUsuarios()


        ModalShow(FrmUsuarios_dialog, FrmUsuarios)
        execJS("init()")
    End Sub
    Protected Sub getLoadUsuarios()
        Dim N_ObjListaDistribucionUsuario As New N_ClsTblListaDistribucionUsuario

        FrmUsuarios_CboUserGestionSearch.Items.Clear()
        FrmUsuarios_CboUserGestionSearch.DataTextField = "TextField"
        FrmUsuarios_CboUserGestionSearch.DataValueField = "ValueField"
        Dim reaultLoadCbo = N_ObjListaDistribucionUsuario.getUsuariosNoSeleccionados(FrmUsuarios_LblIdListaDistribucion.Text)
        FrmUsuarios_CboUserGestionSearch.DataSource = reaultLoadCbo
        FrmUsuarios_CboUserGestionSearch.DataBind()

        If FrmUsuarios_CboUserGestionSearch.Items.Count = 0 Then
            FrmUsuarios_BtnGrabar.Visible = False
            FrmUsuarios_CboUserGestionSearch.Visible = False
        Else
            FrmUsuarios_BtnGrabar.Visible = True
            FrmUsuarios_CboUserGestionSearch.Visible = True
        End If
        getUsuarioSeleccionadosGrilla()

        FrmUsuariosTitulo.InnerHtml = "<i class='far fa-edit'></i>&nbsp;&nbsp;Agregar  <span Class='badge text-bg-warning'>USUARIOS</span>"
        execJS("init()")
    End Sub
    Protected Sub FrmUsuarios_BtnGrabar_click(sender As Object, e As EventArgs)
        Try

            Dim E_ObjListaDistribucionUsuario As New E_ClsTblListaDistribucionUsuario
            Dim N_ObjListaDistribucionUsuario As New N_ClsTblListaDistribucionUsuario

            E_ObjListaDistribucionUsuario.IdListaDistribucion = FrmUsuarios_LblIdListaDistribucion.Text
            E_ObjListaDistribucionUsuario.IdUsuario = FrmUsuarios_CboUserGestionSearch.SelectedValue
            E_ObjListaDistribucionUsuario.LastTransactionUser = ViewState("MyIdUser")


            Dim ResultadoTransaccion As String = N_ObjListaDistribucionUsuario.Insert(E_ObjListaDistribucionUsuario)
            getLoadUsuarios()
            GrillaPpal_Llenar()

        Catch ex As Exception

        End Try
    End Sub

#End Region

#Region "Grilla FrmUsuarios_GrillaUsuarios"
    Private Sub getUsuarioSeleccionadosGrilla()
        Dim N_ObjListaDistribucionUsuario As New N_ClsTblListaDistribucionUsuario
        'Dim DtaTable As DataTable = N_ObjListaDistribucionUsuario.SelectAll(FrmUsuarios_LblIdListaDistribucion.Text)
        Dim DtaTable = SortingPageIndex(N_ObjListaDistribucionUsuario.SelectAll(FrmUsuarios_LblIdListaDistribucion.Text.Trim()), ViewState("FrmUsuarios_GrillaUsuarios_ColumnSorting"), ViewState("FrmUsuarios_GrillaUsuarios_TypeSorting"))





        FrmUsuarios_GrillaUsuarios.DataSource = DtaTable
        FrmUsuarios_GrillaUsuarios.DataBind()

        'If DtaTable.Rows.Count > 0 Then
        '    Dim row = DtaTable.Rows(0)
        '    FrmUsuariosUltAct.InnerHtml = row("UltAct")

        'End If

    End Sub
    Protected Sub FrmUsuarios_GrillaUsuarios_BtnFirst_Click(sender As Object, e As EventArgs)
        getUsuarioSeleccionadosGrilla()
        FrmUsuarios_GrillaUsuarios.PageIndex = 0
        FrmUsuarios_GrillaUsuarios.DataBind()
    End Sub
    Protected Sub FrmUsuarios_GrillaUsuarios_BtnPrev_Click(sender As Object, e As EventArgs)
        getUsuarioSeleccionadosGrilla()
        If (FrmUsuarios_GrillaUsuarios.PageIndex <> 0) Then
            FrmUsuarios_GrillaUsuarios.PageIndex = FrmUsuarios_GrillaUsuarios.PageIndex - 1
            FrmUsuarios_GrillaUsuarios.DataBind()
        End If
    End Sub
    Protected Sub FrmUsuarios_GrillaUsuarios_BtnNext_Click(sender As Object, e As EventArgs)
        getUsuarioSeleccionadosGrilla()
        If (FrmUsuarios_GrillaUsuarios.PageIndex <> FrmUsuarios_GrillaUsuarios.PageCount - 1) Then
            FrmUsuarios_GrillaUsuarios.PageIndex = FrmUsuarios_GrillaUsuarios.PageIndex + 1
            FrmUsuarios_GrillaUsuarios.DataBind()
        End If
    End Sub
    Protected Sub FrmUsuarios_GrillaUsuarios_BtnLast_Click(sender As Object, e As EventArgs)
        getUsuarioSeleccionadosGrilla()
        FrmUsuarios_GrillaUsuarios.PageIndex = FrmUsuarios_GrillaUsuarios.PageCount - 1
        FrmUsuarios_GrillaUsuarios.DataBind()
    End Sub
    Private Sub FrmUsuarios_GrillaUsuarios_PreRender(sender As Object, e As EventArgs) Handles FrmUsuarios_GrillaUsuarios.PreRender
        If FrmUsuarios_GrillaUsuarios.BottomPagerRow IsNot Nothing Then
            If FrmUsuarios_GrillaUsuarios.BottomPagerRow.Visible = False Then
                FrmUsuarios_GrillaUsuarios.BottomPagerRow.Visible = True
            End If
        End If
    End Sub
    Protected Sub FrmUsuarios_GrillaUsuarios_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Pager Then
            Dim PiePaginaUsuarios As Label = e.Row.FindControl("FrmUsuarios_GrillaUsuarios_CurrentPageLabel")
            Dim TotalPaginas As Integer = Convert.ToString(FrmUsuarios_GrillaUsuarios.PageCount).Trim()
            PiePaginaUsuarios.Text = "Página " & Convert.ToString(FrmUsuarios_GrillaUsuarios.PageIndex + 1).Trim() & " de " & TotalPaginas.ToString()

        End If
    End Sub
    Protected Sub FrmUsuarios_btnEliminaUsuario_click(sender As Object, e As EventArgs)
        Try
            Dim btn As LinkButton = sender
            Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
            Dim IdUsuario As Label = CType(FrmUsuarios_GrillaUsuarios.Rows(row.RowIndex).FindControl("LblIdUsuario"), Label)

            Dim N_ObjListaDistribucionUsuario As New N_ClsTblListaDistribucionUsuario
            Dim E_ObjListaDistribucionUsuario As New E_ClsTblListaDistribucionUsuario

            E_ObjListaDistribucionUsuario.IdUsuario = IdUsuario.Text
            E_ObjListaDistribucionUsuario.IdListaDistribucion = FrmUsuarios_LblIdListaDistribucion.Text
            E_ObjListaDistribucionUsuario.LastTransactionUser = ViewState("MyIdUser")

            Dim ResultadoTransaccion As String = N_ObjListaDistribucionUsuario.Delete(E_ObjListaDistribucionUsuario)
            If Mid(ResultadoTransaccion, 1, 2) = "OK" Then
                getLoadUsuarios()
                GrillaPpal_Llenar()
            Else
                MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
                ModalShow(FrmMsg_dialog, FrmMsg)
            End If
            execJS("init()")
        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.Message)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub

    Private Sub FrmUsuarios_GrillaUsuarios_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles FrmUsuarios_GrillaUsuarios.Sorting
        ViewState("FrmUsuarios_GrillaUsuarios_ColumnSorting") = e.SortExpression

        If FrmUsuarios_GrillaUsuarios.Attributes(e.SortExpression) = SortDirection.Ascending Then
            ViewState("FrmUsuarios_GrillaUsuarios_TypeSorting") = "DESC"
            FrmUsuarios_GrillaUsuarios_ChangeHeader()
        Else
            ViewState("FrmUsuarios_GrillaUsuarios_TypeSorting") = "ASC"
            FrmUsuarios_GrillaUsuarios_ChangeHeader()
        End If

        getUsuarioSeleccionadosGrilla()
    End Sub
    Private Sub FrmUsuarios_GrillaUsuarios_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles FrmUsuarios_GrillaUsuarios.RowCommand
        If Trim(e.CommandArgument) = "Persona" Then
            ViewState("FrmUsuarios_GrillaUsuarios_ColumnIndex") = 2
        End If
    End Sub
    Private Sub FrmUsuarios_GrillaUsuarios_ChangeHeader()
        Dim Base As String = "<span class='link-light'>XXXTITULOXXX <i class='fa-solid fa-ellipsis-vertical'></i></span>"
        Dim Type As String

        If ViewState("FrmUsuarios_GrillaUsuarios_TypeSorting") = "DESC" Then
            Type = "<i class='fa-solid fa-arrow-down-z-a'></i>"
        Else
            Type = "<i class='fa-solid fa-arrow-down-a-z'></i>"
        End If

        Dim FrmUsuarios_GrillaUsuarios_Indice = New Integer() {2}
        Dim FrmUsuarios_GrillaUsuarios_Campo() As String = {"Persona"}
        Dim FrmUsuarios_GrillaUsuarios_Titulo() As String = {"Persona"}

        For I = 0 To FrmUsuarios_GrillaUsuarios_Indice.Length - 1
            If ViewState("FrmUsuarios_GrillaUsuarios_ColumnIndex") = FrmUsuarios_GrillaUsuarios_Indice(I) Then
                FrmUsuarios_GrillaUsuarios.Columns(FrmUsuarios_GrillaUsuarios_Indice(I)).HeaderText = "<span class='link-light'>" & FrmUsuarios_GrillaUsuarios_Titulo(I) & " " & Type & "</span>"
                If ViewState("FrmUsuarios_GrillaUsuarios_TypeSorting") = "DESC" Then
                    FrmUsuarios_GrillaUsuarios.Attributes(FrmUsuarios_GrillaUsuarios_Campo(I)) = SortDirection.Descending
                Else
                    FrmUsuarios_GrillaUsuarios.Attributes(FrmUsuarios_GrillaUsuarios_Campo(I)) = SortDirection.Ascending
                End If
            Else
                FrmUsuarios_GrillaUsuarios.Columns(FrmUsuarios_GrillaUsuarios_Indice(I)).HeaderText = Replace(Base, "XXXTITULOXXX", FrmUsuarios_GrillaUsuarios_Titulo(I))
                FrmUsuarios_GrillaUsuarios.Attributes(FrmUsuarios_GrillaUsuarios_Campo(I)) = SortDirection.Descending
            End If
        Next
        execJS("init()")
    End Sub
#End Region


#Region "FrmDelete"
    Protected Sub FrmDelete_BtnCerrar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmDelete_dialog, FrmDelete)
    End Sub
    Protected Sub FrmDelete_BtnEliminar_Click(sender As Object, e As EventArgs)
        Try
            ModalHide(FrmDelete_dialog, FrmDelete)

            Dim E_ObjListaDistribucion As New E_ClsTblListaDistribucion
            Dim N_ObjListaDistribucion As New N_ClsTblListaDistribucion
            Dim ResultadoTransaccion As String

            E_ObjListaDistribucion.IdListaDistribucion = FrmDelete_TxtId.Text.Trim()
            E_ObjListaDistribucion.LastTransactionUser = ViewState("MyIdUser")

            ResultadoTransaccion = N_ObjListaDistribucion.Delete(E_ObjListaDistribucion)
            If ResultadoTransaccion <> "OK" Then
                MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
                ModalShow(FrmMsg_dialog, FrmMsg)
                Exit Sub
            End If

            GrillaPpal_Llenar()

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
#End Region

#Region "FrmMsg"
    Protected Sub FrmMsg_BtnAceptar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmMsg_dialog, FrmMsg)
    End Sub
#End Region

    Private Sub execJS(ByVal script As String)
        Try
            Dim TxtJavascript As String = "<script type='text/javascript'>" + script + "</script>"
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "alerta", TxtJavascript, False)
        Catch ex As Exception

        End Try
    End Sub


End Class