﻿Imports Capa_Entidad
Imports Capa_Negocio
Imports System.Data


Public Class FrmPendientesAprobacion
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Try
        '    'Dim URL As String = Page.Request.UrlReferrer.AbsolutePath
        'Catch ex As Exception
        '    Response.Redirect("../PageError.aspx")
        'End Try


        If Not Page.IsPostBack Then
            'Dim var_krypton As String = OwnDecryption(Page.Request("krypton"))
            'ViewState("MyIdUser") = OwnPageRequest(var_krypton, "IdUser")
            ViewState("MyIdUser") = "carlos.astorga@kantar.com"
            setTableCondicion()
            GrillaPpal_Llenar()


        End If
    End Sub
    Private Sub setTableCondicion()
        ViewState("tablaCondiciones") = Nothing

        Dim tablaCondiciones As New DataTable
        tablaCondiciones.Columns.Add("IdCondicion", GetType(Integer))
        tablaCondiciones.Columns.Add("idCampoCondicion", GetType(String))
        tablaCondiciones.Columns.Add("idOperadorLogico", GetType(String))
        tablaCondiciones.Columns.Add("valor", GetType(String))
        tablaCondiciones.Columns.Add("andOr", GetType(String))
        tablaCondiciones.Columns.Add("CadenaCondicion", GetType(String))
        tablaCondiciones.Columns.Add("StringCampoCondicion", GetType(String))
        tablaCondiciones.Columns.Add("StringOperadorLogico", GetType(String))

        ViewState("tablaCondiciones") = tablaCondiciones
        ViewState("IdCorretalivoCondicion") = 0
        FrmlPpal_IndexUpdate.Text = ""
        'setTablaGridViewCondiciones()

    End Sub


#Region "Grilla Principal"
    Protected Sub GrillaPpal_Llenar()
        Try
            Dim N_ObjCritica As New N_ClsTblCritica
            GrillaPpal.DataSource = N_ObjCritica.SelectAllPenditeAprobacion(ViewState("MyIdUser"))
            GrillaPpal.DataBind()

            GrillaPpal.Columns(1).ItemStyle.HorizontalAlign = HorizontalAlign.Center
            GrillaPpal.Columns(2).ItemStyle.HorizontalAlign = HorizontalAlign.Left
            GrillaPpal.Columns(3).ItemStyle.HorizontalAlign = HorizontalAlign.Left

            TxtBsc.Focus()
        Catch ex As Exception
            GrillaPpal.DataSource = Nothing
            GrillaPpal.DataBind()
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub GrillaPpal_BtnEditar_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
        Dim IdCritica As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblIdCritica"), Label)
        Dim IdCarpeta As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblIdCarpeta"), Label)

        FrmPpal_TxtIdCritica.Text = IdCritica.Text
        FrmPpal_TxtIdCarpeta.Text = IdCarpeta.Text

        ' FrmPpal_Version.Text = 1



        FrmPpalTitulo.InnerHtml = "<i class='far fa-edit'></i>&nbsp;&nbsp;Modificar <span Class='badge text-bg-warning'>Critica</span>"
        FrmPpal_BtnAprobar.Text = "<i class='fa-regular fa-floppy-disk'></i>&nbsp;&nbsp;&nbsp;Aprobar Critica"
        loadFormCritica()


        ModalShow(FrmPpal_dialog, FrmPpal)
    End Sub
    Protected Sub GrillaPpal_BtnEliminar_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
        Dim IdCritica As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblIdCritica"), Label)
        'Dim Workflow As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblWorkflow"), Label)

        FrmDeleteTitulo.InnerHtml = "<i class='fa-solid fa-pen-to-square'></i>&nbsp;&nbsp;Eliminar Critica"

        FrmDelete_TxtId.Text = IdCritica.Text
        FrmDelete_Msg.Text = "Se eliminará la critica <b>Nº" & IdCritica.Text & "</b>."

        ModalShow(FrmDelete_dialog, FrmDelete)
    End Sub
    Protected Sub GrillaPpal_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Pager Then
            Dim PiePagina As Label = e.Row.FindControl("GrillaPpal_CurrentPageLabel")
            Dim TotalPaginas As Integer = Convert.ToString(GrillaPpal.PageCount).Trim()
            PiePagina.Text = "Página " & Convert.ToString(GrillaPpal.PageIndex + 1).Trim() & " de " & TotalPaginas.ToString()
        End If
    End Sub
    Protected Sub GrillaPpal_BtnFirst_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        GrillaPpal.PageIndex = 0
        GrillaPpal.DataBind()
    End Sub
    Protected Sub GrillaPpal_BtnPrev_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        If (GrillaPpal.PageIndex <> 0) Then
            GrillaPpal.PageIndex = GrillaPpal.PageIndex - 1
            GrillaPpal.DataBind()
        End If
    End Sub
    Protected Sub GrillaPpal_BtnNext_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        If (GrillaPpal.PageIndex <> GrillaPpal.PageCount - 1) Then
            GrillaPpal.PageIndex = GrillaPpal.PageIndex + 1
            GrillaPpal.DataBind()
        End If
    End Sub
    Protected Sub GrillaPpal_BtnLast_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        GrillaPpal.PageIndex = GrillaPpal.PageCount - 1
        GrillaPpal.DataBind()
    End Sub
    Private Sub GrillaPpal_PreRender(sender As Object, e As EventArgs) Handles GrillaPpal.PreRender
        'Esto es para que la grilla muestre el paginador aunque tenga menos de 10 registros
        If GrillaPpal.BottomPagerRow IsNot Nothing Then
            If GrillaPpal.BottomPagerRow.Visible = False Then
                GrillaPpal.BottomPagerRow.Visible = True
            End If
        End If
    End Sub
    Private Sub GrillaPpal_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles GrillaPpal.Sorting
        ViewState("GrillaPpal_ColumnSorting") = e.SortExpression

        If GrillaPpal.Attributes(e.SortExpression) = SortDirection.Ascending Then
            ViewState("GrillaPpal_TypeSorting") = "DESC"
            GrillaPpal_ChangeHeader()
        Else
            ViewState("GrillaPpal_TypeSorting") = "ASC"
            GrillaPpal_ChangeHeader()
        End If

        GrillaPpal_Llenar()
    End Sub
    Private Sub GrillaPpal_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GrillaPpal.RowCommand
        If Trim(e.CommandArgument) = "Codigo" Then
            ViewState("GrillaPpal_ColumnIndex") = 2
        End If
    End Sub
    Private Sub GrillaPpal_ChangeHeader()
        Dim Base As String = "<span class='link-light'>XXXCodigoXXX <i class='fa-solid fa-ellipsis-vertical'></i></span>"
        Dim Type As String

        If ViewState("GrillaPpal_TypeSorting") = "DESC" Then
            Type = "<i class='fa-solid fa-arrow-down-z-a'></i>"
        Else
            Type = "<i class='fa-solid fa-arrow-down-a-z'></i>"
        End If

        Dim GrillaPpal_Indice = New Integer() {2}
        Dim GrillaPpal_Campo() As String = {"Codigo"}
        Dim GrillaPpal_Titulo() As String = {"Job Position"}

        For I = 0 To GrillaPpal_Indice.Length - 1
            If ViewState("GrillaPpal_ColumnIndex") = GrillaPpal_Indice(I) Then
                GrillaPpal.Columns(GrillaPpal_Indice(I)).HeaderText = "<span class='link-light'>" & GrillaPpal_Titulo(I) & " " & Type & "</span>"
                If ViewState("GrillaPpal_TypeSorting") = "DESC" Then
                    GrillaPpal.Attributes(GrillaPpal_Campo(I)) = SortDirection.Descending
                Else
                    GrillaPpal.Attributes(GrillaPpal_Campo(I)) = SortDirection.Ascending
                End If
            Else
                GrillaPpal.Columns(GrillaPpal_Indice(I)).HeaderText = Replace(Base, "XXXCodigoXXX", GrillaPpal_Titulo(I))
                GrillaPpal.Attributes(GrillaPpal_Campo(I)) = SortDirection.Descending
            End If
        Next
    End Sub
#End Region

#Region "FrmComentario"
    Private Sub getComentarios()
        Try
            Dim E_ObjTblComentario As New E_ClsTblComentario
            Dim N_ObjTblComentario As New N_ClsTblComentario


            E_ObjTblComentario.IdCritica = FrmPpal_TxtIdCritica.Text
            Dim DtaTable As DataTable = N_ObjTblComentario.SelectAll(E_ObjTblComentario)
            ViewState("Comentarios") = ""
            For Each row As DataRow In DtaTable.Rows

                ViewState("Comentarios") += "<div Class='col-11 mt-4'>"
                ViewState("Comentarios") += "  <span Class='text-success'> " + CStr(row("Fecha")) + "</span>- " + CStr(row("Comentario"))
                ViewState("Comentarios") += " <div Class='text-end'><i> " + CStr(row("LastTransactionUser")) + " - " + CStr(row("Nombre")) + "</i><hr></div></div>"

            Next row

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub FrmPpal_Comentario_click(sender As Object, e As EventArgs)
        TabActiveForm = 2
        TabSelected()
        ModalShow(FrmComentarios_dialog, FrmComentarios)
    End Sub

    Protected Sub FrmComentarios_BtnCerrar_Click(sender As Object, e As EventArgs)
        TabActiveForm = 2
        TabSelected()
        ModalHide(FrmComentarios_dialog, FrmComentarios)
    End Sub

    Protected Sub FrmComentarios_BtnComentar_Click(sender As Object, e As EventArgs)
        Try
            TabActiveForm = 2

            Dim E_ObjTblComentario As New E_ClsTblComentario
            Dim N_ObjTblComentario As New N_ClsTblComentario
            E_ObjTblComentario.Comentario = FrmlPpal_traspasaComentario.Text
            E_ObjTblComentario.LastTransactionUser = ViewState("MyIdUser")
            E_ObjTblComentario.IdCritica = FrmPpal_TxtIdCritica.Text

            N_ObjTblComentario.Insert(E_ObjTblComentario)

            FrmlPpal_traspasaComentario.Text = ""

            TabSelected()
            getComentarios()
            execJS("init()")
            ModalHide(FrmComentarios_dialog, FrmComentarios)

        Catch ex As Exception

        End Try


    End Sub

#End Region


#Region "Index"
    Protected Sub BtnBsc_Click(sender As Object, e As EventArgs)

        GrillaPpal_Llenar()
        'Dim N_ObjEjecucion As New N_ClsTblEjecucion
        'GridViewCondiciones.DataSource = SortingPageIndex(N_ObjEjecucion.SelectAll(TxtBsc.Text.Trim()), ViewState("GrillaPpal_ColumnSorting"), ViewState("GrillaPpal_TypeSorting"))
        'GridViewCondiciones.DataBind()

        'GridViewCondiciones.Columns(1).ItemStyle.HorizontalAlign = HorizontalAlign.Center
        'GridViewCondiciones.Columns(2).ItemStyle.HorizontalAlign = HorizontalAlign.Left
        'GridViewCondiciones.Columns(3).ItemStyle.HorizontalAlign = HorizontalAlign.Left


    End Sub
    Protected Sub BtnAddRegistro_Click(sender As Object, e As EventArgs)
        Try
            FrmPpal_Limpiar()

            FrmPpal_TxtIdCritica.Text = ""
            FrmPpalTitulo.InnerHtml = "Crear <span Class='badge text-bg-warning'>CRITICA</span>"
            FrmPpal_BtnAprobar.Text = "<i class='fa-regular fa-floppy-disk'></i>&nbsp;&nbsp;&nbsp;Guardar Borrador"

            loadFormCritica()

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub


    Private Sub TabSelected()

        Ffrm_principal_TabOpc1.Attributes.Add("class", "nav-link")
        CriticaPanelTab.Attributes.Add("class", "tab-pane fade ")

        Ffrm_principal_TabOpc2.Attributes.Add("class", "nav-link")
        comentarioPanelTab.Attributes.Add("class", "tab-pane fade ")

        Ffrm_principal_TabOpc3.Attributes.Add("class", "nav-link")
        LogPanelTab.Attributes.Add("class", "tab-pane fade ")

        Select Case True
            Case TabActiveForm = "1"
                Ffrm_principal_TabOpc1.Attributes.Add("class", "nav-link active")
                CriticaPanelTab.Attributes.Add("class", "tab-pane fade show active")
            Case TabActiveForm = "2"
                Ffrm_principal_TabOpc2.Attributes.Add("class", "nav-link active")
                comentarioPanelTab.Attributes.Add("class", "tab-pane fade show active")
            Case TabActiveForm = "3"
                Ffrm_principal_TabOpc3.Attributes.Add("class", "nav-link active")
                LogPanelTab.Attributes.Add("class", "tab-pane fade show active")
        End Select


    End Sub
    Public Shared Property TabActiveForm As Integer = 1
    Public Shared Property FrmPpal_Correlativo_label As String

    Private Sub loadFormCritica()
        Try


            TabActiveForm = 1
            TabSelected()
            setTableCondicion()

            FrmPpal_BtnAprobar.Visible = False
            FrmPpal_BtnRechazar.Visible = False

            Dim N_ObjPrefijo As New N_ClsTblPrefijo
            FrmPpal_CboPrefijo.Items.Clear()
            FrmPpal_CboPrefijo.DataTextField = "TextField"
            FrmPpal_CboPrefijo.DataValueField = "ValueField"

            Dim reaultLoadCbo = N_ObjPrefijo.LoadCbo(FrmPpal_TxtIdCarpeta.Text)
            FrmPpal_CboPrefijo.DataSource = reaultLoadCbo
            FrmPpal_CboPrefijo.DataBind()

            Dim N_ObjCritica As New N_ClsTblCritica
            Dim ResultadoCorrelativo As String

            ResultadoCorrelativo = N_ObjCritica.getCorrelativoCarpeta(FrmPpal_TxtIdCarpeta.Text)
            FrmPpal_Correlativo.Text = ResultadoCorrelativo
            FrmPpal_Correlativo_label = ResultadoCorrelativo

            Dim N_ObjVariableAccion As New N_ClsTblVariableAccion
            Frm_CmbVariablesAccion.Items.Clear()
            Frm_CmbVariablesAccion.DataTextField = "TextField"
            Frm_CmbVariablesAccion.DataValueField = "ValueField"
            Dim reaultVariableAccionLoadCbo = N_ObjVariableAccion.LoadCbo()
            Frm_CmbVariablesAccion.DataSource = reaultVariableAccionLoadCbo
            Frm_CmbVariablesAccion.DataBind()

            Frm_CmbVariablesAccion.Items.Insert(0, New ListItem("<< Seleccionar >>", "0"))


            Dim N_ObjVariableCondicion As New N_ClsTblVariableCondicion
            Frm_CmbVariablesCondicion.Items.Clear()
            Frm_CmbVariablesCondicion.DataTextField = "TextField"
            Frm_CmbVariablesCondicion.DataValueField = "ValueField"
            Frm_CmbVariablesCondicion.DataMember = "ValueField"

            Dim reaultVariableCondicionLoadCbo = N_ObjVariableCondicion.LoadCbo()
            Frm_CmbVariablesCondicion.DataSource = reaultVariableCondicionLoadCbo
            Frm_CmbVariablesCondicion.DataBind()

            Frm_CmbVariablesCondicion.Items.Insert(0, New ListItem("<< Seleccionar >>", "0"))


            Dim N_ObjWorkFlow As New N_ClsTblWorkflow
            Dim reaultWorkAprobacionLoadCbo = N_ObjWorkFlow.LoadCbo()
            FrmPpal_CmbWorkFlowAprobacion.DataTextField = "TextField"
            FrmPpal_CmbWorkFlowAprobacion.DataValueField = "ValueField"
            FrmPpal_CmbWorkFlowAprobacion.DataSource = reaultWorkAprobacionLoadCbo
            FrmPpal_CmbWorkFlowAprobacion.DataBind()
            FrmPpal_CmbWorkFlowAprobacion.Items.Insert(0, New ListItem("<< Seleccionar >>", "0"))
            FrmPpal_Version.Text = 1


            setTableCondicion()
            If FrmPpal_TxtIdCritica.Text <> "" Then
                Dim E_ObjCritica As New E_ClsTblCritica
                E_ObjCritica.IdCritica = FrmPpal_TxtIdCritica.Text
                E_ObjCritica = N_ObjCritica.SelectRecord(E_ObjCritica)
                FrmPpal_Version.Text = E_ObjCritica.Version
                FrmPpal_Correlativo.Text = (E_ObjCritica.CorrelativoAux)
                FrmPpal_Correlativo_label = E_ObjCritica.CorrelativoAux

                FrmPpal_CmbWorkFlowAprobacion.SelectedValue = E_ObjCritica.IdWorkflow
                Frm_CmbVariablesAccion.SelectedValue = (E_ObjCritica.IdVariableAccion)

                Dim num = FrmPpal_CmbWorkFlowAprobacion.SelectedValue
                FrmPpal_CmbWorkFlowAprobacionNoEdit.Text = FrmPpal_CmbWorkFlowAprobacion.Items(FrmPpal_CmbWorkFlowAprobacion.SelectedIndex).Text

                Frm_TxtValor.Text = E_ObjCritica.VariableAccionValor


                frmPpal_textFechaVencimiento.Text = DateTime.Parse(E_ObjCritica.FechaRevision).Date.ToString("yyyy-MM-dd")
                FrmPpal_AutorLog.InnerText = E_ObjCritica.Owner
                FrmPpal_FechaCreacionLog.InnerText = E_ObjCritica.FechaCreacion.ToString("yyyy-MM-dd hh:mm:ss")
                FrmPpal_EstadoActualLog.InnerText = E_ObjCritica.EstadoAux

                'Dim dtPendiente As DataTable =
                GrillaRutaAprobacion.DataSource = N_ObjCritica.getPendientesAprobacion(FrmPpal_TxtIdCritica.Text)
                GrillaRutaAprobacion.DataBind()

                Dim isActivoBtonApruebaRechaza As Boolean = True
                For Each row As DataRow In GrillaRutaAprobacion.DataSource.Rows
                    If row("Aprobadores") <> "" Then

                        Dim index As Integer = row("Aprobadores").IndexOf(ViewState("MyIdUser"))
                        If index >= 0 Then
                            isActivoBtonApruebaRechaza = False
                            FrmPpal_BtnAprobar.Visible = True
                            FrmPpal_BtnRechazar.Visible = True
                            Exit For
                        End If
                    End If

                Next row



                FrmPpal_Justificacion.Text = E_ObjCritica.Justificacion
                FrmPpal_TxtJustificacionNoEdit.Text = E_ObjCritica.Justificacion

                Dim N_ObjCriticaCondicion As New N_ClsTblCriticaCondicion
                Dim E_ObjTblCriticaCondicion As New E_ClsTblCriticaCondicion
                Dim DtaTable As DataTable = N_ObjCriticaCondicion.SelectByCritica(FrmPpal_TxtIdCritica.Text)
                FrmPpal_LogCritica.Visible = True

                If E_ObjCritica.IdEstado = 1 Then
                    FrmPpal_panelEdit.Visible = True
                    FrmPpal_panelNoEdit.Visible = False

                Else
                    FrmPpal_panelEdit.Visible = False
                    FrmPpal_panelNoEdit.Visible = True
                End If


                Dim recorre As Integer = 1


                For Each row As DataRow In DtaTable.Rows
                    ViewState("tablaCondiciones").Rows.Add(
                    CInt(ViewState("IdCorretalivoCondicion")),
                    row("IdVariableCondicion"),
                    row("OperadorLogico"),
                    row("Valor1"),
                    row("AndOr"),
                    Replace(row("Valor2"), "##", "'"),
                    row("strVariableCondicion"),
                    row("strOperadorLogico")
                )
                    ViewState("IdCorretalivoCondicion") = CInt(ViewState("IdCorretalivoCondicion")) + 1
                Next row
                getComentarios()
                getLogCritica()
                LoadSetVariableAccion()
                setTablaGridViewCondiciones()
                execJS("init();")
            Else

                FrmPpal_Version.Text = 1
                ResultadoCorrelativo = N_ObjCritica.getCorrelativoCarpeta(FrmPpal_TxtIdCarpeta.Text)
                FrmPpal_Correlativo.Text = ResultadoCorrelativo
                FrmPpal_Correlativo_label = ResultadoCorrelativo
                FrmPpal_CmbWorkFlowAprobacion.Items.Item(0).Selected = True

                'Frm_CmbOperadorLogico.Items.Item(0).Selected = True
                'Frm_CmbVariablesAccion.Items.Clear()

                Frm_CmbVariablesCondicion.Items.Item(0).Selected = True
                FrmPpal_AutorLog.InnerText = ViewState("MyIdUser")
                FrmPpal_FechaCreacionLog.InnerText = DateTime.Now.ToString("dd/MM/yyyy")
                FrmPpal_EstadoActualLog.InnerText = "Creando"
                Frm_TxtValor.Text = ""


                FrmPpal_LogCritica.Visible = False
                execJS("init();")
            End If

            ModalShow(FrmPpal_dialog, FrmPpal)



        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)

        End Try
    End Sub

    Protected Sub setAND(sender As Object, e As EventArgs)
        Dim actualOpc = frmPpal_lbelAndOr.Text
        Dim optionSelected As String = ""

        If actualOpc = "AND" Then
            optionSelected = ""
        Else
            optionSelected = "AND"
        End If
        frmPpal_lbelAndOr.Text = optionSelected

    End Sub
    Protected Sub setOR(sender As Object, e As EventArgs)
        Dim actualOpc = frmPpal_lbelAndOr.Text
        Dim optionSelected As String = ""

        If actualOpc = "OR" Then
            optionSelected = ""
        Else
            optionSelected = "OR"
        End If
        frmPpal_lbelAndOr.Text = optionSelected


    End Sub

    Protected Sub getDetailVariableAccion(sender As Object, e As EventArgs)
        LoadSetVariableAccion()
    End Sub
    Protected Sub getOperadorLogistico(sender As Object, e As EventArgs)

        LoadSetVariableCondicion()
        verifyButtonAddCondition()
    End Sub

    Protected Sub setOperadorLogistico(sender As Object, e As EventArgs)
        LoadSetOperadorLogico()
        verifyButtonAddCondition()
    End Sub
    Protected Sub setValorCondicion(sender As Object, e As EventArgs)
        LoadSetValorCondicion()
        verifyButtonAddCondition()
    End Sub

    Private Sub verifyButtonAddCondition()
        If Frm_CmbVariablesCondicion.SelectedValue <> "0" And Frm_CmbOperadorLogico.SelectedValue <> "0" And Frm_TxtValorCondicion.Text <> "" Then
            FrmPpal_BtntraspasaTranscripcionCondicion.Visible = True
            FrmPpal_BtntraspasaTranscripcionCondicion.Attributes("disabled") = "disabled"
        Else
            FrmPpal_BtntraspasaTranscripcionCondicion.Attributes.Remove("disabled")
            FrmPpal_BtntraspasaTranscripcionCondicion.Visible = False
        End If

    End Sub

    Public Shared Property isComillas As Boolean

    Private Sub LoadSetValorCondicion()
        Dim condicionComillas As String = ""

        If isComillas = True Then
            condicionComillas = "'"
        End If
        frmPpal_lbelValorCondicion.Text = condicionComillas + Frm_TxtValorCondicion.Text + condicionComillas

    End Sub

    Private Sub LoadSetOperadorLogico()

        If Frm_CmbOperadorLogico.SelectedValue <> "0" Then
            Frm_TxtValor.Enabled = True

            Dim N_ObjOperadorLogico As New N_ClsTblOperadorLogico
            Dim E_ObjOperadorLogico As New E_ClsTblOperadorLogico

            E_ObjOperadorLogico.IdOperadorLogico = Frm_CmbOperadorLogico.SelectedValue
            E_ObjOperadorLogico = N_ObjOperadorLogico.SelectRecord(E_ObjOperadorLogico)


            ' Dim campoOperadorLogicoText As String = Frm_CmbVariablesCondicion.SelectedItem.Text
            frmPpal_lbelOperadorLogico.Text = E_ObjOperadorLogico.OperadorLogico

        Else
            Frm_CmbOperadorLogico.Attributes("disabled") = "disabled"
        End If

        TabActiveForm = 1
        TabSelected()

        execJS("init()")


    End Sub

    Private Sub LoadSetVariableCondicion()
        Dim N_ObjVariableCondicion As New N_ClsTblVariableCondicion
        Dim E_ObjVariableCondicion As New E_ClsTblVariableCondicion

        If Frm_CmbVariablesCondicion.SelectedValue <> "0" Then
            Frm_TxtValor.Enabled = True

            Dim N_ObjOperadorLogico As New N_ClsTblOperadorLogico
            Dim E_ObjOperadorLogico As New E_ClsTblOperadorLogico

            E_ObjVariableCondicion.IdVariableCondicion = Frm_CmbVariablesCondicion.SelectedValue
            E_ObjVariableCondicion = N_ObjVariableCondicion.SelectRecord(E_ObjVariableCondicion)


            isComillas = E_ObjVariableCondicion.isComilla
            If isComillas = True Then
                Frm_TxtValorCondicion.Attributes("type") = "text"
            Else
                Frm_TxtValorCondicion.Attributes("type") = "number"
            End If

            LoadSetValorCondicion()

            Frm_CmbOperadorLogico.Items.Clear()
            Frm_CmbOperadorLogico.DataTextField = "TextField"
            Frm_CmbOperadorLogico.DataValueField = "ValueField"
            Dim reaultOperadorLoadCbo = N_ObjOperadorLogico.LoadCbo(Frm_CmbVariablesCondicion.SelectedValue)
            Frm_CmbOperadorLogico.DataSource = reaultOperadorLoadCbo
            Frm_CmbOperadorLogico.DataBind()
            Frm_CmbOperadorLogico.Items.Insert(0, New ListItem("<< Seleccionar >>", "0"))
            Frm_CmbOperadorLogico.Attributes.Remove("disabled")

            Dim campoCondicionText As String = Frm_CmbVariablesCondicion.SelectedItem.Text
            frmPpal_lbelTranscripcionCampo.Text = E_ObjVariableCondicion.VariableCondicion

        Else
            Frm_CmbOperadorLogico.Attributes("disabled") = "disabled"
        End If

        TabActiveForm = 1
        TabSelected()

        execJS("init()")

    End Sub

    Private Sub LoadSetVariableAccion()
        Dim N_ObjVariableAccion As New N_ClsTblVariableAccion
        Dim E_ObjVariableAccion As New E_ClsTblVariableAccion

        If Frm_CmbVariablesAccion.SelectedValue <> "0" Then
            Frm_TxtValor.Enabled = True
            Frm_TxtValor.Attributes.Remove("disabled")

            E_ObjVariableAccion.IdVariableAccion = Frm_CmbVariablesAccion.SelectedValue
            E_ObjVariableAccion = N_ObjVariableAccion.SelectRecord(E_ObjVariableAccion)

            Dim isComillaString As String = ""
            Frm_TxtValor.Attributes("type") = "number"
            If E_ObjVariableAccion.isComilla = True Then
                isComillaString = "'"
                Frm_TxtValor.Attributes("type") = "text"

            End If

            Dim stringCadena As String = "<strong class='text-success'>" + E_ObjVariableAccion.VariableAccion + "</strong> = <strong class='text-danger'>" + isComillaString + "<span id='valorVariableAccion'>" + Frm_TxtValor.Text + "</span>" + isComillaString + "</strong>"
            FrmPpal_ResultLabelTransaccion.Text = stringCadena
            FrmPpal_ResultLabelTransaccionNoEdit.Text = stringCadena

        Else
            'Frm_TxtValor.Enabled = False
            Frm_TxtValor.Attributes("disabled") = "disabled"
            Frm_TxtValor.Attributes.Add("class", "form-control campoAccionChange valorAccionClass")
            'Frm_TxtValor.Attributes.
        End If

        ' Frm_CmbVariablesAccion.SelectedValue = E_ObjVariableAccion.IdVariableAccion
        TabActiveForm = 1
        TabSelected()

        execJS("init()")

    End Sub


    Public correlativoTabla As Integer = 0
    Protected Sub FrmPpal_BtnEditar_Click(sender As Object, e As EventArgs)
        Try
            Dim tbl As DataTable = ViewState("tablaCondiciones")
            Dim btn As LinkButton = sender
            Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
            Dim IdCondicion As Label = CType(GridViewCondiciones.Rows(row.RowIndex).FindControl("LblIdFilaCondicion"), Label)

            Dim index As Integer = 0
            Dim idModifica As Integer = 0
            Dim recorre As Integer
            For Each fila As DataRow In tbl.Rows
                recorre = CInt(fila("IdCondicion"))

                If CInt(IdCondicion.Text) = recorre Then

                    Frm_CmbVariablesCondicion.SelectedValue = fila("idCampoCondicion")
                    LoadSetVariableCondicion()



                    Frm_CmbOperadorLogico.SelectedValue = fila("idOperadorLogico")
                    Frm_TxtValorCondicion.Text = fila("valor")
                    FrmlPpal_OpcAndOr.Text = fila("andOr")

                    frmPpal_lbelTranscripcionCampo.Text = fila("StringCampoCondicion")
                    frmPpal_lbelOperadorLogico.Text = fila("StringOperadorLogico")
                    frmPpal_lbelValorCondicion.Text = fila("valor")
                    frmPpal_lbelAndOr.Text = fila("andOr")

                    FrmlPpal_IndexUpdate.Text = index

                    verifyButtonAddCondition()
                End If
                index += 1
            Next fila

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub FrmPpal_BtnEliminar_Click(sender As Object, e As EventArgs)
        Try
            Dim tbl As DataTable = ViewState("tablaCondiciones")
            Dim btn As LinkButton = sender
            Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
            Dim IdCondicion As Label = CType(GridViewCondiciones.Rows(row.RowIndex).FindControl("LblIdFilaCondicion"), Label)
            'Dim Workflow As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblWorkflow"), Label)

            Dim index As Integer = 0
            Dim idElimina As Integer = 0
            Dim recorre As Integer
            For Each fila As DataRow In tbl.Rows
                recorre = CInt(fila("IdCondicion"))
                If CInt(IdCondicion.Text) = recorre Then
                    idElimina = index
                End If
                index += 1
            Next fila
            tbl.Rows.RemoveAt(idElimina)

            ViewState("tablaCondiciones") = tbl
            setTablaGridViewCondiciones()

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub FrmPpal_BtntraspasaTranscripcionCondicion_click(sender As Object, e As EventArgs)
        Try
            Dim cadenaTraspasa As String
            cadenaTraspasa = "<span style='color:blue;font-weight:bold;'>" + frmPpal_lbelTranscripcionCampo.Text + "</span>&nbsp;"
            cadenaTraspasa += "<span style='color:red;font-weight:bold;'>" + frmPpal_lbelOperadorLogico.Text + "</span>&nbsp;"
            cadenaTraspasa += "<span style='color:green;font-weight:bold;'>" + frmPpal_lbelValorCondicion.Text + "</span>&nbsp;"
            cadenaTraspasa += "<span style='color:black;font-weight:bold;'>" + frmPpal_lbelAndOr.Text + "</span>&nbsp;"


            Dim CmbVariablesCondicion = (Frm_CmbVariablesCondicion.SelectedValue)
            Dim CmbOperadorLogico = (Frm_CmbOperadorLogico.SelectedValue)
            Dim lbelValorCondicion = frmPpal_lbelValorCondicion.Text
            Dim lbelAndOr = frmPpal_lbelAndOr.Text
            Dim cadena = cadenaTraspasa


            If FrmlPpal_IndexUpdate.Text <> "" Then
                Dim tbl As DataTable = ViewState("tablaCondiciones")
                Dim dr As DataRow = ViewState("tablaCondiciones").Rows(CInt(FrmlPpal_IndexUpdate.Text))
                dr("idCampoCondicion") = CmbVariablesCondicion
                dr("idOperadorLogico") = CmbOperadorLogico
                dr("valor") = lbelValorCondicion
                dr("andOr") = lbelAndOr
                dr("CadenaCondicion") = cadenaTraspasa
                dr("StringCampoCondicion") = frmPpal_lbelTranscripcionCampo.Text
                dr("StringOperadorLogico") = frmPpal_lbelOperadorLogico.Text


                ViewState("IdCorretalivoCondicion") = tbl
            Else
                Dim correlativo = CInt(ViewState("IdCorretalivoCondicion"))
                ViewState("tablaCondiciones").Rows.Add(
                    correlativo,
                    CmbVariablesCondicion,
                    CmbOperadorLogico,
                    lbelValorCondicion,
                    lbelAndOr,
                    cadenaTraspasa,
                    frmPpal_lbelTranscripcionCampo.Text,
                    frmPpal_lbelOperadorLogico.Text
                )
                ViewState("IdCorretalivoCondicion") = CInt(ViewState("IdCorretalivoCondicion")) + 1
            End If

            Frm_CmbVariablesCondicion.SelectedValue = 0
            Frm_CmbOperadorLogico.SelectedValue = 0
            Frm_TxtValorCondicion.Text = ""
            cadenaTraspasa = ""

            frmPpal_lbelTranscripcionCampo.Text = ""
            frmPpal_lbelOperadorLogico.Text = ""
            frmPpal_lbelValorCondicion.Text = ""
            frmPpal_lbelAndOr.Text = ""
            FrmlPpal_IndexUpdate.Text = ""


            setTablaGridViewCondiciones()
            verifyButtonAddCondition()


        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try

    End Sub
    Private Sub setTablaGridViewCondiciones()
        Try
            GridViewCondiciones.DataSource = ViewState("tablaCondiciones")
            GridViewCondiciones.DataBind()
            GridViewCondicionesNoEdit.DataSource = ViewState("tablaCondiciones")
            GridViewCondicionesNoEdit.DataBind()
            execJS("init()")
        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try




    End Sub

    Protected Sub GridViewCondiciones_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Pager Then
            'Dim PiePagina As Label = e.Row.FindControl("GrillaPpal_CurrentPageLabel")
            'Dim TotalPaginas As Integer = Convert.ToString(GrillaPpal.PageCount).Trim()
            'PiePagina.Text = "Página " & Convert.ToString(GrillaPpal.PageIndex + 1).Trim() & " de " & TotalPaginas.ToString()
        End If
    End Sub
    Public Shared Property CadenaComentarios As String = ""
    Protected Sub GrillaPpal_BtnEditEjecucion_Click(sender As Object, e As EventArgs)
        FrmPpal_Limpiar()

        FrmPpalTitulo.InnerHtml = "<i class='far fa-edit'></i>&nbsp;&nbsp;Editar <span Class='badge text-bg-warning'>EJECUCIÓN</span>"

        ModalShow(FrmPpal_dialog, FrmPpal)
        execJS("init()")
    End Sub

#End Region


#Region "Log"
    Public Function getLogCritica()
        Try
            Dim N_ObjTblLogCritica As New N_ClsTblLogCritica
            Dim E_ObjTblLogCritica As New E_ClsTblLogCritica

            Dim dt As DataTable = N_ObjTblLogCritica.SelectAll(CInt(FrmPpal_TxtIdCritica.Text))
            'IdAccion    Accion

            '1   Creación Critica
            '2   Modificación Critica
            '3   Eliminación Critica
            '4   Aprobación Critica
            '5   Comentario

            Dim Tabla As String = "
<div class='row'>
    <div class='col-md-12'>
        <div class='main-timeline7'>"
            Dim i As Integer = 0

            For Each row As DataRow In dt.Rows
                i = i + 1

                Dim IdAccion As String = row("IdAccion").ToString()
                Dim Img As String = ""
                Dim Fecha As String = CDate(row("Fecha")).ToString("dd-MM-yyyy HH:mm")


                If IdAccion = 1 Then
                    Img = "<i class='fa-regular fa-calendar-plus' style='position:absolute;top:20px;font-size:2rem;left:20px;'></i>"

                ElseIf IdAccion = 2 Then

                    Img = "<i class='fa-solid fa-sliders' style='position:absolute;top:20px;font-size:2rem;left:20px;'></i>"

                ElseIf IdAccion = 3 Then

                    Img = "<i class='fa-solid fa-rectangle-xmark' style='position:absolute;top:20px;font-size:2rem;left:20px;'></i>"

                ElseIf IdAccion = 4 Then

                    Img = "<i class='fa-regular fa-circle-check' style='position:absolute;top:20px;font-size:2rem;left:20px;'></i>"

                ElseIf IdAccion = 5 Then

                    Img = "<i class='fa-regular fa-comment-dots' style='position:absolute;top:20px;font-size:2rem;left:20px;'></i>"


                End If


                Tabla += "
            <div class='timeline'>
                <div class='timeline-icon'>" + Img + "</div>
                <span class='year'>" + Fecha + "</span>
                <div class='timeline-content'>
                    <h5 class='title'>" + row("Accion") + "</h5>
                    <p class='description'>
                        <b>" + row("Usuario") + "</b>" + row("Comentario") + "
                    </p>
                </div>
            </div>
"



                'If i Mod 2 = 0 Then

                'Tabla = Tabla & "<div class='timeline'>"

                '    Tabla = Tabla & "<div class='timeline-content left'>"

                '    Tabla = Tabla & "<span class='timeline-icon'></span>"

                '    Tabla = Tabla & "<span class='date'> " & Fecha & "&nbsp;</span>"

                '    Tabla = Tabla & "<h2 class='title'>" & Img & "&nbsp;" & row("Accion") & "</h2>"

                '    Tabla = Tabla & "<p class='description'>"

                '    Tabla = Tabla & "<span style='color:green'>" & row("Usuario") & "</span>"

                '    Tabla = Tabla & "<br/>" & row("Comentario") & "</p></div></div>"

                'Else
                '    Tabla = Tabla & "<div class='timeline'>"

                '    Tabla = Tabla & "<div class='timeline-content right'>"

                '    Tabla = Tabla & "<span class='timeline-icon'></span>"

                '    Tabla = Tabla & "<span class='date'> " & Fecha & "&nbsp;</span>"

                '    Tabla = Tabla & "<h2 class='title'>" & Img & "&nbsp;" & row("Accion") & "</h2>"

                '    Tabla = Tabla & "<p class='description'>"

                '    Tabla = Tabla & "<span style='color:green'>" & row("Usuario") & "</span>"

                '    Tabla = Tabla & "<br/>" & row("Comentario") & "</p></div></div>"

                'End If

            Next

            Tabla += "
        </div>
    </div>
</div>"
            ViewState("Logs") = Tabla




        Catch ex As Exception

            Return ex.Message.ToString()

        End Try

    End Function
#End Region


#Region "Formulario Principal"
    Protected Sub FrmPpal_Limpiar()

    End Sub
    Function FrmPpal_ValidarCampos() As Boolean
        Dim SW As Boolean = True

        'If FrmPpal_TxtTitulo.Text.Trim() = "" Then
        '    GetValidador(FrmPpal_TxtTitulo, "form-control specialform", FrmPpal_TxtTitulo_Msj, "* Obligatorio", "ERROR")
        '    SW = False
        'Else
        '    GetValidador(FrmPpal_TxtTitulo, "form-control specialform", FrmPpal_TxtTitulo_Msj, "", "OK")
        'End If

        Return SW
    End Function


    Protected Sub FrmPpal_BtnEnviarAprobacion_Click(sender As Object, e As EventArgs)
        Try
            Dim N_ObjTblCritica As New N_ClsTblCritica
            Dim E_ObjTblCritica As New E_ClsTblCritica

            If FrmPpal_TxtIdCritica.Text = "" Then
                SaveBorradorCritica()
            End If

            E_ObjTblCritica.IdEstado = 2
            E_ObjTblCritica.IdCritica = CInt(FrmPpal_TxtIdCritica.Text)
            E_ObjTblCritica.LastTransactionUser = ViewState("MyIdUser")
            N_ObjTblCritica.UpdateEstado(E_ObjTblCritica)


            'Pendiente enviar el correo y escribir el LOG.. del correo TODO

            Dim textoLog = "Se envia aprobar critica: " + FrmPpal_CboPrefijo.Items(FrmPpal_CboPrefijo.SelectedIndex).Text + FrmPpal_Correlativo_label + "-V1"
            Dim opcLog = 6


            setLogWrite(textoLog, opcLog)
            GrillaPpal_Llenar()

            execJS("init()")

            ModalHide(FrmPpal_dialog, FrmPpal)

            FrmMsg_LblCuerpo.Text = "Se envia a Aprobación la Critica."
            FrmMsg_LblTitulo.Text = "Creación de critica"

            MsgModalExitoso(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Creación de critica", FrmMsg_LblCuerpo, FrmMsg_LblCuerpo.Text)




            ModalShow(FrmMsg_dialog, FrmMsg)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SaveBorradorCritica()
        Try
            Dim N_ObjTblCritica As New N_ClsTblCritica
            Dim E_ObjTblCritica As New E_ClsTblCritica
            Dim ResultadoTransaccion As String
            E_ObjTblCritica.IdCarpeta = CInt(FrmPpal_TxtIdCarpeta.Text)
            E_ObjTblCritica.IdPrefijo = CInt(FrmPpal_CboPrefijo.SelectedValue)
            E_ObjTblCritica.Correlativo = CInt(FrmPpal_Correlativo.Text)
            E_ObjTblCritica.Version = CInt(FrmPpal_Version.Text)
            E_ObjTblCritica.IdEstado = 1
            E_ObjTblCritica.Owner = ViewState("MyIdUser")
            E_ObjTblCritica.LastTransactionUser = ViewState("MyIdUser")

            E_ObjTblCritica.FechaCreacion = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss")
            If frmPpal_textFechaVencimiento.Text <> "" Then
                E_ObjTblCritica.FechaRevision = Convert.ToDateTime(frmPpal_textFechaVencimiento.Text)
            Else
                E_ObjTblCritica.FechaRevision = DateTime.Now.ToString("yyyy-MM-dd")
            End If

            E_ObjTblCritica.IdWorkflow = FrmPpal_CmbWorkFlowAprobacion.SelectedValue
            E_ObjTblCritica.Justificacion = FrmPpal_Justificacion.Text


            E_ObjTblCritica.IdVariableAccion = Frm_CmbVariablesAccion.SelectedValue
            E_ObjTblCritica.VariableAccionValor = Frm_TxtValor.Text

            Dim textoLog As String
            Dim opcLog As Integer

            If FrmPpal_TxtIdCritica.Text <> "" Then
                E_ObjTblCritica.IdCritica = FrmPpal_TxtIdCritica.Text
                ResultadoTransaccion = N_ObjTblCritica.Update(E_ObjTblCritica)

                textoLog = "Se actualiza información Borrador: " + FrmPpal_CboPrefijo.Items(FrmPpal_CboPrefijo.SelectedIndex).Text + FrmPpal_Correlativo_label + "-V1"
                opcLog = 2
            Else
                ResultadoTransaccion = N_ObjTblCritica.Insert(E_ObjTblCritica)
                textoLog = "Se crea Borrador: " + FrmPpal_CboPrefijo.Items(FrmPpal_CboPrefijo.SelectedIndex).Text + FrmPpal_Correlativo_label + "-V1"
                opcLog = 1

            End If

            If Mid(ResultadoTransaccion, 1, 2) = "OK" Then
                Dim N_ObjTblCriticaCondicion As New N_ClsTblCriticaCondicion
                Dim E_ObjTblCriticaCondicion As New E_ClsTblCriticaCondicion
                FrmPpal_TxtIdCritica.Text = Replace(ResultadoTransaccion, "OK", "")


                Dim orden As Integer = 1
                Dim tbl As DataTable = ViewState("tablaCondiciones")
                For Each cell In tbl.Rows
                    E_ObjTblCriticaCondicion.IdCritica = FrmPpal_TxtIdCritica.Text
                    E_ObjTblCriticaCondicion.Orden = orden
                    E_ObjTblCriticaCondicion.IdVariableCondicion = cell("idCampoCondicion")
                    E_ObjTblCriticaCondicion.OperadorLogico = cell("idOperadorLogico")
                    E_ObjTblCriticaCondicion.Valor1 = cell("valor")
                    E_ObjTblCriticaCondicion.Valor2 = cell("CadenaCondicion")
                    E_ObjTblCriticaCondicion.AndOr = cell("andOr")
                    E_ObjTblCriticaCondicion.LastTransactionUser = ViewState("MyIdUser")
                    N_ObjTblCriticaCondicion.Insert(E_ObjTblCriticaCondicion)
                    orden += 1
                Next
                setLogWrite(textoLog, opcLog)


            End If

            GrillaPpal_Llenar()

            execJS("init()")

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try

    End Sub
    Protected Sub FrmPpal_BtnGuardar_Click(sender As Object, e As EventArgs)
        SaveBorradorCritica()

    End Sub

    Private Sub setLogWrite(Cadena As String, IdAccion As Integer)
        Dim N_ObjTblLogCritica As New N_ClsTblLogCritica
        Dim E_ObjTblLogCritica As New E_ClsTblLogCritica
        Dim ResultadoTransaccion As String

        E_ObjTblLogCritica.IdCritica = FrmPpal_TxtIdCritica.Text
        E_ObjTblLogCritica.Comentario = Cadena
        E_ObjTblLogCritica.IdAccion = IdAccion
        E_ObjTblLogCritica.LastTransactionUser = ViewState("MyIdUser")
        E_ObjTblLogCritica.Fecha = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss")
        N_ObjTblLogCritica.Insert(E_ObjTblLogCritica)


    End Sub
    Protected Sub FrmPpal_BtnCerrar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmPpal_dialog, FrmPpal)
    End Sub
#End Region






#Region "FrmAprueboRechazo"
    Protected Sub FrmPpal_BtnAprobar_Click(sender As Object, e As EventArgs)
        Try
            FrmAprueboRechazo_txtOpcion.Text = 1
            FrmAprueboRechazo_header.Attributes.Add("class", "modal-header bg-success")
            FrmAprueboRechazo_headerTitle.InnerText = "Motivo de Apruebo"
            ModalShow(FrmAprueboRechazo_dialog, FrmAprueboRechazo)
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub FrmPpal_BtnRechazar_Click(sender As Object, e As EventArgs)
        Try
            FrmAprueboRechazo_txtOpcion.Text = 2
            FrmAprueboRechazo_header.Attributes.Add("class", "modal-header bg-danger")
            FrmAprueboRechazo_headerTitle.InnerText = "Motivo de Rechazo"

            ModalShow(FrmAprueboRechazo_dialog, FrmAprueboRechazo)
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub FrmAprueboRechazo_BtnGuardar_Click(sender As Object, e As EventArgs)
        Try

            Dim opcAccion As Integer

            If FrmAprueboRechazo_txtOpcion.Text = 1 Then
                opcAccion = 4
            Else
                opcAccion = 3
            End If


            setLogWrite(FrmAprueboRechazo_txtAprueboRechazo.Text, opcAccion)

            If opcAccion = 4 Then
                Dim N_ObjTblLogCritica As New N_ClsTblLogCritica
                Dim E_ObjTblLogCritica As New E_ClsTblLogCritica
                N_ObjTblLogCritica.changeStatusCritica(FrmPpal_TxtIdCritica.Text)
            Else
                Dim N_ObjTblCritica As New N_ClsTblCritica
                Dim E_ObjTblCritica As New E_ClsTblCritica


                E_ObjTblCritica.IdEstado = 3
                E_ObjTblCritica.IdCritica = CInt(FrmPpal_TxtIdCritica.Text)
                E_ObjTblCritica.LastTransactionUser = ViewState("MyIdUser")
                N_ObjTblCritica.UpdateEstado(E_ObjTblCritica)

            End If



            GrillaPpal_Llenar()
            ModalHide(FrmAprueboRechazo_dialog, FrmAprueboRechazo)
            ModalHide(FrmPpal_dialog, FrmPpal)

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub

    Protected Sub FrmAprueboRechazo_BtnCerrar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmAprueboRechazo_dialog, FrmAprueboRechazo)
    End Sub

#End Region


#Region "FrmDelete"
    Protected Sub FrmDelete_BtnCerrar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmDelete_dialog, FrmDelete)
    End Sub
    Protected Sub FrmDelete_BtnEliminar_Click(sender As Object, e As EventArgs)
        Try
            ModalHide(FrmDelete_dialog, FrmDelete)

            Dim E_ObjWorkflow As New E_ClsTblWorkflow
            Dim N_ObjWorkflow As New N_ClsTblWorkflow
            Dim ResultadoTransaccion As String

            E_ObjWorkflow.IdWorkflow = FrmDelete_TxtId.Text.Trim()
            E_ObjWorkflow.LastTransactionUser = ViewState("MyIdUser")

            ResultadoTransaccion = N_ObjWorkflow.Delete(E_ObjWorkflow)
            If ResultadoTransaccion <> "OK" Then
                MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
                ModalShow(FrmMsg_dialog, FrmMsg)
                Exit Sub
            End If


        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
#End Region

#Region "FrmMsg"
    Protected Sub FrmMsg_BtnAceptar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmMsg_dialog, FrmMsg)
    End Sub

    Private Sub execJS(ByVal script As String)
        Try
            Dim TxtJavascript As String = "<script type='text/javascript'>" + script + "</script>"
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "alerta", TxtJavascript, False)
        Catch ex As Exception

        End Try
    End Sub
#End Region

End Class