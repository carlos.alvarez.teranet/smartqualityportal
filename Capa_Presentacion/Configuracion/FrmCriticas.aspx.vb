﻿Imports Capa_Entidad
Imports Capa_Negocio
Imports System.Data
Imports System.IO

Public Class FrmCriticas
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Try
        '    'Dim URL As String = Page.Request.UrlReferrer.AbsolutePath
        'Catch ex As Exception
        '    Response.Redirect("../PageError.aspx")
        'End Try

        If Request.Form("__EVENTTARGET") <> "" Then
            'Si hace click en Breadcrumbs
            If Request.Form("__EVENTTARGET") = "CambioCarpeta" Then
                BreadcrumbsFolder_Llenar(CInt(Request.Form("__EVENTARGUMENT")))
                getCarpeta(CInt(Request.Form("__EVENTARGUMENT")))
            End If

            'Si hace click en Treeview
            If Request.Form("__EVENTTARGET").Replace("ctl00$ContentPlaceHolder1$", "") = "CarpetasTreeView" Then
                BscIdCarpeta(Request.Form("__EVENTARGUMENT").Replace("s", ""))
                Dim Argumento = Request.Form("__EVENTARGUMENT").Replace("s", "")
                Dim ResultArray() As String = Split(Argumento, "\")

                getCarpeta(CInt(ResultArray(UBound(ResultArray))))
            End If
        End If

        If Not Page.IsPostBack Then
            'Dim var_krypton As String = OwnDecryption(Page.Request("krypton"))
            'ViewState("MyIdUser") = OwnPageRequest(var_krypton, "IdUser")
            ViewState("MyIdUser") = "carlos.astorga@kantar.com"
            setTableCondicion()
            initCarpetas()


        End If
    End Sub
    Private Sub setTableCondicion()
        ViewState("tablaCondiciones") = Nothing

        Dim tablaCondiciones As New DataTable
        tablaCondiciones.Columns.Add("IdCondicion", GetType(Integer))
        tablaCondiciones.Columns.Add("idCampoCondicion", GetType(String))
        tablaCondiciones.Columns.Add("idOperadorLogico", GetType(String))
        tablaCondiciones.Columns.Add("valor", GetType(String))
        tablaCondiciones.Columns.Add("andOr", GetType(String))
        tablaCondiciones.Columns.Add("CadenaCondicion", GetType(String))
        tablaCondiciones.Columns.Add("StringCampoCondicion", GetType(String))
        tablaCondiciones.Columns.Add("StringOperadorLogico", GetType(String))

        ViewState("tablaCondiciones") = tablaCondiciones
        ViewState("IdCorretalivoCondicion") = 0
        FrmlPpal_IndexUpdate.Text = ""
        'setTablaGridViewCondiciones()
        execJS("init();")
    End Sub
    Protected Sub BreadcrumbsFolder_Llenar(ByVal IdCarpeta As Integer)
        Try

            Dim E_ObjTblCarpeta As New E_ClsTblCarpeta
            Dim N_ObjTblCarpeta As New N_ClsTblCarpeta

            E_ObjTblCarpeta.IdCarpeta = IdCarpeta

            Dim BreadCrumfolder As String = N_ObjTblCarpeta.BreadcrumbsFolder(E_ObjTblCarpeta)
            DivBreadcrumbsFolder.InnerHtml = BreadCrumfolder
            LblCloneBread.InnerHtml = BreadCrumfolder '.Replace("breadcrumb", "breadcrumb_frm")



            FrmPpal_TxtIdCarpeta.Text = IdCarpeta
            'getCriticas()
            TreeView_SetNode(CStr(IdCarpeta))

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub BscIdCarpeta(ByVal Argumento As String)
        Try
            Dim ResultArray() As String = Split(Argumento, "\")
            BreadcrumbsFolder_Llenar(CInt(ResultArray(UBound(ResultArray))))

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub TreeView_SetNode(ByVal TxtValue As String)
        Dim searchedNode As TreeNode = Nothing
        For Each node As TreeNode In CarpetasTreeView.Nodes
            searchedNode = SearchNodeValue(node, TxtValue)
            If searchedNode Is Nothing Then
                For Each childNode As TreeNode In node.ChildNodes
                    searchedNode = SearchNodeValue(childNode, TxtValue)
                    If searchedNode IsNot Nothing Then
                        searchedNode.[Select]()
                    End If
                Next
            Else
                If searchedNode IsNot Nothing Then
                    searchedNode.[Select]()
                End If
                Exit For
            End If
        Next
    End Sub
    Private Sub getCarpeta(ByVal IdCarpeta As Integer)
        Try

            Dim E_ObjCarpeta As New E_ClsTblCarpeta
            Dim N_ObjCarpeta As New N_ClsTblCarpeta

            E_ObjCarpeta.IdCarpeta = IdCarpeta
            E_ObjCarpeta = N_ObjCarpeta.GetCarpeta(E_ObjCarpeta)

            If E_ObjCarpeta.Contenedora = False Then
                BtnAddRegistro.Visible = False
                BtnFilter_row.Visible = False
            Else
                BtnAddRegistro.Visible = True
                BtnFilter_row.Visible = True

                GrillaPpal_Llenar()
                GrillaPpal.Attributes("Codigo") = SortDirection.Descending
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub initCarpetas()
        Try
            Try
                Dim N_ObjTblCarpeta As New N_ClsTblCarpeta
                Dim Lista As New TreeNodeCollection

                CarpetasTreeView.Nodes.Clear()

                Lista = ConstruirTreevew(N_ObjTblCarpeta.CarpetasTreeViewContenedor)
                For Each row As TreeNode In Lista
                    CarpetasTreeView.Nodes.Add(row)
                Next
                CarpetasTreeView.ExpandAll()

                execJS("init()")



            Catch ex As Exception
                MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
                ModalShow(FrmMsg_dialog, FrmMsg)
            End Try
        Catch ex As Exception

        End Try

    End Sub

#Region "Grilla Principal"
    Protected Sub GrillaPpal_Llenar()
        Try
            Dim N_ObjCritica As New N_ClsTblCritica
            GrillaPpal.DataSource = SortingPageIndex(N_ObjCritica.SelectAll(TxtBsc.Text.Trim(), FrmPpal_TxtIdCarpeta.Text), ViewState("GrillaPpal_ColumnSorting"), ViewState("GrillaPpal_TypeSorting"))
            GrillaPpal.DataBind()

            GrillaPpal.Columns(1).ItemStyle.HorizontalAlign = HorizontalAlign.Center
            GrillaPpal.Columns(2).ItemStyle.HorizontalAlign = HorizontalAlign.Left
            GrillaPpal.Columns(3).ItemStyle.HorizontalAlign = HorizontalAlign.Left

            TxtBsc.Focus()
        Catch ex As Exception
            GrillaPpal.DataSource = Nothing
            GrillaPpal.DataBind()
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub GrillaPpal_BtnEditar_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
        Dim IdCritica As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblIdCritica"), Label)

        FrmPpal_TxtIdCritica.Text = IdCritica.Text

        ' FrmPpal_Version.Text = 1

        FrmPpal_TxtIdTemp.Text = ""

        FrmPpalTitulo.InnerHtml = "<i class='far fa-edit'></i>&nbsp;&nbsp;Modificar <span Class='badge text-bg-warning'>Critica</span>"
        FrmPpal_BtnGuardar.Text = "<i class='fa-regular fa-floppy-disk'></i>&nbsp;&nbsp;&nbsp;Actualizar Borrador"
        loadFormCritica()


        ModalShow(FrmPpal_dialog, FrmPpal)
    End Sub
    Protected Sub GrillaPpal_BtnEliminar_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
        Dim IdCritica As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblIdCritica"), Label)
        'Dim Workflow As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblWorkflow"), Label)

        FrmDelete_Titulo.InnerHtml = "<i class='fa-solid fa-pen-to-square'></i>&nbsp;&nbsp;Eliminar Critica"

        FrmDelete_TxtId.Text = IdCritica.Text
        FrmDelete_Msg.Text = "Se eliminará la critica <b>Nº" & IdCritica.Text & "</b>."

        ModalShow(FrmDelete_dialog, FrmDelete)
    End Sub
    Protected Sub GrillaPpal_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Pager Then
            Dim PiePagina As Label = e.Row.FindControl("GrillaPpal_CurrentPageLabel")
            Dim TotalPaginas As Integer = Convert.ToString(GrillaPpal.PageCount).Trim()
            PiePagina.Text = "Página " & Convert.ToString(GrillaPpal.PageIndex + 1).Trim() & " de " & TotalPaginas.ToString()
        End If
    End Sub
    Protected Sub GrillaPpal_BtnFirst_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        GrillaPpal.PageIndex = 0
        GrillaPpal.DataBind()
    End Sub
    Protected Sub GrillaPpal_BtnPrev_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        If (GrillaPpal.PageIndex <> 0) Then
            GrillaPpal.PageIndex = GrillaPpal.PageIndex - 1
            GrillaPpal.DataBind()
        End If
    End Sub
    Protected Sub GrillaPpal_BtnNext_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        If (GrillaPpal.PageIndex <> GrillaPpal.PageCount - 1) Then
            GrillaPpal.PageIndex = GrillaPpal.PageIndex + 1
            GrillaPpal.DataBind()
        End If
    End Sub
    Protected Sub GrillaPpal_BtnLast_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        GrillaPpal.PageIndex = GrillaPpal.PageCount - 1
        GrillaPpal.DataBind()
    End Sub
    Private Sub GrillaPpal_PreRender(sender As Object, e As EventArgs) Handles GrillaPpal.PreRender
        'Esto es para que la grilla muestre el paginador aunque tenga menos de 10 registros
        If GrillaPpal.BottomPagerRow IsNot Nothing Then
            If GrillaPpal.BottomPagerRow.Visible = False Then
                GrillaPpal.BottomPagerRow.Visible = True
            End If
        End If
    End Sub
    Private Sub GrillaPpal_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles GrillaPpal.Sorting
        ViewState("GrillaPpal_ColumnSorting") = e.SortExpression

        If GrillaPpal.Attributes(e.SortExpression) = SortDirection.Ascending Then
            ViewState("GrillaPpal_TypeSorting") = "DESC"
            GrillaPpal_ChangeHeader()
        Else
            ViewState("GrillaPpal_TypeSorting") = "ASC"
            GrillaPpal_ChangeHeader()
        End If

        GrillaPpal_Llenar()
    End Sub
    Private Sub GrillaPpal_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GrillaPpal.RowCommand
        If Trim(e.CommandArgument) = "Codigo" Then
            ViewState("GrillaPpal_ColumnIndex") = 2
        End If
    End Sub
    Private Sub GrillaPpal_ChangeHeader()
        Dim Base As String = "<span class='link-light'>XXXCodigoXXX <i class='fa-solid fa-ellipsis-vertical'></i></span>"
        Dim Type As String

        If ViewState("GrillaPpal_TypeSorting") = "DESC" Then
            Type = "<i class='fa-solid fa-arrow-down-z-a'></i>"
        Else
            Type = "<i class='fa-solid fa-arrow-down-a-z'></i>"
        End If

        Dim GrillaPpal_Indice = New Integer() {2}
        Dim GrillaPpal_Campo() As String = {"Codigo"}
        Dim GrillaPpal_Titulo() As String = {"Job Position"}

        For I = 0 To GrillaPpal_Indice.Length - 1
            If ViewState("GrillaPpal_ColumnIndex") = GrillaPpal_Indice(I) Then
                GrillaPpal.Columns(GrillaPpal_Indice(I)).HeaderText = "<span class='link-light'>" & GrillaPpal_Titulo(I) & " " & Type & "</span>"
                If ViewState("GrillaPpal_TypeSorting") = "DESC" Then
                    GrillaPpal.Attributes(GrillaPpal_Campo(I)) = SortDirection.Descending
                Else
                    GrillaPpal.Attributes(GrillaPpal_Campo(I)) = SortDirection.Ascending
                End If
            Else
                GrillaPpal.Columns(GrillaPpal_Indice(I)).HeaderText = Replace(Base, "XXXCodigoXXX", GrillaPpal_Titulo(I))
                GrillaPpal.Attributes(GrillaPpal_Campo(I)) = SortDirection.Descending
            End If
        Next
    End Sub
#End Region

#Region "Grilla Comentario"
    'Private Sub GrillaComentario_Llenar()
    '    Try
    '        Dim E_ObjTblComentario As New E_ClsTblComentario
    '        Dim N_ObjTblComentario As New N_ClsTblComentario

    '        Dim DtaTable As DataTable = N_ObjTblComentario.SelectAll(FrmPpal_TxtIdCritica.Text)
    '        ViewState("Comentarios") = ""
    '        For Each row As DataRow In DtaTable.Rows

    '            ViewState("Comentarios") += "<div Class='col-11 mt-4'>"
    '            ViewState("Comentarios") += "  <span Class='text-success'> " + CStr(row("Fecha")) + "</span>- " + CStr(row("Comentario"))
    '            ViewState("Comentarios") += " <div Class='text-end'><i> " + CStr(row("Nombre")) + " - <span style='color:#b22e2e'>" + CStr(row("LastTransactionUser")) + "</i><hr></div></div>"

    '        Next row

    '    Catch ex As Exception

    '    End Try
    'End Sub

    Protected Sub GrillaComentario_Llenar()
        Try
            Dim N_ObjComentario As New N_ClsTblComentario
            Dim E_ObjComentario As New E_ClsTblComentario With {
                .IdTemp = FrmPpal_TxtIdTemp.Text.Trim(),
                .IdCritica = If(String.IsNullOrEmpty(FrmPpal_TxtIdCritica.Text.Trim()), 0, CInt(FrmPpal_TxtIdCritica.Text.Trim()))
            }

            If FrmPpal_TxtIdTemp.Text.Trim() = "" Then
                GrillaComentario.DataSource = SortingPageIndex(N_ObjComentario.SelectAll(E_ObjComentario), ViewState("GrillaComentario_ColumnSorting"), ViewState("GrillaComentario_TypeSorting"))
            Else
                GrillaComentario.DataSource = SortingPageIndex(N_ObjComentario.SelectAll_Temp(E_ObjComentario), ViewState("GrillaComentario_ColumnSorting"), ViewState("GrillaComentario_TypeSorting"))
            End If
            GrillaComentario.DataBind()

            GrillaComentario.Columns(1).ItemStyle.HorizontalAlign = HorizontalAlign.Center
            GrillaComentario.Columns(2).ItemStyle.HorizontalAlign = HorizontalAlign.Left
            GrillaComentario.Columns(3).ItemStyle.HorizontalAlign = HorizontalAlign.Center

        Catch ex As Exception
            GrillaComentario.DataSource = Nothing
            GrillaComentario.DataBind()
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub GrillaComentario_BtnEliminar_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
        Dim IdComentario As Label = CType(GrillaComentario.Rows(row.RowIndex).FindControl("GrillaComentario_LblIdComentario"), Label)
        Dim Nombre As Label = CType(GrillaComentario.Rows(row.RowIndex).FindControl("GrillaComentario_LblNombre"), Label)
        Dim LastTransactionUser As Label = CType(GrillaComentario.Rows(row.RowIndex).FindControl("GrillaComentario_LblLastTransactionUser"), Label)
        Dim Fecha As Label = CType(GrillaComentario.Rows(row.RowIndex).FindControl("GrillaComentario_LblFecha"), Label)

        Dim TimeDif As TimeSpan = Now - DateTime.Parse(Fecha.Text)
        If TimeDif.TotalHours > 24 Or LastTransactionUser.Text.Trim.ToUpper() <> ViewState("MyIdUser").ToString.Trim.ToUpper() Then
            MsgModalAlerta(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Alerta", FrmMsg_LblCuerpo, "Los comentarios solo pueden ser eliminados por su autor y dentro de las 24 horas posteriores a su ingreso.")
            ModalShow(FrmMsg_dialog, FrmMsg)
            Exit Sub
        End If

        FrmDelete_Titulo.InnerHtml = "<i class='fa-solid fa-trash-can'></i>&nbsp;&nbsp;Eliminar Comentario"

        FrmDelete_TxtOpt.Text = "2"
        FrmDelete_TxtId.Text = IdComentario.Text
        FrmDelete_Msg.Text = "Se eliminará el comentario ingresado por <b>" & Nombre.Text & "</b> el <b>" & Fecha.Text & "</b>."

        TabActiveForm = 2
        TabSelected()
        GrillaComentario_Llenar()

        ModalShow(FrmDelete_dialog, FrmDelete)
    End Sub
    Protected Sub GrillaComentario_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Pager Then
            Dim PiePagina As Label = e.Row.FindControl("GrillaComentario_CurrentPageLabel")
            Dim TotalPaginas As Integer = Convert.ToString(GrillaComentario.PageCount).Trim()
            PiePagina.Text = "Página " & Convert.ToString(GrillaComentario.PageIndex + 1).Trim() & " de " & TotalPaginas.ToString()
        End If
    End Sub
    Private Sub GrillaComentario_PreRender(sender As Object, e As EventArgs) Handles GrillaComentario.PreRender
        'Esto es para que la grilla muestre el paginador aunque tenga menos de 10 registros
        If GrillaComentario.BottomPagerRow IsNot Nothing Then
            If GrillaComentario.BottomPagerRow.Visible = False Then
                GrillaComentario.BottomPagerRow.Visible = True
            End If
        End If
    End Sub
#End Region


    Protected Sub FrmPpal_Comentario_click(sender As Object, e As EventArgs)
        TabActiveForm = 2
        TabSelected()

        If FrmPpal_TxtIdCritica.Text = "" Then
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, "Favor de crear la critica antes de comentar")

            ModalShow(FrmMsg_dialog, FrmMsg)
        Else
            ModalShow(FrmComentarios_dialog, FrmComentarios)
        End If
        execJS("init()")


    End Sub

    Protected Sub FrmComentarios_BtnCerrar_Click(sender As Object, e As EventArgs)
        TabActiveForm = 2
        TabSelected()
        ModalHide(FrmComentarios_dialog, FrmComentarios)
    End Sub

    Protected Sub FrmComentarios_BtnComentar_Click(sender As Object, e As EventArgs)
        Try
            TabActiveForm = 2

            Dim E_ObjTblComentario As New E_ClsTblComentario
            Dim N_ObjTblComentario As New N_ClsTblComentario
            E_ObjTblComentario.Comentario = FrmlPpal_traspasaComentario.Text
            E_ObjTblComentario.LastTransactionUser = ViewState("MyIdUser")
            E_ObjTblComentario.IdCritica = FrmPpal_TxtIdCritica.Text

            N_ObjTblComentario.Insert(E_ObjTblComentario)

            FrmlPpal_traspasaComentario.Text = ""

            TabSelected()
            GrillaComentario_Llenar()
            execJS("init()")
            ModalHide(FrmComentarios_dialog, FrmComentarios)

        Catch ex As Exception

        End Try


    End Sub




#Region "Index"
    Protected Sub BtnBsc_Click(sender As Object, e As EventArgs)

        Dim N_ObjEjecucion As New N_ClsTblEjecucion
        GridViewCondiciones.DataSource = SortingPageIndex(N_ObjEjecucion.SelectAll(TxtBsc.Text.Trim()), ViewState("GrillaPpal_ColumnSorting"), ViewState("GrillaPpal_TypeSorting"))
        GridViewCondiciones.DataBind()

        GridViewCondiciones.Columns(1).ItemStyle.HorizontalAlign = HorizontalAlign.Center
        GridViewCondiciones.Columns(2).ItemStyle.HorizontalAlign = HorizontalAlign.Left
        GridViewCondiciones.Columns(3).ItemStyle.HorizontalAlign = HorizontalAlign.Left


    End Sub
    Protected Sub BtnAddRegistro_Click(sender As Object, e As EventArgs)
        Try
            FrmPpal_Limpiar()

            FrmPpal_TxtIdCritica.Text = ""
            FrmPpalTitulo.InnerHtml = "Crear <span Class='badge text-bg-warning'>CRITICA</span>"
            FrmPpal_BtnGuardar.Text = "<i class='fa-regular fa-floppy-disk'></i>&nbsp;&nbsp;&nbsp;Guardar Borrador"

            'Crea un ID temporal que se usa para los adjuntos y comentarios cuando el registro aún no existe
            FrmPpal_TxtIdTemp.Text = ViewState("MyIdUser") & CStr(Now).Replace(" ", "").Replace("-", "").Replace(":", "")

            loadFormCritica()

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub


    Private Sub TabSelected()

        Ffrm_principal_TabOpc1.Attributes.Add("class", "nav-link")
        CriticaPanelTab.Attributes.Add("class", "tab-pane fade ")

        Ffrm_principal_TabOpc2.Attributes.Add("class", "nav-link")
        comentarioPanelTab.Attributes.Add("class", "tab-pane fade ")

        Ffrm_principal_TabOpc3.Attributes.Add("class", "nav-link")
        LogPanelTab.Attributes.Add("class", "tab-pane fade ")

        Select Case True
            Case TabActiveForm = "1"
                Ffrm_principal_TabOpc1.Attributes.Add("class", "nav-link active")
                CriticaPanelTab.Attributes.Add("class", "tab-pane fade show active")
            Case TabActiveForm = "2"
                Ffrm_principal_TabOpc2.Attributes.Add("class", "nav-link active")
                comentarioPanelTab.Attributes.Add("class", "tab-pane fade show active")
            Case TabActiveForm = "3"
                Ffrm_principal_TabOpc3.Attributes.Add("class", "nav-link active")
                LogPanelTab.Attributes.Add("class", "tab-pane fade show active")
        End Select
        execJS("init();")

    End Sub
    Public Shared Property TabActiveForm As Integer = 1
    Public Shared Property FrmPpal_Correlativo_label As String

    Private Sub loadFormCritica()
        Try


            TabActiveForm = 1
            TabSelected()
            setTableCondicion()
            Dim N_ObjPrefijo As New N_ClsTblPrefijo
            FrmPpal_CboPrefijo.Items.Clear()
            FrmPpal_CboPrefijo.DataTextField = "TextField"
            FrmPpal_CboPrefijo.DataValueField = "ValueField"



            Dim reaultLoadCbo = N_ObjPrefijo.LoadCbo(FrmPpal_TxtIdCarpeta.Text)
            FrmPpal_CboPrefijo.DataSource = reaultLoadCbo
            FrmPpal_CboPrefijo.DataBind()

            Dim N_ObjCritica As New N_ClsTblCritica
            Dim ResultadoCorrelativo As String

            ResultadoCorrelativo = N_ObjCritica.getCorrelativoCarpeta(FrmPpal_TxtIdCarpeta.Text)
            FrmPpal_Correlativo.Text = ResultadoCorrelativo
            FrmPpal_Correlativo_label = ResultadoCorrelativo

            Dim N_ObjVariableAccion As New N_ClsTblVariableAccion
            Frm_CmbVariablesAccion.Items.Clear()
            Frm_CmbVariablesAccion.DataTextField = "TextField"
            Frm_CmbVariablesAccion.DataValueField = "ValueField"
            Dim reaultVariableAccionLoadCbo = N_ObjVariableAccion.LoadCbo()
            Frm_CmbVariablesAccion.DataSource = reaultVariableAccionLoadCbo
            Frm_CmbVariablesAccion.DataBind()

            Frm_CmbVariablesAccion.Items.Insert(0, New ListItem("<< Seleccionar >>", "0"))


            Dim N_ObjVariableCondicion As New N_ClsTblVariableCondicion
            Frm_CmbVariablesCondicion.Items.Clear()
            Frm_CmbVariablesCondicion.DataTextField = "TextField"
            Frm_CmbVariablesCondicion.DataValueField = "ValueField"
            Frm_CmbVariablesCondicion.DataMember = "ValueField"

            Dim reaultVariableCondicionLoadCbo = N_ObjVariableCondicion.LoadCbo()
            Frm_CmbVariablesCondicion.DataSource = reaultVariableCondicionLoadCbo
            Frm_CmbVariablesCondicion.DataBind()

            Frm_CmbVariablesCondicion.Items.Insert(0, New ListItem("<< Seleccionar >>", "0"))


            Dim N_ObjWorkFlow As New N_ClsTblWorkflow
            Dim reaultWorkAprobacionLoadCbo = N_ObjWorkFlow.LoadCbo()
            FrmPpal_CmbWorkFlowAprobacion.DataTextField = "TextField"
            FrmPpal_CmbWorkFlowAprobacion.DataValueField = "ValueField"
            FrmPpal_CmbWorkFlowAprobacion.DataSource = reaultWorkAprobacionLoadCbo
            FrmPpal_CmbWorkFlowAprobacion.DataBind()
            FrmPpal_CmbWorkFlowAprobacion.Items.Insert(0, New ListItem("<< Seleccionar >>", "0"))
            FrmPpal_Version.Text = 1


            setTableCondicion()
            If FrmPpal_TxtIdCritica.Text <> "" Then
                Dim E_ObjCritica As New E_ClsTblCritica
                E_ObjCritica.IdCritica = FrmPpal_TxtIdCritica.Text
                E_ObjCritica = N_ObjCritica.SelectRecord(E_ObjCritica)
                FrmPpal_Version.Text = E_ObjCritica.Version
                FrmPpal_Correlativo.Text = (E_ObjCritica.CorrelativoAux)
                FrmPpal_Correlativo_label = E_ObjCritica.CorrelativoAux


                FrmPpal_CmbWorkFlowAprobacion.SelectedValue = E_ObjCritica.IdWorkflow
                Frm_CmbVariablesAccion.SelectedValue = (E_ObjCritica.IdVariableAccion)


                FrmPpal_CmbWorkFlowAprobacionNoEdit.Text = FrmPpal_CmbWorkFlowAprobacion.Items(FrmPpal_CmbWorkFlowAprobacion.SelectedIndex).Text

                Frm_TxtValor.Text = E_ObjCritica.VariableAccionValor




                frmPpal_textFechaVencimiento.Text = DateTime.Parse(E_ObjCritica.FechaRevision).Date.ToString("yyyy-MM-dd")
                FrmPpal_AutorLog.InnerText = E_ObjCritica.Owner
                FrmPpal_FechaCreacionLog.InnerText = E_ObjCritica.FechaCreacion.ToString("yyyy-MM-dd hh:mm:ss")
                FrmPpal_EstadoActualLog.InnerText = E_ObjCritica.EstadoAux

                'Dim dtPendiente As DataTable =
                GrillaRutaAprobacion.DataSource = N_ObjCritica.getPendientesAprobacion(FrmPpal_TxtIdCritica.Text)
                GrillaRutaAprobacion.DataBind()

                GrillaRutaAprobacion.Columns(1).ItemStyle.HorizontalAlign = HorizontalAlign.Left
                GrillaRutaAprobacion.Columns(2).ItemStyle.HorizontalAlign = HorizontalAlign.Left

                FrmPpal_Justificacion.Text = E_ObjCritica.Justificacion
                FrmPpal_TxtJustificacionNoEdit.Text = E_ObjCritica.Justificacion

                Dim N_ObjCriticaCondicion As New N_ClsTblCriticaCondicion
                Dim E_ObjTblCriticaCondicion As New E_ClsTblCriticaCondicion
                Dim DtaTable As DataTable = N_ObjCriticaCondicion.SelectByCritica(FrmPpal_TxtIdCritica.Text)
                FrmPpal_LogCritica.Visible = True

                If E_ObjCritica.IdEstado = 1 Then
                    FrmPpal_panelEdit.Visible = True
                    FrmPpal_panelNoEdit.Visible = False
                    FrmPpal_BtnGuardar.Visible = True
                    FrmPpal_BtnEnviarAprobacion.Visible = True



                Else
                    FrmPpal_panelEdit.Visible = False
                    FrmPpal_panelNoEdit.Visible = True
                    FrmPpal_BtnGuardar.Visible = False
                    FrmPpal_BtnEnviarAprobacion.Visible = False




                End If


                Dim recorre As Integer = 1


                For Each row As DataRow In DtaTable.Rows
                    ViewState("tablaCondiciones").Rows.Add(
                    CInt(ViewState("IdCorretalivoCondicion")),
                    row("IdVariableCondicion"),
                    row("OperadorLogico"),
                    row("Valor1"),
                    row("AndOr"),
                    Replace(row("Valor2"), "##", "'"),
                    row("strVariableCondicion"),
                    row("strOperadorLogico")
                )
                    ViewState("IdCorretalivoCondicion") = CInt(ViewState("IdCorretalivoCondicion")) + 1
                Next row
                GrillaComentario_Llenar()
                GrillaAdjunto_Llenar()

                getLogCritica()
                LoadSetVariableAccion()
                setTablaGridViewCondiciones()
                execJS("init();")
            Else

                FrmPpal_Version.Text = 1
                ResultadoCorrelativo = N_ObjCritica.getCorrelativoCarpeta(FrmPpal_TxtIdCarpeta.Text)
                FrmPpal_Correlativo.Text = ResultadoCorrelativo
                FrmPpal_Correlativo_label = ResultadoCorrelativo
                FrmPpal_CmbWorkFlowAprobacion.Items.Item(0).Selected = True

                'Frm_CmbOperadorLogico.Items.Item(0).Selected = True
                'Frm_CmbVariablesAccion.Items.Clear()

                Frm_CmbVariablesCondicion.Items.Item(0).Selected = True
                FrmPpal_AutorLog.InnerText = ViewState("MyIdUser")
                FrmPpal_FechaCreacionLog.InnerText = DateTime.Now.ToString("dd/MM/yyyy")
                FrmPpal_EstadoActualLog.InnerText = "Creando"
                Frm_TxtValor.Text = ""


                FrmPpal_LogCritica.Visible = False
                execJS("init();")
            End If

            ModalShow(FrmPpal_dialog, FrmPpal)



        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub

    Protected Sub setAND(sender As Object, e As EventArgs)
        Dim actualOpc = frmPpal_lbelAndOr.Text
        Dim optionSelected As String = ""

        If actualOpc = "AND" Then
            optionSelected = ""
        Else
            optionSelected = "AND"
        End If
        frmPpal_lbelAndOr.Text = optionSelected

    End Sub
    Protected Sub setOR(sender As Object, e As EventArgs)
        Dim actualOpc = frmPpal_lbelAndOr.Text
        Dim optionSelected As String = ""

        If actualOpc = "OR" Then
            optionSelected = ""
        Else
            optionSelected = "OR"
        End If
        frmPpal_lbelAndOr.Text = optionSelected


    End Sub

    Protected Sub getDetailVariableAccion(sender As Object, e As EventArgs)
        LoadSetVariableAccion()
    End Sub
    Protected Sub getOperadorLogistico(sender As Object, e As EventArgs)

        LoadSetVariableCondicion()
        verifyButtonAddCondition()
        execJS("init();")
    End Sub

    Protected Sub setOperadorLogistico(sender As Object, e As EventArgs)
        LoadSetOperadorLogico()
        verifyButtonAddCondition()
        execJS("init();")
    End Sub
    Protected Sub setValorCondicion(sender As Object, e As EventArgs)
        LoadSetValorCondicion()
        verifyButtonAddCondition()
        execJS("init();")
    End Sub

    Private Sub verifyButtonAddCondition()
        If Frm_CmbVariablesCondicion.SelectedValue <> "0" And Frm_CmbOperadorLogico.SelectedValue <> "0" And Frm_TxtValorCondicion.Text <> "" Then
            FrmPpal_BtntraspasaTranscripcionCondicion.Visible = True
            FrmPpal_BtntraspasaTranscripcionCondicion.Attributes("disabled") = "disabled"
        Else
            FrmPpal_BtntraspasaTranscripcionCondicion.Attributes.Remove("disabled")
            FrmPpal_BtntraspasaTranscripcionCondicion.Visible = False
        End If
        execJS("init();")
    End Sub

    Public Shared Property isComillas As Boolean

    Private Sub LoadSetValorCondicion()
        Dim condicionComillas As String = ""

        If isComillas = True Then
            condicionComillas = "'"
        End If
        frmPpal_lbelValorCondicion.Text = condicionComillas + Frm_TxtValorCondicion.Text + condicionComillas

    End Sub

    Private Sub LoadSetOperadorLogico()

        If Frm_CmbOperadorLogico.SelectedValue <> "0" Then
            Frm_TxtValor.Enabled = True

            Dim N_ObjOperadorLogico As New N_ClsTblOperadorLogico
            Dim E_ObjOperadorLogico As New E_ClsTblOperadorLogico

            E_ObjOperadorLogico.IdOperadorLogico = Frm_CmbOperadorLogico.SelectedValue
            E_ObjOperadorLogico = N_ObjOperadorLogico.SelectRecord(E_ObjOperadorLogico)


            ' Dim campoOperadorLogicoText As String = Frm_CmbVariablesCondicion.SelectedItem.Text
            frmPpal_lbelOperadorLogico.Text = E_ObjOperadorLogico.OperadorLogico

        Else
            Frm_CmbOperadorLogico.Attributes("disabled") = "disabled"
        End If

        TabActiveForm = 1
        TabSelected()

        execJS("init()")


    End Sub

    Private Sub LoadSetVariableCondicion()
        Dim N_ObjVariableCondicion As New N_ClsTblVariableCondicion
        Dim E_ObjVariableCondicion As New E_ClsTblVariableCondicion

        If Frm_CmbVariablesCondicion.SelectedValue <> "0" Then
            Frm_TxtValor.Enabled = True

            Dim N_ObjOperadorLogico As New N_ClsTblOperadorLogico
            Dim E_ObjOperadorLogico As New E_ClsTblOperadorLogico

            E_ObjVariableCondicion.IdVariableCondicion = Frm_CmbVariablesCondicion.SelectedValue
            E_ObjVariableCondicion = N_ObjVariableCondicion.SelectRecord(E_ObjVariableCondicion)


            isComillas = E_ObjVariableCondicion.isComilla
            If isComillas = True Then
                Frm_TxtValorCondicion.Attributes("type") = "text"
            Else
                Frm_TxtValorCondicion.Attributes("type") = "number"
            End If

            LoadSetValorCondicion()

            Frm_CmbOperadorLogico.Items.Clear()
            Frm_CmbOperadorLogico.DataTextField = "TextField"
            Frm_CmbOperadorLogico.DataValueField = "ValueField"
            Dim reaultOperadorLoadCbo = N_ObjOperadorLogico.LoadCbo(Frm_CmbVariablesCondicion.SelectedValue)
            Frm_CmbOperadorLogico.DataSource = reaultOperadorLoadCbo
            Frm_CmbOperadorLogico.DataBind()
            Frm_CmbOperadorLogico.Items.Insert(0, New ListItem("<< Seleccionar >>", "0"))
            Frm_CmbOperadorLogico.Attributes.Remove("disabled")

            Dim campoCondicionText As String = Frm_CmbVariablesCondicion.SelectedItem.Text
            frmPpal_lbelTranscripcionCampo.Text = E_ObjVariableCondicion.VariableCondicion

        Else
            Frm_CmbOperadorLogico.Attributes("disabled") = "disabled"
        End If

        TabActiveForm = 1
        TabSelected()

        execJS("init()")

    End Sub

    Private Sub LoadSetVariableAccion()
        Dim N_ObjVariableAccion As New N_ClsTblVariableAccion
        Dim E_ObjVariableAccion As New E_ClsTblVariableAccion

        If Frm_CmbVariablesAccion.SelectedValue <> "0" Then
            Frm_TxtValor.Enabled = True
            Frm_TxtValor.Attributes.Remove("disabled")

            E_ObjVariableAccion.IdVariableAccion = Frm_CmbVariablesAccion.SelectedValue
            E_ObjVariableAccion = N_ObjVariableAccion.SelectRecord(E_ObjVariableAccion)

            Dim isComillaString As String = ""
            Frm_TxtValor.Attributes("type") = "number"
            If E_ObjVariableAccion.isComilla = True Then
                isComillaString = "'"
                Frm_TxtValor.Attributes("type") = "text"

            End If

            Dim stringCadena As String = "<strong class='text-success'>" + E_ObjVariableAccion.VariableAccion + "</strong> = <strong class='text-danger'>" + isComillaString + "<span id='valorVariableAccion'>" + Frm_TxtValor.Text + "</span>" + isComillaString + "</strong>"
            FrmPpal_ResultLabelTransaccion.Text = stringCadena
            FrmPpal_ResultLabelTransaccionNoEdit.Text = stringCadena

        Else
            'Frm_TxtValor.Enabled = False
            Frm_TxtValor.Attributes("disabled") = "disabled"
            Frm_TxtValor.Attributes.Add("class", "form-control campoAccionChange valorAccionClass")
            'Frm_TxtValor.Attributes.
        End If

        ' Frm_CmbVariablesAccion.SelectedValue = E_ObjVariableAccion.IdVariableAccion
        TabActiveForm = 1
        TabSelected()

        execJS("init()")


    End Sub


    Public correlativoTabla As Integer = 0
    Protected Sub FrmPpal_BtnEditar_Click(sender As Object, e As EventArgs)
        Try
            Dim tbl As DataTable = ViewState("tablaCondiciones")
            Dim btn As LinkButton = sender
            Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
            Dim IdCondicion As Label = CType(GridViewCondiciones.Rows(row.RowIndex).FindControl("LblIdFilaCondicion"), Label)

            Dim index As Integer = 0
            Dim idModifica As Integer = 0
            Dim recorre As Integer
            For Each fila As DataRow In tbl.Rows
                recorre = CInt(fila("IdCondicion"))

                If CInt(IdCondicion.Text) = recorre Then

                    Frm_CmbVariablesCondicion.SelectedValue = fila("idCampoCondicion")
                    LoadSetVariableCondicion()



                    Frm_CmbOperadorLogico.SelectedValue = fila("idOperadorLogico")
                    Frm_TxtValorCondicion.Text = fila("valor")
                    FrmlPpal_OpcAndOr.Text = fila("andOr")

                    frmPpal_lbelTranscripcionCampo.Text = fila("StringCampoCondicion")
                    frmPpal_lbelOperadorLogico.Text = fila("StringOperadorLogico")
                    frmPpal_lbelValorCondicion.Text = fila("valor")
                    frmPpal_lbelAndOr.Text = fila("andOr")

                    FrmlPpal_IndexUpdate.Text = index

                    verifyButtonAddCondition()
                End If
                index += 1
            Next fila

        Catch ex As Exception

        End Try
    End Sub
    Protected Sub FrmPpal_BtnEliminar_Click(sender As Object, e As EventArgs)
        Try
            Dim tbl As DataTable = ViewState("tablaCondiciones")
            Dim btn As LinkButton = sender
            Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
            Dim IdCondicion As Label = CType(GridViewCondiciones.Rows(row.RowIndex).FindControl("LblIdFilaCondicion"), Label)
            'Dim Workflow As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblWorkflow"), Label)

            Dim index As Integer = 0
            Dim idElimina As Integer = 0
            Dim recorre As Integer
            For Each fila As DataRow In tbl.Rows
                recorre = CInt(fila("IdCondicion"))
                If CInt(IdCondicion.Text) = recorre Then
                    idElimina = index
                End If
                index += 1
            Next fila
            tbl.Rows.RemoveAt(idElimina)

            ViewState("tablaCondiciones") = tbl
            setTablaGridViewCondiciones()

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub FrmPpal_BtntraspasaTranscripcionCondicion_click(sender As Object, e As EventArgs)
        Try
            Dim cadenaTraspasa As String
            cadenaTraspasa = "<span style='color:blue;font-weight:bold;'>" + frmPpal_lbelTranscripcionCampo.Text + "</span>&nbsp;"
            cadenaTraspasa += "<span style='color:red;font-weight:bold;'>" + frmPpal_lbelOperadorLogico.Text + "</span>&nbsp;"
            cadenaTraspasa += "<span style='color:green;font-weight:bold;'>" + frmPpal_lbelValorCondicion.Text + "</span>&nbsp;"
            cadenaTraspasa += "<span style='color:black;font-weight:bold;'>" + frmPpal_lbelAndOr.Text + "</span>&nbsp;"


            Dim CmbVariablesCondicion = (Frm_CmbVariablesCondicion.SelectedValue)
            Dim CmbOperadorLogico = (Frm_CmbOperadorLogico.SelectedValue)
            Dim lbelValorCondicion = frmPpal_lbelValorCondicion.Text
            Dim lbelAndOr = frmPpal_lbelAndOr.Text
            Dim cadena = cadenaTraspasa


            If FrmlPpal_IndexUpdate.Text <> "" Then
                Dim tbl As DataTable = ViewState("tablaCondiciones")
                Dim dr As DataRow = ViewState("tablaCondiciones").Rows(CInt(FrmlPpal_IndexUpdate.Text))
                dr("idCampoCondicion") = CmbVariablesCondicion
                dr("idOperadorLogico") = CmbOperadorLogico
                dr("valor") = lbelValorCondicion
                dr("andOr") = lbelAndOr
                dr("CadenaCondicion") = cadenaTraspasa
                dr("StringCampoCondicion") = frmPpal_lbelTranscripcionCampo.Text
                dr("StringOperadorLogico") = frmPpal_lbelOperadorLogico.Text


                ViewState("IdCorretalivoCondicion") = tbl
            Else
                Dim correlativo = CInt(ViewState("IdCorretalivoCondicion"))
                ViewState("tablaCondiciones").Rows.Add(
                    correlativo,
                    CmbVariablesCondicion,
                    CmbOperadorLogico,
                    lbelValorCondicion,
                    lbelAndOr,
                    cadenaTraspasa,
                    frmPpal_lbelTranscripcionCampo.Text,
                    frmPpal_lbelOperadorLogico.Text
                )
                ViewState("IdCorretalivoCondicion") = CInt(ViewState("IdCorretalivoCondicion")) + 1
            End If

            Frm_CmbVariablesCondicion.SelectedValue = 0
            Frm_CmbOperadorLogico.SelectedValue = 0
            Frm_TxtValorCondicion.Text = ""
            cadenaTraspasa = ""

            frmPpal_lbelTranscripcionCampo.Text = ""
            frmPpal_lbelOperadorLogico.Text = ""
            frmPpal_lbelValorCondicion.Text = ""
            frmPpal_lbelAndOr.Text = ""
            FrmlPpal_IndexUpdate.Text = ""


            setTablaGridViewCondiciones()
            verifyButtonAddCondition()


        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try

    End Sub
    Private Sub setTablaGridViewCondiciones()
        Try
            GridViewCondiciones.DataSource = ViewState("tablaCondiciones")
            GridViewCondiciones.DataBind()
            GridViewCondicionesNoEdit.DataSource = ViewState("tablaCondiciones")
            GridViewCondicionesNoEdit.DataBind()
            execJS("init()")
        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try




    End Sub

    Protected Sub GridViewCondiciones_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Pager Then
            'Dim PiePagina As Label = e.Row.FindControl("GrillaPpal_CurrentPageLabel")
            'Dim TotalPaginas As Integer = Convert.ToString(GrillaPpal.PageCount).Trim()
            'PiePagina.Text = "Página " & Convert.ToString(GrillaPpal.PageIndex + 1).Trim() & " de " & TotalPaginas.ToString()
        End If
    End Sub
    Public Shared Property CadenaComentarios As String = ""
    Protected Sub GrillaPpal_BtnEditEjecucion_Click(sender As Object, e As EventArgs)
        FrmPpal_Limpiar()

        FrmPpalTitulo.InnerHtml = "<i class='far fa-edit'></i>&nbsp;&nbsp;Editar <span Class='badge text-bg-warning'>EJECUCIÓN</span>"
        FrmPpal_BtnGuardar.Text = "<i class='fa-regular fa-floppy-disk'></i>&nbsp;&nbsp;&nbsp;Actualizar Borrador"



        ModalShow(FrmPpal_dialog, FrmPpal)
        execJS("init()")
    End Sub

#End Region


#Region "Log"
    Public Function getLogCritica()
        Try
            Dim N_ObjTblLogCritica As New N_ClsTblLogCritica
            Dim E_ObjTblLogCritica As New E_ClsTblLogCritica

            Dim dt As DataTable = N_ObjTblLogCritica.SelectAll(CInt(FrmPpal_TxtIdCritica.Text))
            'IdAccion    Accion

            '1   Creación Critica
            '2   Modificación Critica
            '3   Eliminación Critica
            '4   Aprobación Critica
            '5   Comentario

            Dim Tabla As String = "
<div class='row'>
    <div class='col-md-12'>
        <div class='main-timeline7'>"
            Dim i As Integer = 0

            For Each row As DataRow In dt.Rows
                i = i + 1

                Dim IdAccion As String = row("IdAccion").ToString()
                Dim Img As String = ""
                Dim Fecha As String = CDate(row("Fecha")).ToString("dd-MM-yyyy HH:mm")


                If IdAccion = 1 Then
                    Img = "<i class='fa-regular fa-calendar-plus' style='position:absolute;top:20px;font-size:2rem;left:20px;'></i>"

                ElseIf IdAccion = 2 Then

                    Img = "<i class='fa-solid fa-sliders' style='position:absolute;top:20px;font-size:2rem;left:20px;'></i>"

                ElseIf IdAccion = 3 Then

                    Img = "<i class='fa-solid fa-rectangle-xmark' style='position:absolute;top:20px;font-size:2rem;left:20px;'></i>"

                ElseIf IdAccion = 4 Then

                    Img = "<i class='fa-regular fa-circle-check' style='position:absolute;top:20px;font-size:2rem;left:20px;'></i>"

                ElseIf IdAccion = 5 Then

                    Img = "<i class='fa-regular fa-comment-dots' style='position:absolute;top:20px;font-size:2rem;left:20px;'></i>"


                End If


                Tabla += "
            <div class='timeline'>
                <div class='timeline-icon'>" + Img + "</div>
                <span class='year'>" + Fecha + "</span>
                <div class='timeline-content'>
                    <h5 class='title'>" + row("Accion") + "</h5>
                    <p class='description'>
                        <b>" + row("Usuario") + "</b>" + row("Comentario") + "
                    </p>
                </div>
            </div>
"



                'If i Mod 2 = 0 Then

                'Tabla = Tabla & "<div class='timeline'>"

                '    Tabla = Tabla & "<div class='timeline-content left'>"

                '    Tabla = Tabla & "<span class='timeline-icon'></span>"

                '    Tabla = Tabla & "<span class='date'> " & Fecha & "&nbsp;</span>"

                '    Tabla = Tabla & "<h2 class='title'>" & Img & "&nbsp;" & row("Accion") & "</h2>"

                '    Tabla = Tabla & "<p class='description'>"

                '    Tabla = Tabla & "<span style='color:green'>" & row("Usuario") & "</span>"

                '    Tabla = Tabla & "<br/>" & row("Comentario") & "</p></div></div>"

                'Else
                '    Tabla = Tabla & "<div class='timeline'>"

                '    Tabla = Tabla & "<div class='timeline-content right'>"

                '    Tabla = Tabla & "<span class='timeline-icon'></span>"

                '    Tabla = Tabla & "<span class='date'> " & Fecha & "&nbsp;</span>"

                '    Tabla = Tabla & "<h2 class='title'>" & Img & "&nbsp;" & row("Accion") & "</h2>"

                '    Tabla = Tabla & "<p class='description'>"

                '    Tabla = Tabla & "<span style='color:green'>" & row("Usuario") & "</span>"

                '    Tabla = Tabla & "<br/>" & row("Comentario") & "</p></div></div>"

                'End If

            Next

            Tabla += "
        </div>
    </div>
</div>"
            ViewState("Logs") = Tabla




        Catch ex As Exception

            Return ex.Message.ToString()

        End Try

    End Function
#End Region


#Region "Formulario Principal"
    Protected Sub FrmPpal_Limpiar()

        FrmPpal_panelEdit.Visible = True
        FrmPpal_panelNoEdit.Visible = False
        FrmPpal_BtnGuardar.Visible = True
        FrmPpal_BtnEnviarAprobacion.Visible = True

        FrmPpal_Version.Text = ""
        FrmPpal_Correlativo.Text = ""
        'FrmPpal_CmbWorkFlowAprobacion.SelectedValue = 0
        Frm_TxtValor.Text = ""
        frmPpal_textFechaVencimiento.Text = ""
        FrmPpal_AutorLog.InnerText = ""
        FrmPpal_FechaCreacionLog.InnerText = ""
        FrmPpal_EstadoActualLog.InnerText = ""
        FrmPpal_Justificacion.Text = ""
        FrmPpal_TxtJustificacionNoEdit.Text = ""
        setTableCondicion()
        FrmPpal_ResultLabelTransaccion.Text = ""
        setTablaGridViewCondiciones()

        ViewState("Logs") = ""
        ViewState("Comentarios") = ""
        FrmlPpal_traspasaComentario.Text = ""


    End Sub
    Function FrmPpal_ValidarCampos() As Boolean
        Dim SW As Boolean = True

        'If FrmPpal_TxtTitulo.Text.Trim() = "" Then
        '    GetValidador(FrmPpal_TxtTitulo, "form-control specialform", FrmPpal_TxtTitulo_Msj, "* Obligatorio", "ERROR")
        '    SW = False
        'Else
        '    GetValidador(FrmPpal_TxtTitulo, "form-control specialform", FrmPpal_TxtTitulo_Msj, "", "OK")
        'End If

        Return SW
    End Function


    Protected Sub FrmPpal_BtnEnviarAprobacion_Click(sender As Object, e As EventArgs)
        Try
            Dim N_ObjTblCritica As New N_ClsTblCritica
            Dim E_ObjTblCritica As New E_ClsTblCritica

            If FrmPpal_TxtIdCritica.Text = "" Then
                SaveBorradorCritica()
            End If

            E_ObjTblCritica.IdEstado = 2
            E_ObjTblCritica.IdCritica = CInt(FrmPpal_TxtIdCritica.Text)
            E_ObjTblCritica.LastTransactionUser = ViewState("MyIdUser")
            N_ObjTblCritica.UpdateEstado(E_ObjTblCritica)


            'Pendiente enviar el correo y escribir el LOG.. del correo TODO

            Dim textoLog = "Se envia aprobar critica: " + FrmPpal_CboPrefijo.Items(FrmPpal_CboPrefijo.SelectedIndex).Text + FrmPpal_Correlativo_label + "-V1"
            Dim opcLog = 6


            setLogWrite(textoLog, opcLog)
            getCarpeta(CInt(FrmPpal_TxtIdCarpeta.Text))
            execJS("init()")

            ModalHide(FrmPpal_dialog, FrmPpal)

            FrmMsg_LblCuerpo.Text = "Se envia a Aprobación la Critica."
            FrmMsg_LblTitulo.Text = "Creación de critica"

            MsgModalExitoso(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Creación de critica", FrmMsg_LblCuerpo, FrmMsg_LblCuerpo.Text)




            ModalShow(FrmMsg_dialog, FrmMsg)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub SaveBorradorCritica()
        Try
            Dim N_ObjTblCritica As New N_ClsTblCritica
            Dim E_ObjTblCritica As New E_ClsTblCritica
            Dim ResultadoTransaccion As String
            E_ObjTblCritica.IdCarpeta = CInt(FrmPpal_TxtIdCarpeta.Text)
            E_ObjTblCritica.IdPrefijo = CInt(FrmPpal_CboPrefijo.SelectedValue)
            E_ObjTblCritica.Correlativo = CInt(FrmPpal_Correlativo.Text)
            E_ObjTblCritica.Version = CInt(FrmPpal_Version.Text)
            E_ObjTblCritica.IdEstado = 1
            E_ObjTblCritica.Owner = ViewState("MyIdUser")
            E_ObjTblCritica.LastTransactionUser = ViewState("MyIdUser")

            E_ObjTblCritica.FechaCreacion = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss")
            If frmPpal_textFechaVencimiento.Text <> "" Then
                E_ObjTblCritica.FechaRevision = Convert.ToDateTime(frmPpal_textFechaVencimiento.Text)
            Else
                E_ObjTblCritica.FechaRevision = DateTime.Now.ToString("yyyy-MM-dd")
            End If

            E_ObjTblCritica.IdWorkflow = FrmPpal_CmbWorkFlowAprobacion.SelectedValue
            E_ObjTblCritica.Justificacion = FrmPpal_Justificacion.Text


            E_ObjTblCritica.IdVariableAccion = Frm_CmbVariablesAccion.SelectedValue
            E_ObjTblCritica.VariableAccionValor = Frm_TxtValor.Text

            Dim textoLog As String
            Dim opcLog As Integer

            If FrmPpal_TxtIdCritica.Text <> "" Then
                E_ObjTblCritica.IdCritica = FrmPpal_TxtIdCritica.Text
                ResultadoTransaccion = N_ObjTblCritica.Update(E_ObjTblCritica)

                textoLog = "Se actualiza información Borrador: " + FrmPpal_CboPrefijo.Items(FrmPpal_CboPrefijo.SelectedIndex).Text + FrmPpal_Correlativo_label + "-V1"
                opcLog = 2
            Else
                ResultadoTransaccion = N_ObjTblCritica.Insert(E_ObjTblCritica)
                textoLog = "Se crea Borrador: " + FrmPpal_CboPrefijo.Items(FrmPpal_CboPrefijo.SelectedIndex).Text + FrmPpal_Correlativo_label + "-V1"
                opcLog = 1

            End If

            If Mid(ResultadoTransaccion, 1, 2) = "OK" Then
                Dim N_ObjTblCriticaCondicion As New N_ClsTblCriticaCondicion
                Dim E_ObjTblCriticaCondicion As New E_ClsTblCriticaCondicion
                FrmPpal_TxtIdCritica.Text = Replace(ResultadoTransaccion, "OK", "")


                Dim orden As Integer = 1
                Dim tbl As DataTable = ViewState("tablaCondiciones")
                For Each cell In tbl.Rows
                    E_ObjTblCriticaCondicion.IdCritica = FrmPpal_TxtIdCritica.Text
                    E_ObjTblCriticaCondicion.Orden = orden
                    E_ObjTblCriticaCondicion.IdVariableCondicion = cell("idCampoCondicion")
                    E_ObjTblCriticaCondicion.OperadorLogico = cell("idOperadorLogico")
                    E_ObjTblCriticaCondicion.Valor1 = cell("valor")
                    E_ObjTblCriticaCondicion.Valor2 = cell("CadenaCondicion")
                    E_ObjTblCriticaCondicion.AndOr = cell("andOr")
                    E_ObjTblCriticaCondicion.LastTransactionUser = ViewState("MyIdUser")
                    N_ObjTblCriticaCondicion.Insert(E_ObjTblCriticaCondicion)
                    orden += 1
                Next
                setLogWrite(textoLog, opcLog)


            End If

            getCarpeta(CInt(FrmPpal_TxtIdCarpeta.Text))

            execJS("init()")

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try

    End Sub
    Protected Sub FrmPpal_BtnGuardar_Click(sender As Object, e As EventArgs)
        SaveBorradorCritica()

    End Sub

    Private Sub setLogWrite(Cadena As String, IdAccion As Integer)
        Dim N_ObjTblLogCritica As New N_ClsTblLogCritica
        Dim E_ObjTblLogCritica As New E_ClsTblLogCritica
        Dim ResultadoTransaccion As String

        E_ObjTblLogCritica.IdCritica = FrmPpal_TxtIdCritica.Text
        E_ObjTblLogCritica.Comentario = Cadena
        E_ObjTblLogCritica.IdAccion = IdAccion
        E_ObjTblLogCritica.LastTransactionUser = ViewState("MyIdUser")
        E_ObjTblLogCritica.Fecha = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss")
        N_ObjTblLogCritica.Insert(E_ObjTblLogCritica)


    End Sub
    Protected Sub FrmPpal_BtnCerrar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmPpal_dialog, FrmPpal)
    End Sub
#End Region



#Region "FrmMsg"
    Protected Sub FrmMsg_BtnAceptar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmMsg_dialog, FrmMsg)
    End Sub

    Private Sub execJS(ByVal script As String)
        Try
            Dim TxtJavascript As String = "<script type='text/javascript'>" + script + "</script>"
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "alerta", TxtJavascript, False)
        Catch ex As Exception

        End Try
    End Sub
#End Region


#Region "Grilla Adjunto"
    Protected Sub GrillaAdjunto_Llenar()
        Try
            Dim N_ObjAdjunto As New N_ClsTblAdjunto
            Dim E_ObjAdjunto As New E_ClsTblAdjunto With {
                .IdTemp = FrmPpal_TxtIdTemp.Text.Trim(),
                .IdCritica = If(String.IsNullOrEmpty(FrmPpal_TxtIdCritica.Text.Trim()), 0, CInt(FrmPpal_TxtIdCritica.Text.Trim()))
            }

            If FrmPpal_TxtIdTemp.Text.Trim() = "" Then
                GrillaAdjunto.DataSource = SortingPageIndex(N_ObjAdjunto.SelectAll(E_ObjAdjunto), ViewState("GrillaAdjunto_ColumnSorting"), ViewState("GrillaAdjunto_TypeSorting")) 'GrillaAdjunto_RegCount
            Else
                GrillaAdjunto.DataSource = SortingPageIndex(N_ObjAdjunto.SelectAll_Temp(E_ObjAdjunto), ViewState("GrillaAdjunto_ColumnSorting"), ViewState("GrillaAdjunto_TypeSorting")) 'GrillaAdjunto_RegCount
            End If
            GrillaAdjunto.DataBind()

            GrillaAdjunto.Columns(1).ItemStyle.HorizontalAlign = HorizontalAlign.Center
            GrillaAdjunto.Columns(2).ItemStyle.HorizontalAlign = HorizontalAlign.Left
            GrillaAdjunto.Columns(3).ItemStyle.HorizontalAlign = HorizontalAlign.Center

        Catch ex As Exception
            GrillaAdjunto.DataSource = Nothing
            GrillaAdjunto.DataBind()
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub

    Protected Sub FrmPpal_AddAdjunto_Click(sender As Object, e As EventArgs)
        FrmAdjunto_Limpiar()
        FrmAdjunto_TxtIdTemp.Text = FrmPpal_TxtIdTemp.Text
        FrmAdjunto_TxtIdCritica.Text = FrmPpal_TxtIdCritica.Text
        ModalShow(FrmAdjunto_dialog, FrmAdjunto)
    End Sub

    Protected Sub GrillaAdjunto_BtnDescargar_Click(sender As Object, e As EventArgs)
        Try
            Dim btn As LinkButton = sender
            Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
            Dim IdAdjunto As Label = CType(GrillaAdjunto.Rows(row.RowIndex).FindControl("GrillaAdjunto_LblIdAdjunto"), Label)
            Dim AdjName As Label = CType(GrillaAdjunto.Rows(row.RowIndex).FindControl("GrillaAdjunto_LblAdjName"), Label)

            Dim ObjAdjunto As Byte()

            Dim N_ObjAdjunto As New N_ClsTblAdjunto
            Dim E_ObjAdjunto As New E_ClsTblAdjunto With {
                .IdAdjunto = IdAdjunto.Text
            }

            If FrmPpal_TxtIdTemp.Text.Trim() = "" Then
                ObjAdjunto = N_ObjAdjunto.GetAdjunto(E_ObjAdjunto)
            Else
                ObjAdjunto = N_ObjAdjunto.GetAdjuntoTemp(E_ObjAdjunto)
            End If

            System.IO.File.WriteAllBytes(Server.MapPath("").Replace("\Configuracion", "\TempFile") & "/" & AdjName.Text.Trim(), ObjAdjunto)

            'Esto abre en una nueva ventana
            Dim TxtJavascript As String = "<script type='text/javascript'>window.open('../TempFile/" & AdjName.Text.Trim() & "?time" & CStr(Now).Replace(" ", "") & "', '_blank');</script>"
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "", TxtJavascript, False)

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub GrillaAdjunto_BtnEliminar_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
        Dim IdAdjunto As Label = CType(GrillaAdjunto.Rows(row.RowIndex).FindControl("GrillaAdjunto_LblIdAdjunto"), Label)
        Dim AdjName As Label = CType(GrillaAdjunto.Rows(row.RowIndex).FindControl("GrillaAdjunto_LblAdjName"), Label)
        Dim LastTransactionUser As Label = CType(GrillaAdjunto.Rows(row.RowIndex).FindControl("GrillaAdjunto_LblLastTransactionUser"), Label)
        Dim Fecha As Label = CType(GrillaAdjunto.Rows(row.RowIndex).FindControl("GrillaAdjunto_LblFecha"), Label)

        Dim TimeDif As TimeSpan = Now - DateTime.Parse(Fecha.Text)
        If TimeDif.TotalHours > 24 Or LastTransactionUser.Text.Trim.ToUpper() <> ViewState("MyIdUser").ToString.Trim.ToUpper() Then
            MsgModalAlerta(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Alerta", FrmMsg_LblCuerpo, "Los adjuntos solo pueden ser eliminados por su autor y dentro de las 24 horas posteriores a su ingreso.")
            ModalShow(FrmMsg_dialog, FrmMsg)
            Exit Sub
        End If

        FrmDelete_Titulo.InnerHtml = "<i class='fa-solid fa-trash-can'></i>&nbsp;&nbsp;Eliminar Adjunto"

        FrmDelete_TxtOpt.Text = "3"
        FrmDelete_TxtId.Text = IdAdjunto.Text
        FrmDelete_Msg.Text = "Se eliminará el adjunto <b>" & AdjName.Text & "</b>"

        ModalShow(FrmDelete_dialog, FrmDelete)
    End Sub
    Protected Sub GrillaAdjunto_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Pager Then
            Dim PiePagina As Label = e.Row.FindControl("GrillaAdjunto_CurrentPageLabel")
            Dim TotalPaginas As Integer = Convert.ToString(GrillaAdjunto.PageCount).Trim()
            PiePagina.Text = "Página " & Convert.ToString(GrillaAdjunto.PageIndex + 1).Trim() & " de " & TotalPaginas.ToString()
        End If
    End Sub
    Private Sub GrillaAdjunto_PreRender(sender As Object, e As EventArgs) Handles GrillaAdjunto.PreRender
        'Esto es para que la grilla muestre el paginador aunque tenga menos de 10 registros
        If GrillaAdjunto.BottomPagerRow IsNot Nothing Then
            If GrillaAdjunto.BottomPagerRow.Visible = False Then
                GrillaAdjunto.BottomPagerRow.Visible = True
            End If
        End If
    End Sub
#End Region


#Region "Formulario Adjunto"
    Protected Sub FrmAdjunto_Limpiar()
        FrmAdjunto_TxtIdTemp.Text = ""
        FrmAdjunto_TxtIdCritica.Text = ""
        FrmAdjunto_TxtComentario.Text = ""

        FrmAdjunto_Upload.Attributes.Clear()
        FrmAdjunto_Upload.FileContent.Dispose()
        GetValidador(FrmAdjunto_Upload, "form-control form-control-sm", FrmAdjunto_Upload_Msj, "", "LIMPIAR")
    End Sub
    Function FrmAdjunto_ValidarCampos() As Boolean
        Dim SW As Boolean = True

        If FrmAdjunto_Upload.HasFile Then
            GetValidador(FrmAdjunto_Upload, "form-control form-control-sm", FrmAdjunto_Upload_Msj, "", "OK")
        Else
            GetValidador(FrmAdjunto_Upload, "form-control form-control-sm", FrmAdjunto_Upload_Msj, "", "ERROR")
            SW = False
        End If

        Return SW
    End Function
    Public Function FrmAdjunto_GetSizeFile(ByVal TxtSize As Long) As String
        Dim UM As String
        Dim TxtSizeConv As Double

        If TxtSize < 1024 Then
            UM = "bytes"
            TxtSizeConv = TxtSize
        ElseIf TxtSize < 1048576 Then
            UM = "KB"
            TxtSizeConv = TxtSize / 1024
        Else
            UM = "MB"
            TxtSizeConv = TxtSize / 1048576
        End If

        Return String.Format("{0:n2} {1}", TxtSizeConv, UM)
    End Function
    Protected Sub FrmAdjunto_BtnGuardar_Click() ' sender As Object, e As EventArgs
        Try
            If Not FrmAdjunto_ValidarCampos() Then
                ModalShow(FrmPpal_dialog, FrmPpal)
                ModalShow(FrmAdjunto_dialog, FrmAdjunto)
                Exit Sub
            End If

            Dim ResultadoTransaccion As String
            Dim N_ObjAdjunto As New N_ClsTblAdjunto
            Dim E_ObjAdjunto As New E_ClsTblAdjunto With {
                .IdTemp = FrmAdjunto_TxtIdTemp.Text.Trim(),
                .IdCritica = If(String.IsNullOrEmpty(FrmAdjunto_TxtIdCritica.Text.Trim()), 0, CInt(FrmAdjunto_TxtIdCritica.Text.Trim())),
                .Comentario = FrmAdjunto_TxtComentario.Text.Trim(),
                .AdjName = FrmAdjunto_Upload.FileName,
                .AdjFile = FrmAdjunto_Upload.FileBytes,
                .AdjSize = FrmAdjunto_GetSizeFile(FrmAdjunto_Upload.PostedFile.ContentLength),
                .AdjExt = Path.GetExtension(FrmAdjunto_Upload.FileName),
                .LastTransactionUser = ViewState("MyIdUser")
            }

            If FrmAdjunto_TxtIdTemp.Text.Trim() = "" Then
                ResultadoTransaccion = N_ObjAdjunto.Insert(E_ObjAdjunto)
            Else
                ResultadoTransaccion = N_ObjAdjunto.Insert_Temp(E_ObjAdjunto)
            End If
            If ResultadoTransaccion <> "OK" Then
                FrmAdjunto_Upload.Attributes.Clear()
                FrmAdjunto_Upload.FileContent.Dispose()
                GetValidador(FrmAdjunto_Upload, "form-control form-control-sm", FrmAdjunto_Upload_Msj, "", "LIMPIAR")
                MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
                ModalShow(FrmMsg_dialog, FrmMsg)
                Exit Sub
            End If

            ModalShow(FrmPpal_dialog, FrmPpal)
            GrillaAdjunto_Llenar()

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub FrmAdjunto_BtnCerrar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmAdjunto_dialog, FrmAdjunto)
    End Sub
#End Region

#Region "FrmDelete"
    Protected Sub FrmDelete_BtnCerrar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmDelete_dialog, FrmDelete)
    End Sub
    Protected Sub FrmDelete_BtnEliminar_Click(sender As Object, e As EventArgs)
        Try
            Dim ResultadoTransaccion As String
            ModalHide(FrmDelete_dialog, FrmDelete)

            ''If FrmDelete_TxtOpt.Text = "1" Then
            'Dim N_ObjActivo As New N_ClsTblActivo
            '    Dim E_ObjActivo As New E_ClsTblActivo With {
            '        .IdCritica = FrmDelete_TxtId.Text.Trim(),
            '        .LastTransactionUser = ViewState("MyIdUser")
            '    }

            '    ResultadoTransaccion = N_ObjActivo.Delete(E_ObjActivo)
            '    If ResultadoTransaccion <> "OK" Then
            '        MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
            '        ModalShow(FrmMsg_dialog, FrmMsg)
            '        Exit Sub
            '    End If

            '    GrillaPpal_Llenar()
            'ElseIf FrmDelete_TxtOpt.Text = "2" Then
            '    Dim N_ObjComentario As New N_ClsTblComentario
            '    Dim E_ObjComentario As New E_ClsTblComentario With {
            '        .IdComentario = FrmDelete_TxtId.Text.Trim(),
            '        .LastTransactionUser = ViewState("MyIdUser")
            '    }

            '    If FrmPpal_TxtIdTemp.Text.Trim() = "" Then
            '        ResultadoTransaccion = N_ObjComentario.Delete(E_ObjComentario)
            '    Else
            '        ResultadoTransaccion = N_ObjComentario.Delete_Temp(E_ObjComentario)
            '    End If
            '    If ResultadoTransaccion <> "OK" Then
            '        MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
            '        ModalShow(FrmMsg_dialog, FrmMsg)
            '        Exit Sub
            '    End If

            '    GrillaComentario_Llenar()
            'ElseIf FrmDelete_TxtOpt.Text = "3" Then
            '    Dim N_ObjAdjunto As New N_ClsTblAdjunto
            '    Dim E_ObjAdjunto As New E_ClsTblAdjunto With {
            '        .IdAdjunto = FrmDelete_TxtId.Text.Trim(),
            '        .LastTransactionUser = ViewState("MyIdUser")
            '    }

            '    If FrmPpal_TxtIdTemp.Text.Trim() = "" Then
            '        ResultadoTransaccion = N_ObjAdjunto.Delete(E_ObjAdjunto)
            '    Else
            '        ResultadoTransaccion = N_ObjAdjunto.Delete_Temp(E_ObjAdjunto)
            '    End If
            '    If ResultadoTransaccion <> "OK" Then
            '        MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
            '        ModalShow(FrmMsg_dialog, FrmMsg)
            '        Exit Sub
            '    End If

            '    GrillaAdjunto_Llenar()
            'End If

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
#End Region


End Class