﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class FrmCarpetas

    '''<summary>
    '''Control PnProgress.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents PnProgress As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control UpProgress.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents UpProgress As Global.System.Web.UI.UpdateProgress

    '''<summary>
    '''Control ModalProgress.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ModalProgress As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''Control UpdatePanelIndex.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents UpdatePanelIndex As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control PanelIndex.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents PanelIndex As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control BtnAddRegistro.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents BtnAddRegistro As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control CarpetasTreeView.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CarpetasTreeView As Global.System.Web.UI.WebControls.TreeView

    '''<summary>
    '''Control FrmPpal_TxtIdCarpeta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_TxtIdCarpeta As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmPpal_TxtIdCarpetaPadre.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_TxtIdCarpetaPadre As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmPpal_TxtNivel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_TxtNivel As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmPpal_TxtSwTipo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_TxtSwTipo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmPpal_LastOptionSelected.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_LastOptionSelected As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control Ffrm_principal_contenedorCriticas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Ffrm_principal_contenedorCriticas As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control DivBreadcrumbsFolder.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents DivBreadcrumbsFolder As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpal_TxtCarpeta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_TxtCarpeta As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmPpal_TxtCarpeta_Msj.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_TxtCarpeta_Msj As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpal_DivUltAct.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_DivUltAct As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpal_ChkPrivada.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_ChkPrivada As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmPpal_ChkEstado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_ChkEstado As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmPpal_ChkContenedorCriticas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_ChkContenedorCriticas As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmPpal_DivStoreProcedure.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_DivStoreProcedure As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpal_TxtStoreProcedure.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_TxtStoreProcedure As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmPpal_TxtStoreProcedure_Msj.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_TxtStoreProcedure_Msj As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control Ffrm_principal_LabelAcordeonOpc1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Ffrm_principal_LabelAcordeonOpc1 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpal_LstListaDistribucion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_LstListaDistribucion As Global.System.Web.UI.WebControls.CheckBoxList

    '''<summary>
    '''Control Ffrm_principal_LabelAcordeonOpc2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Ffrm_principal_LabelAcordeonOpc2 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpal_CboUserGestionSearch.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_CboUserGestionSearch As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control FrmPpal_BtnGrabar_personaAutorizada.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_BtnGrabar_personaAutorizada As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmPpal_GrillaAutoriza.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_GrillaAutoriza As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control hfSelected.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents hfSelected As Global.System.Web.UI.WebControls.HiddenField

    '''<summary>
    '''Control Ffrm_principal_LabelAcordeonOpc3.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Ffrm_principal_LabelAcordeonOpc3 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpal_TxtPrefijo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_TxtPrefijo As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control LinkButton2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents LinkButton2 As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmPpal_GrillaPrefijo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_GrillaPrefijo As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control FrmPpal_botonera.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_botonera As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpal_BtnGrabar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_BtnGrabar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmPpal_BtnAddSubCarpeta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_BtnAddSubCarpeta As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmPpal_BtnVer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_BtnVer As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control FrmPpal.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''Control FrmPpal_Panel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_Panel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control FrmPpal_UpdatePanel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_UpdatePanel As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control FrmPpal_dialog.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_dialog As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpalTitulo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpalTitulo As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmCarpetaRaiz_TxtCarpeta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmCarpetaRaiz_TxtCarpeta As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmCarpetaRaiz_TxtCarpeta_Msj.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmCarpetaRaiz_TxtCarpeta_Msj As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control pathBrath.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents pathBrath As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpal_BtnCerrar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_BtnCerrar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmPpal_BtnGuardar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_BtnGuardar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmMsg_BtnVer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_BtnVer As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control FrmMsg.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''Control FrmMsg_Panel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_Panel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control FrmMsg_UpdatePanel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_UpdatePanel As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control FrmMsg_dialog.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_dialog As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmMsg_TipoMensaje.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_TipoMensaje As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmMsg_TipoMensajeIcono.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_TipoMensajeIcono As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmMsg_LblTitulo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_LblTitulo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control FrmMsg_LblCuerpo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_LblCuerpo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control FrmMsg_BtnAceptar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_BtnAceptar As Global.System.Web.UI.WebControls.Button
End Class
