﻿Imports Capa_Entidad
Imports Capa_Negocio
Imports System.Data


Public Class FrmProgramacionEjecucion
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Dim URL As String = Page.Request.UrlReferrer.AbsolutePath
        Catch ex As Exception
            'Response.Redirect("../PageError.aspx")
        End Try
        If Request.Form("__EVENTTARGET") <> "" Then
            If Request.Form("__EVENTTARGET") = "UploadEvent" Then
                GrillaPpal_Llenar()
            End If
        End If
        If Not Page.IsPostBack Then
            Dim var_krypton As String = OwnDecryption(Page.Request("krypton"))
            ViewState("MyIdUser") = OwnPageRequest(var_krypton, "IdUser")

            GrillaPpal_Llenar()
            GrillaPpal.Attributes("Titulo") = SortDirection.Descending
        End If
    End Sub

#Region "Index"
    Protected Sub BtnBsc_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
    End Sub
    Protected Sub BtnAddRegistro_Click(sender As Object, e As EventArgs)
        Try
            FrmPpal_Limpiar()

            FrmPpalTitulo.InnerHtml = "<i class='far fa-edit'></i>&nbsp;&nbsp;Crear <span Class='badge text-bg-warning'>EJECUCIÓN</span>"
            FrmPpal_BtnGuardar.Text = "<i class='fa-regular fa-floppy-disk'></i>&nbsp;&nbsp;&nbsp;Agregar"

            FrmPpal_CboTipoEjecucion.Items.Insert(0, New ListItem("<< Seleccionar >>", "0"))
            FrmPpal_CboTipoEjecucion.Items.Insert(1, New ListItem("Inmediata", "1"))
            FrmPpal_CboTipoEjecucion.Items.Insert(2, New ListItem("Programada", "2"))
            FrmPpal_CboTipoEjecucion.Items.Item(0).Selected = True
            FrmPpal_TxtTitulo.Focus()

            getCarpetasCriticas()
            getListaDistribucion()


            ModalShow(FrmPpal_dialog, FrmPpal)
            execJS("init()")
        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub


    Private Sub getListaDistribucion()
        Try
            Dim N_ObjListaDistribucion As New N_ClsTblListaDistribucion
            FrmPpal_chkListaDistribucion.Items.Clear()
            FrmPpal_chkListaDistribucion.DataTextField = "TextField"
            FrmPpal_chkListaDistribucion.DataValueField = "ValueField"
            Dim dt As DataTable = N_ObjListaDistribucion.LoadCbo()
            FrmPpal_chkListaDistribucion.DataSource = dt
            FrmPpal_chkListaDistribucion.DataBind()


        Catch ex As Exception

        End Try
    End Sub


    Private Sub getCarpetasCriticas()
        Try
            Dim N_ObjTblCarpeta As New N_ClsTblCarpeta
            Dim E_ObjCarpeta As New E_ClsTblCarpeta
            Dim getCarpetaContenedora As DataTable = N_ObjTblCarpeta.GetCarpetasContenedoras()

            Dim tabla As New DataTable
            tabla.Columns.Add("ValueField", GetType(Integer))
            tabla.Columns.Add("TextField", GetType(String))

            Dim cadenaConcat As String = ""
            For Each row As DataRow In getCarpetaContenedora.Rows
                If CInt(row("cantCriticas")) > 0 Then
                    E_ObjCarpeta.IdCarpeta = CInt(row("IdCarpeta"))
                    cadenaConcat = String.Concat(N_ObjTblCarpeta.getSubCarpetaRuta(E_ObjCarpeta), " <strong>(", CStr(row("cantCriticas")), ")</strong>")
                    tabla.Rows.Add(CInt(row("IdCarpeta")), cadenaConcat)
                End If
            Next row

            FrmPpal_cboCarpetas.Items.Clear()
            FrmPpal_cboCarpetas.DataTextField = "TextField"
            FrmPpal_cboCarpetas.DataValueField = "ValueField"
            FrmPpal_cboCarpetas.DataSource = tabla
            FrmPpal_cboCarpetas.DataBind()


        Catch ex As Exception

        End Try
    End Sub

    Public Shared Property CadenaComentarios As String = ""
    Protected Sub GrillaPpal_BtnEditEjecucion_Click(sender As Object, e As EventArgs)
        FrmPpal_Limpiar()

        FrmPpalTitulo.InnerHtml = "<i class='far fa-edit'></i>&nbsp;&nbsp;Editar <span Class='badge text-bg-warning'>EJECUCIÓN</span>"
        FrmPpal_BtnGuardar.Text = "<i class='fa-regular fa-floppy-disk'></i>&nbsp;&nbsp;&nbsp;Agregar"

        FrmPpal_CboTipoEjecucion.Items.Insert(0, New ListItem("<< Seleccionar >>", "0"))
        FrmPpal_CboTipoEjecucion.Items.Insert(1, New ListItem("Inmediata", "1"))
        FrmPpal_CboTipoEjecucion.Items.Insert(2, New ListItem("Programada", "2"))
        FrmPpal_TxtTitulo.Focus()

        ModalShow(FrmPpal_dialog, FrmPpal)
        execJS("init()")
    End Sub


    Protected Sub FrmPpal_Comentario_click(sender As Object, e As EventArgs)
        Try

        Catch ex As Exception

        End Try

    End Sub


#End Region

#Region "Grilla Principal"
    Protected Sub GrillaPpal_Llenar()
        Try
            Dim N_ObjEjecucion As New N_ClsTblEjecucion
            GrillaPpal.DataSource = SortingPageIndex(N_ObjEjecucion.SelectAll(TxtBsc.Text.Trim()), ViewState("GrillaPpal_ColumnSorting"), ViewState("GrillaPpal_TypeSorting"))
            GrillaPpal.DataBind()

            GrillaPpal.Columns(1).ItemStyle.HorizontalAlign = HorizontalAlign.Center
            GrillaPpal.Columns(2).ItemStyle.HorizontalAlign = HorizontalAlign.Left
            GrillaPpal.Columns(3).ItemStyle.HorizontalAlign = HorizontalAlign.Left

            TxtBsc.Focus()
        Catch ex As Exception
            GrillaPpal.DataSource = Nothing
            GrillaPpal.DataBind()
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub GrillaPpal_BtnEditar_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
        Dim IdWorkflow As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblIdWorkflow"), Label)

        GetRegistro(IdWorkflow.Text)

        FrmPpalTitulo.InnerHtml = "<i class='far fa-edit'></i>&nbsp;&nbsp;Modificar <span Class='badge text-bg-warning'>WorkFlow</span>"
        FrmPpal_BtnGuardar.Text = "<i class='fa-regular fa-floppy-disk'></i>&nbsp;&nbsp;&nbsp;Actualizar"
        FrmPpal_TxtTitulo.Focus()

        ModalShow(FrmPpal_dialog, FrmPpal)
    End Sub
    Protected Sub GrillaPpal_BtnEliminar_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
        Dim IdWorkflow As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblIdWorkflow"), Label)
        Dim Workflow As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblWorkflow"), Label)

        FrmDeleteTitulo.InnerHtml = "<i class='fa-solid fa-pen-to-square'></i>&nbsp;&nbsp;Eliminar Job Position"

        FrmDelete_TxtId.Text = IdWorkflow.Text
        FrmDelete_Msg.Text = "Se eliminará el job position <b>" & Workflow.Text & "</b>."

        ModalShow(FrmDelete_dialog, FrmDelete)
    End Sub
    Protected Sub GrillaPpal_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Pager Then
            Dim PiePagina As Label = e.Row.FindControl("GrillaPpal_CurrentPageLabel")
            Dim TotalPaginas As Integer = Convert.ToString(GrillaPpal.PageCount).Trim()
            PiePagina.Text = "Página " & Convert.ToString(GrillaPpal.PageIndex + 1).Trim() & " de " & TotalPaginas.ToString()
        End If
    End Sub
    Protected Sub GrillaPpal_BtnFirst_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        GrillaPpal.PageIndex = 0
        GrillaPpal.DataBind()
    End Sub
    Protected Sub GrillaPpal_BtnPrev_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        If (GrillaPpal.PageIndex <> 0) Then
            GrillaPpal.PageIndex = GrillaPpal.PageIndex - 1
            GrillaPpal.DataBind()
        End If
    End Sub
    Protected Sub GrillaPpal_BtnNext_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        If (GrillaPpal.PageIndex <> GrillaPpal.PageCount - 1) Then
            GrillaPpal.PageIndex = GrillaPpal.PageIndex + 1
            GrillaPpal.DataBind()
        End If
    End Sub
    Protected Sub GrillaPpal_BtnLast_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        GrillaPpal.PageIndex = GrillaPpal.PageCount - 1
        GrillaPpal.DataBind()
    End Sub
    Private Sub GrillaPpal_PreRender(sender As Object, e As EventArgs) Handles GrillaPpal.PreRender
        'Esto es para que la grilla muestre el paginador aunque tenga menos de 10 registros
        If GrillaPpal.BottomPagerRow IsNot Nothing Then
            If GrillaPpal.BottomPagerRow.Visible = False Then
                GrillaPpal.BottomPagerRow.Visible = True
            End If
        End If
    End Sub
    Private Sub GrillaPpal_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles GrillaPpal.Sorting
        ViewState("GrillaPpal_ColumnSorting") = e.SortExpression

        If GrillaPpal.Attributes(e.SortExpression) = SortDirection.Ascending Then
            ViewState("GrillaPpal_TypeSorting") = "DESC"
            GrillaPpal_ChangeHeader()
        Else
            ViewState("GrillaPpal_TypeSorting") = "ASC"
            GrillaPpal_ChangeHeader()
        End If

        GrillaPpal_Llenar()
    End Sub
    Private Sub GrillaPpal_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GrillaPpal.RowCommand
        If Trim(e.CommandArgument) = "Workflow" Then
            ViewState("GrillaPpal_ColumnIndex") = 2
        End If
    End Sub
    Private Sub GrillaPpal_ChangeHeader()
        Dim Base As String = "<span class='link-light'>XXXTITULOXXX <i class='fa-solid fa-ellipsis-vertical'></i></span>"
        Dim Type As String

        If ViewState("GrillaPpal_TypeSorting") = "DESC" Then
            Type = "<i class='fa-solid fa-arrow-down-z-a'></i>"
        Else
            Type = "<i class='fa-solid fa-arrow-down-a-z'></i>"
        End If

        Dim GrillaPpal_Indice = New Integer() {2}
        Dim GrillaPpal_Campo() As String = {"Workflow"}
        Dim GrillaPpal_Titulo() As String = {"Job Position"}

        For I = 0 To GrillaPpal_Indice.Length - 1
            If ViewState("GrillaPpal_ColumnIndex") = GrillaPpal_Indice(I) Then
                GrillaPpal.Columns(GrillaPpal_Indice(I)).HeaderText = "<span class='link-light'>" & GrillaPpal_Titulo(I) & " " & Type & "</span>"
                If ViewState("GrillaPpal_TypeSorting") = "DESC" Then
                    GrillaPpal.Attributes(GrillaPpal_Campo(I)) = SortDirection.Descending
                Else
                    GrillaPpal.Attributes(GrillaPpal_Campo(I)) = SortDirection.Ascending
                End If
            Else
                GrillaPpal.Columns(GrillaPpal_Indice(I)).HeaderText = Replace(Base, "XXXTITULOXXX", GrillaPpal_Titulo(I))
                GrillaPpal.Attributes(GrillaPpal_Campo(I)) = SortDirection.Descending
            End If
        Next
    End Sub
#End Region

#Region "Formulario Principal"
    Protected Sub GetRegistro(ByVal IdWorkflow As Integer)
        Try
            FrmPpal_Limpiar()
            'FrmPpal_CboLineManeger_Llenar()
            Dim E_ObjWorkflow As New E_ClsTblWorkflow
            Dim N_ObjWorkflow As New N_ClsTblWorkflow
            E_ObjWorkflow.IdWorkflow = IdWorkflow
            E_ObjWorkflow = N_ObjWorkflow.SelectRecord(E_ObjWorkflow)

            FrmPpal_TxtIdEjecucion.Text = E_ObjWorkflow.IdWorkflow
            FrmPpal_TxtTitulo.Text = E_ObjWorkflow.Workflow.Trim()
            'FrmPpal_CboLineManeger.SelectedValue = E_ObjWorkflow.LineManeger



            FrmPpal_DivUltAct.InnerHtml = E_ObjWorkflow.UltAct


            execJS("init()")

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub FrmPpal_Limpiar()
        FrmPpal_DivUltAct.InnerHtml = ""
        FrmPpal_TxtIdEjecucion.Text = ""

        FrmPpal_TxtTitulo.Text = ""
        GetValidador(FrmPpal_TxtTitulo, "form-control specialform", FrmPpal_TxtTitulo_Msj, "", "LIMPIAR")

    End Sub
    Function FrmPpal_ValidarCampos() As Boolean
        Dim SW As Boolean = True

        If FrmPpal_TxtTitulo.Text.Trim() = "" Then
            GetValidador(FrmPpal_TxtTitulo, "form-control specialform", FrmPpal_TxtTitulo_Msj, "* Obligatorio", "ERROR")
            SW = False
        Else
            GetValidador(FrmPpal_TxtTitulo, "form-control specialform", FrmPpal_TxtTitulo_Msj, "", "OK")
        End If

        Return SW
    End Function
    Protected Sub FrmPpal_BtnGuardar_Click(sender As Object, e As EventArgs)
        Try
            If Not FrmPpal_ValidarCampos() Then
                Exit Sub
            End If

            Dim ResultadoTransaccion As String
            Dim E_ObjEjecucion As New E_ClsTblEjecucion
            Dim N_ObjEjecucion As New N_ClsTblEjecucion

            E_ObjEjecucion.Titulo = FrmPpal_TxtTitulo.Text.Trim()
            E_ObjEjecucion.TipoEjecucion = FrmPpal_CboTipoEjecucion.SelectedIndex
            E_ObjEjecucion.LastTransactionUser = ViewState("MyIdUser")

            If E_ObjEjecucion.TipoEjecucion = 2 Then
                E_ObjEjecucion.FechaHoraProgramada = Convert.ToDateTime(FrmPpal_TxtFechaHoraPlanificacion.Text).ToString("yyyy-MM-dd HH:mm:ss")
            End If
            'IdEjecucionPadre
            'FechaHoraProgramada
            'FechaHoraReal

            If FrmPpal_BtnGuardar.Text = "<i class='fa-regular fa-floppy-disk'></i>&nbsp;&nbsp;&nbsp;Agregar" Then
                E_ObjEjecucion.Owner = ViewState("MyIdUser")
                E_ObjEjecucion.FechaCreacion = DateTime.Now
                E_ObjEjecucion.FechaHoraReal = DateTime.Now
                E_ObjEjecucion.IdEstado = 1

                ResultadoTransaccion = N_ObjEjecucion.Insert(E_ObjEjecucion)
                If ResultadoTransaccion <> "OK" Then
                    GetValidador(FrmPpal_TxtTitulo, "form-control specialform", FrmPpal_TxtTitulo_Msj, "", "LIMPIAR")
                    MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
                    ModalShow(FrmMsg_dialog, FrmMsg)
                    Exit Sub
                End If
            Else
                E_ObjEjecucion.IdEjecucion = FrmPpal_TxtIdEjecucion.Text.Trim()
                ResultadoTransaccion = N_ObjEjecucion.Update(E_ObjEjecucion)
                If ResultadoTransaccion <> "OK" Then
                    GetValidador(FrmPpal_TxtTitulo, "form-control specialform", FrmPpal_TxtTitulo_Msj, "", "LIMPIAR")
                    MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
                    ModalShow(FrmMsg_dialog, FrmMsg)
                    Exit Sub
                End If
            End If

            GrillaPpal_Llenar()
            ModalHide(FrmPpal_dialog, FrmPpal)

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub FrmPpal_BtnCerrar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmPpal_dialog, FrmPpal)
    End Sub
#End Region

#Region "FrmDelete"
    Protected Sub FrmDelete_BtnCerrar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmDelete_dialog, FrmDelete)
    End Sub
    Protected Sub FrmDelete_BtnEliminar_Click(sender As Object, e As EventArgs)
        Try
            ModalHide(FrmDelete_dialog, FrmDelete)

            Dim E_ObjWorkflow As New E_ClsTblWorkflow
            Dim N_ObjWorkflow As New N_ClsTblWorkflow
            Dim ResultadoTransaccion As String

            E_ObjWorkflow.IdWorkflow = FrmDelete_TxtId.Text.Trim()
            E_ObjWorkflow.LastTransactionUser = ViewState("MyIdUser")

            ResultadoTransaccion = N_ObjWorkflow.Delete(E_ObjWorkflow)
            If ResultadoTransaccion <> "OK" Then
                MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
                ModalShow(FrmMsg_dialog, FrmMsg)
                Exit Sub
            End If

            GrillaPpal_Llenar()

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
#End Region

#Region "FrmMsg"
    Protected Sub FrmMsg_BtnAceptar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmMsg_dialog, FrmMsg)
    End Sub

    Private Sub execJS(ByVal script As String)
        Try
            Dim TxtJavascript As String = "<script type='text/javascript'>" + script + "</script>"
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "alerta", TxtJavascript, False)
        Catch ex As Exception

        End Try
    End Sub
#End Region

End Class