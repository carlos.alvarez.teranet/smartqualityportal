﻿Imports Capa_Entidad
Imports Capa_Negocio

Public Class FrmCategoria
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Dim URL As String = Page.Request.UrlReferrer.AbsolutePath
        Catch ex As Exception
            'Response.Redirect("../PageError.aspx")
        End Try
        If Request.Form("__EVENTTARGET") <> "" Then
            If Request.Form("__EVENTTARGET") = "UploadEvent" Then
                GrillaPpal_Llenar()
            End If
        End If
        If Not Page.IsPostBack Then
            ViewState("MyIdUser") = "carlos.astorga@kantar.com"

            GrillaPpal_Llenar()
            GrillaPpal.Attributes("Categoria") = SortDirection.Descending
        End If
    End Sub

#Region "Index"
    Protected Sub BtnBsc_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
    End Sub
    Protected Sub BtnAddRegistro_Click(sender As Object, e As EventArgs)
        FrmPpal_Limpiar()

        FrmPpalTitulo.InnerHtml = "<i class='far fa-edit'></i>&nbsp;&nbsp;Agregar <span Class='badge text-bg-warning'>CATEGORÍA</span>"
        FrmPpal_BtnGuardar.Text = "<i class='fa-regular fa-floppy-disk'></i>&nbsp;&nbsp;&nbsp;Guardar"

        FrmPpal_TxtCategoria.Focus()
        ModalShow(FrmPpal_dialog, FrmPpal)
    End Sub
#End Region

#Region "Grilla Principal"
    Protected Sub GrillaPpal_Llenar()
        Try
            Dim N_ObjCategoria As New N_ClsTblCategoria
            GrillaPpal.DataSource = SortingPageIndex(N_ObjCategoria.SelectAll(TxtBsc.Text.Trim()), ViewState("GrillaPpal_ColumnSorting"), ViewState("GrillaPpal_TypeSorting"))
            GrillaPpal.DataBind()

            GrillaPpal.Columns(1).ItemStyle.HorizontalAlign = HorizontalAlign.Center
            GrillaPpal.Columns(2).ItemStyle.HorizontalAlign = HorizontalAlign.Left
            GrillaPpal.Columns(3).ItemStyle.HorizontalAlign = HorizontalAlign.Center

            TxtBsc.Focus()
        Catch ex As Exception
            GrillaPpal.DataSource = Nothing
            GrillaPpal.DataBind()
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub GrillaPpal_BtnEditar_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
        Dim IdCategoria As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblIdCategoria"), Label)

        GetRegistro(IdCategoria.Text)

        FrmPpalTitulo.InnerHtml = "<i class='far fa-edit'></i>&nbsp;&nbsp;Modificar <span Class='badge text-bg-warning'>CATEGORÍA</span>"
        FrmPpal_BtnGuardar.Text = "<i class='fa-regular fa-floppy-disk'></i>&nbsp;&nbsp;&nbsp;Actualizar"
        FrmPpal_TxtCategoria.Focus()

        ModalShow(FrmPpal_dialog, FrmPpal)
    End Sub
    Protected Sub GrillaPpal_BtnEliminar_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
        Dim IdCategoria As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblIdCategoria"), Label)
        Dim Categoria As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblCategoria"), Label)

        FrmDeleteTitulo.InnerHtml = "<i class='fa-solid fa-pen-to-square'></i>&nbsp;&nbsp;Eliminar Categoría"

        FrmDelete_TxtId.Text = IdCategoria.Text
        FrmDelete_Msg.Text = "Se eliminará la categoría <b>" & Categoria.Text & "</b>."

        ModalShow(FrmDelete_dialog, FrmDelete)
    End Sub
    Protected Sub GrillaPpal_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Pager Then
            Dim PiePagina As Label = e.Row.FindControl("GrillaPpal_CurrentPageLabel")
            Dim TotalPaginas As Integer = Convert.ToString(GrillaPpal.PageCount).Trim()
            PiePagina.Text = "Página " & Convert.ToString(GrillaPpal.PageIndex + 1).Trim() & " de " & TotalPaginas.ToString()
        End If
    End Sub
    Protected Sub GrillaPpal_BtnFirst_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        GrillaPpal.PageIndex = 0
        GrillaPpal.DataBind()
    End Sub
    Protected Sub GrillaPpal_BtnPrev_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        If (GrillaPpal.PageIndex <> 0) Then
            GrillaPpal.PageIndex = GrillaPpal.PageIndex - 1
            GrillaPpal.DataBind()
        End If
    End Sub
    Protected Sub GrillaPpal_BtnNext_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        If (GrillaPpal.PageIndex <> GrillaPpal.PageCount - 1) Then
            GrillaPpal.PageIndex = GrillaPpal.PageIndex + 1
            GrillaPpal.DataBind()
        End If
    End Sub
    Protected Sub GrillaPpal_BtnLast_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        GrillaPpal.PageIndex = GrillaPpal.PageCount - 1
        GrillaPpal.DataBind()
    End Sub
    Private Sub GrillaPpal_PreRender(sender As Object, e As EventArgs) Handles GrillaPpal.PreRender
        'Esto es para que la grilla muestre el paginador aunque tenga menos de 10 registros
        If GrillaPpal.BottomPagerRow IsNot Nothing Then
            If GrillaPpal.BottomPagerRow.Visible = False Then
                GrillaPpal.BottomPagerRow.Visible = True
            End If
        End If
    End Sub
    Private Sub GrillaPpal_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles GrillaPpal.Sorting
        ViewState("GrillaPpal_ColumnSorting") = e.SortExpression

        If GrillaPpal.Attributes(e.SortExpression) = SortDirection.Ascending Then
            ViewState("GrillaPpal_TypeSorting") = "DESC"
            GrillaPpal_ChangeHeader()
        Else
            ViewState("GrillaPpal_TypeSorting") = "ASC"
            GrillaPpal_ChangeHeader()
        End If

        GrillaPpal_Llenar()
    End Sub
    Private Sub GrillaPpal_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GrillaPpal.RowCommand
        If Trim(e.CommandArgument) = "Categoria" Then
            ViewState("GrillaPpal_ColumnIndex") = 2
        End If
    End Sub
    Private Sub GrillaPpal_ChangeHeader()
        Dim Base As String = "<span class='link-light'>XXXTITULOXXX <i class='fa-solid fa-ellipsis-vertical'></i></span>"
        Dim Type As String

        If ViewState("GrillaPpal_TypeSorting") = "DESC" Then
            Type = "<i class='fa-solid fa-arrow-down-z-a'></i>"
        Else
            Type = "<i class='fa-solid fa-arrow-down-a-z'></i>"
        End If

        Dim GrillaPpal_Indice = New Integer() {2}
        Dim GrillaPpal_Campo() As String = {"Categoria"}
        Dim GrillaPpal_Titulo() As String = {"Categoría"}

        For I = 0 To GrillaPpal_Indice.Length - 1
            If ViewState("GrillaPpal_ColumnIndex") = GrillaPpal_Indice(I) Then
                GrillaPpal.Columns(GrillaPpal_Indice(I)).HeaderText = "<span class='link-light'>" & GrillaPpal_Titulo(I) & " " & Type & "</span>"
                If ViewState("GrillaPpal_TypeSorting") = "DESC" Then
                    GrillaPpal.Attributes(GrillaPpal_Campo(I)) = SortDirection.Descending
                Else
                    GrillaPpal.Attributes(GrillaPpal_Campo(I)) = SortDirection.Ascending
                End If
            Else
                GrillaPpal.Columns(GrillaPpal_Indice(I)).HeaderText = Replace(Base, "XXXTITULOXXX", GrillaPpal_Titulo(I))
                GrillaPpal.Attributes(GrillaPpal_Campo(I)) = SortDirection.Descending
            End If
        Next
    End Sub
#End Region

#Region "Formulario Principal"
    Protected Sub GetRegistro(ByVal IdCategoria As Integer)
        Try
            FrmPpal_Limpiar()

            Dim E_ObjCategoria As New E_ClsTblCategoria
            Dim N_ObjCategoria As New N_ClsTblCategoria
            E_ObjCategoria.IdCategoria = IdCategoria
            E_ObjCategoria = N_ObjCategoria.GetRegistro(E_ObjCategoria)

            FrmPpal_TxtIdCategoria.Text = E_ObjCategoria.IdCategoria
            FrmPpal_TxtCategoria.Text = E_ObjCategoria.Categoria.Trim()
            FrmPpal_DivUltAct.InnerHtml = E_ObjCategoria.Out_UltAct

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub FrmPpal_Limpiar()
        FrmPpal_DivUltAct.InnerHtml = ""
        FrmPpal_TxtIdCategoria.Text = ""

        FrmPpal_TxtCategoria.Text = ""
        GetValidador(FrmPpal_TxtCategoria, "form-control specialform", FrmPpal_TxtCategoria_Msj, "", "LIMPIAR")

    End Sub
    Function FrmPpal_ValidarCampos() As Boolean
        Dim SW As Boolean = True

        If FrmPpal_TxtCategoria.Text.Trim() = "" Then
            GetValidador(FrmPpal_TxtCategoria, "form-control specialform", FrmPpal_TxtCategoria_Msj, "* Obligatorio", "ERROR")
            SW = False
        Else
            GetValidador(FrmPpal_TxtCategoria, "form-control specialform", FrmPpal_TxtCategoria_Msj, "", "OK")
        End If

        Return SW
    End Function
    Protected Sub FrmPpal_BtnGuardar_Click(sender As Object, e As EventArgs)
        Try
            If Not FrmPpal_ValidarCampos() Then
                Exit Sub
            End If

            Dim ResultadoTransaccion As String
            Dim E_ObjCategoria As New E_ClsTblCategoria
            Dim N_ObjCategoria As New N_ClsTblCategoria

            E_ObjCategoria.Categoria = FrmPpal_TxtCategoria.Text.Trim()
            E_ObjCategoria.LastTransactionUser = ViewState("MyIdUser")

            If FrmPpal_BtnGuardar.Text = "<i class='fa-regular fa-floppy-disk'></i>&nbsp;&nbsp;&nbsp;Guardar" Then
                ResultadoTransaccion = N_ObjCategoria.Insert(E_ObjCategoria)
                If ResultadoTransaccion <> "OK" Then
                    GetValidador(FrmPpal_TxtCategoria, "form-control specialform", FrmPpal_TxtCategoria_Msj, "", "LIMPIAR")
                    MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
                    ModalShow(FrmMsg_dialog, FrmMsg)
                    Exit Sub
                End If
            Else
                E_ObjCategoria.IdCategoria = FrmPpal_TxtIdCategoria.Text.Trim()
                ResultadoTransaccion = N_ObjCategoria.Update(E_ObjCategoria)
                If ResultadoTransaccion <> "OK" Then
                    GetValidador(FrmPpal_TxtCategoria, "form-control specialform", FrmPpal_TxtCategoria_Msj, "", "LIMPIAR")
                    MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
                    ModalShow(FrmMsg_dialog, FrmMsg)
                    Exit Sub
                End If
            End If

            GrillaPpal_Llenar()
            ModalHide(FrmPpal_dialog, FrmPpal)

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub FrmPpal_BtnCerrar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmPpal_dialog, FrmPpal)
    End Sub
#End Region

#Region "FrmDelete"
    Protected Sub FrmDelete_BtnCerrar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmDelete_dialog, FrmDelete)
    End Sub
    Protected Sub FrmDelete_BtnEliminar_Click(sender As Object, e As EventArgs)
        Try
            ModalHide(FrmDelete_dialog, FrmDelete)

            Dim E_ObjCategoria As New E_ClsTblCategoria
            Dim N_ObjCategoria As New N_ClsTblCategoria
            Dim ResultadoTransaccion As String

            E_ObjCategoria.IdCategoria = FrmDelete_TxtId.Text.Trim()
            E_ObjCategoria.LastTransactionUser = ViewState("MyIdUser")

            ResultadoTransaccion = N_ObjCategoria.Delete(E_ObjCategoria)
            If ResultadoTransaccion <> "OK" Then
                MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
                ModalShow(FrmMsg_dialog, FrmMsg)
                Exit Sub
            End If

            GrillaPpal_Llenar()

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
#End Region

#Region "FrmMsg"
    Protected Sub FrmMsg_BtnAceptar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmMsg_dialog, FrmMsg)
    End Sub
#End Region

End Class