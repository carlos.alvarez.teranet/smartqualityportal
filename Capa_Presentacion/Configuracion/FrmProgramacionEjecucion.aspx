﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/GlbMaster.Master" CodeBehind="FrmProgramacionEjecucion.aspx.vb" Inherits="Capa_Presentacion.FrmProgramacionEjecucion" %>
<%@ Register Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script src="../assets/js/jsUpdateProgress.js"></script>

    <!-- Font Lato + css -->
    <link href="../assets/css/lato.css?v=v1" rel="stylesheet" />
    <link href="../assets/css/FrmProgramacionEjecucion.css?v=v1" rel="stylesheet" />

    <script type="text/javascript">
        var ModalProgress = '<%= ModalProgress.ClientID %>';
    </script>  

    <asp:Panel ID="PnProgress" runat="server">
        <asp:UpdateProgress ID="UpProgress" runat="server">
            <ProgressTemplate>
                <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                        <div class="modal-body">
                            <div class="d-flex align-items-center text-primary">
                                <img src='../assets/images/loadingLogin.gif' style='max-width:50px;'>
                            </div>
                        </div>
                    </div>
                </div>
            </ProgressTemplate>
        </asp:UpdateProgress>
    </asp:Panel>
    <asp:ModalPopupExtender ID="ModalProgress" runat="server" TargetControlID="PnProgress" BackgroundCssClass="TeraModalBackground" PopupControlID="PnProgress"  />
    

    <!-- MOSTRAR DATOS EN EL GRIDVIEW -->
    <asp:UpdatePanel runat="server" ID="UpdatePanelIndex">
        <ContentTemplate>
            <asp:Panel runat="server" ID="PanelIndex" CssClass="center-block">
                <div class="container-fluid">
                    <br />
                    <div class="row shadow-sm rounded">
                        <div class="col-11">                       
                            <h2 style="color:#26BD75; font-family:'Lato'; font-weight:900 !important;">Programación de <span class="badge text-bg-warning">EJECUCIÓN</span></h2>
                        </div>
                        <div class="col-1">
                            <span class="float-end">
                                <asp:LinkButton 
                                    ID="BtnAddRegistro" 
                                    runat="server" 
                                    CssClass="btn btn-success btn-addButton" 
                                    OnClick="BtnAddRegistro_Click">
                                       <i class="fa-solid fa-plus"></i>
                                </asp:LinkButton>
                            </span>
                        </div>
                    </div>

                    <br />
                    <div class="row">
                        <div class="col-5">
                            <div class="input-group mb-3">
                                <label class="input-group-text border-secondary text-white bg-dark" for="TxtBsc"><i class="fa-solid fa-magnifying-glass"></i>&nbsp;Buscar&nbsp;&nbsp;</label>
                                <asp:TextBox ID="TxtBsc" CssClass="form-control border-secondary form-control-sm"
                                    placeholder="para buscar valores múltiples utilice el separador ;" style="font-size: 1.1rem !important;"
                                    onkeypress="if ((event || window.event).keyCode == 13) { var node = (event || window.event).target || (event || window.event).srcElement; if (node.type == 'text' && node.id == 'ContentPlaceHolder1_TxtBsc') { return false; } }"
                                    autocomplete="off" runat="server"></asp:TextBox>
                                <asp:LinkButton ID="BtnBsc" CssClass="btn btn-dark"  OnClick="BtnBsc_Click" runat="server">
                                    &nbsp;&nbsp;<i class="fa-brands fa-golang" style="height:1.8em !important; vertical-align: inherit !important;"></i>&nbsp;&nbsp;
                                </asp:LinkButton>
                            </div>
                        </div>
                    </div> 

                    <br />
                    <div class="row">
                        <div class="col-12">
                            <div class="gradientLineKantar" style="float:left;"></div>
                            <asp:GridView ID="GrillaPpal" runat="server" CssClass="TeraGridview table table-bordered bg-light"
                                Style="text-align:center; vertical-align:middle; font-size:1rem; max-width:1600px; margin-left: 0px; width:98% !important;"
                                PageSize="50" AllowPaging="true" AutoGenerateColumns="false" 
                                HorizontalAlign="Center" AllowSorting="true"
                                OnRowDataBound="GrillaPpal_RowDataBound">
                            <HeaderStyle CssClass="bg-dark text-center text-white" Font-Size="Larger" Font-Bold="false" />                                                             
                                <Columns>                                    
                                    <%-- 0 --%>
                                    <asp:TemplateField HeaderText="" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("IdEjecucion") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- 1 --%>
                                    <asp:TemplateField HeaderText="<span class='link-light'>ID</span>" ItemStyle-Width="50">
                                        <ItemTemplate>
                                            <asp:Label ID="LblIdWorkflow" runat="server" Text='<%# Eval("IdEjecucion") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- 2 --%>
                                    <asp:TemplateField HeaderText="<span class='link-light'>Titulo <i class='fa-solid fa-ellipsis-vertical'></i></span>" SortExpression="Workflow">
                                        <ItemTemplate>
                                            <asp:Label ID="LblWorkflow" runat="server" Text='<%# Eval("Titulo") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- 3 --%>
                                    <asp:TemplateField HeaderText="<span class='link-light'>Creador </span>" > 
                                        <ItemTemplate>
                                           <asp:Label ID="LblWorkflow" runat="server" Text='<%# Eval("Owner") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                    <%-- 4 --%>
                                    <asp:TemplateField HeaderText="<span class='link-light'>Programación</span> " >
                                        <ItemTemplate>
                                             <asp:Label ID="LblWorkflow" runat="server" Text='<%# Eval("FechaHoraProgramada") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <%-- 5 --%>
                                    <asp:TemplateField HeaderText="<span class='link-light'>Estado</span> " >
                                        <ItemTemplate>
                                            <asp:Label ID="LblWorkflow" runat="server" Text='<%# Eval("Estado") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>


                                     <%-- 6 --%>
                                    <asp:TemplateField HeaderText="<span class='link-light'>Inicio</span> " >
                                        <ItemTemplate>

                                        </ItemTemplate>
                                    </asp:TemplateField>

                                     <%-- 7 --%>
                                    <asp:TemplateField HeaderText="<span class='link-light'>Término</span> " >
                                        <ItemTemplate>
                                        
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                     <%-- 8 --%>
                                    <asp:TemplateField HeaderText="<span class='link-light'>Duración</span> " >
                                        <ItemTemplate>
                                             <div class="list-user">
                                               
                                             </div>
                                        </ItemTemplate>
                                    </asp:TemplateField>



                                    <%-- 9 --%>
                                    <asp:TemplateField HeaderText="<span class='link-light'><i class='fa-regular fa-eye'></i></span>" ItemStyle-Width="50">
                                        <ItemTemplate>
                                            <asp:LinkButton CssClass="btn btn-success btn-sm" ID="GrillaPpal_BtnEditEjecucion" OnClick="GrillaPpal_BtnEditEjecucion_Click" runat="server"><i class="fa-regular fa-eye"></i></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                                <PagerTemplate>                                    
                                    <nav style="float:right;">
                                        <div class="btn-group" role="group">
                                            <asp:LinkButton ID="GrillaPpal_BtnFirst"    runat="server" CssClass="btn btn-outline-light btn-sm" Text="Primera Página"      ToolTip="Primera Página"    OnClick="GrillaPpal_BtnFirst_Click"><i class="fa-solid fa-backward-fast"></i></asp:LinkButton>
                                            <asp:LinkButton ID="GrillaPpal_BtnPrev"     runat="server" CssClass="btn btn-outline-light btn-sm" Text="Página Anterior"     ToolTip="Página Anterior"   OnClick="GrillaPpal_BtnPrev_Click"><i class="fa-solid fa-arrow-left"></i></asp:LinkButton>
                                            <asp:LinkButton ID="GrillaPpal_BtnNext"     runat="server" CssClass="btn btn-outline-light btn-sm" Text="Página Siguiente"    ToolTip="Página Siguiente"  OnClick="GrillaPpal_BtnNext_Click"><i class="fa-solid fa-arrow-right"></i></asp:LinkButton>
                                            <asp:LinkButton ID="GrillaPpal_BtnLast"     runat="server" CssClass="btn btn-outline-light btn-sm" Text="Última Página"       ToolTip="Última Página"     OnClick="GrillaPpal_BtnLast_Click"><i class="fa-solid fa-forward-fast"></i></asp:LinkButton>
                                        </div>
                                        <div class="justify-content-center text-white" style="font-size:0.8rem">
                                            <asp:Label ID="GrillaPpal_CurrentPageLabel" runat="server" />                            
                                        </div>
                                    </nav>
                                </PagerTemplate>
                                <EmptyDataTemplate>
                                    <div class="alert alert-warning align-items-center" role="alert">
                                        <h1><i class="fa-solid fa-triangle-exclamation"></i></h1>
                                        <div>No existen registros para mostrar.</div>
                                    </div>
                                </EmptyDataTemplate>                                
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
<!-- FIN MOSTRAR DATOS EN EL GRIDVIEW -->


    <!-- FORMULARIO -->
<asp:Button ID="FrmPpal_BtnVer" runat="server" Style="display:none" />
<asp:ModalPopupExtender ID="FrmPpal" runat="server" TargetControlID="FrmPpal_BtnVer" PopupControlID="FrmPpal_Panel" BackgroundCssClass="TeraModalBackground"></asp:ModalPopupExtender>
<asp:Panel ID="FrmPpal_Panel" runat="server">
    <asp:UpdatePanel ID="FrmPpal_UpdatePanel" runat="server">
        <ContentTemplate>
            <div class="modal" id="modal-FrmPrincipal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div id="FrmPpal_dialog" class="modal-dialog modal-lg" style="display:none;" runat="server">
                    <div class="modal-content">
                        <div class="modal-header bg-dark text-white">
                            <h4 ID="FrmPpalTitulo" class="modal-title fs-5" runat="server"></h4>
                        </div>
                
                        <div class="modal-body">
                            <asp:TextBox ID="FrmPpal_TxtIdEjecucion" style="display:none;" runat="server" />

                            <div class="row ">
                                
                                <div class="col-12">
                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <button 
                                                class="nav-link active" 
                                                runat="server" 
                                                ID="Ffrm_principal_TabOpc1" 
                                                data-bs-toggle="tab" 
                                                data-bs-target="#ContentPlaceHolder1_Crítica" 
                                                type="button" role="tab" 
                                                aria-controls="ContentPlaceHolder1_Crítica" 
                                                aria-selected="true"
                                                data-id="1"
                                                OnClientClick="opcionTab(1)"
                                                >
                                                    Ejecución
                                            </button>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <button 
                                                class="nav-link" 
                                                runat="server" 
                                                data-id="2"
                                                ID="Ffrm_principal_TabOpc2"  
                                                data-bs-toggle="tab" 
                                                data-bs-target="#ContentPlaceHolder1_comentario" 
                                                type="button" role="tab" aria-controls="ContentPlaceHolder1_comentario" 
                                                aria-selected="false"
                                                >
                                                Comentarios
                                            </button>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <button 
                                                class="nav-link" 
                                                data-id="3"
                                                runat="server" 
                                                ID="Ffrm_principal_TabOpc3" 
                                                data-bs-toggle="tab" 
                                                data-bs-target="#ContentPlaceHolder1_Log" 
                                                type="button" role="tab" aria-controls="ContentPlaceHolder1_Log" 
                                                aria-selected="false"
                                                OnClientClick="opcionTab(3)">
                                                    Log
                                            </button>
                                        </li>
                                    </ul>
                                </div>

                                <div class="tab-content" id="myTabContent">
                                    <div class="tab-pane fade show active" runat="server" id="Crítica" role="tabpanel" aria-labelledby="Crítica-tab">
                                        <div class="row">

                                            <div class="col-12 mt-3">
                                                <div class="alert alert-primary" role="alert">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <i class="fa-solid fa-user"></i> Autor: 
                                                        </div>
                                                        <div class="col-6">
                                                            <i class="fa-solid fa-calendar-days"></i> Fecha Creación: 
                                                        </div>

                                                        <div class="col-6">
                                                            <i class="fa-solid fa-tag"></i> Estado: 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-7 ">
                                                <label class="" for="FrmPpal_TxtTitulo" style="color:#000 !important;"><h6 style="margin-bottom:0.7rem !important; font-size:1.2rem !important;">Título</h6></label> 
                                                <asp:TextBox ID="FrmPpal_TxtTitulo" class="form-control" MaxLength="30" autocomplete="off" runat="server" />
                                                <div ID="FrmPpal_TxtTitulo_Msj" class="" runat="server"></div>
                                            </div>

                                            <div class="col-5 ">
                                                <label class="" for="FrmPpal_TxtTitulo" style="color:#000 !important;">
                                                    <h6 style="margin-bottom:0.7rem !important; font-size:1.2rem !important;">Tipo Ejecución</h6>

                                                </label> 
                                                <asp:DropDownList  CssClass="form-select checkTipoEjecucion" placeholder="<<Seleccionar>>"   ID="FrmPpal_CboTipoEjecucion" runat="server" ></asp:DropDownList>
                                            </div>
                                        

                                            <div class="row mt-4" id="row-planificacion">
                                                <div class="col-6 ">
                                                    <label class="" for="FrmPpal_TxtFechaHoraPlanificacion" style="color:#000 !important;"><h6 style="margin-bottom:0.7rem !important; font-size:1.2rem !important;">Fecha y hora de planificación</h6></label> 
                                                    <asp:TextBox ID="FrmPpal_TxtFechaHoraPlanificacion" TextMode="DateTimeLocal"  class="form-control " MaxLength="30" autocomplete="off" runat="server" />
                                                    <div ID="FrmPpal_TxtFechaHoraPlanificacion_Msj" class="" runat="server"></div>
                                                </div>
                                                <div class="col-6"></div>
                                            </div>
                                            <div class="row mt-4">
                                        <div class="col-6 ">
                                            <label class="" for="FrmPpal_ChkEnvioCorreo" style="color:#000 !important;"><h6 style="margin-bottom:0.7rem !important; font-size:1.2rem !important;">Distribuir Resultado por Mail</h6></label> 
                                            <div class="form-check form-switch">
                                                <input runat="server" class="form-check-input FrmPpal_RdEstado"  type="checkbox" role="switch" id="FrmPpal_RdEstado" >
                                                <label class="form-check-label" for="flexSwitchCheckChecked" id="cambiaTitle"> No </label>
                                            </div>

                                            <div class="row mt-4" id="panel-lista-distribucion" style="display:none">
                                                <div class="col-12">
                                                    <label class="" for="FrmPpal_ChkEnvioCorreo" style="color:#000 !important;">
                                                        <h6 style="margin-bottom:0.7rem !important; font-size:1.2rem !important;">
                                                            Lista de Distribución
                                                        </h6>
                                                    </label> 
                                                    <div class="panel-wrap">
                                                        <%--<asp:Label runat="server" ID="FrmPpal_LabelCarpetas"></asp:Label>--%>
                                                        <asp:CheckBoxList ID="FrmPpal_chkListaDistribucion" CssClass="checkbox LstBox  " RepeatDirection="Vertical" RepeatColumns="1" runat="server"></asp:CheckBoxList>
                                                    </div>
                                    
                                        
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-6 ">
                                            <label class="" for="FrmPpal_TxtFechaHoraPlanificacion" style="color:#000 !important;">
                                                <h6 style="margin-bottom:0.7rem !important; font-size:1.2rem !important;">Críticas a Ejecutar</h6>
                                            </label> 
                                            <div class="panel-wrap">
                                                <asp:CheckBoxList ID="FrmPpal_cboCarpetas" CssClass="checkbox LstBox  " RepeatDirection="Vertical" RepeatColumns="1" runat="server"></asp:CheckBoxList>
                                            </div>
                                   
                                        </div>
                                
                                    </div>

                                    
                                            <div class="row">
                                                <div class="col-12">
                                                </div>
                                            </div>
                                            <div id="FrmPpal_DivUltAct" style="font-size:0.8rem !important; color:#26BD75 !important; text-align:right !important" runat="server"></div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" runat="server" id="comentario" role="tabpanel" aria-labelledby="comentario-tab">
                                        <div class="row mt-4">
                                            <div class="col-12 ">
                                                <div class="float-end" style="margin-right:30px">
                                                    <button class="btn btn-success btn-sm" type="button" onclick="setNuevoComentario()">
                                                        <i class="fa-solid fa-comment"></i>
                                                        add comentario
                                                    </button>
                                                </div>
                                                <h4>Comentarios</h4>
                                                <div class="row mt-4" >
                                                    <div class="col-11 add-new-comentario" style="display:none">
                                                        <h4>Ingrese comentario</h4>
                                                        <asp:TextBox runat="server"  TextMode="MultiLine" ID="FrmlPpal_traspasaComentario" class="form-control"  style="width:100%; height:143px;"></asp:TextBox>
                                                        <asp:LinkButton runat="server" class="btn btn-primary btn-sm mt-2" type="button"  >
                                                            <i class="fa-solid fa-comment"></i>
                                                            Almacenar comentario
                                                        </asp:LinkButton>

                                                        <br />
                                                        <hr />
                                                    </div>
                                                    <%=CadenaComentarios %>
                                                </div>
                           
                                            </div>
                                        </div>
                                    </div>

                                    <div class="tab-pane fade" runat="server" id="Log" role="tabpanel" aria-labelledby="Log-tab">
                                        <div class="row">
                                            <div class="col-12">
                                                loremip
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                            <div class="modal-footer bg-light"  style="border-top:2px solid #535362 !important;">
                                <asp:LinkButton ID="FrmPpal_BtnCerrar"    runat="server" CssClass="btn btn-secondary" ToolTip="Cerrar" OnClick="FrmPpal_BtnCerrar_Click"><i class="fa-regular fa-share-from-square"></i>&nbsp;&nbsp;&nbsp;Cerrar</asp:LinkButton>
                                &nbsp;&nbsp;&nbsp;
                                <asp:LinkButton ID="FrmPpal_BtnGuardar"    runat="server" CssClass="btn btn-success row-botonera" ToolTip="Actualizar" OnClick="FrmPpal_BtnGuardar_Click"></asp:LinkButton>
                            </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<!-- FIN FORMULARIO -->


<!-- FORMULARIO ELIMINAR-->
<asp:Button ID="FrmDelete_BtnVer" runat="server" Style="display:none" />
<asp:ModalPopupExtender ID="FrmDelete" runat="server" TargetControlID="FrmDelete_BtnVer" PopupControlID="FrmDelete_Panel" BackgroundCssClass="TeraModalBackground"></asp:ModalPopupExtender>
<asp:Panel ID="FrmDelete_Panel" runat="server">
    <asp:UpdatePanel ID="FrmDelete_UpdatePanel" runat="server">
        <ContentTemplate>
            <div class="modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div id="FrmDelete_dialog" class="modal-dialog modal-dialog-centered" style="display:none;" runat="server">
                    <div class="modal-content">
                        <div class="modal-header bg-danger text-white">
                            <h4 ID="FrmDeleteTitulo" class="modal-title fs-5" runat="server"></h4>
                        </div>                
                        <div class="modal-body">
                            <asp:TextBox ID="FrmDelete_TxtId" style="display:none;" runat="server" />
                            <asp:Label ID="FrmDelete_Msg" runat="server"></asp:Label>

                            <br /><br />
                            <p class="text-center"><b>¿DESEA CONTINUAR?</b></p>

                        </div> <%--modal-body--%>
                
                        <div class="modal-footer bg-light" style="border-top:1px solid #535362 !important;">
                            <asp:LinkButton ID="FrmDelete_BtnCerrar"    runat="server" CssClass="btn btn-secondary btn-sm" ToolTip="Cerrar"        OnClick="FrmDelete_BtnCerrar_Click"><i class="fa-regular fa-share-from-square"></i>&nbsp;&nbsp;&nbsp;Cerrar</asp:LinkButton>
                            &nbsp;&nbsp;&nbsp;
                            <asp:LinkButton ID="FrmDelete_BtnEliminar"    runat="server" CssClass="btn btn-danger btn-sm"   ToolTip="Actualizar"    OnClick="FrmDelete_BtnEliminar_Click"><i class='fa-solid fa-pen-to-square'></i>&nbsp;&nbsp;&nbsp;Elimnar</asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<!-- FIN FORMULARIO -->

<!-- MENSAJES -->
<asp:Button ID="FrmMsg_BtnVer" runat="server" Text="" style="display:none;" />
<asp:ModalPopupExtender ID="FrmMsg" runat="server" TargetControlID="FrmMsg_BtnVer" PopupControlID="FrmMsg_UpdatePanel" BackgroundCssClass="TeraModalBackground"></asp:ModalPopupExtender>
<asp:Panel ID="FrmMsg_Panel" runat="server">
    <asp:UpdatePanel ID="FrmMsg_UpdatePanel" runat="server">
        <ContentTemplate>
            <div class="modal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                <div id="FrmMsg_dialog" class="modal-dialog modal-lg modal-dialog-centered" style="display:none;" runat="server">
                    <div class="modal-content">
                        <div id="FrmMsg_TipoMensaje" class="" style="color:#FFF !important" runat="server">                            
                            <h4 class="modal-title fs-5" style="color:#FFF !important;">
                                    <i ID="FrmMsg_TipoMensajeIcono" class="" runat="server"></i>&nbsp;
                                    <asp:Label ID="FrmMsg_LblTitulo" runat="server" Text=""></asp:Label>
                            </h4>
                        </div>

                        <div class="modal-body">
                            <asp:Label ID="FrmMsg_LblCuerpo" runat="server" Text=""></asp:Label>
                        </div> <%--modal-body--%>

                        <div class="modal-footer bg-light" style="border-top:1px solid #535362 !important;">
                            <asp:Button ID="FrmMsg_BtnAceptar" Width="50px" runat="server" Text="OK" CssClass="" OnClick="FrmMsg_BtnAceptar_Click" data-bs-dismiss="modal" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<!-- FIN MENSAJES -->




<script>
    function init() {
        $(document).ready(function () {
            var select2 = $('.select2').select2({
                placeholder: $(this).data('placeholder'),
                dropdownParent: $("#modal-FrmRutaAprobacion")

            });

            $(".nav-link").click(function () {
                opcionTab($(this).attr('data-id'))
            })

            $(".FrmPpal_RdEstado").change(function () {
                compareSwitch()
            })

            $(".checkTipoEjecucion").change(function () {
                checkTipoEjecucion()
            })
        })
    }

    function setNuevoComentario() {
        $(".add-new-comentario").fadeIn()
    }
    function compareSwitch() {

        if ($(".FrmPpal_RdEstado").is(":checked")) {
            $("#cambiaTitle").html("Si")
            $("#panel-lista-distribucion").fadeIn()
        } else {
            $("#cambiaTitle").html("No")
            $("#panel-lista-distribucion").hide();
        }
    }

    function opcionTab(opc) {
        if (opc == 1) {
            $(".row-botonera").fadeIn()
        } else {
            $(".row-botonera").hide()
        }
    }

    function checkTipoEjecucion() {
        console.log('OPCION...',$(".checkTipoEjecucion option:selected").val())
        if ($(".checkTipoEjecucion option:selected").val()==2) {
            
            $("#row-planificacion").fadeIn()
        } else {
           
            $("#row-planificacion").hide();
        }
    }
    

    init()
    compareSwitch()
</script>


</asp:Content>
