﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class FrmEjecucion

    '''<summary>
    '''Control PnProgress.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents PnProgress As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control UpProgress.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents UpProgress As Global.System.Web.UI.UpdateProgress

    '''<summary>
    '''Control ModalProgress.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ModalProgress As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''Control UpdatePanelIndex.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents UpdatePanelIndex As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control PanelIndex.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents PanelIndex As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control BtnAddRegistro.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents BtnAddRegistro As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmPpal_TxtIdCarpeta.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_TxtIdCarpeta As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control BtnFilter_row.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents BtnFilter_row As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control TxtBsc.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents TxtBsc As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control BtnBsc.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents BtnBsc As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control GrillaPpal.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GrillaPpal As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control FrmPpal_BtnVer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_BtnVer As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control FrmPpal.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''Control FrmPpal_Panel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_Panel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control FrmPpal_UpdatePanel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_UpdatePanel As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control FrmPpal_dialog.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_dialog As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpal_AddComentario.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_AddComentario As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmPpal_AddAdjunto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_AddAdjunto As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmPpalTitulo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpalTitulo As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpal_TxtIdEjecucion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_TxtIdEjecucion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmPpal_TxtIdTemp.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_TxtIdTemp As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control Ffrm_principal_TabOpc1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Ffrm_principal_TabOpc1 As Global.System.Web.UI.HtmlControls.HtmlButton

    '''<summary>
    '''Control Ffrm_principal_TabOpc2.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Ffrm_principal_TabOpc2 As Global.System.Web.UI.HtmlControls.HtmlButton

    '''<summary>
    '''Control Ffrm_principal_TabOpc4.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Ffrm_principal_TabOpc4 As Global.System.Web.UI.HtmlControls.HtmlButton

    '''<summary>
    '''Control Ffrm_principal_TabOpc3.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Ffrm_principal_TabOpc3 As Global.System.Web.UI.HtmlControls.HtmlButton

    '''<summary>
    '''Control CriticaPanelTab.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents CriticaPanelTab As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpal_TxtTituloEjecucion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_TxtTituloEjecucion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmPpal_TxtTituloEjecucion_Msj.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_TxtTituloEjecucion_Msj As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpal_CboTipoEjecucion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_CboTipoEjecucion As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control FrmPpal_CellDistribuirFechaHora.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_CellDistribuirFechaHora As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''Control FrmPpal_txtFechaHoraPlanificacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_txtFechaHoraPlanificacion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control GridViewEjecucionHijos.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GridViewEjecucionHijos As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control FrmPpal_CellDistribuirResultado.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_CellDistribuirResultado As Global.System.Web.UI.HtmlControls.HtmlTableRow

    '''<summary>
    '''Control FrmPpal_RdDistribucionResultadoMail.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_RdDistribucionResultadoMail As Global.System.Web.UI.HtmlControls.HtmlInputCheckBox

    '''<summary>
    '''Control FrmPpal_LstListaDistribucion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_LstListaDistribucion As Global.System.Web.UI.WebControls.CheckBoxList

    '''<summary>
    '''Control FrmPpal_ChkCarpetaCriticas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_ChkCarpetaCriticas As Global.System.Web.UI.WebControls.CheckBoxList

    '''<summary>
    '''Control FrmPpal_DivUltAct.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_DivUltAct As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control adjuntoPanelTab.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents adjuntoPanelTab As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control GrillaAdjunto_RegCount.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GrillaAdjunto_RegCount As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control GrillaAdjunto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GrillaAdjunto As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control comentarioPanelTab.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents comentarioPanelTab As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control LogPanelTab.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents LogPanelTab As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpal_BtnRepetirEjecucion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_BtnRepetirEjecucion As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmPpal_BtnCerrar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_BtnCerrar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmPpal_BtnAlmacenar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_BtnAlmacenar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmAdjunto_BtnVer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmAdjunto_BtnVer As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control FrmAdjunto.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmAdjunto As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''Control FrmAdjunto_Panel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmAdjunto_Panel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control FrmAdjunto_UpdatePanel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmAdjunto_UpdatePanel As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control FrmAdjunto_dialog.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmAdjunto_dialog As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmAdjunto_TxtIdTemp.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmAdjunto_TxtIdTemp As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmAdjunto_TxtIdEjecucion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmAdjunto_TxtIdEjecucion As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmAdjunto_TxtComentario.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmAdjunto_TxtComentario As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmAdjunto_Upload.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmAdjunto_Upload As Global.System.Web.UI.WebControls.FileUpload

    '''<summary>
    '''Control FrmAdjunto_Upload_Msj.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmAdjunto_Upload_Msj As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmAdjunto_BtnCerrar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmAdjunto_BtnCerrar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmAdjunto_BtnGuardar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmAdjunto_BtnGuardar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmComentarios_BtnVer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmComentarios_BtnVer As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control FrmComentarios.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmComentarios As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''Control FrmComentarios_Panel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmComentarios_Panel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control FrmComentarios_UpdatePanel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmComentarios_UpdatePanel As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control FrmComentarios_dialog.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmComentarios_dialog As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmlPpal_traspasaComentario.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmlPpal_traspasaComentario As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmComentarios_BtnCerrar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmComentarios_BtnCerrar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmComentarios_BtnComentar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmComentarios_BtnComentar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmCalendarioEjecucion_BtnVer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmCalendarioEjecucion_BtnVer As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control FrmCalendarioEjecucion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmCalendarioEjecucion As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''Control FrmCalendarioEjecucion_Panel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmCalendarioEjecucion_Panel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control FrmCalendarioEjecucion_UpdatePanel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmCalendarioEjecucion_UpdatePanel As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control FrmCalendarioEjecucion_dialog.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmCalendarioEjecucion_dialog As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpal_TxtUnaVezFecha.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_TxtUnaVezFecha As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmPpal_TxtDiariamenteFecha.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_TxtDiariamenteFecha As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmPpal_TxtDiariamenteRepite.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_TxtDiariamenteRepite As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmPpal_TxtSemanalmenteFecha.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_TxtSemanalmenteFecha As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmPpal_TxtSemanalmenteRepite.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_TxtSemanalmenteRepite As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmPpal_ChkSemanalmenteDiasSemana.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_ChkSemanalmenteDiasSemana As Global.System.Web.UI.WebControls.CheckBoxList

    '''<summary>
    '''Control FrmPpal_TxtMensualmenteFecha.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_TxtMensualmenteFecha As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmPpal_ChkMesesMensualmente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_ChkMesesMensualmente As Global.System.Web.UI.WebControls.CheckBoxList

    '''<summary>
    '''Control FrmPpal_ChkDiaNumeroMensualmente.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_ChkDiaNumeroMensualmente As Global.System.Web.UI.WebControls.CheckBoxList

    '''<summary>
    '''Control LinkButton3.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents LinkButton3 As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmCalendarioEjecucion_BtnCerrar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmCalendarioEjecucion_BtnCerrar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmDelete_BtnVer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete_BtnVer As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control FrmDelete.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''Control FrmDelete_Panel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete_Panel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control FrmDelete_UpdatePanel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete_UpdatePanel As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control FrmDelete_dialog.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete_dialog As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmDelete_Titulo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete_Titulo As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmDelete_TxtOpt.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete_TxtOpt As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmDelete_TxtId.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete_TxtId As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmDelete_Msg.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete_Msg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control FrmDelete_BtnCerrar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete_BtnCerrar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmDelete_BtnEliminar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete_BtnEliminar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmMsg_BtnVer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_BtnVer As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control FrmMsg.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''Control FrmMsg_Panel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_Panel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control FrmMsg_UpdatePanel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_UpdatePanel As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control FrmMsg_dialog.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_dialog As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmMsg_TipoMensaje.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_TipoMensaje As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmMsg_TipoMensajeIcono.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_TipoMensajeIcono As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmMsg_LblTitulo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_LblTitulo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control FrmMsg_LblCuerpo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_LblCuerpo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control FrmMsg_BtnAceptar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_BtnAceptar As Global.System.Web.UI.WebControls.Button
End Class
