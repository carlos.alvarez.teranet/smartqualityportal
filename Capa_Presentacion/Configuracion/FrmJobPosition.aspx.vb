﻿Imports Capa_Entidad
Imports Capa_Negocio

Public Class FrmJobPosition
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            'Dim URL As String = Page.Request.UrlReferrer.AbsolutePath
        Catch ex As Exception
            Response.Redirect("../PageError.aspx")
        End Try

        If Not Page.IsPostBack Then
            Dim var_krypton As String = OwnDecryption(Page.Request("krypton"))
            ViewState("MyIdUser") = OwnPageRequest(var_krypton, "IdUser")

            GrillaPpal_Llenar()
            GrillaPpal.Attributes("JobPosition") = SortDirection.Descending
        End If
    End Sub

#Region "Index"
    Protected Sub BtnBsc_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
    End Sub
    Protected Sub BtnAddRegistro_Click(sender As Object, e As EventArgs)
        FrmPpal_Limpiar()

        FrmPpalTitulo.InnerHtml = "<i class='far fa-edit'></i>&nbsp;&nbsp;Agregar <span Class='badge text-bg-warning'>JOB POSITION</span>"
        FrmPpal_BtnGuardar.Text = "<i class='fa-regular fa-floppy-disk'></i>&nbsp;&nbsp;&nbsp;Guardar"
        FrmPpal_CboLineManeger_Llenar()

        FrmPpal_TxtJobPosition.Focus()

        ModalShow(FrmPpal_dialog, FrmPpal)
        execJS("init()")
    End Sub


    Protected Sub FrmPpal_CboLineManeger_Llenar()

        Dim N_ObjLeeUser As New N_ClsLeeUser

        FrmPpal_CboLineManeger.Items.Clear()
        FrmPpal_CboLineManeger.DataTextField = "TextField"
        FrmPpal_CboLineManeger.DataValueField = "ValueField"
        FrmPpal_CboLineManeger.DataSource = N_ObjLeeUser.LoadCbo()
        FrmPpal_CboLineManeger.DataBind()

        'FrmPpal_CboLineManeger.Items.Insert(0, New ListItem("<< Seleccionar >>", "0"))
        'FrmPpal_CboLineManeger.Items.Item(0).Selected = True


    End Sub
#End Region

#Region "Grilla Principal"
    Protected Sub GrillaPpal_Llenar()
        Try
            Dim N_ObjJobPosition As New N_ClsTblJobPosition
            GrillaPpal.DataSource = SortingPageIndex(N_ObjJobPosition.SelectAll(TxtBsc.Text.Trim()), ViewState("GrillaPpal_ColumnSorting"), ViewState("GrillaPpal_TypeSorting"))
            GrillaPpal.DataBind()

            GrillaPpal.Columns(1).ItemStyle.HorizontalAlign = HorizontalAlign.Center
            GrillaPpal.Columns(2).ItemStyle.HorizontalAlign = HorizontalAlign.Left
            GrillaPpal.Columns(3).ItemStyle.HorizontalAlign = HorizontalAlign.Left

            TxtBsc.Focus()
        Catch ex As Exception
            GrillaPpal.DataSource = Nothing
            GrillaPpal.DataBind()
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub GrillaPpal_BtnEditar_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
        Dim IdJobPosition As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblIdJobPosition"), Label)

        GetRegistro(IdJobPosition.Text)

        FrmPpalTitulo.InnerHtml = "<i class='far fa-edit'></i>&nbsp;&nbsp;Modificar <span Class='badge text-bg-warning'>JOB POSITION</span>"
        FrmPpal_BtnGuardar.Text = "<i class='fa-regular fa-floppy-disk'></i>&nbsp;&nbsp;&nbsp;Actualizar"
        FrmPpal_TxtJobPosition.Focus()

        ModalShow(FrmPpal_dialog, FrmPpal)
    End Sub
    Protected Sub GrillaPpal_BtnEliminar_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
        Dim IdJobPosition As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblIdJobPosition"), Label)
        Dim JobPosition As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblJobPosition"), Label)

        FrmDeleteTitulo.InnerHtml = "<i class='fa-solid fa-pen-to-square'></i>&nbsp;&nbsp;Eliminar Job Position"

        FrmDelete_TxtId.Text = IdJobPosition.Text
        FrmDelete_Msg.Text = "Se eliminará el job position <b>" & JobPosition.Text & "</b>."

        ModalShow(FrmDelete_dialog, FrmDelete)
    End Sub
    Protected Sub GrillaPpal_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Pager Then
            Dim PiePagina As Label = e.Row.FindControl("GrillaPpal_CurrentPageLabel")
            Dim TotalPaginas As Integer = Convert.ToString(GrillaPpal.PageCount).Trim()
            PiePagina.Text = "Página " & Convert.ToString(GrillaPpal.PageIndex + 1).Trim() & " de " & TotalPaginas.ToString()
        End If
    End Sub
    Protected Sub GrillaPpal_BtnFirst_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        GrillaPpal.PageIndex = 0
        GrillaPpal.DataBind()
    End Sub
    Protected Sub GrillaPpal_BtnPrev_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        If (GrillaPpal.PageIndex <> 0) Then
            GrillaPpal.PageIndex = GrillaPpal.PageIndex - 1
            GrillaPpal.DataBind()
        End If
    End Sub
    Protected Sub GrillaPpal_BtnNext_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        If (GrillaPpal.PageIndex <> GrillaPpal.PageCount - 1) Then
            GrillaPpal.PageIndex = GrillaPpal.PageIndex + 1
            GrillaPpal.DataBind()
        End If
    End Sub
    Protected Sub GrillaPpal_BtnLast_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        GrillaPpal.PageIndex = GrillaPpal.PageCount - 1
        GrillaPpal.DataBind()
    End Sub
    Private Sub GrillaPpal_PreRender(sender As Object, e As EventArgs) Handles GrillaPpal.PreRender
        'Esto es para que la grilla muestre el paginador aunque tenga menos de 10 registros
        If GrillaPpal.BottomPagerRow IsNot Nothing Then
            If GrillaPpal.BottomPagerRow.Visible = False Then
                GrillaPpal.BottomPagerRow.Visible = True
            End If
        End If
    End Sub
    Private Sub GrillaPpal_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles GrillaPpal.Sorting
        ViewState("GrillaPpal_ColumnSorting") = e.SortExpression

        If GrillaPpal.Attributes(e.SortExpression) = SortDirection.Ascending Then
            ViewState("GrillaPpal_TypeSorting") = "DESC"
            GrillaPpal_ChangeHeader()
        Else
            ViewState("GrillaPpal_TypeSorting") = "ASC"
            GrillaPpal_ChangeHeader()
        End If

        GrillaPpal_Llenar()
    End Sub
    Private Sub GrillaPpal_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GrillaPpal.RowCommand
        If Trim(e.CommandArgument) = "JobPosition" Then
            ViewState("GrillaPpal_ColumnIndex") = 2
        End If
    End Sub
    Private Sub GrillaPpal_ChangeHeader()
        Dim Base As String = "<span class='link-light'>XXXTITULOXXX <i class='fa-solid fa-ellipsis-vertical'></i></span>"
        Dim Type As String

        If ViewState("GrillaPpal_TypeSorting") = "DESC" Then
            Type = "<i class='fa-solid fa-arrow-down-z-a'></i>"
        Else
            Type = "<i class='fa-solid fa-arrow-down-a-z'></i>"
        End If

        Dim GrillaPpal_Indice = New Integer() {2}
        Dim GrillaPpal_Campo() As String = {"JobPosition"}
        Dim GrillaPpal_Titulo() As String = {"Job Position"}

        For I = 0 To GrillaPpal_Indice.Length - 1
            If ViewState("GrillaPpal_ColumnIndex") = GrillaPpal_Indice(I) Then
                GrillaPpal.Columns(GrillaPpal_Indice(I)).HeaderText = "<span class='link-light'>" & GrillaPpal_Titulo(I) & " " & Type & "</span>"
                If ViewState("GrillaPpal_TypeSorting") = "DESC" Then
                    GrillaPpal.Attributes(GrillaPpal_Campo(I)) = SortDirection.Descending
                Else
                    GrillaPpal.Attributes(GrillaPpal_Campo(I)) = SortDirection.Ascending
                End If
            Else
                GrillaPpal.Columns(GrillaPpal_Indice(I)).HeaderText = Replace(Base, "XXXTITULOXXX", GrillaPpal_Titulo(I))
                GrillaPpal.Attributes(GrillaPpal_Campo(I)) = SortDirection.Descending
            End If
        Next
    End Sub
#End Region

#Region "Formulario Principal"
    Protected Sub GetRegistro(ByVal IdJobPosition As Integer)
        Try
            FrmPpal_Limpiar()
            FrmPpal_CboLineManeger_Llenar()
            Dim E_ObjJobPosition As New E_ClsTblJobPosition
            Dim N_ObjJobPosition As New N_ClsTblJobPosition
            E_ObjJobPosition.IdJobPosition = IdJobPosition
            E_ObjJobPosition = N_ObjJobPosition.SelectRecord(E_ObjJobPosition)

            FrmPpal_TxtIdJobPosition.Text = E_ObjJobPosition.IdJobPosition
            FrmPpal_TxtJobPosition.Text = E_ObjJobPosition.JobPosition.Trim()
            FrmPpal_CboLineManeger.SelectedValue = E_ObjJobPosition.LineManeger



            FrmPpal_DivUltAct.InnerHtml = E_ObjJobPosition.UltAct


            execJS("init()")

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub FrmPpal_Limpiar()
        FrmPpal_DivUltAct.InnerHtml = ""
        FrmPpal_TxtIdJobPosition.Text = ""

        FrmPpal_TxtJobPosition.Text = ""
        GetValidador(FrmPpal_TxtJobPosition, "form-control specialform", FrmPpal_TxtJobPosition_Msj, "", "LIMPIAR")

    End Sub
    Function FrmPpal_ValidarCampos() As Boolean
        Dim SW As Boolean = True

        If FrmPpal_TxtJobPosition.Text.Trim() = "" Then
            GetValidador(FrmPpal_TxtJobPosition, "form-control specialform", FrmPpal_TxtJobPosition_Msj, "* Obligatorio", "ERROR")
            SW = False
        Else
            GetValidador(FrmPpal_TxtJobPosition, "form-control specialform", FrmPpal_TxtJobPosition_Msj, "", "OK")
        End If

        Return SW
    End Function
    Protected Sub FrmPpal_BtnGuardar_Click(sender As Object, e As EventArgs)
        Try
            If Not FrmPpal_ValidarCampos() Then
                Exit Sub
            End If

            Dim ResultadoTransaccion As String
            Dim E_ObjJobPosition As New E_ClsTblJobPosition
            Dim N_ObjJobPosition As New N_ClsTblJobPosition

            E_ObjJobPosition.JobPosition = FrmPpal_TxtJobPosition.Text.Trim()
            E_ObjJobPosition.LineManeger = FrmPpal_CboLineManeger.SelectedValue
            E_ObjJobPosition.LastTransactionUser = ViewState("MyIdUser")
            E_ObjJobPosition.Estado = True

            If FrmPpal_BtnGuardar.Text = "<i class='fa-regular fa-floppy-disk'></i>&nbsp;&nbsp;&nbsp;Guardar" Then
                ResultadoTransaccion = N_ObjJobPosition.Insert(E_ObjJobPosition)
                If ResultadoTransaccion <> "OK" Then
                    GetValidador(FrmPpal_TxtJobPosition, "form-control specialform", FrmPpal_TxtJobPosition_Msj, "", "LIMPIAR")
                    MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
                    ModalShow(FrmMsg_dialog, FrmMsg)
                    Exit Sub
                End If
            Else
                E_ObjJobPosition.IdJobPosition = FrmPpal_TxtIdJobPosition.Text.Trim()
                ResultadoTransaccion = N_ObjJobPosition.Update(E_ObjJobPosition)
                If ResultadoTransaccion <> "OK" Then
                    GetValidador(FrmPpal_TxtJobPosition, "form-control specialform", FrmPpal_TxtJobPosition_Msj, "", "LIMPIAR")
                    MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
                    ModalShow(FrmMsg_dialog, FrmMsg)
                    Exit Sub
                End If
            End If

            GrillaPpal_Llenar()
            ModalHide(FrmPpal_dialog, FrmPpal)

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub FrmPpal_BtnCerrar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmPpal_dialog, FrmPpal)
    End Sub
#End Region


#Region "FormularioAprobadores"
    'Cierra Modal Aprobadores
    Protected Sub FrmPpalAprobadores_BtnCerrar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmAprobadores_dialog, FrmAprobadores)
    End Sub

    'Abre modal Aprobadores
    Protected Sub GrillaPpal_BtnAddAprobadores_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
        Dim IdJobPosition As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblIdJobPosition"), Label)
        FrmAprobadores_LblIdJobPosition.Text = IdJobPosition.Text

        getLoadAprobadores()


        ModalShow(FrmAprobadores_dialog, FrmAprobadores)
        execJS("init()")
    End Sub


    Protected Sub getLoadAprobadores()
        Dim N_ObjJobPositionAprobador As New N_ClsTblJobPositionAprobador

        FrmAprobadores_CboUserGestionSearch.Items.Clear()
        FrmAprobadores_CboUserGestionSearch.DataTextField = "TextField"
        FrmAprobadores_CboUserGestionSearch.DataValueField = "ValueField"
        Dim reaultLoadCbo = N_ObjJobPositionAprobador.getAprobadoresDisponibles(FrmAprobadores_LblIdJobPosition.Text)
        FrmAprobadores_CboUserGestionSearch.DataSource = reaultLoadCbo
        FrmAprobadores_CboUserGestionSearch.DataBind()

        If FrmAprobadores_CboUserGestionSearch.Items.Count = 0 Then
            FrmAprobadores_BtnGrabar.Visible = False
            FrmAprobadores_CboUserGestionSearch.Visible = False
        Else
            FrmAprobadores_BtnGrabar.Visible = True
            FrmAprobadores_CboUserGestionSearch.Visible = True
        End If

        Dim DtaTable As DataTable = N_ObjJobPositionAprobador.SelectAll(FrmAprobadores_LblIdJobPosition.Text)
        FrmAprobadores_GrillaUsuarios.DataSource = DtaTable
        FrmAprobadores_GrillaUsuarios.DataBind()

        If DtaTable.Rows.Count > 0 Then
            Dim row = DtaTable.Rows(0)
            FrmAprobadoresUltAct.InnerHtml = row("UltAct")

        End If
        FrmAprobadoresTitulo.InnerHtml = "<i class='far fa-edit'></i>&nbsp;&nbsp;Agregar Responsable de  <span Class='badge text-bg-warning'>Aprobación</span>"

    End Sub

    Protected Sub FrmAprobadores_GrillaUsuarios_BtnFirst_Click(sender As Object, e As EventArgs)
        getLoadAprobadores()
        FrmAprobadores_GrillaUsuarios.PageIndex = 0
        FrmAprobadores_GrillaUsuarios.DataBind()
    End Sub
    Protected Sub FrmAprobadores_GrillaUsuarios_BtnPrev_Click(sender As Object, e As EventArgs)
        getLoadAprobadores()
        If (FrmAprobadores_GrillaUsuarios.PageIndex <> 0) Then
            FrmAprobadores_GrillaUsuarios.PageIndex = FrmAprobadores_GrillaUsuarios.PageIndex - 1
            FrmAprobadores_GrillaUsuarios.DataBind()
        End If
    End Sub
    Protected Sub FrmAprobadores_GrillaUsuarios_BtnNext_Click(sender As Object, e As EventArgs)
        getLoadAprobadores()
        If (FrmAprobadores_GrillaUsuarios.PageIndex <> FrmAprobadores_GrillaUsuarios.PageCount - 1) Then
            FrmAprobadores_GrillaUsuarios.PageIndex = FrmAprobadores_GrillaUsuarios.PageIndex + 1
            FrmAprobadores_GrillaUsuarios.DataBind()
        End If
    End Sub
    Protected Sub FrmAprobadores_GrillaUsuarios_BtnLast_Click(sender As Object, e As EventArgs)
        getLoadAprobadores()
        FrmAprobadores_GrillaUsuarios.PageIndex = FrmAprobadores_GrillaUsuarios.PageCount - 1
        FrmAprobadores_GrillaUsuarios.DataBind()
    End Sub
    Private Sub FrmAprobadores_GrillaUsuarios_PreRender(sender As Object, e As EventArgs) Handles FrmAprobadores_GrillaUsuarios.PreRender
        'Esto es para que la grilla muestre el paginador aunque tenga menos de 10 registros
        If FrmAprobadores_GrillaUsuarios.BottomPagerRow IsNot Nothing Then
            If FrmAprobadores_GrillaUsuarios.BottomPagerRow.Visible = False Then
                FrmAprobadores_GrillaUsuarios.BottomPagerRow.Visible = True
            End If
        End If
    End Sub


    Protected Sub FrmAprobadores_GrillaUsuarios_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Pager Then
            Dim PiePaginaAprobador As Label = e.Row.FindControl("FrmAprobadores_GrillaUsuarios_CurrentPageLabel")
            Dim TotalPaginas As Integer = Convert.ToString(FrmAprobadores_GrillaUsuarios.PageCount).Trim()
            PiePaginaAprobador.Text = "Página " & Convert.ToString(FrmAprobadores_GrillaUsuarios.PageIndex + 1).Trim() & " de " & TotalPaginas.ToString()

        End If
    End Sub

    Protected Sub FrmAprobadores_btnEliminaUsuario_click(sender As Object, e As EventArgs)
        Try
            Dim btn As LinkButton = sender
            Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
            Dim IdUsuario As Label = CType(FrmAprobadores_GrillaUsuarios.Rows(row.RowIndex).FindControl("LblIdUsuario"), Label)

            Dim N_ObjJobPositionAprobador As New N_ClsTblJobPositionAprobador
            Dim E_ObjJobPositionAprobador As New E_ClsTblJobPositionAprobador

            E_ObjJobPositionAprobador.IdAprobador = IdUsuario.Text
            E_ObjJobPositionAprobador.IdJobPosition = FrmAprobadores_LblIdJobPosition.Text
            E_ObjJobPositionAprobador.LastTransactionUser = ViewState("MyIdUser")

            Dim ResultadoTransaccion As String = N_ObjJobPositionAprobador.Delete(E_ObjJobPositionAprobador)
            If Mid(ResultadoTransaccion, 1, 2) = "OK" Then
                getLoadAprobadores()
                GrillaPpal_Llenar()
            Else
                MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
                ModalShow(FrmMsg_dialog, FrmMsg)
            End If
            execJS("init()")
        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.Message)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub



    Protected Sub FrmAprobadores_BtnGrabar_click(sender As Object, e As EventArgs)
        Try

            Dim E_ObjJobPositionAprobador As New E_ClsTblJobPositionAprobador
            Dim N_ObjJobPositionAprobador As New N_ClsTblJobPositionAprobador

            E_ObjJobPositionAprobador.IdJobPosition = FrmAprobadores_LblIdJobPosition.Text
            E_ObjJobPositionAprobador.IdAprobador = FrmAprobadores_CboUserGestionSearch.SelectedValue
            E_ObjJobPositionAprobador.LastTransactionUser = ViewState("MyIdUser")


            Dim ResultadoTransaccion As String = N_ObjJobPositionAprobador.Insert(E_ObjJobPositionAprobador)
            getLoadAprobadores()
            GrillaPpal_Llenar()

        Catch ex As Exception

        End Try
    End Sub

#End Region


#Region "FrmDelete"
    Protected Sub FrmDelete_BtnCerrar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmDelete_dialog, FrmDelete)
    End Sub
    Protected Sub FrmDelete_BtnEliminar_Click(sender As Object, e As EventArgs)
        Try
            ModalHide(FrmDelete_dialog, FrmDelete)

            Dim E_ObjJobPosition As New E_ClsTblJobPosition
            Dim N_ObjJobPosition As New N_ClsTblJobPosition
            Dim ResultadoTransaccion As String

            E_ObjJobPosition.IdJobPosition = FrmDelete_TxtId.Text.Trim()
            E_ObjJobPosition.LastTransactionUser = ViewState("MyIdUser")

            ResultadoTransaccion = N_ObjJobPosition.Delete(E_ObjJobPosition)
            If ResultadoTransaccion <> "OK" Then
                MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
                ModalShow(FrmMsg_dialog, FrmMsg)
                Exit Sub
            End If

            GrillaPpal_Llenar()

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
#End Region

#Region "FrmMsg"
    Protected Sub FrmMsg_BtnAceptar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmMsg_dialog, FrmMsg)
    End Sub

    Private Sub execJS(ByVal script As String)
        Try
            Dim TxtJavascript As String = "<script type='text/javascript'>" + script + "</script>"
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "alerta", TxtJavascript, False)
        Catch ex As Exception

        End Try
    End Sub
#End Region

End Class