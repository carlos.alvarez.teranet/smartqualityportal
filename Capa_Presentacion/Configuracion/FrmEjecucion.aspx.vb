﻿Imports Capa_Entidad
Imports Capa_Negocio
Imports System.Globalization
Imports System.IO
Imports System.Data


Public Class FrmEjecucion
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Try
        '    'Dim URL As String = Page.Request.UrlReferrer.AbsolutePath
        'Catch ex As Exception
        '    Response.Redirect("../PageError.aspx")
        'End Try


        If Not Page.IsPostBack Then
            'Dim var_krypton As String = OwnDecryption(Page.Request("krypton"))
            'ViewState("MyIdUser") = OwnPageRequest(var_krypton, "IdUser")
            ViewState("MyIdUser") = "carlos.astorga@kantar.com"
            ViewState("Logs") = ""
            ViewState("Comentarios") = ""
            'FrmPpal_Limpiar()
            GrillaPpal_Llenar()


        End If
    End Sub

    Protected Sub getDetailTipoEjecucion_change(sender As Object, e As EventArgs)
        Try
            If FrmPpal_CboTipoEjecucion.SelectedValue = "2" Then
                'FrmPpal_CellDistribuirResultado.Visible = True
                FrmPpal_CellDistribuirFechaHora.Visible = True
                FrmPpal_BtnRepetirEjecucion.Visible = True
                'ViewState("ListaDistribucion") = ""

            Else
                'FrmPpal_CellDistribuirResultado.Visible = False
                FrmPpal_CellDistribuirFechaHora.Visible = False
                FrmPpal_BtnRepetirEjecucion.Visible = False

                'ViewState("ListaDistribucion") = "none"


            End If

            execJS("init()")


        Catch ex As Exception

        End Try
    End Sub


#Region "Grilla Principal"

    Protected Sub GrillaPpal_Llenar()
        Try
            Dim N_ObjEjecucion As New N_ClsTblEjecucion
            'GrillaPpal.DataSource = N_ObjCritica.SelectAllPenditeAprobacion(ViewState("MyIdUser"))
            GrillaPpal.DataSource = SortingPageIndex(N_ObjEjecucion.SelectAll(TxtBsc.Text.Trim()), ViewState("GrillaPpal_ColumnSorting"), ViewState("GrillaPpal_TypeSorting"))

            GrillaPpal.DataBind()

            GrillaPpal.Columns(1).ItemStyle.HorizontalAlign = HorizontalAlign.Center
            GrillaPpal.Columns(2).ItemStyle.HorizontalAlign = HorizontalAlign.Left
            GrillaPpal.Columns(3).ItemStyle.HorizontalAlign = HorizontalAlign.Left

            TxtBsc.Focus()
        Catch ex As Exception
            GrillaPpal.DataSource = Nothing
            GrillaPpal.DataBind()
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub GrillaPpal_BtnEditar_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
        Dim IdEjecucion As Label = CType(GrillaPpal.Rows(row.RowIndex).FindControl("LblIdEjecucion"), Label)

        FrmPpal_TxtIdEjecucion.Text = IdEjecucion.Text

        FrmPpalTitulo.InnerHtml = "&nbsp; &nbsp; &nbsp; <i class='far fa-edit'></i>&nbsp;&nbsp;Modificar <span Class='badge text-bg-warning'>Ejecución</span>"
        FrmPpal_BtnAlmacenar.Text = "<i Class='fa-regular fa-save'></i>&nbsp;&nbsp;&nbsp;Actualizar"
        FrmPpal_Limpiar()

        loadFormEjecucion()


        ModalShow(FrmPpal_dialog, FrmPpal)
    End Sub

    Protected Sub GrillaPpal_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Pager Then
            Dim PiePagina As Label = e.Row.FindControl("GrillaPpal_CurrentPageLabel")
            Dim TotalPaginas As Integer = Convert.ToString(GrillaPpal.PageCount).Trim()
            PiePagina.Text = "Página " & Convert.ToString(GrillaPpal.PageIndex + 1).Trim() & " de " & TotalPaginas.ToString()
        End If
    End Sub
    Protected Sub GrillaPpal_BtnFirst_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        GrillaPpal.PageIndex = 0
        GrillaPpal.DataBind()
    End Sub
    Protected Sub GrillaPpal_BtnPrev_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        If (GrillaPpal.PageIndex <> 0) Then
            GrillaPpal.PageIndex = GrillaPpal.PageIndex - 1
            GrillaPpal.DataBind()
        End If
    End Sub
    Protected Sub GrillaPpal_BtnNext_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        If (GrillaPpal.PageIndex <> GrillaPpal.PageCount - 1) Then
            GrillaPpal.PageIndex = GrillaPpal.PageIndex + 1
            GrillaPpal.DataBind()
        End If
    End Sub
    Protected Sub GrillaPpal_BtnLast_Click(sender As Object, e As EventArgs)
        GrillaPpal_Llenar()
        GrillaPpal.PageIndex = GrillaPpal.PageCount - 1
        GrillaPpal.DataBind()
    End Sub
    Private Sub GrillaPpal_PreRender(sender As Object, e As EventArgs) Handles GrillaPpal.PreRender
        'Esto es para que la grilla muestre el paginador aunque tenga menos de 10 registros
        If GrillaPpal.BottomPagerRow IsNot Nothing Then
            If GrillaPpal.BottomPagerRow.Visible = False Then
                GrillaPpal.BottomPagerRow.Visible = True
            End If
        End If
    End Sub
    Private Sub GrillaPpal_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles GrillaPpal.Sorting
        ViewState("GrillaPpal_ColumnSorting") = e.SortExpression

        If GrillaPpal.Attributes(e.SortExpression) = SortDirection.Ascending Then
            ViewState("GrillaPpal_TypeSorting") = "DESC"
            GrillaPpal_ChangeHeader()
        Else
            ViewState("GrillaPpal_TypeSorting") = "ASC"
            GrillaPpal_ChangeHeader()
        End If

        GrillaPpal_Llenar()
    End Sub
    Private Sub GrillaPpal_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles GrillaPpal.RowCommand
        If Trim(e.CommandArgument) = "Codigo" Then
            ViewState("GrillaPpal_ColumnIndex") = 2
        End If
    End Sub
    Private Sub GrillaPpal_ChangeHeader()
        Dim Base As String = "<span class='link-light'>XXXCodigoXXX <i class='fa-solid fa-ellipsis-vertical'></i></span>"
        Dim Type As String

        If ViewState("GrillaPpal_TypeSorting") = "DESC" Then
            Type = "<i class='fa-solid fa-arrow-down-z-a'></i>"
        Else
            Type = "<i class='fa-solid fa-arrow-down-a-z'></i>"
        End If

        Dim GrillaPpal_Indice = New Integer() {2}
        Dim GrillaPpal_Campo() As String = {"Codigo"}
        Dim GrillaPpal_Titulo() As String = {"Job Position"}

        For I = 0 To GrillaPpal_Indice.Length - 1
            If ViewState("GrillaPpal_ColumnIndex") = GrillaPpal_Indice(I) Then
                GrillaPpal.Columns(GrillaPpal_Indice(I)).HeaderText = "<span class='link-light'>" & GrillaPpal_Titulo(I) & " " & Type & "</span>"
                If ViewState("GrillaPpal_TypeSorting") = "DESC" Then
                    GrillaPpal.Attributes(GrillaPpal_Campo(I)) = SortDirection.Descending
                Else
                    GrillaPpal.Attributes(GrillaPpal_Campo(I)) = SortDirection.Ascending
                End If
            Else
                GrillaPpal.Columns(GrillaPpal_Indice(I)).HeaderText = Replace(Base, "XXXCodigoXXX", GrillaPpal_Titulo(I))
                GrillaPpal.Attributes(GrillaPpal_Campo(I)) = SortDirection.Descending
            End If
        Next
    End Sub
    Protected Sub BtnBsc_Click(sender As Object, e As EventArgs)

        GrillaPpal_Llenar()
    End Sub
#End Region

#Region "Index"

    Protected Sub BtnAddRegistro_Click(sender As Object, e As EventArgs)
        Try
            FrmPpal_Limpiar()

            FrmPpal_TxtIdTemp.Text = ViewState("MyIdUser") & CStr(Now).Replace(" ", "").Replace("-", "").Replace(":", "")


            FrmPpal_TxtIdEjecucion.Text = ""
            FrmPpalTitulo.InnerHtml = "Crear <span Class='badge text-bg-warning'>Ejecución</span>"
            FrmPpal_BtnAlmacenar.Text = "<i Class='fa-regular fa-save'></i>&nbsp;&nbsp;&nbsp;Crear"
            loadFormEjecucion()


        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub

#End Region



#Region "Formulario Principal"

    Public Shared Property TabActiveForm As Integer = 1
    Private Sub TabSelected()

        Ffrm_principal_TabOpc1.Attributes.Add("class", "nav-link")
        CriticaPanelTab.Attributes.Add("class", "tab-pane fade ")

        Ffrm_principal_TabOpc2.Attributes.Add("class", "nav-link")
        comentarioPanelTab.Attributes.Add("class", "tab-pane fade ")

        Ffrm_principal_TabOpc3.Attributes.Add("class", "nav-link")
        LogPanelTab.Attributes.Add("class", "tab-pane fade ")

        Ffrm_principal_TabOpc4.Attributes.Add("class", "nav-link")
        adjuntoPanelTab.Attributes.Add("class", "tab-pane fade ")

        Select Case True

            Case TabActiveForm = "1"
                Ffrm_principal_TabOpc1.Attributes.Add("class", "nav-link active")
                CriticaPanelTab.Attributes.Add("class", "tab-pane fade show active")

            Case TabActiveForm = "2"
                Ffrm_principal_TabOpc2.Attributes.Add("class", "nav-link active")
                comentarioPanelTab.Attributes.Add("class", "tab-pane fade show active")

            Case TabActiveForm = "3"
                Ffrm_principal_TabOpc3.Attributes.Add("class", "nav-link active")
                LogPanelTab.Attributes.Add("class", "tab-pane fade show active")

            Case TabActiveForm = "4"
                Ffrm_principal_TabOpc4.Attributes.Add("class", "nav-link active")
                adjuntoPanelTab.Attributes.Add("class", "tab-pane fade show active")
        End Select


    End Sub


    Private Sub loadFormEjecucion()
        Try
            FrmPpal_Limpiar()

            TabActiveForm = "1"
            TabSelected()
            Dim N_ObjListaDistribucion As New N_ClsTblListaDistribucion
            FrmPpal_LstListaDistribucion.Items.Clear()
            FrmPpal_LstListaDistribucion.DataTextField = "TextField"
            FrmPpal_LstListaDistribucion.DataValueField = "ValueField"
            Dim dt As DataTable = N_ObjListaDistribucion.LoadCbo()
            FrmPpal_LstListaDistribucion.DataSource = dt
            FrmPpal_LstListaDistribucion.DataBind()

            Dim N_ObjCarpeta As New N_ClsTblCarpeta
            FrmPpal_ChkCarpetaCriticas.Items.Clear()
            FrmPpal_ChkCarpetaCriticas.DataTextField = "TextField"
            FrmPpal_ChkCarpetaCriticas.DataValueField = "ValueField"
            Dim dtCarpeta As DataTable = N_ObjCarpeta.LoadCarpetasCriticas()
            FrmPpal_ChkCarpetaCriticas.DataSource = dtCarpeta
            FrmPpal_ChkCarpetaCriticas.DataBind()



            'FrmPpal_LblFechasSeleccionadas.InnerHtml = ""

            If FrmPpal_TxtIdEjecucion.Text <> "" Then
                Dim E_ObjTblEjecucion As New E_ClsTblEjecucion
                Dim N_ObjTblEjecucion As New N_ClsTblEjecucion
                E_ObjTblEjecucion.IdEjecucion = FrmPpal_TxtIdEjecucion.Text

                E_ObjTblEjecucion = N_ObjTblEjecucion.SelectRecord(E_ObjTblEjecucion)
                FrmPpal_DivUltAct.InnerHtml = E_ObjTblEjecucion.UltAct


                FrmPpal_TxtTituloEjecucion.Text = E_ObjTblEjecucion.Titulo
                FrmPpal_CboTipoEjecucion.SelectedValue = E_ObjTblEjecucion.TipoEjecucion
                FrmPpal_txtFechaHoraPlanificacion.Text = DateTime.Parse(E_ObjTblEjecucion.FechaHoraProgramada).Date.ToString("yyyy-MM-dd HH:mm")

                If FrmPpal_CboTipoEjecucion.SelectedValue = "2" Then
                    FrmPpal_CellDistribuirFechaHora.Visible = True
                    FrmPpal_BtnRepetirEjecucion.Visible = True
                Else
                    FrmPpal_CellDistribuirFechaHora.Visible = False
                    FrmPpal_BtnRepetirEjecucion.Visible = False
                End If

                Dim N_ObjNubEjecucionListaDistribucion As New N_ClsTblNubEjecucionListaDistribucion
                Dim DtaTableListaDistribucion As DataTable = N_ObjNubEjecucionListaDistribucion.SelectAll(CInt(FrmPpal_TxtIdEjecucion.Text))

                For Each row As DataRow In DtaTableListaDistribucion.Rows
                    FrmPpal_LstListaDistribucion.Items.FindByValue(row("IdListaDistribucion")).Selected = True
                    ViewState("ListaDistribucion") = ""
                    FrmPpal_RdDistribucionResultadoMail.Attributes("checked") = True
                Next row

                Dim N_ObjNubEjecucionCarpeta As New N_ClsTblNubEjecucionCarpeta
                Dim DtaTableCarpetas As DataTable = N_ObjNubEjecucionCarpeta.SelectAll(CInt(FrmPpal_TxtIdEjecucion.Text))
                For Each row As DataRow In DtaTableCarpetas.Rows
                    FrmPpal_ChkCarpetaCriticas.Items.FindByValue(row("IdCarpeta")).Selected = True
                Next row


                getHijosEjecucion()
            End If


            ModalShow(FrmPpal_dialog, FrmPpal)
            execJS("init()")

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub

    Private Sub getHijosEjecucion()

        Dim E_ObjTblEjecucion As New E_ClsTblEjecucion
        Dim N_ObjTblEjecucion As New N_ClsTblEjecucion
        Dim DtaTable As DataTable = N_ObjTblEjecucion.SelectAllChild(CInt(FrmPpal_TxtIdEjecucion.Text))
        Dim arrFechas(DtaTable.Rows.Count) As String
        Dim arrIden(DtaTable.Rows.Count) As Integer

        Dim i As Integer = 0
        For Each row As DataRow In DtaTable.Rows
            arrFechas(i) = CStr(row("FechaHoraProgramada"))
            arrIden(i) = row("IdEjecucion")
            i += 1

        Next row
        arrStringDiasSeleccionados(arrFechas, arrIden, True)


    End Sub

    Protected Sub FrmPpal_Limpiar()
        FrmPpal_TxtTituloEjecucion.Text = ""
        ViewState("Comentarios") = ""
        ViewState("ListaDistribucion") = "none"
        FrmPpal_DivUltAct.InnerHtml=""
        GrillaAdjunto.DataSource = Nothing


        ViewState("tableDiasSeleccionados") = Nothing
        GridViewEjecucionHijos.DataSource = ViewState("tableDiasSeleccionados")
        GridViewEjecucionHijos.DataBind()

        FrmPpal_RdDistribucionResultadoMail.Attributes.Remove("checked")
        FrmPpal_RdDistribucionResultadoMail.Attributes("checked") = False
        FrmPpal_RdDistribucionResultadoMail.Checked = False

        FrmPpal_CboTipoEjecucion.SelectedValue = "0"


        FrmPpal_CellDistribuirFechaHora.Visible = False
        FrmPpal_BtnRepetirEjecucion.Visible = False

        For i = 0 To FrmPpal_LstListaDistribucion.Items.Count - 1
            FrmPpal_LstListaDistribucion.Items(i).Selected = False
        Next

        For i = 0 To FrmPpal_ChkCarpetaCriticas.Items.Count - 1
            FrmPpal_ChkCarpetaCriticas.Items(i).Selected = False
        Next

    End Sub
    Function FrmPpal_ValidarCampos() As Boolean
        Dim SW As Boolean = True

        'If FrmPpal_TxtTitulo.Text.Trim() = "" Then
        '    GetValidador(FrmPpal_TxtTitulo, "form-control specialform", FrmPpal_TxtTitulo_Msj, "* Obligatorio", "ERROR")
        '    SW = False
        'Else
        '    GetValidador(FrmPpal_TxtTitulo, "form-control specialform", FrmPpal_TxtTitulo_Msj, "", "OK")
        'End If

        Return SW
    End Function


    Protected Sub FrmPpal_BtnAlmacenar_Click(sender As Object, e As EventArgs)
        Try


            Dim E_ObjTblEjecucion As New E_ClsTblEjecucion
            Dim N_ObjTblEjecucion As New N_ClsTblEjecucion
            Dim ResultadoTransaccion As String

            E_ObjTblEjecucion.Titulo = FrmPpal_TxtTituloEjecucion.Text
            '
            E_ObjTblEjecucion.IdEjecucionPadre = 0
            E_ObjTblEjecucion.Owner = ViewState("MyIdUser")
            E_ObjTblEjecucion.FechaCreacion = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss")
            E_ObjTblEjecucion.IdEstado = 7
            E_ObjTblEjecucion.TipoEjecucion = FrmPpal_CboTipoEjecucion.SelectedValue

            If E_ObjTblEjecucion.TipoEjecucion = "2" Then
                Dim FechaHoraProgramada As String = Convert.ToDateTime(FrmPpal_txtFechaHoraPlanificacion.Text).ToString("yyyy-MM-dd hh:mm:ss")
                E_ObjTblEjecucion.FechaHoraProgramada = FechaHoraProgramada
            Else
                E_ObjTblEjecucion.FechaHoraProgramada = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss")
            End If

            E_ObjTblEjecucion.LastTransactionUser = ViewState("MyIdUser")

            Dim logStr As String = ""
            Dim logIdAccion As Integer = 0
            If FrmPpal_TxtIdEjecucion.Text <> "" Then
                E_ObjTblEjecucion.IdEjecucion = FrmPpal_TxtIdEjecucion.Text
                ResultadoTransaccion = N_ObjTblEjecucion.Update(E_ObjTblEjecucion)

                logIdAccion = 9
                logStr = "Modificación de ejecución"
            Else
                ResultadoTransaccion = N_ObjTblEjecucion.Insert(E_ObjTblEjecucion)

                logStr = "Creacion de ejecución"
                logIdAccion = 8
            End If




            If Mid(ResultadoTransaccion, 1, 2) = "OK" Then
                FrmPpal_TxtIdEjecucion.Text = Replace(ResultadoTransaccion, "OK", "")
                setLogWrite(logStr, logIdAccion)



                If ViewState("opcRecursivo") = 1 Then
                    Dim diasSelected As DataTable = ViewState("tableDiasSeleccionados")
                    Dim E_ObjTblEjecucionExtra As New E_ClsTblEjecucion
                    For Each fila As DataRow In diasSelected.Rows


                        E_ObjTblEjecucionExtra.IdEjecucionPadre = FrmPpal_TxtIdEjecucion.Text
                        E_ObjTblEjecucionExtra.FechaHoraProgramada = fila("FechaHoraProgramada")
                        E_ObjTblEjecucionExtra.Titulo = FrmPpal_TxtTituloEjecucion.Text
                        E_ObjTblEjecucionExtra.Owner = ViewState("MyIdUser")
                        E_ObjTblEjecucionExtra.FechaCreacion = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss")
                        E_ObjTblEjecucionExtra.IdEstado = 7
                        E_ObjTblEjecucionExtra.TipoEjecucion = FrmPpal_CboTipoEjecucion.SelectedValue
                        E_ObjTblEjecucionExtra.LastTransactionUser = ViewState("MyIdUser")
                        N_ObjTblEjecucion.Insert(E_ObjTblEjecucionExtra)
                    Next
                End If




                'If E_ObjTblEjecucion.TipoEjecucion = 2 Then
                Dim E_ObjNubEjecucionListaDistribucion As New E_ClsTblNubEjecucionListaDistribucion
                Dim N_ObjNubEjecucionListaDistribucion As New N_ClsTblNubEjecucionListaDistribucion


                For i = 0 To FrmPpal_LstListaDistribucion.Items.Count - 1
                    If FrmPpal_LstListaDistribucion.Items(i).Selected = True Then
                        E_ObjNubEjecucionListaDistribucion.IdEjecucion = FrmPpal_TxtIdEjecucion.Text
                        E_ObjNubEjecucionListaDistribucion.IdListaDistribucion = FrmPpal_LstListaDistribucion.Items(i).Value
                        E_ObjNubEjecucionListaDistribucion.LastTransactionUser = ViewState("MyIdUser")
                        N_ObjNubEjecucionListaDistribucion.Insert(E_ObjNubEjecucionListaDistribucion)
                    End If
                Next
                'End If


                Dim E_ObjNubEjecucionCarpeta As New E_ClsTblNubEjecucionCarpeta
                Dim N_ObjNubEjecucionCarpeta As New N_ClsTblNubEjecucionCarpeta


                For i = 0 To FrmPpal_ChkCarpetaCriticas.Items.Count - 1
                    If FrmPpal_ChkCarpetaCriticas.Items(i).Selected = True Then
                        E_ObjNubEjecucionCarpeta.IdEjecucion = FrmPpal_TxtIdEjecucion.Text
                        E_ObjNubEjecucionCarpeta.IdCarpeta = FrmPpal_ChkCarpetaCriticas.Items(i).Value
                        E_ObjNubEjecucionCarpeta.LastTransactionUser = ViewState("MyIdUser")
                        N_ObjNubEjecucionCarpeta.Insert(E_ObjNubEjecucionCarpeta)
                    End If
                Next

                If E_ObjTblEjecucion.TipoEjecucion = "1" Then

                    N_ObjTblEjecucion.EjecutaCritica(FrmPpal_TxtIdEjecucion.Text)

                End If
                GrillaPpal_Llenar()
                ModalHide(FrmPpal_dialog, FrmPpal)
            End If
        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try

    End Sub

    Protected Sub FrmPpal_BtnCerrar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmPpal_dialog, FrmPpal)
    End Sub
#End Region

#Region "FrmCalendarioEjecucion"
    Private Sub getComentariosCalendarioEjecucion()
        Try
            Dim E_ObjTblComentario As New E_ClsTblComentario
            Dim N_ObjTblComentario As New N_ClsTblComentario


            E_ObjTblComentario.IdCritica = FrmPpal_TxtIdEjecucion.Text

            Dim DtaTable As DataTable = N_ObjTblComentario.SelectAll(E_ObjTblComentario)
            ViewState("Comentarios") = ""
            For Each row As DataRow In DtaTable.Rows

                ViewState("Comentarios") += "<div Class='col-11 mt-4'>"
                ViewState("Comentarios") += "  <span Class='text-success'> " + CStr(row("Fecha")) + "</span>- " + CStr(row("Comentario"))
                ViewState("Comentarios") += "   <div Class='text-end'><i> " + CStr(row("LastTransactionUser")) + " - " + CStr(row("Nombre")) + "</i><hr></div></div>"

            Next row
            TabActiveForm = 2

        Catch ex As Exception

        End Try
    End Sub


    Private Sub configuraDiasSemana()
        Try
            ViewState("arrDiasSeleccionados") = Nothing
            ViewState("tablaDias") = Nothing
            ViewState("tableDiasSeleccionados") = Nothing
            ViewState("opcRecursivo") = 0
            'FrmPpal_LblFechasSeleccionadas.InnerHtml = ""

            Dim tablaDias As New DataTable
            tablaDias.Columns.Add("ValueField", GetType(Integer))
            tablaDias.Columns.Add("TextField", GetType(String))

            FrmPpal_TxtUnaVezFecha.Text = ""
            FrmPpal_TxtDiariamenteFecha.Text = ""
            FrmPpal_TxtDiariamenteRepite.Text = ""
            FrmPpal_TxtSemanalmenteFecha.Text = ""
            FrmPpal_TxtSemanalmenteRepite.Text = ""
            FrmPpal_TxtMensualmenteFecha.Text = ""



            tablaDias.Rows.Add(1, "Lunes")
            tablaDias.Rows.Add(2, "Martes")
            tablaDias.Rows.Add(3, "Miercoles")

            tablaDias.Rows.Add(4, "Jueves")
            tablaDias.Rows.Add(5, "Viernes")
            tablaDias.Rows.Add(6, "Sabado")
            tablaDias.Rows.Add(7, "Domingo")
            ViewState("tablaDias") = tablaDias

            ViewState("tablaMeses") = Nothing
            Dim tablaMeses As New DataTable
            tablaMeses.Columns.Add("ValueField", GetType(Integer))
            tablaMeses.Columns.Add("TextField", GetType(String))

            tablaMeses.Rows.Add(1, "Enero")
            tablaMeses.Rows.Add(2, "Febrero")
            tablaMeses.Rows.Add(3, "Marzo")
            tablaMeses.Rows.Add(4, "Abril")
            tablaMeses.Rows.Add(5, "Mayo")
            tablaMeses.Rows.Add(6, "Junio")
            tablaMeses.Rows.Add(7, "Julio")
            tablaMeses.Rows.Add(8, "Agosto")
            tablaMeses.Rows.Add(9, "Septiembre")
            tablaMeses.Rows.Add(10, "Octubre")
            tablaMeses.Rows.Add(11, "Noviembre")
            tablaMeses.Rows.Add(12, "Diciembre")
            ViewState("tablaMeses") = tablaMeses


            ViewState("tablaDiasNumero") = Nothing
            Dim tablaDiasNumero As New DataTable
            tablaDiasNumero.Columns.Add("ValueField", GetType(Integer))
            tablaDiasNumero.Columns.Add("TextField", GetType(String))

            Dim i As Integer = 1

            While i <= 30
                tablaDiasNumero.Rows.Add(i, CStr(i))
                i += 1
            End While


            ViewState("tablaDiasNumero") = tablaDiasNumero
            execJS("init()")

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub

    Protected Sub FrmPpal_BtnUnaVez_click(sender As Object, e As EventArgs)
        Try
            FrmPpal_txtFechaHoraPlanificacion.Text = FrmPpal_TxtUnaVezFecha.Text
            ModalHide(FrmCalendarioEjecucion_dialog, FrmCalendarioEjecucion)
        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try

    End Sub

    Protected Sub FrmCalendarioEjecucion_BtnDiariamente_click(sender As Object, e As EventArgs)
        Try

            Dim fechaSelecciona As DateTime = Convert.ToDateTime(FrmPpal_TxtDiariamenteFecha.Text)
            Dim cantDia As Integer = CInt(FrmPpal_TxtDiariamenteRepite.Text)
            Dim arrFechas(cantDia) As String
            Dim arrIden(cantDia) As Integer

            For i As Integer = 0 To cantDia
                arrFechas(i) = CStr(fechaSelecciona.AddDays(i))
                arrIden(i) = i
            Next

            arrStringDiasSeleccionados(arrFechas, arrIden, False)

            ModalHide(FrmCalendarioEjecucion_dialog, FrmCalendarioEjecucion)
        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try

    End Sub

    Private Sub arrStringDiasSeleccionados(arrFechas() As String, arrKeyInteger() As Integer, isBD As Boolean)
        Try


            ViewState("tableDiasSeleccionados") = Nothing
            Dim tableDiasSeleccionados As New DataTable

            tableDiasSeleccionados.Columns.Add("IdEjecucion", GetType(Integer))
            tableDiasSeleccionados.Columns.Add("FechaHoraProgramada", GetType(String))
            ViewState("opcRecursivo") = 0
            'Dim cadenaHtml As String = ""
            ViewState("tableDiasSeleccionados") = Nothing

            If arrFechas.Length > 0 Then
                'cadenaHtml = "<ul>"
                Dim keyIden As Integer = 0
                For Each item As String In arrFechas
                    If item <> Nothing Then


                        '            Dim buttonEliminar As String = "<asp:LinkButton class='btn btn-sm btn-danger' OnClick='" + metodoEliminar + "' CommandArgument='" + CStr(arrKeyInteger(keyIden - 1)) + "' CommandName'hisBtnClick' runat='server' style='margin-left:20px'><i class='fa-solid fa-trash'></i></asp:LinkButton>"

                        '            cadenaHtml += "<li style='margin-top:5px;'>" + item + " " + buttonEliminar + " </li>"
                        tableDiasSeleccionados.Rows.Add(CStr(arrKeyInteger(keyIden)), item)

                        '            keyIden += 1
                        ViewState("opcRecursivo") = 1
                    End If
                Next
                ViewState("tableDiasSeleccionados") = tableDiasSeleccionados
                GridViewEjecucionHijos.DataSource = ViewState("tableDiasSeleccionados")
                GridViewEjecucionHijos.DataBind()
                'cadenaHtml += "</ul>"
            End If

            'FrmPpal_LblFechasSeleccionadas.InnerHtml = cadenaHtml



        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)

        End Try
    End Sub


    Protected Sub FrmPpal_BtnEliminar_Ejecucion_click(sender As Object, e As EventArgs)
        Try
            Dim btn As LinkButton = sender
            Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
            Dim IdEjecucion As Label = CType(GridViewEjecucionHijos.Rows(row.RowIndex).FindControl("LblIdEjecucion"), Label)

            getHijosEjecucion()

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub FrmPpal_BtnSemanalmentePlanifica_click(sender As Object, e As EventArgs)
        Try

            Dim fechaSelecciona As DateTime = Convert.ToDateTime(FrmPpal_TxtSemanalmenteFecha.Text)
            Dim cantSemanas As Integer = CInt(FrmPpal_TxtSemanalmenteRepite.Text)
            Dim cantDias As Integer = cantSemanas * 7
            Dim arrFechas(cantDias) As String
            Dim arrIden(cantDias) As Integer
            Dim arrDiasSelected(6) As Integer

            Dim indexSelected = 0
            For i = 0 To FrmPpal_ChkSemanalmenteDiasSemana.Items.Count - 1
                If FrmPpal_ChkSemanalmenteDiasSemana.Items(i).Selected = True Then
                    arrDiasSelected(indexSelected) = FrmPpal_ChkSemanalmenteDiasSemana.Items(i).Value
                    indexSelected += 1
                    'ReDim arrDiasSelected(arrDiasSelected.Length + 1)
                End If
            Next

            Dim ci As CultureInfo = CultureInfo.InvariantCulture
            Dim indexSelectedDias = 0
            For i As Integer = 0 To cantDias
                Dim diaSeleccionado = fechaSelecciona.AddDays(i)
                Dim ds As DayOfWeek = CInt(ci.Calendar.GetDayOfWeek(diaSeleccionado))

                If ds = 0 Then
                    ds = 7
                End If
                Dim index As Integer = Array.FindIndex(arrDiasSelected, Function(s) s = ds)

                If index <> -1 Then
                    arrFechas(indexSelectedDias) = CStr(diaSeleccionado)
                    arrIden(indexSelectedDias) = indexSelectedDias
                    indexSelectedDias += 1
                End If

            Next

            arrStringDiasSeleccionados(arrFechas, arrIden, False)


            ModalHide(FrmCalendarioEjecucion_dialog, FrmCalendarioEjecucion)
        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try

    End Sub

    Protected Sub FrmPpal_BtnRepetirEjecucion_Click(sender As Object, e As EventArgs)

        configuraDiasSemana()

        FrmPpal_ChkSemanalmenteDiasSemana.Items.Clear()
        FrmPpal_ChkSemanalmenteDiasSemana.DataTextField = "TextField"
        FrmPpal_ChkSemanalmenteDiasSemana.DataValueField = "ValueField"
        FrmPpal_ChkSemanalmenteDiasSemana.DataSource = ViewState("tablaDias")
        FrmPpal_ChkSemanalmenteDiasSemana.DataBind()


        FrmPpal_ChkDiaNumeroMensualmente.Items.Clear()
        FrmPpal_ChkDiaNumeroMensualmente.DataTextField = "TextField"
        FrmPpal_ChkDiaNumeroMensualmente.DataValueField = "ValueField"
        FrmPpal_ChkDiaNumeroMensualmente.DataSource = ViewState("tablaDiasNumero")
        FrmPpal_ChkDiaNumeroMensualmente.DataBind()

        FrmPpal_ChkMesesMensualmente.Items.Clear()
        FrmPpal_ChkMesesMensualmente.DataTextField = "TextField"
        FrmPpal_ChkMesesMensualmente.DataValueField = "ValueField"
        FrmPpal_ChkMesesMensualmente.DataSource = ViewState("tablaMeses")
        FrmPpal_ChkMesesMensualmente.DataBind()


        ModalShow(FrmCalendarioEjecucion_dialog, FrmCalendarioEjecucion)
    End Sub



    Protected Sub FrmCalendarioEjecucion_BtnCerrar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmCalendarioEjecucion_dialog, FrmCalendarioEjecucion)
    End Sub

    Protected Sub FrmCalendarioEjecucion_BtnUnaVez_Click(sender As Object, e As EventArgs)
        Try

            Dim E_ObjTblComentario As New E_ClsTblComentario
            Dim N_ObjTblComentario As New N_ClsTblComentario
            E_ObjTblComentario.Comentario = FrmlPpal_traspasaComentario.Text
            E_ObjTblComentario.LastTransactionUser = ViewState("MyIdUser")
            E_ObjTblComentario.IdCritica = FrmPpal_TxtIdEjecucion.Text

            N_ObjTblComentario.Insert(E_ObjTblComentario)

            FrmlPpal_traspasaComentario.Text = ""


            getComentarios()
            execJS("init()")
            ModalHide(FrmCalendarioEjecucion_dialog, FrmCalendarioEjecucion)

        Catch ex As Exception

        End Try


    End Sub

#End Region
#Region "Comntarios"
    Private Sub getComentarios()
        Try
            Dim E_ObjTblComentarioEjecucion As New E_ClsTblComentarioEjecucion
            Dim N_ObjTblComentarioEjecucion As New N_ClsTblComentarioEjecucion

            Dim DtaTable As DataTable = N_ObjTblComentarioEjecucion.SelectAll(FrmPpal_TxtIdEjecucion.Text)
            ViewState("Comentarios") = ""
            For Each row As DataRow In DtaTable.Rows

                ViewState("Comentarios") += "<div Class='col-11 mt-4'>"
                ViewState("Comentarios") += "  <span Class='text-success'> " + CStr(row("Fecha")) + "</span>- " + CStr(row("Comentario"))
                ViewState("Comentarios") += " <div Class='text-end'><i> " + CStr(row("LastTransactionUser")) + " - " + CStr(row("Nombre")) + "</i><hr></div></div>"

            Next row

        Catch ex As Exception

        End Try
    End Sub

    Protected Sub FrmPpal_Comentario_click(sender As Object, e As EventArgs)
        TabActiveForm = 2
        TabSelected()
        ModalShow(FrmComentarios_dialog, FrmComentarios)
    End Sub


    Protected Sub FrmComentarios_BtnCerrar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmComentarios_dialog, FrmComentarios)
    End Sub

    Protected Sub FrmComentarios_BtnComentar_Click(sender As Object, e As EventArgs)
        Try

            Dim E_ObjTblComentarioEjecucion As New E_ClsTblComentarioEjecucion
            Dim N_ObjTblComentarioEjecucion As New N_ClsTblComentarioEjecucion
            E_ObjTblComentarioEjecucion.Comentario = FrmlPpal_traspasaComentario.Text
            E_ObjTblComentarioEjecucion.LastTransactionUser = ViewState("MyIdUser")
            E_ObjTblComentarioEjecucion.IdEjecucion = FrmPpal_TxtIdEjecucion.Text

            N_ObjTblComentarioEjecucion.Insert(E_ObjTblComentarioEjecucion)

            FrmlPpal_traspasaComentario.Text = ""


            TabActiveForm = 2
            TabSelected()

            getComentarios()
            execJS("init()")
            ModalHide(FrmComentarios_dialog, FrmComentarios)
            ModalShow(FrmPpal_dialog, FrmPpal)
        Catch ex As Exception

        End Try


    End Sub

#End Region

#Region "Log"

    Private Sub setLogWrite(Cadena As String, IdAccion As Integer)
        Dim N_ObjTblLogEjecucion As New N_ClsTblLogEjecucion
        Dim E_ObjTblLogEjecucion As New E_ClsTblLogEjecucion
        Dim ResultadoTransaccion As String

        E_ObjTblLogEjecucion.IdEjecucion = FrmPpal_TxtIdEjecucion.Text
        E_ObjTblLogEjecucion.Comentario = Cadena
        E_ObjTblLogEjecucion.IdAccion = IdAccion
        E_ObjTblLogEjecucion.LastTransactionUser = ViewState("MyIdUser")
        E_ObjTblLogEjecucion.Fecha = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss")
        N_ObjTblLogEjecucion.Insert(E_ObjTblLogEjecucion)


    End Sub
#End Region

#Region "FrmMsg"
    Protected Sub FrmMsg_BtnAceptar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmMsg_dialog, FrmMsg)
    End Sub

    Private Sub execJS(ByVal script As String)
        Try
            Dim TxtJavascript As String = "<script type='text/javascript'>" + script + "</script>"
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "alerta", TxtJavascript, False)
        Catch ex As Exception

        End Try
    End Sub
#End Region





#Region "Grilla Adjunto"
    Protected Sub GrillaAdjunto_Llenar()
        Try
            Dim N_ObjAdjunto As New N_ClsTblAdjunto
            Dim E_ObjAdjunto As New E_ClsTblAdjunto With {
                .IdTemp = FrmPpal_TxtIdTemp.Text.Trim(),
                .IdCritica = If(String.IsNullOrEmpty(FrmPpal_TxtIdEjecucion.Text.Trim()), 0, CInt(FrmPpal_TxtIdEjecucion.Text.Trim()))
            }

            If FrmPpal_TxtIdTemp.Text.Trim() = "" Then
                GrillaAdjunto.DataSource = SortingPageIndex(N_ObjAdjunto.SelectAll(E_ObjAdjunto), ViewState("GrillaAdjunto_ColumnSorting"), ViewState("GrillaAdjunto_TypeSorting")) 'GrillaAdjunto_RegCount
            Else
                GrillaAdjunto.DataSource = SortingPageIndex(N_ObjAdjunto.SelectAll_Temp(E_ObjAdjunto), ViewState("GrillaAdjunto_ColumnSorting"), ViewState("GrillaAdjunto_TypeSorting")) 'GrillaAdjunto_RegCount
            End If
            GrillaAdjunto.DataBind()

            GrillaAdjunto.Columns(1).ItemStyle.HorizontalAlign = HorizontalAlign.Center
            GrillaAdjunto.Columns(2).ItemStyle.HorizontalAlign = HorizontalAlign.Left
            GrillaAdjunto.Columns(3).ItemStyle.HorizontalAlign = HorizontalAlign.Center

        Catch ex As Exception
            GrillaAdjunto.DataSource = Nothing
            GrillaAdjunto.DataBind()
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub

    Protected Sub FrmPpal_AddAdjunto_Click(sender As Object, e As EventArgs)
        FrmAdjunto_Limpiar()

        FrmAdjunto_TxtIdTemp.Text = FrmPpal_TxtIdTemp.Text
        FrmAdjunto_TxtIdEjecucion.Text = FrmPpal_TxtIdEjecucion.Text
        ModalShow(FrmAdjunto_dialog, FrmAdjunto)
        TabActiveForm = "4"
        TabSelected()
    End Sub

    Protected Sub GrillaAdjunto_BtnDescargar_Click(sender As Object, e As EventArgs)
        Try
            Dim btn As LinkButton = sender
            Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
            Dim IdAdjunto As Label = CType(GrillaAdjunto.Rows(row.RowIndex).FindControl("GrillaAdjunto_LblIdAdjunto"), Label)
            Dim AdjName As Label = CType(GrillaAdjunto.Rows(row.RowIndex).FindControl("GrillaAdjunto_LblAdjName"), Label)

            Dim ObjAdjunto As Byte()

            Dim N_ObjAdjunto As New N_ClsTblAdjunto
            Dim E_ObjAdjunto As New E_ClsTblAdjunto With {
                .IdAdjunto = IdAdjunto.Text
            }

            If FrmPpal_TxtIdTemp.Text.Trim() = "" Then
                ObjAdjunto = N_ObjAdjunto.GetAdjunto(E_ObjAdjunto)
            Else
                ObjAdjunto = N_ObjAdjunto.GetAdjuntoTemp(E_ObjAdjunto)
            End If

            System.IO.File.WriteAllBytes(Server.MapPath("").Replace("\Configuracion", "\TempFile") & "/" & AdjName.Text.Trim(), ObjAdjunto)

            'Esto abre en una nueva ventana
            Dim TxtJavascript As String = "<script type='text/javascript'>window.open('../TempFile/" & AdjName.Text.Trim() & "?time" & CStr(Now).Replace(" ", "") & "', '_blank');</script>"
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "", TxtJavascript, False)
            TabActiveForm = 4
            TabSelected()
        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub GrillaAdjunto_BtnEliminar_Click(sender As Object, e As EventArgs)
        Dim btn As LinkButton = sender
        Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
        Dim IdAdjunto As Label = CType(GrillaAdjunto.Rows(row.RowIndex).FindControl("GrillaAdjunto_LblIdAdjunto"), Label)
        Dim AdjName As Label = CType(GrillaAdjunto.Rows(row.RowIndex).FindControl("GrillaAdjunto_LblAdjName"), Label)
        Dim LastTransactionUser As Label = CType(GrillaAdjunto.Rows(row.RowIndex).FindControl("GrillaAdjunto_LblLastTransactionUser"), Label)
        Dim Fecha As Label = CType(GrillaAdjunto.Rows(row.RowIndex).FindControl("GrillaAdjunto_LblFecha"), Label)

        Dim TimeDif As TimeSpan = Now - DateTime.Parse(Fecha.Text)
        If TimeDif.TotalHours > 24 Or LastTransactionUser.Text.Trim.ToUpper() <> ViewState("MyIdUser").ToString.Trim.ToUpper() Then
            MsgModalAlerta(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Alerta", FrmMsg_LblCuerpo, "Los adjuntos solo pueden ser eliminados por su autor y dentro de las 24 horas posteriores a su ingreso.")
            ModalShow(FrmMsg_dialog, FrmMsg)
            Exit Sub
        End If

        FrmDelete_Titulo.InnerHtml = "<i class='fa-solid fa-trash-can'></i>&nbsp;&nbsp;Eliminar Adjunto"

        FrmDelete_TxtOpt.Text = "3"
        FrmDelete_TxtId.Text = IdAdjunto.Text
        FrmDelete_Msg.Text = "Se eliminará el adjunto <b>" & AdjName.Text & "</b>"

        ModalShow(FrmDelete_dialog, FrmDelete)
        TabActiveForm = "4"
        TabSelected()
    End Sub
    Protected Sub GrillaAdjunto_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Pager Then
            Dim PiePagina As Label = e.Row.FindControl("GrillaAdjunto_CurrentPageLabel")
            Dim TotalPaginas As Integer = Convert.ToString(GrillaAdjunto.PageCount).Trim()
            PiePagina.Text = "Página " & Convert.ToString(GrillaAdjunto.PageIndex + 1).Trim() & " de " & TotalPaginas.ToString()
        End If
    End Sub
    Private Sub GrillaAdjunto_PreRender(sender As Object, e As EventArgs) Handles GrillaAdjunto.PreRender
        'Esto es para que la grilla muestre el paginador aunque tenga menos de 10 registros
        If GrillaAdjunto.BottomPagerRow IsNot Nothing Then
            If GrillaAdjunto.BottomPagerRow.Visible = False Then
                GrillaAdjunto.BottomPagerRow.Visible = True
            End If
        End If
    End Sub
#End Region


#Region "Formulario Adjunto"
    Protected Sub FrmAdjunto_Limpiar()
        FrmAdjunto_TxtIdTemp.Text = ""
        FrmAdjunto_TxtIdEjecucion.Text = ""
        FrmAdjunto_TxtComentario.Text = ""

        FrmAdjunto_Upload.Attributes.Clear()
        FrmAdjunto_Upload.FileContent.Dispose()
        GetValidador(FrmAdjunto_Upload, "form-control form-control-sm", FrmAdjunto_Upload_Msj, "", "LIMPIAR")
        TabActiveForm = 4
        TabSelected()
    End Sub
    Function FrmAdjunto_ValidarCampos() As Boolean
        Dim SW As Boolean = True

        If FrmAdjunto_Upload.HasFile Then
            GetValidador(FrmAdjunto_Upload, "form-control form-control-sm", FrmAdjunto_Upload_Msj, "", "OK")
        Else
            GetValidador(FrmAdjunto_Upload, "form-control form-control-sm", FrmAdjunto_Upload_Msj, "", "ERROR")
            SW = False
        End If


        Return SW
    End Function
    Public Function FrmAdjunto_GetSizeFile(ByVal TxtSize As Long) As String
        Dim UM As String
        Dim TxtSizeConv As Double

        If TxtSize < 1024 Then
            UM = "bytes"
            TxtSizeConv = TxtSize
        ElseIf TxtSize < 1048576 Then
            UM = "KB"
            TxtSizeConv = TxtSize / 1024
        Else
            UM = "MB"
            TxtSizeConv = TxtSize / 1048576
        End If

        Return String.Format("{0:n2} {1}", TxtSizeConv, UM)
    End Function
    Protected Sub FrmAdjunto_BtnGuardar_Click() ' sender As Object, e As EventArgs
        Try
            If Not FrmAdjunto_ValidarCampos() Then
                ModalShow(FrmPpal_dialog, FrmPpal)
                ModalShow(FrmAdjunto_dialog, FrmAdjunto)
                Exit Sub
            End If

            Dim ResultadoTransaccion As String
            Dim N_ObjAdjunto As New N_ClsTblAdjunto
            Dim E_ObjAdjunto As New E_ClsTblAdjunto With {
                .IdTemp = FrmAdjunto_TxtIdTemp.Text.Trim(),
                .IdCritica = If(String.IsNullOrEmpty(FrmAdjunto_TxtIdEjecucion.Text.Trim()), 0, CInt(FrmAdjunto_TxtIdEjecucion.Text.Trim())),
                .Comentario = FrmAdjunto_TxtComentario.Text.Trim(),
                .AdjName = FrmAdjunto_Upload.FileName,
                .AdjFile = FrmAdjunto_Upload.FileBytes,
                .AdjSize = FrmAdjunto_GetSizeFile(FrmAdjunto_Upload.PostedFile.ContentLength),
                .AdjExt = Path.GetExtension(FrmAdjunto_Upload.FileName),
                .LastTransactionUser = ViewState("MyIdUser")
            }

            If FrmAdjunto_TxtIdTemp.Text.Trim() = "" Then
                ResultadoTransaccion = N_ObjAdjunto.Insert(E_ObjAdjunto)
            Else
                ResultadoTransaccion = N_ObjAdjunto.Insert_Temp(E_ObjAdjunto)
            End If
            If ResultadoTransaccion <> "OK" Then
                FrmAdjunto_Upload.Attributes.Clear()
                FrmAdjunto_Upload.FileContent.Dispose()
                GetValidador(FrmAdjunto_Upload, "form-control form-control-sm", FrmAdjunto_Upload_Msj, "", "LIMPIAR")
                MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
                ModalShow(FrmMsg_dialog, FrmMsg)
                Exit Sub
            End If

            ModalShow(FrmPpal_dialog, FrmPpal)
            GrillaAdjunto_Llenar()
            TabActiveForm = 4
            TabSelected()

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub FrmAdjunto_BtnCerrar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmAdjunto_dialog, FrmAdjunto)
    End Sub
#End Region

#Region "FrmDelete"
    Protected Sub FrmDelete_BtnCerrar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmDelete_dialog, FrmDelete)
    End Sub
    Protected Sub FrmDelete_BtnEliminar_Click(sender As Object, e As EventArgs)
        Try
            Dim ResultadoTransaccion As String
            ModalHide(FrmDelete_dialog, FrmDelete)

            ''If FrmDelete_TxtOpt.Text = "1" Then
            'Dim N_ObjActivo As New N_ClsTblActivo
            '    Dim E_ObjActivo As New E_ClsTblActivo With {
            '        .IdCritica = FrmDelete_TxtId.Text.Trim(),
            '        .LastTransactionUser = ViewState("MyIdUser")
            '    }

            '    ResultadoTransaccion = N_ObjActivo.Delete(E_ObjActivo)
            '    If ResultadoTransaccion <> "OK" Then
            '        MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
            '        ModalShow(FrmMsg_dialog, FrmMsg)
            '        Exit Sub
            '    End If

            '    GrillaPpal_Llenar()
            'ElseIf FrmDelete_TxtOpt.Text = "2" Then
            '    Dim N_ObjComentario As New N_ClsTblComentario
            '    Dim E_ObjComentario As New E_ClsTblComentario With {
            '        .IdComentario = FrmDelete_TxtId.Text.Trim(),
            '        .LastTransactionUser = ViewState("MyIdUser")
            '    }

            '    If FrmPpal_TxtIdTemp.Text.Trim() = "" Then
            '        ResultadoTransaccion = N_ObjComentario.Delete(E_ObjComentario)
            '    Else
            '        ResultadoTransaccion = N_ObjComentario.Delete_Temp(E_ObjComentario)
            '    End If
            '    If ResultadoTransaccion <> "OK" Then
            '        MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
            '        ModalShow(FrmMsg_dialog, FrmMsg)
            '        Exit Sub
            '    End If

            '    GrillaComentario_Llenar()
            'ElseIf FrmDelete_TxtOpt.Text = "3" Then
            '    Dim N_ObjAdjunto As New N_ClsTblAdjunto
            '    Dim E_ObjAdjunto As New E_ClsTblAdjunto With {
            '        .IdAdjunto = FrmDelete_TxtId.Text.Trim(),
            '        .LastTransactionUser = ViewState("MyIdUser")
            '    }

            '    If FrmPpal_TxtIdTemp.Text.Trim() = "" Then
            '        ResultadoTransaccion = N_ObjAdjunto.Delete(E_ObjAdjunto)
            '    Else
            '        ResultadoTransaccion = N_ObjAdjunto.Delete_Temp(E_ObjAdjunto)
            '    End If
            '    If ResultadoTransaccion <> "OK" Then
            '        MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
            '        ModalShow(FrmMsg_dialog, FrmMsg)
            '        Exit Sub
            '    End If

            '    GrillaAdjunto_Llenar()
            'End If

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
#End Region

End Class