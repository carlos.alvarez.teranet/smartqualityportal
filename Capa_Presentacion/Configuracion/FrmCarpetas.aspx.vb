﻿Imports Capa_Entidad
Imports Capa_Negocio
Imports System.Data

Public Class FrmCarpetas
    Inherits System.Web.UI.Page
#Region "index"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Request.Form("__EVENTTARGET") <> "" Then
            'Si hace click en Breadcrumbs
            If Request.Form("__EVENTTARGET") = "CambioCarpeta" Then
                BreadcrumbsFolder_Llenar(CInt(Request.Form("__EVENTARGUMENT")))
                getCarpeta()
            End If

            'Si hace click en Treeview
            If Request.Form("__EVENTTARGET").Replace("ctl00$ContentPlaceHolder1$", "") = "CarpetasTreeView" Then
                BscIdCarpeta(Request.Form("__EVENTARGUMENT").Replace("s", ""))
                getCarpeta()
            End If
        End If

        If Not Page.IsPostBack Then
            Dim var_krypton As String = OwnDecryption(Page.Request("krypton"))
            ViewState("MyIdUser") = OwnPageRequest(var_krypton, "IdUser")
            'ViewState("MyIdUser") = "carlos.alvarez@teranet.cl"



            FrmPpal_GrillaAutoriza.Attributes("Usuario") = SortDirection.Descending
            FrmPpal_GrillaPrefijo.Attributes("Prefijo") = SortDirection.Descending


            getListDistribucion()
            initCarpetas()


        End If
    End Sub

    Dim LastIdCarpeta As String
    Private Sub initCarpetas()
        Try
            Try
                Dim N_ObjTblCarpeta As New N_ClsTblCarpeta
                Dim Lista As New TreeNodeCollection

                CarpetasTreeView.Nodes.Clear()

                Lista = ConstruirTreevew(N_ObjTblCarpeta.CarpetasTreeViewContenedor)
                For Each row As TreeNode In Lista
                    CarpetasTreeView.Nodes.Add(row)
                Next
                CarpetasTreeView.ExpandAll()

                execJS("init()")



            Catch ex As Exception
                MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
                ModalShow(FrmMsg_dialog, FrmMsg)
            End Try
        Catch ex As Exception

        End Try

    End Sub


    Protected Sub FrmPpal_Habilita()
        FrmPpal_TxtCarpeta.Enabled = True
        FrmPpal_TxtCarpeta.Attributes.Remove("disabled")

        FrmPpal_TxtStoreProcedure.Enabled = True
        FrmPpal_TxtStoreProcedure.Attributes.Remove("disabled")



        FrmPpal_ChkPrivada.Enabled = True
        FrmPpal_ChkPrivada.Attributes.Remove("disabled")

        FrmPpal_ChkEstado.Enabled = True
        FrmPpal_ChkEstado.Attributes.Remove("disabled")

        FrmPpal_ChkContenedorCriticas.Enabled = True
        FrmPpal_ChkContenedorCriticas.Attributes.Remove("disabled")

        FrmPpal_botonera.Visible = True

        ' FrmPpal_BtnGrabar.Attributes("disabled") = "disabled"
        'FrmPpal_BtnGrabar.Enabled = False

        execJS("Init")

    End Sub
    Protected Sub FrmPpal_BtnAddSubCarpeta_click(sender As Object, e As EventArgs)
        Try

            FrmPpal_TxtSwTipo.Text = "S" ' SUBCARPETA
            Dim tmpFrmPpal_TxtIdCarpetaPadre = FrmPpal_TxtIdCarpeta.Text
            Dim tmpFrmPpal_TxtNivel = FrmPpal_TxtNivel.Text
            LastIdCarpeta = FrmPpal_TxtIdCarpeta.Text
            FrmPpal_Limpiar()
            FrmPpal_TxtIdCarpetaPadre.Text = tmpFrmPpal_TxtIdCarpetaPadre
            FrmPpal_TxtNivel.Text = tmpFrmPpal_TxtNivel

            FrmPpalTitulo.InnerHtml = "<i class='far fa-edit'></i>&nbsp;&nbsp;Agregar <span Class='badge text-bg-warning'>Sub carpeta</span>"


            pathBrath.Visible = True
            execJS("eliminaHtml()")


            FrmPpal_BtnGrabar.Text = "<i class='fas fa-save'></i> Crear SubCarpeta"
            FrmPpal_BtnAddSubCarpeta.Visible = False

            ModalShow(FrmPpal_dialog, FrmPpal)
            FrmPpal.Show()
            execJS("init()")

        Catch ex As Exception

        End Try
    End Sub

    Function FrmCarpetaRaiz_ValidarCampos() As Boolean
        Dim SW As Boolean = True
        If FrmCarpetaRaiz_TxtCarpeta.Text.Trim() = "" Then
            GetValidador(FrmCarpetaRaiz_TxtCarpeta, "form-control specialform", FrmCarpetaRaiz_TxtCarpeta_Msj, "* Obligatorio", "ERROR")
            SW = False
        Else
            GetValidador(FrmCarpetaRaiz_TxtCarpeta, "form-control specialform", FrmCarpetaRaiz_TxtCarpeta_Msj, "", "OK")
        End If

        Return SW

    End Function
    Protected Sub FrmPpal_BtnGuardar_Click(sender As Object, e As EventArgs)
        Try
            If Not FrmCarpetaRaiz_ValidarCampos() Then
                Exit Sub
            End If

            Dim E_ObjCarpeta As New E_ClsTblCarpeta



            E_ObjCarpeta.Carpeta = FrmCarpetaRaiz_TxtCarpeta.Text.Trim()
            E_ObjCarpeta.LastTransactionUser = ViewState("MyIdUser")

            E_ObjCarpeta.StoreProcedure = ""
            E_ObjCarpeta.IdCarpeta = 0

            If FrmPpal_TxtSwTipo.Text = "S" Then
                E_ObjCarpeta.Nivel = CInt(FrmPpal_TxtNivel.Text) + 1
                E_ObjCarpeta.Estado = 1
                E_ObjCarpeta.IdCarpeta = 0
                E_ObjCarpeta.IdCarpetaPadre = FrmPpal_TxtIdCarpetaPadre.Text

            Else
                E_ObjCarpeta.Nivel = 1
                E_ObjCarpeta.Estado = 1
                E_ObjCarpeta.IdCarpeta = 0
            End If



            TransacCarpeta(E_ObjCarpeta)
            FrmCarpetaRaiz_TxtCarpeta.Text = ""
            initCarpetas()
            getCarpeta()


            ModalHide(FrmPpal_dialog, FrmPpal)

            FrmPpal.Hide()
        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub

    Private Sub getCarpeta()
        Try

            If FrmPpal_TxtIdCarpeta.Text.Trim() = "" Then Exit Sub
            FrmPpal_LastOptionSelected.Text = 1


            LastIdCarpeta = FrmPpal_TxtIdCarpeta.Text

            Dim E_ObjCarpeta As New E_ClsTblCarpeta
            Dim N_ObjCarpeta As New N_ClsTblCarpeta

            E_ObjCarpeta.IdCarpeta = FrmPpal_TxtIdCarpeta.Text
            E_ObjCarpeta = N_ObjCarpeta.GetCarpeta(E_ObjCarpeta)

            FrmPpal_CboLineManeger_Llenar()

            FrmPpal_Limpiar()

            FrmPpal_TxtSwTipo.Text.Trim()


            FrmPpal_TxtIdCarpeta.Text = E_ObjCarpeta.IdCarpeta
            FrmPpal_TxtIdCarpetaPadre.Text = E_ObjCarpeta.IdCarpetaPadre
            FrmPpal_TxtNivel.Text = E_ObjCarpeta.Nivel
            FrmPpal_TxtCarpeta.Text = Trim(E_ObjCarpeta.Carpeta)
            FrmPpal_TxtStoreProcedure.Text = Trim(E_ObjCarpeta.StoreProcedure)
            FrmPpal_DivUltAct.InnerHtml = E_ObjCarpeta.UltAct


            If CStr(E_ObjCarpeta.Confidencial).Trim().ToUpper = "TRUE" Or CStr(E_ObjCarpeta.Confidencial).Trim() = "1" Then
                FrmPpal_ChkPrivada.Attributes("checked") = True
                FrmPpal_ChkPrivada.Text = "1"
            Else
                FrmPpal_ChkPrivada.Attributes.Remove("checked")
                FrmPpal_ChkPrivada.Text = ""
            End If

            If CStr(E_ObjCarpeta.Estado).Trim().ToUpper = "TRUE" Or CStr(E_ObjCarpeta.Estado).Trim() = "1" Then
                FrmPpal_ChkEstado.Attributes("checked") = True
                FrmPpal_ChkEstado.Text = "1"
            Else
                FrmPpal_ChkEstado.Attributes.Remove("checked")
                FrmPpal_ChkEstado.Text = ""
            End If

            If CStr(E_ObjCarpeta.Contenedora).Trim().ToUpper = "TRUE" Or CStr(E_ObjCarpeta.Contenedora).Trim() = "1" Then
                FrmPpal_ChkContenedorCriticas.Attributes("checked") = True
                FrmPpal_ChkContenedorCriticas.Text = "1"
                FrmPpal_BtnAddSubCarpeta.Visible = False

                selectedChecked()
                GrillaPrefijo_Llenar()
                getAutorizadores()
                FrmPpal_LastOptionSelected.Text = "1"
                accordionSelected()



            Else
                FrmPpal_ChkContenedorCriticas.Attributes.Remove("checked")
                FrmPpal_ChkContenedorCriticas.Text = ""

                FrmPpal_BtnAddSubCarpeta.Visible = True
            End If

            FrmPpal_BtnGrabar.Text = "<i class='fas fa-save'></i> Actualizar"

            '
            FrmPpal_Habilita()

            Ffrm_principal_contenedorCriticas.Visible = True
            If E_ObjCarpeta.cantHijos > 0 Then
                FrmPpal_ChkContenedorCriticas.Attributes.Remove("checked")
                FrmPpal_ChkContenedorCriticas.Text = ""
                FrmPpal_ChkContenedorCriticas.Enabled = False
                FrmPpal_ChkContenedorCriticas.Attributes("disabled") = "disabled"

            End If

            execJS("init()")
        Catch ex As Exception

        End Try
    End Sub



    Private Sub FrmPpal_Limpiar()
        FrmPpal_TxtIdCarpeta.Text = ""
        FrmPpal_TxtIdCarpetaPadre.Text = ""
        FrmPpal_TxtNivel.Text = ""
        FrmPpal_TxtCarpeta.Text = ""
        FrmPpal_TxtStoreProcedure.Text = ""
        'FrmPpal_LstIdTipoArchivo_Llenar()


        FrmPpal_TxtCarpeta.Enabled = False
        FrmPpal_TxtCarpeta.Attributes("disabled") = "disabled"

        FrmPpal_TxtStoreProcedure.Enabled = False
        FrmPpal_TxtStoreProcedure.Attributes("disabled") = "disabled"




        FrmPpal_ChkPrivada.Attributes.Remove("checked")
        FrmPpal_ChkPrivada.Text = ""

        FrmPpal_ChkPrivada.Enabled = False
        FrmPpal_ChkPrivada.Attributes("disabled") = "disabled"


        FrmPpal_ChkPrivada.Attributes.Remove("checked")
        FrmPpal_ChkPrivada.Text = ""
        FrmPpal_ChkPrivada.Enabled = False
        FrmPpal_ChkEstado.Attributes("disabled") = "disabled"


        FrmPpal_ChkContenedorCriticas.Attributes.Remove("checked")
        FrmPpal_ChkContenedorCriticas.Text = ""
        FrmPpal_ChkContenedorCriticas.Enabled = False
        FrmPpal_ChkContenedorCriticas.Attributes("disabled") = "disabled"

        For i = 0 To FrmPpal_LstListaDistribucion.Items.Count - 1
            FrmPpal_LstListaDistribucion.Items(i).Selected = False
        Next



        GetValidador(FrmPpal_TxtCarpeta, "form-control specialform", FrmPpal_TxtCarpeta_Msj, "", "LIMPIAR")
        GetValidador(FrmPpal_TxtStoreProcedure, "form-control specialform", FrmPpal_TxtStoreProcedure_Msj, "", "LIMPIAR")




        execJS("init()")


    End Sub
    Protected Sub BreadcrumbsFolder_Llenar(ByVal IdCarpeta As Integer)
        Try

            Dim E_ObjTblCarpeta As New E_ClsTblCarpeta
            Dim N_ObjTblCarpeta As New N_ClsTblCarpeta

            E_ObjTblCarpeta.IdCarpeta = IdCarpeta
            Dim HtmlBreath = N_ObjTblCarpeta.BreadcrumbsFolder(E_ObjTblCarpeta)
            DivBreadcrumbsFolder.InnerHtml = HtmlBreath
            pathBrath.InnerHtml = HtmlBreath
            FrmPpal_TxtIdCarpeta.Text = IdCarpeta
            TreeView_SetNode(CStr(IdCarpeta))


        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub BscIdCarpeta(ByVal Argumento As String)
        Try
            Dim ResultArray() As String = Split(Argumento, "\")
            BreadcrumbsFolder_Llenar(CInt(ResultArray(UBound(ResultArray))))

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub TreeView_SetNode(ByVal TxtValue As String)
        Dim searchedNode As TreeNode = Nothing
        For Each node As TreeNode In CarpetasTreeView.Nodes
            searchedNode = SearchNodeValue(node, TxtValue)
            If searchedNode Is Nothing Then
                For Each childNode As TreeNode In node.ChildNodes
                    searchedNode = SearchNodeValue(childNode, TxtValue)
                    If searchedNode IsNot Nothing Then
                        searchedNode.[Select]()
                    End If
                Next
            Else
                If searchedNode IsNot Nothing Then
                    searchedNode.[Select]()
                End If
                Exit For
            End If
        Next
    End Sub
    Private Sub execJS(ByVal script As String)
        Try
            Dim TxtJavascript As String = "<script type='text/javascript'>" + script + "</script>"
            ScriptManager.RegisterStartupScript(Me, GetType(Page), "alerta", TxtJavascript, False)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub accordionSelected()

        Ffrm_principal_LabelAcordeonOpc1.Attributes.Add("class", "accordion-collapse collapse")
        Ffrm_principal_LabelAcordeonOpc2.Attributes.Add("class", "accordion-collapse collapse")
        Ffrm_principal_LabelAcordeonOpc3.Attributes.Add("class", "accordion-collapse collapse")

        Select Case True
            Case FrmPpal_LastOptionSelected.Text = "1" : Ffrm_principal_LabelAcordeonOpc1.Attributes.Add("class", "accordion-collapse collapse show")
            Case FrmPpal_LastOptionSelected.Text = "2" : Ffrm_principal_LabelAcordeonOpc2.Attributes.Add("class", "accordion-collapse collapse show")
            Case FrmPpal_LastOptionSelected.Text = "3" : Ffrm_principal_LabelAcordeonOpc3.Attributes.Add("class", "accordion-collapse collapse show")
        End Select


    End Sub



#End Region


#Region "Grilla Autoriza"

    Protected Sub FrmPpal_CboLineManeger_Llenar()
        Try
            Dim N_ObjLeeUser As New N_ClsLeeUser

            FrmPpal_CboUserGestionSearch.Items.Clear()
            FrmPpal_CboUserGestionSearch.DataTextField = "TextField"
            FrmPpal_CboUserGestionSearch.DataValueField = "ValueField"


            Dim reaultLoadCbo = N_ObjLeeUser.LoadCbo()

            FrmPpal_CboUserGestionSearch.DataSource = reaultLoadCbo
            FrmPpal_CboUserGestionSearch.DataBind()

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub getAutorizadores()
        Try
            Dim N_ObjCarpetaAutoriza As New N_ClsTblCarpetaAutorizado
            Dim DtaTable As DataTable = N_ObjCarpetaAutoriza.SelectAllbyCarpeta(FrmPpal_TxtIdCarpeta.Text)
            FrmPpal_GrillaAutoriza.DataSource = SortingPageIndex(N_ObjCarpetaAutoriza.SelectAllbyCarpeta(FrmPpal_TxtIdCarpeta.Text), ViewState("FrmPpal_GrillaAutoriza_ColumnSorting"), ViewState("FrmPpal_GrillaAutoriza_TypeSorting"))
            FrmPpal_GrillaAutoriza.DataBind()

            FrmPpal_GrillaAutoriza.Columns(1).ItemStyle.HorizontalAlign = HorizontalAlign.Center
            FrmPpal_GrillaAutoriza.Columns(2).ItemStyle.HorizontalAlign = HorizontalAlign.Left
            FrmPpal_GrillaAutoriza.Columns(3).ItemStyle.HorizontalAlign = HorizontalAlign.Center

            For Each row As DataRow In DtaTable.Rows
                FrmPpal_CboUserGestionSearch.Items.Remove(FrmPpal_CboUserGestionSearch.Items.FindByValue(row("IdAutorizado")))
            Next row

            If (FrmPpal_CboUserGestionSearch.Items.Count = 0) Then

                FrmPpal_BtnGrabar_personaAutorizada.Enabled = False
                FrmPpal_BtnGrabar_personaAutorizada.Attributes("disabled") = "disabled"
            Else
                FrmPpal_BtnGrabar_personaAutorizada.Enabled = True
                FrmPpal_BtnGrabar_personaAutorizada.Attributes.Remove("disabled")
            End If

        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub FfrmPrincipal_btnElimina_click(sender As Object, e As EventArgs)
        Try
            Dim btn As LinkButton = sender
            Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
            Dim IdAutorizado As Label = CType(FrmPpal_GrillaAutoriza.Rows(row.RowIndex).FindControl("LblIdAutorizado"), Label)

            Dim E_ObjCarpetaAutoriza As New E_ClsTblCarpetaAutorizado
            Dim N_ObjCarpetaAutoriza As New N_ClsTblCarpetaAutorizado
            E_ObjCarpetaAutoriza.IdAutorizado = IdAutorizado.Text
            E_ObjCarpetaAutoriza.IdCarpeta = FrmPpal_TxtIdCarpeta.Text

            FrmPpal_LastOptionSelected.Text = 2

            Dim ResultadoTransaccion As String = N_ObjCarpetaAutoriza.Delete(E_ObjCarpetaAutoriza)
            If Mid(ResultadoTransaccion, 1, 2) = "OK" Then

                FrmPpal_CboLineManeger_Llenar()
                getAutorizadores()
            Else
                MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
                ModalShow(FrmMsg_dialog, FrmMsg)

            End If
            accordionSelected()
            execJS("init()")


        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub FrmPpal_BtnGrabar_personaAutorizada_click(sender As Object, e As EventArgs)
        Try

            Dim E_ObjCarpetaAutoriza As New E_ClsTblCarpetaAutorizado
            Dim N_ObjCarpetaAutoriza As New N_ClsTblCarpetaAutorizado

            FrmPpal_LastOptionSelected.Text = 2
            E_ObjCarpetaAutoriza.IdCarpeta = FrmPpal_TxtIdCarpeta.Text
            E_ObjCarpetaAutoriza.IdAutorizado = FrmPpal_CboUserGestionSearch.SelectedValue
            E_ObjCarpetaAutoriza.LastTransactionUser = ViewState("MyIdUser")

            Dim ResultadoTransaccion As String = N_ObjCarpetaAutoriza.Insert(E_ObjCarpetaAutoriza)

            If Mid(ResultadoTransaccion, 1, 2) = "OK" Then
                FrmPpal_TxtPrefijo.Text = ""


                getAutorizadores()
            Else
                MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
                ModalShow(FrmMsg_dialog, FrmMsg)
            End If

            accordionSelected()
            execJS("init()")



        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub FrmPpal_GrillaAutoriza_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Pager Then
            Dim PiePagina As Label = e.Row.FindControl("FrmPpal_GrillaAutoriza_CurrentPageLabel")
            Dim TotalPaginas As Integer = Convert.ToString(FrmPpal_GrillaAutoriza.PageCount).Trim()
            PiePagina.Text = "Página " & Convert.ToString(FrmPpal_GrillaAutoriza.PageIndex + 1).Trim() & " de " & TotalPaginas.ToString()

        End If
        execJS("init()")

    End Sub

    Protected Sub FrmPpal_GrillaAutoriza_BtnFirst_Click(sender As Object, e As EventArgs)
        getAutorizadores()
        FrmPpal_GrillaAutoriza.PageIndex = 0
        FrmPpal_GrillaAutoriza.DataBind()
    End Sub
    Protected Sub FrmPpal_GrillaAutoriza_BtnPrev_Click(sender As Object, e As EventArgs)
        getAutorizadores()
        If (FrmPpal_GrillaAutoriza.PageIndex <> 0) Then
            FrmPpal_GrillaAutoriza.PageIndex = FrmPpal_GrillaAutoriza.PageIndex - 1
            FrmPpal_GrillaAutoriza.DataBind()
        End If
    End Sub
    Protected Sub FrmPpal_GrillaAutoriza_BtnNext_Click(sender As Object, e As EventArgs)
        getAutorizadores()
        If (FrmPpal_GrillaAutoriza.PageIndex <> FrmPpal_GrillaAutoriza.PageCount - 1) Then
            FrmPpal_GrillaAutoriza.PageIndex = FrmPpal_GrillaAutoriza.PageIndex + 1
            FrmPpal_GrillaAutoriza.DataBind()
        End If
    End Sub
    Protected Sub FrmPpal_GrillaAutoriza_BtnLast_Click(sender As Object, e As EventArgs)
        getAutorizadores()
        FrmPpal_GrillaAutoriza.PageIndex = FrmPpal_GrillaAutoriza.PageCount - 1
        FrmPpal_GrillaAutoriza.DataBind()
    End Sub
    Private Sub FrmPpal_GrillaAutoriza_PreRender(sender As Object, e As EventArgs) Handles FrmPpal_GrillaAutoriza.PreRender
        'Esto es para que la grilla muestre el paginador aunque tenga menos de 10 registros
        If FrmPpal_GrillaAutoriza.BottomPagerRow IsNot Nothing Then
            If FrmPpal_GrillaAutoriza.BottomPagerRow.Visible = False Then
                FrmPpal_GrillaAutoriza.BottomPagerRow.Visible = True
            End If
        End If
    End Sub
    Private Sub FrmPpal_GrillaAutoriza_Sorting(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewSortEventArgs) Handles FrmPpal_GrillaAutoriza.Sorting
        ViewState(" FrmPpal_GrillaAutoriza_ColumnSorting") = e.SortExpression

        If FrmPpal_GrillaAutoriza.Attributes(e.SortExpression) = SortDirection.Ascending Then
            ViewState(" FrmPpal_GrillaAutoriza_TypeSorting") = "DESC"
            FrmPpal_GrillaAutoriza_ChangeHeader()
        Else
            ViewState(" FrmPpal_GrillaAutoriza_TypeSorting") = "ASC"
            FrmPpal_GrillaAutoriza_ChangeHeader()
        End If

        getAutorizadores()
    End Sub
    Private Sub FrmPpal_GrillaAutoriza_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles FrmPpal_GrillaAutoriza.RowCommand
        If Trim(e.CommandArgument) = "ListaDistribucion" Then
            ViewState(" FrmPpal_GrillaAutoriza_ColumnIndex") = 2
        End If
    End Sub
    Private Sub FrmPpal_GrillaAutoriza_ChangeHeader()
        Dim Base As String = "<span class='link-light'>XXXTITULOXXX <i class='fa-solid fa-ellipsis-vertical'></i></span>"
        Dim Type As String

        If ViewState(" FrmPpal_GrillaAutoriza_TypeSorting") = "DESC" Then
            Type = "<i class='fa-solid fa-arrow-down-z-a'></i>"
        Else
            Type = "<i class='fa-solid fa-arrow-down-a-z'></i>"
        End If

        Dim FrmPpal_GrillaAutoriza_Indice = New Integer() {2}
        Dim FrmPpal_GrillaAutoriza_Campo() As String = {"IdAutorizado"}
        Dim FrmPpal_GrillaAutoriza_Titulo() As String = {"Usuario"}

        For I = 0 To FrmPpal_GrillaAutoriza_Indice.Length - 1
            If ViewState(" FrmPpal_GrillaAutoriza_ColumnIndex") = FrmPpal_GrillaAutoriza_Indice(I) Then
                FrmPpal_GrillaAutoriza.Columns(FrmPpal_GrillaAutoriza_Indice(I)).HeaderText = "<span class='link-light'>" & FrmPpal_GrillaAutoriza_Titulo(I) & " " & Type & "</span>"
                If ViewState(" FrmPpal_GrillaAutoriza_TypeSorting") = "DESC" Then
                    FrmPpal_GrillaAutoriza.Attributes(FrmPpal_GrillaAutoriza_Campo(I)) = SortDirection.Descending
                Else
                    FrmPpal_GrillaAutoriza.Attributes(FrmPpal_GrillaAutoriza_Campo(I)) = SortDirection.Ascending
                End If
            Else
                FrmPpal_GrillaAutoriza.Columns(FrmPpal_GrillaAutoriza_Indice(I)).HeaderText = Replace(Base, "XXXTITULOXXX", FrmPpal_GrillaAutoriza_Titulo(I))
                FrmPpal_GrillaAutoriza.Attributes(FrmPpal_GrillaAutoriza_Campo(I)) = SortDirection.Descending
            End If
        Next
    End Sub
#End Region


#Region "Prefijo"
    Protected Sub FrmPpal_BtnAddPrefijo_Click(sender As Object, e As EventArgs)

        Try

            Dim E_ObjPrefijo As New E_ClsTblPrefijo
            Dim N_ObjPrefijo As New N_ClsTblPrefijo
            Dim ResultadoTransaccion As String

            FrmPpal_LastOptionSelected.Text = 3
            accordionSelected()

            'If Not FrmPrefijo_ValidarCampos() Then
            ' Exit Sub
            'End If

            E_ObjPrefijo.IdCarpeta = FrmPpal_TxtIdCarpeta.Text
            E_ObjPrefijo.Prefijo = FrmPpal_TxtPrefijo.Text.Trim()
            E_ObjPrefijo.Estado = True 'IIf(FrmPpal_ChkEstadoPrefijo.Text.Trim() = "1", True, False)

            E_ObjPrefijo.LastTransactionUser = ViewState("MyIdUser")
            ResultadoTransaccion = N_ObjPrefijo.Insert(E_ObjPrefijo)


            If Mid(ResultadoTransaccion, 1, 2) = "OK" Then
                FrmPpal_TxtPrefijo.Text = ""


                GrillaPrefijo_Llenar()
                execJS("init()")

                'CarpetasTreeView_Llenar()
                'BreadcrumbsFolder_Llenar(FrmPpal_TxtIdCarpeta.Text)
                'GetCarpeta()
                'MsgBox("funco")
                'ObjMensaje.SetMensajeExitoso(MensajeModal_TipoMensaje, MensajeModal_TipoMensajeIcono, MensajeModal_BtnAceptar, MensajeModal_LblTitulo, "Mensaje", MensajeModal_LblCuerpo, "Registro creado.")
            Else
                MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
                ModalShow(FrmMsg_dialog, FrmMsg)
            End If


        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub GrillaPrefijo_Llenar()
        Try
            Dim N_ObjPrefijo As New N_ClsTblPrefijo

            FrmPpal_GrillaPrefijo.DataSource = SortingPageIndex(N_ObjPrefijo.SelectAllbyCarpeta(FrmPpal_TxtIdCarpeta.Text), ViewState("FrmPpal_GrillaPrefijo_ColumnSorting"), ViewState("FrmPpal_GrillaPrefijo_TypeSorting"))
            FrmPpal_GrillaPrefijo.DataBind()

        Catch ex As Exception
            FrmPpal_GrillaPrefijo.DataSource = Nothing
            FrmPpal_GrillaPrefijo.DataBind()
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub getListDistribucion()
        Dim N_ObjListaDistribucion As New N_ClsTblListaDistribucion
        FrmPpal_LstListaDistribucion.Items.Clear()
        FrmPpal_LstListaDistribucion.DataTextField = "TextField"
        FrmPpal_LstListaDistribucion.DataValueField = "ValueField"
        Dim dt As DataTable = N_ObjListaDistribucion.LoadCbo()
        FrmPpal_LstListaDistribucion.DataSource = dt
        FrmPpal_LstListaDistribucion.DataBind()
    End Sub
    Protected Sub selectedChecked()
        Try
            Dim N_ObjListaDistribucion As New N_ClsTblListaDistribucion


            Dim E_ObjNubCarpetaListaDistribucion As New E_ClsTblNubCarpetaListaDistribucion
            Dim N_ObjNubCarpetaListaDistribucion As New N_ClsTblNubCarpetaListaDistribucion
            Dim getListaDistribucionChecked As DataTable = N_ObjNubCarpetaListaDistribucion.SelectAll(FrmPpal_TxtIdCarpeta.Text)

            For Each row As DataRow In getListaDistribucionChecked.Rows
                'FrmPpal_LstListaDistribucion.Items.FindByValue(row("IdListaDistribucion")).Attributes("checked") = True
                'FrmPpal_ChkPrivada.Attributes("checked") = True

                For i = 0 To FrmPpal_LstListaDistribucion.Items.Count - 1
                    If FrmPpal_LstListaDistribucion.Items(i).Value = row("IdListaDistribucion") Then
                        FrmPpal_LstListaDistribucion.Items(i).Selected = True
                        Exit For

                    End If
                Next

            Next row


        Catch ex As Exception
            FrmPpal_GrillaPrefijo.DataSource = Nothing
            FrmPpal_GrillaPrefijo.DataBind()
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub

    Protected Sub FrmPpal_GrillaPrefijo_RowDataBound(sender As Object, e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.Pager Then
            Dim PiePagina As Label = e.Row.FindControl("FrmPpal_GrillaPrefijo_CurrentPageLabel")
            Dim TotalPaginas As Integer = Convert.ToString(FrmPpal_GrillaPrefijo.PageCount).Trim()
            PiePagina.Text = "Página " & Convert.ToString(FrmPpal_GrillaPrefijo.PageIndex + 1).Trim() & " de " & TotalPaginas.ToString()
        End If
        FrmPpal_LastOptionSelected.Text = 3
        accordionSelected()
    End Sub
    Protected Sub FrmPpal_GrillaPrefijo_BtnFirst_Click(sender As Object, e As EventArgs)
        GrillaPrefijo_Llenar()
        FrmPpal_GrillaPrefijo.PageIndex = 0
        FrmPpal_GrillaPrefijo.DataBind()
        FrmPpal_LastOptionSelected.Text = 3
        accordionSelected()
        execJS("init()")
    End Sub
    Protected Sub FrmPpal_GrillaPrefijo_BtnPrev_Click(sender As Object, e As EventArgs)
        GrillaPrefijo_Llenar()
        If (FrmPpal_GrillaPrefijo.PageIndex <> 0) Then
            FrmPpal_GrillaPrefijo.PageIndex = FrmPpal_GrillaPrefijo.PageIndex - 1
            FrmPpal_GrillaPrefijo.DataBind()

        End If
        FrmPpal_LastOptionSelected.Text = 3
        accordionSelected()
        execJS("init()")
    End Sub
    Protected Sub FrmPpal_GrillaPrefijo_BtnNext_Click(sender As Object, e As EventArgs)
        GrillaPrefijo_Llenar()
        If (FrmPpal_GrillaPrefijo.PageIndex <> FrmPpal_GrillaPrefijo.PageCount - 1) Then
            FrmPpal_GrillaPrefijo.PageIndex = FrmPpal_GrillaPrefijo.PageIndex + 1
            FrmPpal_GrillaPrefijo.DataBind()


        End If
        FrmPpal_LastOptionSelected.Text = 3
        accordionSelected()
        execJS("init()")
    End Sub
    Protected Sub FrmPpal_GrillaPrefijo_BtnLast_Click(sender As Object, e As EventArgs)
        GrillaPrefijo_Llenar()
        FrmPpal_GrillaPrefijo.PageIndex = FrmPpal_GrillaPrefijo.PageCount - 1
        FrmPpal_GrillaPrefijo.DataBind()
        FrmPpal_LastOptionSelected.Text = 3
        accordionSelected()
        execJS("init()")

    End Sub
    Protected Sub FfrmPrincipal_btnElimina_prefijo_click(sender As Object, e As EventArgs)
        Try
            Dim btn As LinkButton = sender
            Dim row As GridViewRow = CType(btn.NamingContainer, GridViewRow)
            Dim IdPrefijo As Label = CType(FrmPpal_GrillaPrefijo.Rows(row.RowIndex).FindControl("LblIdPrefijo"), Label)

            Dim E_ObjPrefijo As New E_ClsTblPrefijo
            Dim N_ObjPrefijo As New N_ClsTblPrefijo
            E_ObjPrefijo.IdPrefijo = IdPrefijo.Text
            E_ObjPrefijo.IdCarpeta = CType(FrmPpal_TxtIdCarpeta.Text, Integer)

            FrmPpal_LastOptionSelected.Text = 3

            Dim ResultadoTransaccion As String = N_ObjPrefijo.Delete(E_ObjPrefijo)
            If Mid(ResultadoTransaccion, 1, 2) = "OK" Then

                GrillaPrefijo_Llenar()
            Else
                MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ResultadoTransaccion)
                ModalShow(FrmMsg_dialog, FrmMsg)
            End If
            accordionSelected()
            execJS("init()")


        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub

#End Region

#Region "Formulario Modal"
    Protected Sub FrmCarpetaRaiz_BtnAdd_click(sender As Object, e As EventArgs)
        Try
            FrmPpalTitulo.InnerHtml = "<i class='far fa-edit'></i>&nbsp;&nbsp;Agregar <span Class='badge text-bg-warning'>Carpeta Raíz</span>"
            FrmPpal_BtnGuardar.Text = "<i class='fa-regular fa-floppy-disk'></i>&nbsp;&nbsp;&nbsp;Guardar"

            pathBrath.Visible = False
            ModalShow(FrmPpal_dialog, FrmPpal)
            FrmPpal.Show()
            execJS("init()")

        Catch ex As Exception

        End Try
    End Sub
    Function TransacCarpeta(ByVal E_ObjCarpeta As E_ClsTblCarpeta) As Boolean

        Try
            Dim N_ObjTblCarpeta As New N_ClsTblCarpeta
            Dim ResultadoTransaccion As String

            If E_ObjCarpeta.IdCarpeta <> 0 Then
                ResultadoTransaccion = N_ObjTblCarpeta.Update(E_ObjCarpeta)
            Else
                ResultadoTransaccion = N_ObjTblCarpeta.InsertSubCarpeta(E_ObjCarpeta)

            End If

            If Mid(ResultadoTransaccion, 1, 2) = "OK" Then
                If FrmPpal_TxtIdCarpeta.Text <> "" Then

                Else
                    FrmPpal_TxtIdCarpeta.Text = Replace(ResultadoTransaccion, "OK", "")
                End If

                Return True


            Else
                Return False

            End If

        Catch ex As Exception

        End Try
    End Function
    Function FrmCarpeta_ValidarCampos() As Boolean
        Dim SW As Boolean = True
        If FrmPpal_TxtCarpeta.Text.Trim() = "" Then
            GetValidador(FrmPpal_TxtCarpeta, "form-control specialform", FrmPpal_TxtCarpeta_Msj, "* Obligatorio", "ERROR")

            SW = False
        Else
            GetValidador(FrmPpal_TxtCarpeta, "form-control specialform", FrmPpal_TxtCarpeta_Msj, "", "OK")
        End If

        Return SW



    End Function
    Protected Sub FrmPpal_BtnGrabar_Click(sender As Object, e As EventArgs)
        Try
            If Not FrmCarpeta_ValidarCampos() Then
                execJS("init()")
                Exit Sub
            End If

            Dim E_ObjCarpeta As New E_ClsTblCarpeta

            E_ObjCarpeta.Carpeta = FrmPpal_TxtCarpeta.Text.Trim()
            E_ObjCarpeta.StoreProcedure = FrmPpal_TxtStoreProcedure.Text.Trim()

            E_ObjCarpeta.LastTransactionUser = ViewState("MyIdUser")
            E_ObjCarpeta.Confidencial = IIf(FrmPpal_ChkPrivada.Text.Trim() = "1", True, False)
            E_ObjCarpeta.Estado = IIf(FrmPpal_ChkEstado.Text.Trim() = "1", True, False)
            E_ObjCarpeta.Contenedora = IIf(FrmPpal_ChkContenedorCriticas.Text.Trim() = "1", True, False)

            If E_ObjCarpeta.Contenedora = False Then
                FrmPpal_TxtStoreProcedure.Text = ""
            End If



            If FrmPpal_TxtIdCarpeta.Text <> "" Then
                E_ObjCarpeta.IdCarpeta = FrmPpal_TxtIdCarpeta.Text
                E_ObjCarpeta.Nivel = FrmPpal_TxtNivel.Text
                E_ObjCarpeta.IdCarpetaPadre = FrmPpal_TxtIdCarpeta.Text

            Else
                If FrmPpal_TxtSwTipo.Text.Trim = "C" Then
                    E_ObjCarpeta.Nivel = 1

                Else
                    E_ObjCarpeta.IdCarpetaPadre = FrmPpal_TxtIdCarpetaPadre.Text
                    E_ObjCarpeta.Nivel = CInt(FrmPpal_TxtNivel.Text) + 1
                End If
            End If

            Dim E_ObjNubCarpetaListaDistribucion As New E_ClsTblNubCarpetaListaDistribucion
            Dim N_ObjNubCarpetaListaDistribucion As New N_ClsTblNubCarpetaListaDistribucion

            If TransacCarpeta(E_ObjCarpeta) = True Then

                E_ObjNubCarpetaListaDistribucion.IdCarpeta = CInt(FrmPpal_TxtIdCarpeta.Text)
                N_ObjNubCarpetaListaDistribucion.Delete(E_ObjNubCarpetaListaDistribucion)

                If E_ObjCarpeta.Contenedora = False Then
                    Dim E_ObjCarpetaAutoriza As New E_ClsTblCarpetaAutorizado
                    Dim N_ObjCarpetaAutoriza As New N_ClsTblCarpetaAutorizado

                    N_ObjCarpetaAutoriza.DeleteAllCarpeta(FrmPpal_TxtIdCarpeta.Text)

                    Dim E_ObjPrefijo As New E_ClsTblPrefijo
                    Dim N_ObjPrefijo As New N_ClsTblPrefijo

                    E_ObjPrefijo.IdCarpeta = FrmPpal_TxtIdCarpeta.Text
                    N_ObjPrefijo.Delete(E_ObjPrefijo)

                Else

                    For i = 0 To FrmPpal_LstListaDistribucion.Items.Count - 1
                        If FrmPpal_LstListaDistribucion.Items(i).Selected = True Then
                            E_ObjNubCarpetaListaDistribucion.IdCarpeta = FrmPpal_TxtIdCarpeta.Text
                            E_ObjNubCarpetaListaDistribucion.IdListaDistribucion = FrmPpal_LstListaDistribucion.Items(i).Value
                            E_ObjNubCarpetaListaDistribucion.LastTransactionUser = ViewState("MyIdUser")
                            N_ObjNubCarpetaListaDistribucion.Insert(E_ObjNubCarpetaListaDistribucion)
                        End If
                    Next

                    FrmPpal_Limpiar()


                End If
            End If





            initCarpetas()
            execJS("init()")
            getCarpeta()
            ModalHide(FrmPpal_dialog, FrmPpal)


            FrmPpal.Hide()
        Catch ex As Exception
            MsgModalError(FrmMsg_TipoMensaje, FrmMsg_TipoMensajeIcono, FrmMsg_BtnAceptar, FrmMsg_LblTitulo, "Error", FrmMsg_LblCuerpo, ex.ToString)
            ModalShow(FrmMsg_dialog, FrmMsg)
        End Try
    End Sub
    Protected Sub FrmPpal_BtnCerrar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmPpal_dialog, FrmPpal)
        execJS("init()")
    End Sub

#End Region


#Region "FrmMsg"
    Protected Sub FrmMsg_BtnAceptar_Click(sender As Object, e As EventArgs)
        ModalHide(FrmMsg_dialog, FrmMsg)
        execJS("init()")
    End Sub
#End Region



End Class