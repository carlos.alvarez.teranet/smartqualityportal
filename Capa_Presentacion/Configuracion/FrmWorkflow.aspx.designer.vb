﻿'------------------------------------------------------------------------------
' <generado automáticamente>
'     Este código fue generado por una herramienta.
'
'     Los cambios en este archivo podrían causar un comportamiento incorrecto y se perderán si
'     se vuelve a generar el código. 
' </generado automáticamente>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class FrmWorkflow

    '''<summary>
    '''Control PnProgress.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents PnProgress As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control UpProgress.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents UpProgress As Global.System.Web.UI.UpdateProgress

    '''<summary>
    '''Control ModalProgress.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents ModalProgress As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''Control UpdatePanelIndex.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents UpdatePanelIndex As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control PanelIndex.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents PanelIndex As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control BtnAddRegistro.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents BtnAddRegistro As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control TxtBsc.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents TxtBsc As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control BtnBsc.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents BtnBsc As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control GrillaPpal.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents GrillaPpal As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control FrmPpal_BtnVer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_BtnVer As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control FrmPpal.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''Control FrmPpal_Panel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_Panel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control FrmPpal_UpdatePanel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_UpdatePanel As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control FrmPpal_dialog.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_dialog As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpalTitulo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpalTitulo As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpal_TxtIdWorkflow.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_TxtIdWorkflow As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmPpal_TxtWorkflow.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_TxtWorkflow As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmPpal_TxtWorkflow_Msj.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_TxtWorkflow_Msj As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpal_DivUltAct.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_DivUltAct As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmPpal_BtnCerrar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_BtnCerrar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmPpal_BtnGuardar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmPpal_BtnGuardar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmRutaAprobacion_BtnVer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmRutaAprobacion_BtnVer As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control FrmRutaAprobacion.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmRutaAprobacion As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''Control FrmRutaAprobacion_Panel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmRutaAprobacion_Panel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control FrmRutaAprobacion_UpdatePanel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmRutaAprobacion_UpdatePanel As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control FrmRutaAprobacion_dialog.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmRutaAprobacion_dialog As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmRutaAprobacionTitulo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmRutaAprobacionTitulo As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmRutaAprobacion_LblIdWorkflow.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmRutaAprobacion_LblIdWorkflow As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmRutaAprobacion_CboUserGestionSearch.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmRutaAprobacion_CboUserGestionSearch As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control FrmRutaAprobacion_BtnGrabar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmRutaAprobacion_BtnGrabar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmRutaAprobacion_GrillaJobPosition.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmRutaAprobacion_GrillaJobPosition As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control FrmRutaAprobacionUltAct.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmRutaAprobacionUltAct As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmRutaAprobacion_CboCarpetaGestionSearch.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmRutaAprobacion_CboCarpetaGestionSearch As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Control FrmRutaAprobacionCarpeta_BtnGrabar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmRutaAprobacionCarpeta_BtnGrabar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmRutaAprobacion_GrillaCarpetas.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmRutaAprobacion_GrillaCarpetas As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''Control Div1.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents Div1 As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmRutaAprobacion_BtnCerrar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmRutaAprobacion_BtnCerrar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmDelete_BtnVer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete_BtnVer As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control FrmDelete.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''Control FrmDelete_Panel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete_Panel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control FrmDelete_UpdatePanel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete_UpdatePanel As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control FrmDelete_dialog.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete_dialog As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmDeleteTitulo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDeleteTitulo As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmDelete_TxtId.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete_TxtId As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''Control FrmDelete_Msg.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete_Msg As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control FrmDelete_BtnCerrar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete_BtnCerrar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmDelete_BtnEliminar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmDelete_BtnEliminar As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''Control FrmMsg_BtnVer.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_BtnVer As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Control FrmMsg.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''Control FrmMsg_Panel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_Panel As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Control FrmMsg_UpdatePanel.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_UpdatePanel As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Control FrmMsg_dialog.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_dialog As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmMsg_TipoMensaje.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_TipoMensaje As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmMsg_TipoMensajeIcono.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_TipoMensajeIcono As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''Control FrmMsg_LblTitulo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_LblTitulo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control FrmMsg_LblCuerpo.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_LblCuerpo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Control FrmMsg_BtnAceptar.
    '''</summary>
    '''<remarks>
    '''Campo generado automáticamente.
    '''Para modificarlo, mueva la declaración del campo del archivo del diseñador al archivo de código subyacente.
    '''</remarks>
    Protected WithEvents FrmMsg_BtnAceptar As Global.System.Web.UI.WebControls.Button
End Class
