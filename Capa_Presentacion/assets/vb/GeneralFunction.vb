﻿Imports System.Security.Cryptography
Imports System.DirectoryServices
Module GeneralFunction
    Public Function GenerateMD5(ByVal input As String) As String
        Dim md5Hash As MD5 = MD5.Create()
        Dim data As Byte() = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input))
        Dim sBuilder As New StringBuilder()
        Dim i As Integer
        For i = 0 To data.Length - 1
            sBuilder.Append(data(i).ToString("x2"))
        Next i

        Return sBuilder.ToString()
    End Function




    Public Function ConstruirTreevew(ByVal Tabla As DataTable) As TreeNodeCollection
        Dim ListaTreeview As New TreeNodeCollection
        Dim dt As DataTable = GetChildData(-1, Tabla)

        For Each dr As DataRow In dt.Rows
            Dim parentNode As New TreeNode()

            parentNode.Text = dr("NodeText").ToString()
            parentNode.Value = dr("NodeID").ToString()

            AddNodes(parentNode, Tabla)
            ListaTreeview.Add(parentNode)
        Next

        Return ListaTreeview
    End Function
    Private Sub AddNodes(ByRef node As TreeNode, ByVal Tabla As DataTable)
        Dim dt As DataTable = GetChildData(Convert.ToInt32(node.Value), Tabla)

        For Each row As DataRow In dt.Rows
            Dim childNode As New TreeNode()

            childNode.Value = row("NodeID").ToString()
            childNode.Text = row("NodeText").ToString()

            AddNodes(childNode, Tabla)
            node.ChildNodes.Add(childNode)
        Next
    End Sub
    Private Function GetChildData(parentId As Integer, ByVal Tabla As DataTable) As DataTable
        Dim dt As New DataTable()

        dt.Columns.AddRange(New DataColumn() {New DataColumn("NodeId", GetType(Integer)), New DataColumn("ParentId", GetType(Integer)), New DataColumn("NodeText")})

        For Each dr As DataRow In Tabla.Rows
            If dr(1).ToString() <> parentId.ToString() Then
                Continue For
            End If

            Dim row As DataRow = dt.NewRow()
            row("NodeId") = dr("NodeId")
            row("ParentId") = dr("ParentId")
            row("NodeText") = dr("NodeText")
            dt.Rows.Add(row)
        Next

        Return dt
    End Function
    Public Function SearchNodeValue(ByVal node As TreeNode, ByVal Optional TxtValue As String = Nothing) As TreeNode
        If CStr(node.Value).Trim() = TxtValue.Trim() Then Return node
        Dim tn As TreeNode = Nothing
        For Each childNode As TreeNode In node.ChildNodes
            tn = SearchNodeValue(childNode, TxtValue)
            If tn IsNot Nothing Then Exit For
        Next
        If tn IsNot Nothing Then
            node.Expand()
        End If
        Return tn
    End Function
    Public Function SearchNodeText(ByVal node As TreeNode, ByVal Optional TxtValue As String = Nothing) As TreeNode
        If node.Text.Trim() = TxtValue.Trim() Then Return node
        Dim tn As TreeNode = Nothing
        For Each childNode As TreeNode In node.ChildNodes
            tn = SearchNodeText(childNode, TxtValue)
            If tn IsNot Nothing Then Exit For
        Next
        If tn IsNot Nothing Then
            node.Expand()
        End If
        Return tn
    End Function

    Public Function OwnEncryption(ByVal Input As String) As String
        Dim IV() As Byte = ASCIIEncoding.ASCII.GetBytes("TERAnetK") 'La clave debe ser de 8 caracteres
        Dim EncryptionKey() As Byte = Convert.FromBase64String("TERAnetodPfTnYspBIy/KANTARpstmfu") 'No se puede alterar la cantidad de caracteres pero si la clave
        Dim buffer() As Byte = Encoding.UTF8.GetBytes(Input)
        Dim des As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider
        des.Key = EncryptionKey
        des.IV = IV

        OwnEncryption = Convert.ToBase64String(des.CreateEncryptor().TransformFinalBlock(buffer, 0, buffer.Length()))

        OwnEncryption = Replace(OwnEncryption, ":", "ktr01")
        OwnEncryption = Replace(OwnEncryption, "/", "ktr02")
        OwnEncryption = Replace(OwnEncryption, "?", "ktr03")
        OwnEncryption = Replace(OwnEncryption, "#", "ktr04")
        OwnEncryption = Replace(OwnEncryption, "[", "ktr05")
        OwnEncryption = Replace(OwnEncryption, "]", "ktr06")
        OwnEncryption = Replace(OwnEncryption, "@", "ktr07")
        OwnEncryption = Replace(OwnEncryption, "!", "ktr08")
        OwnEncryption = Replace(OwnEncryption, "$", "ktr09")
        OwnEncryption = Replace(OwnEncryption, "&", "ktr10")
        OwnEncryption = Replace(OwnEncryption, "'", "ktr11")
        OwnEncryption = Replace(OwnEncryption, "(", "ktr12")
        OwnEncryption = Replace(OwnEncryption, ")", "ktr13")
        OwnEncryption = Replace(OwnEncryption, "*", "ktr14")
        OwnEncryption = Replace(OwnEncryption, "+", "ktr15")
        OwnEncryption = Replace(OwnEncryption, ",", "ktr16")
        OwnEncryption = Replace(OwnEncryption, ";", "ktr17")
        OwnEncryption = Replace(OwnEncryption, "=", "ktr18")

        Return OwnEncryption

    End Function
    Public Function OwnDecryption(ByVal Input As String) As String
        Input = Replace(Input, "ktr01", ":")
        Input = Replace(Input, "ktr02", "/")
        Input = Replace(Input, "ktr03", "?")
        Input = Replace(Input, "ktr04", "#")
        Input = Replace(Input, "ktr05", "[")
        Input = Replace(Input, "ktr06", "]")
        Input = Replace(Input, "ktr07", "@")
        Input = Replace(Input, "ktr08", "!")
        Input = Replace(Input, "ktr09", "$")
        Input = Replace(Input, "ktr10", "&")
        Input = Replace(Input, "ktr11", "'")
        Input = Replace(Input, "ktr12", "(")
        Input = Replace(Input, "ktr13", ")")
        Input = Replace(Input, "ktr14", "*")
        Input = Replace(Input, "ktr15", "+")
        Input = Replace(Input, "ktr16", ",")
        Input = Replace(Input, "ktr17", ";")
        Input = Replace(Input, "ktr18", "=")

        Dim IV() As Byte = ASCIIEncoding.ASCII.GetBytes("TERAnetK") 'La clave debe ser de 8 caracteres
        Dim EncryptionKey() As Byte = Convert.FromBase64String("TERAnetodPfTnYspBIy/KANTARpstmfu") 'No se puede alterar la cantidad de caracteres pero si la clave
        Dim buffer() As Byte = Convert.FromBase64String(Input)
        Dim des As TripleDESCryptoServiceProvider = New TripleDESCryptoServiceProvider
        des.Key = EncryptionKey
        des.IV = IV

        Return Encoding.UTF8.GetString(des.CreateDecryptor().TransformFinalBlock(buffer, 0, buffer.Length()))

    End Function
    Public Function OwnPageRequest(ByVal URL As String, ByVal Valor As String) As String
        Try
            Dim Url_Texto As String = URL & "&"
            Dim Url_Largo As Integer = Len(Url_Texto)
            Dim Url_Desde As String

            Dim Var_Nombre As String = Valor & "="
            Dim Var_Largo As Integer = Len(Var_Nombre)
            Dim Var_Valor As String

            Dim Mid_Desde As Integer
            Dim Mid_Hasta As Integer

            Mid_Desde = Strings.InStr(Url_Texto, Var_Nombre) + Var_Largo
            If Mid_Desde = Var_Largo Then Return ""

            Url_Desde = Mid(Url_Texto, Mid_Desde, Url_Largo - Var_Largo)
            Mid_Hasta = Strings.InStr(Url_Desde, "&") - 1
            Var_Valor = Mid(Url_Desde, 1, Mid_Hasta)

            Return Var_Valor

        Catch ex As Exception
            Return "ERROR"
        End Try
    End Function


    '**************************************************************
    '**************************************************************
    'Funciones de Formulario
    Public Sub GetValidador(ByVal ObjCampo As Object, ByVal ObjCampoCss As String, ByVal ObjTipoMsj As Object, ByVal TxtTipoMsj As Object, ByVal Accion As String)
        'OPCIONES: LIMPIAR - OK - ERROR

        If Accion = "LIMPIAR" Then
            ObjCampo.Attributes.Add("class", ObjCampoCss)
            ObjTipoMsj.Attributes.Add("class", "")
            ObjTipoMsj.InnerHtml = ""
        ElseIf Accion = "OK" Then
            ObjCampo.Attributes.Add("class", ObjCampoCss & " is-valid")
            ObjTipoMsj.Attributes.Add("class", "valid-feedback")
            ObjTipoMsj.InnerHtml = TxtTipoMsj
        ElseIf Accion = "ERROR" Then
            ObjCampo.Attributes.Add("class", ObjCampoCss & " is-invalid")
            ObjTipoMsj.Attributes.Add("class", "invalid-feedback")
            ObjTipoMsj.InnerHtml = TxtTipoMsj
        End If

        Exit Sub
    End Sub
    Public Sub SetMensajeExitoso(ObjTipoMensaje As Object, ObjTipoMensajeIcono As Object, ObjTipoMensajeBoton As Object, ByVal ObjTitulo As Object, ByVal TtxtTitulo As String, ByVal ObjMensaje As Object, ByVal TxtMensaje As String)
        ObjTipoMensaje.Attributes.Add("class", "modal-dialog bootstrap-dialog type-success fade in")
        ObjTipoMensajeIcono.Attributes.Add("class", "glyphicon glyphicon-ok")
        ObjTipoMensajeBoton.Attributes.Add("class", "btn btn-success")
        ObjTitulo.Text = TtxtTitulo
        ObjMensaje.Text = TxtMensaje

    End Sub
    Public Sub SetMensajeError(ObjTipoMensaje As Object, ObjTipoMensajeIcono As Object, ObjTipoMensajeBoton As Object, ByVal ObjTitulo As Object, ByVal TtxtTitulo As String, ByVal ObjMensaje As Object, ByVal TxtMensaje As String)
        'ObjTipoMensaje.Attributes.Add("class", "modal-dialog bootstrap-dialog type-danger fade in")

        ObjTipoMensaje.Attributes.Add("class", "modal-dialog bootstrap-dialog type-danger")
        ObjTipoMensajeIcono.Attributes.Add("class", "far fa-times-circle fa-lg")
        ObjTipoMensajeBoton.Attributes.Add("class", "btn btn-outline-danger btn-sm")
        ObjTitulo.Text = TtxtTitulo
        ObjMensaje.Text = TxtMensaje
    End Sub
    Public Sub SetMensajeAlerta(ObjTipoMensaje As Object, ObjTipoMensajeIcono As Object, ObjTipoMensajeBoton As Object, ByVal ObjTitulo As Object, ByVal TtxtTitulo As String, ByVal ObjMensaje As Object, ByVal TxtMensaje As String)
        ObjTipoMensaje.Attributes.Add("class", "modal-dialog bootstrap-dialog type-warning fade in")
        ObjTipoMensajeIcono.Attributes.Add("class", "glyphicon glyphicon-alert")
        ObjTipoMensajeBoton.Attributes.Add("class", "btn btn-warning")
        ObjTitulo.Text = TtxtTitulo
        ObjMensaje.Text = TxtMensaje
    End Sub
    Public Sub MsgModalExitoso(ObjTipoMensaje As Object, ObjTipoMensajeIcono As Object, ObjTipoMensajeBoton As Object, ByVal ObjTitulo As Object, ByVal TtxtTitulo As String, ByVal ObjMensaje As Object, ByVal TxtMensaje As String)
        ObjTipoMensaje.Attributes.Add("class", "modal-header bg-success")
        ObjTipoMensajeIcono.Attributes.Add("class", "fas fa-check")
        ObjTipoMensajeBoton.Attributes.Add("class", "btn btn-outline-success")
        ObjTitulo.Text = TtxtTitulo
        ObjMensaje.Text = TxtMensaje
    End Sub
    Public Sub MsgModalError(ObjTipoMensaje As Object, ObjTipoMensajeIcono As Object, ObjTipoMensajeBoton As Object, ByVal ObjTitulo As Object, ByVal TtxtTitulo As String, ByVal ObjMensaje As Object, ByVal TxtMensaje As String)
        ObjTipoMensaje.Attributes.Add("class", "modal-header bg-danger")
        ObjTipoMensajeIcono.Attributes.Add("class", "fas fa-times")
        ObjTipoMensajeBoton.Attributes.Add("class", "btn btn-outline-danger")
        ObjTitulo.Text = TtxtTitulo
        ObjMensaje.Text = TxtMensaje
    End Sub
    Public Sub MsgModalAlerta(ObjTipoMensaje As Object, ObjTipoMensajeIcono As Object, ObjTipoMensajeBoton As Object, ByVal ObjTitulo As Object, ByVal TtxtTitulo As String, ByVal ObjMensaje As Object, ByVal TxtMensaje As String)
        ObjTipoMensaje.Attributes.Add("class", "modal-header bg-warning")
        ObjTipoMensajeIcono.Attributes.Add("class", "fas fa-exclamation-triangle")
        ObjTipoMensajeBoton.Attributes.Add("class", "btn btn-outline-warning")
        ObjTitulo.Text = TtxtTitulo
        ObjMensaje.Text = TxtMensaje
    End Sub

    Public Sub ModalShow(Dialog As Object, Modal As Object)
        Dialog.Attributes.Add("style", "display:block;")
        Modal.Show()
    End Sub
    Public Sub ModalHide(Dialog As Object, Modal As Object)
        Dialog.Attributes.Add("style", "display:none;")
        Modal.Hide()
    End Sub

    '**************************************************************
    '**************************************************************

    Public Function GeneraKeyPass() As String
        Dim TxtAno As String = Trim(CStr(Year(Today)))
        Dim TxtMes As String = Trim(CStr(Month(Today)))
        Dim TxtDia As String = Trim(CStr(Day(Today)))

        If Len(TxtMes) < 2 Then TxtMes = "0" & TxtMes
        If Len(TxtDia) < 2 Then TxtDia = "0" & TxtDia

        Return GenerateMD5("KANTAR" & TxtAno & TxtMes & TxtDia)
    End Function
    Public Function GetModuleSecurityAccess(ByVal Krypton As String) As DataTable
        Dim TxtIdUser As String = ""
        Dim TxtIdModulo As String = ""
        Dim TxtIdOpcion As String = ""
        Dim TxtKeySesion As String
        Try
            Krypton = OwnDecryption(Krypton)

            TxtIdUser = OwnPageRequest(Krypton, "IdUser")
            TxtIdModulo = OwnPageRequest(Krypton, "IdModulo")
            TxtIdOpcion = OwnPageRequest(Krypton, "IdOpcion")
            TxtKeySesion = OwnPageRequest(Krypton, "KeySesion")

            Dim Dt As New DataTable()
            Dim Servicio As New GlobalService.GlobalServiceSoapClient
            Dt = Servicio.ModuleSecurityAccess(GeneraKeyPass(), TxtIdUser, TxtIdModulo, TxtIdOpcion, TxtKeySesion)

            Return Dt
        Catch ex As Exception
            Dim DtResultError As New DataTable(tableName:="Table")
            DtResultError.Columns.Add("IdUser")
            DtResultError.Columns.Add("IdModulo")
            DtResultError.Columns.Add("IdPerfil")
            DtResultError.Columns.Add("Perfil")
            DtResultError.Columns.Add("IdOpcion")
            DtResultError.Columns.Add("Opcion")
            DtResultError.Columns.Add("IdObjeto")
            DtResultError.Columns.Add("Objeto")
            DtResultError.Columns.Add("Error")

            Dim DtNewRow As DataRow = DtResultError.NewRow()
            DtNewRow("IdUser") = TxtIdUser
            DtNewRow("IdModulo") = TxtIdModulo
            DtNewRow("IdPerfil") = 0
            DtNewRow("Perfil") = ""
            DtNewRow("IdOpcion") = TxtIdOpcion
            DtNewRow("Opcion") = ""
            DtNewRow("IdObjeto") = 0
            DtNewRow("Objeto") = ""
            DtNewRow("Error") = ex.Message.Trim()
            DtResultError.Rows.Add(DtNewRow)

            Return DtResultError
        End Try
    End Function


    Public Function GetSecurityControl(ByVal dt As DataTable, ByVal op As String) As Boolean
        Try
            Dim FileRows As DataRow()
            FileRows = dt.[Select](op)

            If Trim(op) = "Error<>''" Then 'Si existe algún error
                If FileRows.Length >= 1 Then
                    GetSecurityControl = True
                Else
                    GetSecurityControl = False
                End If
            Else
                If FileRows.Length = 0 Then 'Resto de las opciones
                    GetSecurityControl = False
                Else
                    GetSecurityControl = True
                End If
            End If

            Return GetSecurityControl

        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function SortingPageIndex(ByVal dt As DataTable, ByVal ColumnSorting As String, ByVal TypeSorting As String) As DataView
        Dim dv As New DataView(dt)
        If Trim(ColumnSorting) <> "" Then dv.Sort = ColumnSorting & " " & TypeSorting

        'Esto es para que si viene el NroFila con el ROW_NUMBER desde SQL, ordena el número en el metodo de ordenación
        If dv.Table.Columns.Contains("NroFila") Then
            Dim i As Integer = 0
            For Each drv As DataRowView In dv
                i += 1
                drv("NroFila") = i
            Next
        End If

        Return dv
    End Function

End Module
