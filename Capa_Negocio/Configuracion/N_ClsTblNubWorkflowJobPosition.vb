Imports Capa_Dato
Imports Capa_Entidad

Public Class N_ClsTblNubWorkflowJobPosition

	Public Function SelectRecord(ByVal E_ObjNubWorkflowJobPosition As E_ClsTblNubWorkflowJobPosition) As E_ClsTblNubWorkflowJobPosition
		Dim D_ObjNubWorkflowJobPosition As New D_ClsTblNubWorkflowJobPosition
		Dim dt As New DataTable
		dt = D_ObjNubWorkflowJobPosition.SelectRecord(E_ObjNubWorkflowJobPosition).Tables(0)
		If (dt.Rows.Count > 0) Then
			If dt.Rows(0).Item("IdWorkflow") Is DBNull.Value Then
				E_ObjNubWorkflowJobPosition.IdWorkflow = 0
			Else
				E_ObjNubWorkflowJobPosition.IdWorkflow = dt.Rows(0).Item("IdWorkflow")
			End If

			If dt.Rows(0).Item("IdJobPosition") Is DBNull.Value Then
				E_ObjNubWorkflowJobPosition.IdJobPosition = 0
			Else
				E_ObjNubWorkflowJobPosition.IdJobPosition = dt.Rows(0).Item("IdJobPosition")
			End If

			If dt.Rows(0).Item("PosicionRuta") Is DBNull.Value Then
				E_ObjNubWorkflowJobPosition.PosicionRuta = 0
			Else
				E_ObjNubWorkflowJobPosition.PosicionRuta = dt.Rows(0).Item("PosicionRuta")
			End If

			If dt.Rows(0).Item("LastTransactionUser") Is DBNull.Value Then
				E_ObjNubWorkflowJobPosition.LastTransactionUser = ""
			Else
				E_ObjNubWorkflowJobPosition.LastTransactionUser = dt.Rows(0).Item("LastTransactionUser")
			End If

			If dt.Rows(0).Item("LastTransactionDate") Is DBNull.Value Then
			Else
				E_ObjNubWorkflowJobPosition.LastTransactionDate = dt.Rows(0).Item("LastTransactionDate")
			End If

		Else
			E_ObjNubWorkflowJobPosition = Nothing
		End If

		Return E_ObjNubWorkflowJobPosition
	End Function

	Public Function SelectAll(ByVal IdWorkFlow As Integer) As DataTable
		Dim D_ObjNubWorkflowJobPosition As New D_ClsTblNubWorkflowJobPosition
		Return D_ObjNubWorkflowJobPosition.SelectAll(IdWorkFlow).Tables(0)
	End Function

	Public Function Insert(ByVal E_ObjNubWorkflowJobPosition As E_ClsTblNubWorkflowJobPosition) As String
		Dim D_ObjNubWorkflowJobPosition As New D_ClsTblNubWorkflowJobPosition
		Return D_ObjNubWorkflowJobPosition.Insert(E_ObjNubWorkflowJobPosition)
	End Function

	Public Function Update(ByVal E_ObjNubWorkflowJobPosition As E_ClsTblNubWorkflowJobPosition) As String
		Dim D_ObjNubWorkflowJobPosition As New D_ClsTblNubWorkflowJobPosition
		Return D_ObjNubWorkflowJobPosition.Update(E_ObjNubWorkflowJobPosition)
	End Function

	Public Function Delete(ByVal E_ObjNubWorkflowJobPosition As E_ClsTblNubWorkflowJobPosition) As String
		Dim D_ObjNubWorkflowJobPosition As New D_ClsTblNubWorkflowJobPosition
		Return D_ObjNubWorkflowJobPosition.Delete(E_ObjNubWorkflowJobPosition)
	End Function


End Class

