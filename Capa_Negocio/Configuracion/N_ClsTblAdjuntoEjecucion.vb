﻿Imports Capa_Dato
Imports Capa_Entidad
Public Class N_ClsTblAdjuntoEjecucion
    Public Function SelectAll(ByVal E_ObjAdjunto As E_ClsTblAdjuntoEjecucion) As DataTable
        Dim D_ObjAdjunto As New D_ClsTblAdjuntoEjecucion
        Return D_ObjAdjunto.SelectAll(E_ObjAdjunto).Tables(0)
    End Function
    Public Function Insert(ByVal E_ObjAdjunto As E_ClsTblAdjuntoEjecucion) As String
        Dim D_ObjAdjunto As New D_ClsTblAdjuntoEjecucion
        Return D_ObjAdjunto.Insert(E_ObjAdjunto)
    End Function
    Public Function Delete(ByVal E_ObjAdjunto As E_ClsTblAdjuntoEjecucion) As String
        Dim D_ObjAdjunto As New D_ClsTblAdjuntoEjecucion
        Return D_ObjAdjunto.Delete(E_ObjAdjunto)
    End Function
    Public Function GetAdjunto(ByVal E_ObjAdjunto As E_ClsTblAdjuntoEjecucion) As Byte()
        Dim D_ObjAdjunto As New D_ClsTblAdjuntoEjecucion
        Return D_ObjAdjunto.GetAdjunto(E_ObjAdjunto)
    End Function

    Public Function SelectAll_Temp(ByVal E_ObjAdjunto As E_ClsTblAdjuntoEjecucion) As DataTable
        Dim D_ObjAdjunto As New D_ClsTblAdjuntoEjecucion
        Return D_ObjAdjunto.SelectAll_Temp(E_ObjAdjunto).Tables(0)
    End Function
    Public Function Insert_Temp(ByVal E_ObjAdjunto As E_ClsTblAdjuntoEjecucion) As String
        Dim D_ObjAdjunto As New D_ClsTblAdjuntoEjecucion
        Return D_ObjAdjunto.Insert_Temp(E_ObjAdjunto)
    End Function
    Public Function Delete_Temp(ByVal E_ObjAdjunto As E_ClsTblAdjuntoEjecucion) As String
        Dim D_ObjAdjunto As New D_ClsTblAdjuntoEjecucion
        Return D_ObjAdjunto.Delete_Temp(E_ObjAdjunto)
    End Function
    Public Function GetAdjuntoTemp(ByVal E_ObjAdjunto As E_ClsTblAdjuntoEjecucion) As Byte()
        Dim D_ObjAdjunto As New D_ClsTblAdjuntoEjecucion
        Return D_ObjAdjunto.GetAdjuntoTemp(E_ObjAdjunto)
    End Function
End Class
