Imports Capa_Dato
Imports Capa_Entidad

Public Class N_ClsTblEjecucion

	Public Function SelectRecord(ByVal E_ObjEjecucion As E_ClsTblEjecucion) As E_ClsTblEjecucion
		Dim D_ObjEjecucion As New D_ClsTblEjecucion
		Dim dt As New DataTable
		dt = D_ObjEjecucion.SelectRecord(E_ObjEjecucion).Tables(0)
		If (dt.Rows.Count > 0) Then
			If dt.Rows(0).Item("IdEjecucion") Is DBNull.Value Then
				E_ObjEjecucion.IdEjecucion = 0
			Else
				E_ObjEjecucion.IdEjecucion = dt.Rows(0).Item("IdEjecucion")
			End If

			If dt.Rows(0).Item("IdEjecucionPadre") Is DBNull.Value Then
				E_ObjEjecucion.IdEjecucionPadre = 0
			Else
				E_ObjEjecucion.IdEjecucionPadre = dt.Rows(0).Item("IdEjecucionPadre")
			End If


			If dt.Rows(0).Item("UltAct") Is DBNull.Value Then
				E_ObjEjecucion.UltAct = ""
			Else
				E_ObjEjecucion.UltAct = dt.Rows(0).Item("UltAct")
			End If


			If dt.Rows(0).Item("Owner") Is DBNull.Value Then
				E_ObjEjecucion.Owner = ""
			Else
				E_ObjEjecucion.Owner = dt.Rows(0).Item("Owner")
			End If

			If dt.Rows(0).Item("FechaCreacion") Is DBNull.Value Then
				E_ObjEjecucion.FechaCreacion = "19000101"
			Else
				E_ObjEjecucion.FechaCreacion = dt.Rows(0).Item("FechaCreacion")
			End If



			If dt.Rows(0).Item("IdEstado") Is DBNull.Value Then
				E_ObjEjecucion.IdEstado = 0
			Else
				E_ObjEjecucion.IdEstado = dt.Rows(0).Item("IdEstado")
			End If

			If dt.Rows(0).Item("Titulo") Is DBNull.Value Then
				E_ObjEjecucion.Titulo = ""
			Else
				E_ObjEjecucion.Titulo = dt.Rows(0).Item("Titulo")
			End If

			If dt.Rows(0).Item("TipoEjecucion") Is DBNull.Value Then
				E_ObjEjecucion.TipoEjecucion = ""
			Else
				E_ObjEjecucion.TipoEjecucion = dt.Rows(0).Item("TipoEjecucion")
			End If

			If dt.Rows(0).Item("FechaHoraProgramada") Is DBNull.Value Then
				E_ObjEjecucion.FechaHoraProgramada = "19000101"
			Else
				E_ObjEjecucion.FechaHoraProgramada = dt.Rows(0).Item("FechaHoraProgramada")
			End If

			If dt.Rows(0).Item("FechaHoraTermino") Is DBNull.Value Then
				E_ObjEjecucion.FechaHoraTermino = "1900-01-01 00:00"
			Else
				E_ObjEjecucion.FechaHoraTermino = dt.Rows(0).Item("FechaHoraTermino")
			End If



			If dt.Rows(0).Item("FechaHoraReal") Is DBNull.Value Then
				E_ObjEjecucion.FechaHoraReal = "1900-01-01 00:00"
			Else
				E_ObjEjecucion.FechaHoraReal = dt.Rows(0).Item("FechaHoraReal")
			End If

			If dt.Rows(0).Item("LastTransactionUser") Is DBNull.Value Then
				E_ObjEjecucion.LastTransactionUser = ""
			Else
				E_ObjEjecucion.LastTransactionUser = dt.Rows(0).Item("LastTransactionUser")
			End If

		Else
			E_ObjEjecucion = Nothing
		End If

		Return E_ObjEjecucion
	End Function

	Public Function SelectAll(ByVal TxtBsc As String) As DataTable
		Dim D_ObjEjecucion As New D_ClsTblEjecucion
		Return D_ObjEjecucion.SelectAll(TxtBsc).Tables(0)
	End Function

	Public Function SelectAllChild(ByVal IdEjecucion As Integer) As DataTable
		Dim D_ObjEjecucion As New D_ClsTblEjecucion
		Return D_ObjEjecucion.SelectAllChild(IdEjecucion).Tables(0)
	End Function

	Public Function EjecutaCritica(ByVal IdEjecucion As Integer) As String
		Dim D_ObjEjecucion As New D_ClsTblEjecucion
		Return D_ObjEjecucion.EjecutaCritica(IdEjecucion)
	End Function




	Public Function Insert(ByVal E_ObjEjecucion As E_ClsTblEjecucion) As String
		Dim D_ObjEjecucion As New D_ClsTblEjecucion
		Return D_ObjEjecucion.Insert(E_ObjEjecucion)
	End Function

	Public Function Update(ByVal E_ObjEjecucion As E_ClsTblEjecucion) As String
		Dim D_ObjEjecucion As New D_ClsTblEjecucion
		Return D_ObjEjecucion.Update(E_ObjEjecucion)
	End Function

	Public Function Delete(ByVal E_ObjEjecucion As E_ClsTblEjecucion) As String
		Dim D_ObjEjecucion As New D_ClsTblEjecucion
		Return D_ObjEjecucion.Delete(E_ObjEjecucion)
	End Function


End Class

