Imports Capa_Dato
Imports Capa_Entidad
Public Class N_ClsTblCarpeta
    Public Function GetCarpeta(ByVal E_ObjCarpeta As E_ClsTblCarpeta) As E_ClsTblCarpeta
        Dim dt As New DataTable
        Dim D_ObjCarpeta As New D_ClsTblCarpeta
        dt = D_ObjCarpeta.GetCarpeta(E_ObjCarpeta).Tables(0)

        If (dt.Rows.Count() > 0) Then
            If dt.Rows(0).Item("IdCarpeta") Is DBNull.Value Then
                E_ObjCarpeta.IdCarpeta = 0
            Else
                E_ObjCarpeta.IdCarpeta = dt.Rows(0).Item("IdCarpeta")
            End If

            If dt.Rows(0).Item("IdCarpetaPadre") Is DBNull.Value Then
                E_ObjCarpeta.IdCarpetaPadre = 0
            Else
                E_ObjCarpeta.IdCarpetaPadre = dt.Rows(0).Item("IdCarpetaPadre")
            End If

            If dt.Rows(0).Item("Carpeta") Is DBNull.Value Then
                E_ObjCarpeta.Carpeta = ""
            Else
                E_ObjCarpeta.Carpeta = dt.Rows(0).Item("Carpeta")
            End If

            If dt.Rows(0).Item("UltAct") Is DBNull.Value Then
                E_ObjCarpeta.UltAct = ""
            Else
                E_ObjCarpeta.UltAct = dt.Rows(0).Item("UltAct")
            End If



            If dt.Rows(0).Item("StoreProcedure") Is DBNull.Value Then
                E_ObjCarpeta.StoreProcedure = ""
            Else
                E_ObjCarpeta.StoreProcedure = dt.Rows(0).Item("StoreProcedure")
            End If

            If dt.Rows(0).Item("Nivel") Is DBNull.Value Then
                E_ObjCarpeta.Nivel = 0
            Else
                E_ObjCarpeta.Nivel = dt.Rows(0).Item("Nivel")
            End If

            If dt.Rows(0).Item("Confidencial") Is DBNull.Value Then
                E_ObjCarpeta.Confidencial = False
            Else
                E_ObjCarpeta.Confidencial = dt.Rows(0).Item("Confidencial")
            End If

            If dt.Rows(0).Item("Contenedora") Is DBNull.Value Then
                E_ObjCarpeta.Contenedora = False
            Else
                E_ObjCarpeta.Contenedora = dt.Rows(0).Item("Contenedora")
            End If


            If dt.Rows(0).Item("Estado") Is DBNull.Value Then
                E_ObjCarpeta.Estado = "N"
            Else
                E_ObjCarpeta.Estado = dt.Rows(0).Item("Estado")
            End If


            If dt.Rows(0).Item("cantHijos") Is DBNull.Value Then
                E_ObjCarpeta.cantHijos = dt.Rows(0).Item("cantHijos")
            Else
                E_ObjCarpeta.cantHijos = dt.Rows(0).Item("cantHijos")
            End If



        End If

        Return E_ObjCarpeta

    End Function
    Public Function InsertCarpeta(ByVal E_ObjCarpeta As E_ClsTblCarpeta) As String
        Dim D_ObjCarpeta As New D_ClsTblCarpeta
        Return D_ObjCarpeta.InsertCarpeta(E_ObjCarpeta)
    End Function


    Public Function getSubCarpetaRuta(ByVal E_ObjCarpeta As E_ClsTblCarpeta) As String
        Dim D_ObjCarpeta As New D_ClsTblCarpeta
        Dim subCadena As String = ""

        Dim dt As New DataTable
        subCadena = D_ObjCarpeta.getSubCarpetaRuta(E_ObjCarpeta)

        Return subCadena
    End Function






    Public Function InsertSubCarpeta(ByVal E_ObjCarpeta As E_ClsTblCarpeta) As String
        Dim D_ObjCarpeta As New D_ClsTblCarpeta
        Return D_ObjCarpeta.InsertSubCarpeta(E_ObjCarpeta)
    End Function
    Public Function Update(ByVal E_ObjCarpeta As E_ClsTblCarpeta) As String
        Dim D_ObjCarpeta As New D_ClsTblCarpeta
        Return D_ObjCarpeta.Update(E_ObjCarpeta)
    End Function
    Public Function Delete(ByVal E_ObjCarpeta As E_ClsTblCarpeta) As String
        Dim D_ObjCarpeta As New D_ClsTblCarpeta
        Return D_ObjCarpeta.Delete(E_ObjCarpeta)
    End Function
    Public Function CarpetasTreeView() As DataTable
        Dim D_ObjCarpeta As New D_ClsTblCarpeta
        Return D_ObjCarpeta.CarpetasTreeView().Tables(0)
    End Function



    Public Function SelectLoadCmb(ByVal IdWorkFlow As String) As DataTable
        Dim D_ObjCarpeta As New D_ClsTblCarpeta
        Return D_ObjCarpeta.SelectLoadCmb(IdWorkFlow).Tables(0)
    End Function

    Public Function GetCarpetasContenedoras() As DataTable

        Dim D_ObjCarpeta As New D_ClsTblCarpeta
        Return D_ObjCarpeta.GetCarpetasContenedoras().Tables(0)

    End Function

    Public Function LoadCarpetasCriticas() As DataTable

        Dim D_ObjCarpeta As New D_ClsTblCarpeta
        Return D_ObjCarpeta.LoadCarpetasCriticas().Tables(0)

    End Function




    Public Function CarpetasTreeViewContenedor() As DataTable
        Dim D_ObjCarpeta As New D_ClsTblCarpeta
        Return D_ObjCarpeta.CarpetasTreeViewContenedor().Tables(0)
    End Function


    Public Function BreadcrumbsFolder(ByVal E_ObjCarpeta As E_ClsTblCarpeta) As String
        Dim D_ObjCarpeta As New D_ClsTblCarpeta
        Return D_ObjCarpeta.BreadcrumbsFolder(E_ObjCarpeta)
    End Function
    Public Function BreadcrumbsFolderSimple(ByVal E_ObjCarpeta As E_ClsTblCarpeta) As String
        Dim D_ObjCarpeta As New D_ClsTblCarpeta
        Return D_ObjCarpeta.BreadcrumbsFolderSimple(E_ObjCarpeta)
    End Function

End Class
