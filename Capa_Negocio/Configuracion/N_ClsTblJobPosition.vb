Imports Capa_Dato
Imports Capa_Entidad

Public Class N_ClsTblJobPosition

	Public Function SelectRecord(ByVal E_ObjJobPosition As E_ClsTblJobPosition) As E_ClsTblJobPosition
		Dim D_ObjJobPosition As New D_ClsTblJobPosition
		Dim dt As New DataTable
		dt = D_ObjJobPosition.SelectRecord(E_ObjJobPosition).Tables(0)
		If (dt.Rows.Count > 0) Then
			If dt.Rows(0).Item("IdJobPosition") Is DBNull.Value Then
				E_ObjJobPosition.IdJobPosition = 0
			Else
				E_ObjJobPosition.IdJobPosition = dt.Rows(0).Item("IdJobPosition")
			End If

			If dt.Rows(0).Item("JobPosition") Is DBNull.Value Then
				E_ObjJobPosition.JobPosition = ""
			Else
				E_ObjJobPosition.JobPosition = dt.Rows(0).Item("JobPosition")
			End If

			If dt.Rows(0).Item("UltAct") Is DBNull.Value Then
				E_ObjJobPosition.UltAct = ""
			Else
				E_ObjJobPosition.UltAct = dt.Rows(0).Item("UltAct")
			End If




			If dt.Rows(0).Item("LineManeger") Is DBNull.Value Then
				E_ObjJobPosition.LineManeger = ""
			Else
				E_ObjJobPosition.LineManeger = dt.Rows(0).Item("LineManeger")
			End If

			If dt.Rows(0).Item("Estado") Is DBNull.Value Then
				E_ObjJobPosition.Estado = 0
			Else
				E_ObjJobPosition.Estado = dt.Rows(0).Item("Estado")
			End If

			If dt.Rows(0).Item("LastTransactionUser") Is DBNull.Value Then
				E_ObjJobPosition.LastTransactionUser = ""
			Else
				E_ObjJobPosition.LastTransactionUser = dt.Rows(0).Item("LastTransactionUser")
			End If

			If dt.Rows(0).Item("LastTransactionDate") Is DBNull.Value Then
				E_ObjJobPosition.LastTransactionDate = "19000101"
			Else
				E_ObjJobPosition.LastTransactionDate = dt.Rows(0).Item("LastTransactionDate")
			End If

		Else
			E_ObjJobPosition = Nothing
		End If

		Return E_ObjJobPosition
	End Function

	Public Function SelectAll(ByVal TxtBsc As String) As DataTable
		Dim D_ObjJobPosition As New D_ClsTblJobPosition
		Return D_ObjJobPosition.SelectAll(TxtBsc).Tables(0)
	End Function

	Public Function SelecAllCboWorkFlow(ByVal IdWorkFlow As String) As DataTable
		Dim D_ObjJobPosition As New D_ClsTblJobPosition
		Return D_ObjJobPosition.SelecAllCboWorkFlow(IdWorkFlow).Tables(0)
	End Function



	Public Function Insert(ByVal E_ObjJobPosition As E_ClsTblJobPosition) As String
		Dim D_ObjJobPosition As New D_ClsTblJobPosition
		Return D_ObjJobPosition.Insert(E_ObjJobPosition)
	End Function

	Public Function Update(ByVal E_ObjJobPosition As E_ClsTblJobPosition) As String
		Dim D_ObjJobPosition As New D_ClsTblJobPosition
		Return D_ObjJobPosition.Update(E_ObjJobPosition)
	End Function

	Public Function Delete(ByVal E_ObjJobPosition As E_ClsTblJobPosition) As String
		Dim D_ObjJobPosition As New D_ClsTblJobPosition
		Return D_ObjJobPosition.Delete(E_ObjJobPosition)
	End Function


End Class

