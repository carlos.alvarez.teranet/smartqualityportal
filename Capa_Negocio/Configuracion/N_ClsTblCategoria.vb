﻿Imports Capa_Dato
Imports Capa_Entidad
Public Class N_ClsTblCategoria
    Public Function GetRegistro(ByVal E_ObjCategoria As E_ClsTblCategoria) As E_ClsTblCategoria
        Dim dt As New DataTable
        Dim D_ObjCategoria As New D_ClsTblCategoria
        dt = D_ObjCategoria.GetRegistro(E_ObjCategoria).Tables(0)

        If (dt.Rows.Count() > 0) Then
            If dt.Rows(0).Item("IdCategoria") Is DBNull.Value Then
                E_ObjCategoria.IdCategoria = 0
            Else
                E_ObjCategoria.IdCategoria = dt.Rows(0).Item("IdCategoria")
            End If

            If dt.Rows(0).Item("Categoria") Is DBNull.Value Then
                E_ObjCategoria.Categoria = ""
            Else
                E_ObjCategoria.Categoria = dt.Rows(0).Item("Categoria")
            End If

            If dt.Rows(0).Item("UltAct") Is DBNull.Value Then
                E_ObjCategoria.Out_UltAct = ""
            Else
                E_ObjCategoria.Out_UltAct = dt.Rows(0).Item("UltAct")
            End If
        End If

        Return E_ObjCategoria
    End Function
    Public Function SelectAll(ByVal TxtBsc As String) As DataTable
        Dim D_ObjCategoria As New D_ClsTblCategoria
        Return D_ObjCategoria.SelectAll(TxtBsc).Tables(0)
    End Function
    Public Function Insert(ByVal E_ObjCategoria As E_ClsTblCategoria) As String
        Dim D_ObjCategoria As New D_ClsTblCategoria
        Return D_ObjCategoria.Insert(E_ObjCategoria)
    End Function
    Public Function Update(ByVal E_ObjCategoria As E_ClsTblCategoria) As String
        Dim D_ObjCategoria As New D_ClsTblCategoria
        Return D_ObjCategoria.Update(E_ObjCategoria)
    End Function
    Public Function Delete(ByVal E_ObjCategoria As E_ClsTblCategoria) As String
        Dim D_ObjCategoria As New D_ClsTblCategoria
        Return D_ObjCategoria.Delete(E_ObjCategoria)
    End Function
    Public Function LoadCbo() As DataTable
        Dim D_ObjCategoria As New D_ClsTblCategoria
        Return D_ObjCategoria.LoadCbo().Tables(0)
    End Function
End Class
