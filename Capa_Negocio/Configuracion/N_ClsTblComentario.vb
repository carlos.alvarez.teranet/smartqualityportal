Imports Capa_Dato
Imports Capa_Entidad
Public Class N_ClsTblComentario
    Public Function SelectAll(ByVal E_ObjComentario As E_ClsTblComentario) As DataTable
        Dim D_ObjComentario As New D_ClsTblComentario
        Return D_ObjComentario.SelectAll(E_ObjComentario).Tables(0)
    End Function
    Public Function Insert(ByVal E_ObjComentario As E_ClsTblComentario) As String
        Dim D_ObjComentario As New D_ClsTblComentario
        Return D_ObjComentario.Insert(E_ObjComentario)
    End Function
    Public Function Delete(ByVal E_ObjComentario As E_ClsTblComentario) As String
        Dim D_ObjComentario As New D_ClsTblComentario
        Return D_ObjComentario.Delete(E_ObjComentario)
    End Function

    Public Function SelectAll_Temp(ByVal E_ObjComentario As E_ClsTblComentario) As DataTable
        Dim D_ObjComentario As New D_ClsTblComentario
        Return D_ObjComentario.SelectAll_Temp(E_ObjComentario).Tables(0)
    End Function
    Public Function Insert_Temp(ByVal E_ObjComentario As E_ClsTblComentario) As String
        Dim D_ObjComentario As New D_ClsTblComentario
        Return D_ObjComentario.Insert_Temp(E_ObjComentario)
    End Function
    Public Function Delete_Temp(ByVal E_ObjComentario As E_ClsTblComentario) As String
        Dim D_ObjComentario As New D_ClsTblComentario
        Return D_ObjComentario.Delete_Temp(E_ObjComentario)
    End Function
End Class
