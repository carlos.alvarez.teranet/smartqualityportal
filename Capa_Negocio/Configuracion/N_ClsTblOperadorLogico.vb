Imports Capa_Dato
Imports Capa_Entidad

Public Class N_ClsTblOperadorLogico

	Public Function SelectRecord(ByVal E_ObjOperadorLogico As E_ClsTblOperadorLogico) As E_ClsTblOperadorLogico
		Dim D_ObjOperadorLogico As New D_ClsTblOperadorLogico
		Dim dt As New DataTable
		dt = D_ObjOperadorLogico.SelectRecord(E_ObjOperadorLogico).Tables(0)
		If (dt.Rows.Count > 0) Then
			If dt.Rows(0).Item("IdOperadorLogico") Is DBNull.Value Then
				E_ObjOperadorLogico.IdOperadorLogico = 0
			Else
				E_ObjOperadorLogico.IdOperadorLogico = dt.Rows(0).Item("IdOperadorLogico")
			End If

			If dt.Rows(0).Item("OperadorLogico") Is DBNull.Value Then
				E_ObjOperadorLogico.OperadorLogico = ""
			Else
				E_ObjOperadorLogico.OperadorLogico = dt.Rows(0).Item("OperadorLogico")
			End If

			If dt.Rows(0).Item("NombreOperadorLogico") Is DBNull.Value Then
				E_ObjOperadorLogico.NombreOperadorLogico = ""
			Else
				E_ObjOperadorLogico.NombreOperadorLogico = dt.Rows(0).Item("NombreOperadorLogico")
			End If

		Else
			E_ObjOperadorLogico = Nothing
		End If

		Return E_ObjOperadorLogico
	End Function
	Public Function LoadCbo(ByVal IdVariableCondicion As Integer) As DataTable
		Dim D_ObjOperadorLogico As New D_ClsTblOperadorLogico
		Return D_ObjOperadorLogico.LoadCbo(IdVariableCondicion).Tables(0)
	End Function
	Public Function SelectAll(ByVal TxtBsc As String) As DataTable
		Dim D_ObjOperadorLogico As New D_ClsTblOperadorLogico
		Return D_ObjOperadorLogico.SelectAll(TxtBsc).Tables(0)
	End Function

	Public Function Insert(ByVal E_ObjOperadorLogico As E_ClsTblOperadorLogico) As String
		Dim D_ObjOperadorLogico As New D_ClsTblOperadorLogico
		Return D_ObjOperadorLogico.Insert(E_ObjOperadorLogico)
	End Function

	Public Function Update(ByVal E_ObjOperadorLogico As E_ClsTblOperadorLogico) As String
		Dim D_ObjOperadorLogico As New D_ClsTblOperadorLogico
		Return D_ObjOperadorLogico.Update(E_ObjOperadorLogico)
	End Function

	Public Function Delete(ByVal E_ObjOperadorLogico As E_ClsTblOperadorLogico) As String
		Dim D_ObjOperadorLogico As New D_ClsTblOperadorLogico
		Return D_ObjOperadorLogico.Delete(E_ObjOperadorLogico)
	End Function


End Class

