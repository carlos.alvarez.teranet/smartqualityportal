Imports Capa_Dato
Imports Capa_Entidad

Public Class N_ClsTblNubCarpetaListaDistribucion

	Public Function SelectRecord(ByVal E_ObjNubCarpetaListaDistribucion As E_ClsTblNubCarpetaListaDistribucion) As E_ClsTblNubCarpetaListaDistribucion
		Dim D_ObjNubCarpetaListaDistribucion As New D_ClsTblNubCarpetaListaDistribucion
		Dim dt As New DataTable
		dt = D_ObjNubCarpetaListaDistribucion.SelectRecord(E_ObjNubCarpetaListaDistribucion).Tables(0)
		If (dt.Rows.Count > 0) Then
			If dt.Rows(0).Item("IdCarpeta") Is DBNull.Value Then
				E_ObjNubCarpetaListaDistribucion.IdCarpeta = 0
			Else
				E_ObjNubCarpetaListaDistribucion.IdCarpeta = dt.Rows(0).Item("IdCarpeta")
			End If

			If dt.Rows(0).Item("IdListaDistribucion") Is DBNull.Value Then
				E_ObjNubCarpetaListaDistribucion.IdListaDistribucion = 0
			Else
				E_ObjNubCarpetaListaDistribucion.IdListaDistribucion = dt.Rows(0).Item("IdListaDistribucion")
			End If

			If dt.Rows(0).Item("LastTransactionUser") Is DBNull.Value Then
				E_ObjNubCarpetaListaDistribucion.LastTransactionUser = ""
			Else
				E_ObjNubCarpetaListaDistribucion.LastTransactionUser = dt.Rows(0).Item("LastTransactionUser")
			End If

			If dt.Rows(0).Item("LastTransactionDate") Is DBNull.Value Then
				E_ObjNubCarpetaListaDistribucion.LastTransactionDate = "19000101"
			Else
				E_ObjNubCarpetaListaDistribucion.LastTransactionDate = dt.Rows(0).Item("LastTransactionDate")
			End If

		Else
			E_ObjNubCarpetaListaDistribucion = Nothing
		End If

		Return E_ObjNubCarpetaListaDistribucion
	End Function

	Public Function SelectAll(ByVal IdCarpeta As Integer) As DataTable
		Dim D_ObjNubCarpetaListaDistribucion As New D_ClsTblNubCarpetaListaDistribucion
		Return D_ObjNubCarpetaListaDistribucion.SelectAll(IdCarpeta).Tables(0)
	End Function

	Public Function Insert(ByVal E_ObjNubCarpetaListaDistribucion As E_ClsTblNubCarpetaListaDistribucion) As String
		Dim D_ObjNubCarpetaListaDistribucion As New D_ClsTblNubCarpetaListaDistribucion
		Return D_ObjNubCarpetaListaDistribucion.Insert(E_ObjNubCarpetaListaDistribucion)
	End Function

	Public Function Update(ByVal E_ObjNubCarpetaListaDistribucion As E_ClsTblNubCarpetaListaDistribucion) As String
		Dim D_ObjNubCarpetaListaDistribucion As New D_ClsTblNubCarpetaListaDistribucion
		Return D_ObjNubCarpetaListaDistribucion.Update(E_ObjNubCarpetaListaDistribucion)
	End Function

	Public Function Delete(ByVal E_ObjNubCarpetaListaDistribucion As E_ClsTblNubCarpetaListaDistribucion) As String
		Dim D_ObjNubCarpetaListaDistribucion As New D_ClsTblNubCarpetaListaDistribucion
		Return D_ObjNubCarpetaListaDistribucion.Delete(E_ObjNubCarpetaListaDistribucion)
	End Function


End Class

