Imports Capa_Dato
Imports Capa_Entidad

Public Class N_ClsTblListaDistribucionUsuario

	Public Function SelectRecord(ByVal E_ObjListaDistribucionUsuario As E_ClsTblListaDistribucionUsuario) As E_ClsTblListaDistribucionUsuario
		Dim D_ObjListaDistribucionUsuario As New D_ClsTblListaDistribucionUsuario
		Dim dt As New DataTable
		dt = D_ObjListaDistribucionUsuario.SelectRecord(E_ObjListaDistribucionUsuario).Tables(0)
		If (dt.Rows.Count > 0) Then
			If dt.Rows(0).Item("IdListaDistribucion") Is DBNull.Value Then
				E_ObjListaDistribucionUsuario.IdListaDistribucion = 0
			Else
				E_ObjListaDistribucionUsuario.IdListaDistribucion = dt.Rows(0).Item("IdListaDistribucion")
			End If

			If dt.Rows(0).Item("IdUsuario") Is DBNull.Value Then
				E_ObjListaDistribucionUsuario.IdUsuario = ""
			Else
				E_ObjListaDistribucionUsuario.IdUsuario = dt.Rows(0).Item("IdUsuario")
			End If

			If dt.Rows(0).Item("LastTransactionUser") Is DBNull.Value Then
				E_ObjListaDistribucionUsuario.LastTransactionUser = ""
			Else
				E_ObjListaDistribucionUsuario.LastTransactionUser = dt.Rows(0).Item("LastTransactionUser")
			End If

			If dt.Rows(0).Item("LastTransactionDate") Is DBNull.Value Then
				E_ObjListaDistribucionUsuario.LastTransactionDate = "19000101"
			Else
				E_ObjListaDistribucionUsuario.LastTransactionDate = dt.Rows(0).Item("LastTransactionDate")
			End If

		Else
			E_ObjListaDistribucionUsuario = Nothing
		End If

		Return E_ObjListaDistribucionUsuario
	End Function

	Public Function SelectAll(ByVal IdListadistribucion As String) As DataTable
		Dim D_ObjListaDistribucionUsuario As New D_ClsTblListaDistribucionUsuario
		Return D_ObjListaDistribucionUsuario.SelectAll(IdListadistribucion).Tables(0)
	End Function

	Public Function SelectAllbyListaDistribucion(ByVal IdListaDistribucion As Integer) As DataTable
		Dim D_ObjListaDistribucionUsuario As New D_ClsTblListaDistribucionUsuario
		Return D_ObjListaDistribucionUsuario.SelectAllbyListaDistribucion(IdListaDistribucion).Tables(0)
	End Function

	Public Function getUsuariosNoSeleccionados(ByVal IdListaDistribucion As Integer) As DataTable
		Dim D_ObjListaDistribucionUsuario As New D_ClsTblListaDistribucionUsuario
		Return D_ObjListaDistribucionUsuario.getUsuariosNoSeleccionados(IdListaDistribucion).Tables(0)
	End Function





	Public Function Insert(ByVal E_ObjListaDistribucionUsuario As E_ClsTblListaDistribucionUsuario) As String
		Dim D_ObjListaDistribucionUsuario As New D_ClsTblListaDistribucionUsuario
		Return D_ObjListaDistribucionUsuario.Insert(E_ObjListaDistribucionUsuario)
	End Function

	Public Function Update(ByVal E_ObjListaDistribucionUsuario As E_ClsTblListaDistribucionUsuario) As String
		Dim D_ObjListaDistribucionUsuario As New D_ClsTblListaDistribucionUsuario
		Return D_ObjListaDistribucionUsuario.Update(E_ObjListaDistribucionUsuario)
	End Function

	Public Function Delete(ByVal E_ObjListaDistribucionUsuario As E_ClsTblListaDistribucionUsuario) As String
		Dim D_ObjListaDistribucionUsuario As New D_ClsTblListaDistribucionUsuario
		Return D_ObjListaDistribucionUsuario.Delete(E_ObjListaDistribucionUsuario)
	End Function


End Class

