Imports Capa_Dato
Imports Capa_Entidad

Public Class N_ClsTblCritica

	Public Function SelectRecord(ByVal E_ObjCritica As E_ClsTblCritica) As E_ClsTblCritica
		Dim D_ObjCritica As New D_ClsTblCritica
		Dim dt As New DataTable
		dt = D_ObjCritica.SelectRecord(E_ObjCritica).Tables(0)
		If (dt.Rows.Count > 0) Then
			If dt.Rows(0).Item("IdCritica") Is DBNull.Value Then
				E_ObjCritica.IdCritica = 0
			Else
				E_ObjCritica.IdCritica = dt.Rows(0).Item("IdCritica")
			End If

			If dt.Rows(0).Item("IdCarpeta") Is DBNull.Value Then
				E_ObjCritica.IdCarpeta = 0
			Else
				E_ObjCritica.IdCarpeta = dt.Rows(0).Item("IdCarpeta")
			End If

			If dt.Rows(0).Item("IdPrefijo") Is DBNull.Value Then
				E_ObjCritica.IdPrefijo = 0
			Else
				E_ObjCritica.IdPrefijo = dt.Rows(0).Item("IdPrefijo")
			End If

			If dt.Rows(0).Item("Correlativo") Is DBNull.Value Then
				E_ObjCritica.Correlativo = 0
			Else
				E_ObjCritica.Correlativo = dt.Rows(0).Item("Correlativo")
			End If

			If dt.Rows(0).Item("CorrelativoAux") Is DBNull.Value Then
				E_ObjCritica.CorrelativoAux = ""
			Else
				E_ObjCritica.CorrelativoAux = dt.Rows(0).Item("CorrelativoAux")
			End If

			If dt.Rows(0).Item("EstadoAux") Is DBNull.Value Then
				E_ObjCritica.EstadoAux = ""
			Else
				E_ObjCritica.EstadoAux = dt.Rows(0).Item("EstadoAux")
			End If

			If dt.Rows(0).Item("Version") Is DBNull.Value Then
				E_ObjCritica.Version = 0
			Else
				E_ObjCritica.Version = dt.Rows(0).Item("Version")
			End If

			If dt.Rows(0).Item("IdEstado") Is DBNull.Value Then
				E_ObjCritica.IdEstado = 0
			Else
				E_ObjCritica.IdEstado = dt.Rows(0).Item("IdEstado")
			End If

			If dt.Rows(0).Item("Owner") Is DBNull.Value Then
				E_ObjCritica.Owner = ""
			Else
				E_ObjCritica.Owner = dt.Rows(0).Item("Owner")
			End If

			If dt.Rows(0).Item("FechaCreacion") Is DBNull.Value Then
				E_ObjCritica.FechaCreacion = "19000101"
			Else
				E_ObjCritica.FechaCreacion = dt.Rows(0).Item("FechaCreacion")
			End If

			If dt.Rows(0).Item("FechaRevision") Is DBNull.Value Then
				E_ObjCritica.FechaRevision = "19000101"
			Else
				E_ObjCritica.FechaRevision = dt.Rows(0).Item("FechaRevision")
			End If

			If dt.Rows(0).Item("IdWorkflow") Is DBNull.Value Then
				E_ObjCritica.IdWorkflow = 0
			Else
				E_ObjCritica.IdWorkflow = dt.Rows(0).Item("IdWorkflow")
			End If

			If dt.Rows(0).Item("Justificacion") Is DBNull.Value Then
				E_ObjCritica.Justificacion = ""
			Else
				E_ObjCritica.Justificacion = dt.Rows(0).Item("Justificacion")
			End If

			If dt.Rows(0).Item("IdVariableAccion") Is DBNull.Value Then
				E_ObjCritica.IdVariableAccion = 0
			Else
				E_ObjCritica.IdVariableAccion = dt.Rows(0).Item("IdVariableAccion")
			End If

			If dt.Rows(0).Item("VariableAccionValor") Is DBNull.Value Then
				E_ObjCritica.VariableAccionValor = ""
			Else
				E_ObjCritica.VariableAccionValor = dt.Rows(0).Item("VariableAccionValor")
			End If

			If dt.Rows(0).Item("LastTransactionUser") Is DBNull.Value Then
				E_ObjCritica.LastTransactionUser = ""
			Else
				E_ObjCritica.LastTransactionUser = dt.Rows(0).Item("LastTransactionUser")
			End If

			If dt.Rows(0).Item("LastTransactionDate") Is DBNull.Value Then
				E_ObjCritica.LastTransactionDate = "19000101"
			Else
				E_ObjCritica.LastTransactionDate = dt.Rows(0).Item("LastTransactionDate")
			End If

		Else
			E_ObjCritica = Nothing
		End If

		Return E_ObjCritica
	End Function

	Public Function SelectAll(ByVal TxtBsc As String, ByVal IdCarpeta As String) As DataTable
		Dim D_ObjCritica As New D_ClsTblCritica
		Return D_ObjCritica.SelectAll(TxtBsc, IdCarpeta).Tables(0)
	End Function


	Public Function SelectAllPenditeAprobacion(ByVal IdUsuario As String) As DataTable
		Dim D_ObjCritica As New D_ClsTblCritica
		Return D_ObjCritica.SelectAllPenditeAprobacion(IdUsuario).Tables(0)
	End Function

	Public Function getPendientesAprobacion(ByVal IdCritica As Integer) As DataTable
		Dim D_ObjCritica As New D_ClsTblCritica
		Return D_ObjCritica.getPendientesAprobacion(IdCritica).Tables(0)
	End Function




	Public Function getCorrelativoCarpeta(ByVal IdCarpeta As Integer) As String

		Dim D_ObjCritica As New D_ClsTblCritica
		Return D_ObjCritica.getCorrelativoCarpeta(IdCarpeta)
	End Function



	Public Function Insert(ByVal E_ObjCritica As E_ClsTblCritica) As String
		Dim D_ObjCritica As New D_ClsTblCritica
		Return D_ObjCritica.Insert(E_ObjCritica)
	End Function

	Public Function Update(ByVal E_ObjCritica As E_ClsTblCritica) As String
		Dim D_ObjCritica As New D_ClsTblCritica
		Return D_ObjCritica.Update(E_ObjCritica)
	End Function

	Public Function UpdateEstado(ByVal E_ObjCritica As E_ClsTblCritica) As String
		Dim D_ObjCritica As New D_ClsTblCritica
		Return D_ObjCritica.UpdateEstado(E_ObjCritica)
	End Function


	Public Function Delete(ByVal E_ObjCritica As E_ClsTblCritica) As String
		Dim D_ObjCritica As New D_ClsTblCritica
		Return D_ObjCritica.Delete(E_ObjCritica)
	End Function


End Class

