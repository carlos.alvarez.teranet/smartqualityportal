Imports Capa_Dato
Imports Capa_Entidad

Public Class N_ClsTblNubWorkflowCarpeta

	Public Function SelectRecord(ByVal E_ObjNubWorkflowCarpeta As E_ClsTblNubWorkflowCarpeta) As E_ClsTblNubWorkflowCarpeta
		Dim D_ObjNubWorkflowCarpeta As New D_ClsTblNubWorkflowCarpeta
		Dim dt As New DataTable
		dt = D_ObjNubWorkflowCarpeta.SelectRecord(E_ObjNubWorkflowCarpeta).Tables(0)
		If (dt.Rows.Count > 0) Then
			If dt.Rows(0).Item("IdWorkflow") Is DBNull.Value Then
				E_ObjNubWorkflowCarpeta.IdWorkflow = 0
			Else
				E_ObjNubWorkflowCarpeta.IdWorkflow = dt.Rows(0).Item("IdWorkflow")
			End If

			If dt.Rows(0).Item("IdCarpeta") Is DBNull.Value Then
				E_ObjNubWorkflowCarpeta.IdCarpeta = 0
			Else
				E_ObjNubWorkflowCarpeta.IdCarpeta = dt.Rows(0).Item("IdCarpeta")
			End If

			If dt.Rows(0).Item("LastTransactionUser") Is DBNull.Value Then
				E_ObjNubWorkflowCarpeta.LastTransactionUser = ""
			Else
				E_ObjNubWorkflowCarpeta.LastTransactionUser = dt.Rows(0).Item("LastTransactionUser")
			End If

			If dt.Rows(0).Item("LastTransactionDate") Is DBNull.Value Then
			Else
				E_ObjNubWorkflowCarpeta.LastTransactionDate = dt.Rows(0).Item("LastTransactionDate")
			End If

		Else
			E_ObjNubWorkflowCarpeta = Nothing 
		End If

		Return E_ObjNubWorkflowCarpeta
	End Function

	Public Function SelectAll(ByVal IdWorkFlow As String) As DataTable
		Dim D_ObjNubWorkflowCarpeta As New D_ClsTblNubWorkflowCarpeta
		Return D_ObjNubWorkflowCarpeta.SelectAll(IdWorkFlow).Tables(0)
	End Function

	Public Function Insert(ByVal E_ObjNubWorkflowCarpeta As E_ClsTblNubWorkflowCarpeta) As String
		Dim D_ObjNubWorkflowCarpeta As New D_ClsTblNubWorkflowCarpeta
		Return D_ObjNubWorkflowCarpeta.Insert(E_ObjNubWorkflowCarpeta)
	End Function

	Public Function Update(ByVal E_ObjNubWorkflowCarpeta As E_ClsTblNubWorkflowCarpeta) As String
		Dim D_ObjNubWorkflowCarpeta As New D_ClsTblNubWorkflowCarpeta
		Return D_ObjNubWorkflowCarpeta.Update(E_ObjNubWorkflowCarpeta)
	End Function

	Public Function Delete(ByVal E_ObjNubWorkflowCarpeta As E_ClsTblNubWorkflowCarpeta) As String
		Dim D_ObjNubWorkflowCarpeta As New D_ClsTblNubWorkflowCarpeta
		Return D_ObjNubWorkflowCarpeta.Delete(E_ObjNubWorkflowCarpeta)
	End Function


End Class 

