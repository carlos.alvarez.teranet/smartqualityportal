Imports Capa_Dato
Imports Capa_Entidad

Public Class N_ClsTblLogEjecucion

	Public Function SelectRecord(ByVal E_ObjLogEjecucion As E_ClsTblLogEjecucion) As E_ClsTblLogEjecucion
		Dim D_ObjLogEjecucion As New D_ClsTblLogEjecucion
		Dim dt As New DataTable
		dt = D_ObjLogEjecucion.SelectRecord(E_ObjLogEjecucion).Tables(0)
		If (dt.Rows.Count > 0) Then
			If dt.Rows(0).Item("IdLogEjecucion") Is DBNull.Value Then
				E_ObjLogEjecucion.IdLogEjecucion = 0
			Else
				E_ObjLogEjecucion.IdLogEjecucion = dt.Rows(0).Item("IdLogEjecucion")
			End If

			If dt.Rows(0).Item("IdEjecucion") Is DBNull.Value Then
				E_ObjLogEjecucion.IdEjecucion = 0
			Else
				E_ObjLogEjecucion.IdEjecucion = dt.Rows(0).Item("IdEjecucion")
			End If

			If dt.Rows(0).Item("Fecha") Is DBNull.Value Then
				E_ObjLogEjecucion.Fecha = "19000101"
			Else
				E_ObjLogEjecucion.Fecha = dt.Rows(0).Item("Fecha")
			End If

			If dt.Rows(0).Item("IdAccion") Is DBNull.Value Then
				E_ObjLogEjecucion.IdAccion = 0
			Else
				E_ObjLogEjecucion.IdAccion = dt.Rows(0).Item("IdAccion")
			End If

			If dt.Rows(0).Item("Comentario") Is DBNull.Value Then
				E_ObjLogEjecucion.Comentario = ""
			Else
				E_ObjLogEjecucion.Comentario = dt.Rows(0).Item("Comentario")
			End If

			If dt.Rows(0).Item("LastTransactionUser") Is DBNull.Value Then
				E_ObjLogEjecucion.LastTransactionUser = ""
			Else
				E_ObjLogEjecucion.LastTransactionUser = dt.Rows(0).Item("LastTransactionUser")
			End If

			If dt.Rows(0).Item("LastTransactionDate") Is DBNull.Value Then
				E_ObjLogEjecucion.LastTransactionDate = "19000101"
			Else
				E_ObjLogEjecucion.LastTransactionDate = dt.Rows(0).Item("LastTransactionDate")
			End If

		Else
			E_ObjLogEjecucion = Nothing
		End If

		Return E_ObjLogEjecucion
	End Function

	Public Function SelectAll(ByVal IdEjecucion As Integer) As DataTable
		Dim D_ObjLogEjecucion As New D_ClsTblLogEjecucion
		Return D_ObjLogEjecucion.SelectAll(IdEjecucion).Tables(0)
	End Function

	Public Function changeStatusEjecucion(ByVal IdEjecucion As Integer) As DataTable
		Dim D_ObjLogEjecucion As New D_ClsTblLogEjecucion
		Return D_ObjLogEjecucion.changeStatusEjecucion(IdEjecucion).Tables(0)
	End Function



	Public Function Insert(ByVal E_ObjLogEjecucion As E_ClsTblLogEjecucion) As String
		Dim D_ObjLogEjecucion As New D_ClsTblLogEjecucion
		Return D_ObjLogEjecucion.Insert(E_ObjLogEjecucion)
	End Function

	Public Function Update(ByVal E_ObjLogEjecucion As E_ClsTblLogEjecucion) As String
		Dim D_ObjLogEjecucion As New D_ClsTblLogEjecucion
		Return D_ObjLogEjecucion.Update(E_ObjLogEjecucion)
	End Function

	Public Function Delete(ByVal E_ObjLogEjecucion As E_ClsTblLogEjecucion) As String
		Dim D_ObjLogEjecucion As New D_ClsTblLogEjecucion
		Return D_ObjLogEjecucion.Delete(E_ObjLogEjecucion)
	End Function


End Class

