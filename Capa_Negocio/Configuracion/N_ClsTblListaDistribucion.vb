Imports Capa_Dato
Imports Capa_Entidad

Public Class N_ClsTblListaDistribucion

	Public Function SelectRecord(ByVal E_ObjListaDistribucion As E_ClsTblListaDistribucion) As E_ClsTblListaDistribucion
		Dim D_ObjListaDistribucion As New D_ClsTblListaDistribucion
		Dim dt As New DataTable
		dt = D_ObjListaDistribucion.SelectRecord(E_ObjListaDistribucion).Tables(0)
		If (dt.Rows.Count > 0) Then
			If dt.Rows(0).Item("IdListaDistribucion") Is DBNull.Value Then
				E_ObjListaDistribucion.IdListaDistribucion = 0
			Else
				E_ObjListaDistribucion.IdListaDistribucion = dt.Rows(0).Item("IdListaDistribucion")
			End If

			If dt.Rows(0).Item("ListaDistribucion") Is DBNull.Value Then
				E_ObjListaDistribucion.ListaDistribucion = ""
			Else
				E_ObjListaDistribucion.ListaDistribucion = dt.Rows(0).Item("ListaDistribucion")
			End If

			If dt.Rows(0).Item("UltAct") Is DBNull.Value Then
				E_ObjListaDistribucion.UltAct = ""
			Else
				E_ObjListaDistribucion.UltAct = dt.Rows(0).Item("UltAct")
			End If



			If dt.Rows(0).Item("Estado") Is DBNull.Value Then
				E_ObjListaDistribucion.Estado = 0
			Else
				E_ObjListaDistribucion.Estado = dt.Rows(0).Item("Estado")
			End If

			If dt.Rows(0).Item("LastTransactionUser") Is DBNull.Value Then
				E_ObjListaDistribucion.LastTransactionUser = ""
			Else
				E_ObjListaDistribucion.LastTransactionUser = dt.Rows(0).Item("LastTransactionUser")
			End If

			If dt.Rows(0).Item("LastTransactionDate") Is DBNull.Value Then
				E_ObjListaDistribucion.LastTransactionDate = "19000101"
			Else
				E_ObjListaDistribucion.LastTransactionDate = dt.Rows(0).Item("LastTransactionDate")
			End If

		Else
			E_ObjListaDistribucion = Nothing
		End If

		Return E_ObjListaDistribucion
	End Function



	Public Function SelectAll(ByVal TxtBsc As String) As DataTable
		Dim D_ObjListaDistribucion As New D_ClsTblListaDistribucion
		Return D_ObjListaDistribucion.SelectAll(TxtBsc).Tables(0)
	End Function

	Public Function Insert(ByVal E_ObjListaDistribucion As E_ClsTblListaDistribucion) As String
		Dim D_ObjListaDistribucion As New D_ClsTblListaDistribucion
		Return D_ObjListaDistribucion.Insert(E_ObjListaDistribucion)
	End Function

	Public Function Update(ByVal E_ObjListaDistribucion As E_ClsTblListaDistribucion) As String
		Dim D_ObjListaDistribucion As New D_ClsTblListaDistribucion
		Return D_ObjListaDistribucion.Update(E_ObjListaDistribucion)
	End Function

	Public Function delete(ByVal E_ObjListaDistribucion As E_ClsTblListaDistribucion) As String
		Dim D_ObjListaDistribucion As New D_ClsTblListaDistribucion
		Return D_ObjListaDistribucion.Delete(E_ObjListaDistribucion)
	End Function



	Public Function LoadCbo() As DataTable
		Dim D_ObjListaDistribucion As New D_ClsTblListaDistribucion
		Return D_ObjListaDistribucion.LoadCbo().Tables(0)
	End Function

End Class

