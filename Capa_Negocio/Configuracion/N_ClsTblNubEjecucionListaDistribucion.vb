Imports Capa_Dato
Imports Capa_Entidad

Public Class N_ClsTblNubEjecucionListaDistribucion

	Public Function SelectRecord(ByVal E_ObjNubEjecucionListaDistribucion As E_ClsTblNubEjecucionListaDistribucion) As E_ClsTblNubEjecucionListaDistribucion
		Dim D_ObjNubEjecucionListaDistribucion As New D_ClsTblNubEjecucionListaDistribucion
		Dim dt As New DataTable
		dt = D_ObjNubEjecucionListaDistribucion.SelectRecord(E_ObjNubEjecucionListaDistribucion).Tables(0)
		If (dt.Rows.Count > 0) Then
			If dt.Rows(0).Item("IdEjecucion") Is DBNull.Value Then
				E_ObjNubEjecucionListaDistribucion.IdEjecucion = 0
			Else
				E_ObjNubEjecucionListaDistribucion.IdEjecucion = dt.Rows(0).Item("IdEjecucion")
			End If

			If dt.Rows(0).Item("IdListaDistribucion") Is DBNull.Value Then
				E_ObjNubEjecucionListaDistribucion.IdListaDistribucion = 0
			Else
				E_ObjNubEjecucionListaDistribucion.IdListaDistribucion = dt.Rows(0).Item("IdListaDistribucion")
			End If

			If dt.Rows(0).Item("LastTransactionUser") Is DBNull.Value Then
				E_ObjNubEjecucionListaDistribucion.LastTransactionUser = ""
			Else
				E_ObjNubEjecucionListaDistribucion.LastTransactionUser = dt.Rows(0).Item("LastTransactionUser")
			End If

			If dt.Rows(0).Item("LastTransactionDate") Is DBNull.Value Then
				E_ObjNubEjecucionListaDistribucion.LastTransactionDate = "19000101"
			Else
				E_ObjNubEjecucionListaDistribucion.LastTransactionDate = dt.Rows(0).Item("LastTransactionDate")
			End If

		Else
			E_ObjNubEjecucionListaDistribucion = Nothing
		End If

		Return E_ObjNubEjecucionListaDistribucion
	End Function

	Public Function SelectAll(ByVal IdEjecucion As Integer) As DataTable
		Dim D_ObjNubEjecucionListaDistribucion As New D_ClsTblNubEjecucionListaDistribucion
		Return D_ObjNubEjecucionListaDistribucion.SelectAll(IdEjecucion).Tables(0)
	End Function

	Public Function Insert(ByVal E_ObjNubEjecucionListaDistribucion As E_ClsTblNubEjecucionListaDistribucion) As String
		Dim D_ObjNubEjecucionListaDistribucion As New D_ClsTblNubEjecucionListaDistribucion
		Return D_ObjNubEjecucionListaDistribucion.Insert(E_ObjNubEjecucionListaDistribucion)
	End Function

	Public Function Update(ByVal E_ObjNubEjecucionListaDistribucion As E_ClsTblNubEjecucionListaDistribucion) As String
		Dim D_ObjNubEjecucionListaDistribucion As New D_ClsTblNubEjecucionListaDistribucion
		Return D_ObjNubEjecucionListaDistribucion.Update(E_ObjNubEjecucionListaDistribucion)
	End Function

	Public Function Delete(ByVal E_ObjNubEjecucionListaDistribucion As E_ClsTblNubEjecucionListaDistribucion) As String
		Dim D_ObjNubEjecucionListaDistribucion As New D_ClsTblNubEjecucionListaDistribucion
		Return D_ObjNubEjecucionListaDistribucion.Delete(E_ObjNubEjecucionListaDistribucion)
	End Function


End Class

