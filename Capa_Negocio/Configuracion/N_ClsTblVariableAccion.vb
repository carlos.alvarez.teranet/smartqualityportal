Imports Capa_Dato
Imports Capa_Entidad

Public Class N_ClsTblVariableAccion

	Public Function SelectRecord(ByVal E_ObjVariableAccion As E_ClsTblVariableAccion) As E_ClsTblVariableAccion
		Dim D_ObjVariableAccion As New D_ClsTblVariableAccion
		Dim dt As New DataTable
		dt = D_ObjVariableAccion.SelectRecord(E_ObjVariableAccion).Tables(0)
		If (dt.Rows.Count > 0) Then
			If dt.Rows(0).Item("IdVariableAccion") Is DBNull.Value Then
				E_ObjVariableAccion.IdVariableAccion = 0
			Else
				E_ObjVariableAccion.IdVariableAccion = dt.Rows(0).Item("IdVariableAccion")
			End If

			If dt.Rows(0).Item("VariableAccion") Is DBNull.Value Then
				E_ObjVariableAccion.VariableAccion = ""
			Else
				E_ObjVariableAccion.VariableAccion = dt.Rows(0).Item("VariableAccion")
			End If

			If dt.Rows(0).Item("IdTipoDato") Is DBNull.Value Then
				E_ObjVariableAccion.IdTipoDato = 0
			Else
				E_ObjVariableAccion.IdTipoDato = dt.Rows(0).Item("IdTipoDato")
			End If

			If dt.Rows(0).Item("Largo") Is DBNull.Value Then
				E_ObjVariableAccion.Largo = ""
			Else
				E_ObjVariableAccion.Largo = dt.Rows(0).Item("Largo")
			End If

			If dt.Rows(0).Item("Tabla") Is DBNull.Value Then
				E_ObjVariableAccion.Tabla = ""
			Else
				E_ObjVariableAccion.Tabla = dt.Rows(0).Item("Tabla")
			End If

			If dt.Rows(0).Item("Estado") Is DBNull.Value Then
				E_ObjVariableAccion.Estado = 0
			Else
				E_ObjVariableAccion.Estado = dt.Rows(0).Item("Estado")
			End If

			If dt.Rows(0).Item("LastTransactionUser") Is DBNull.Value Then
				E_ObjVariableAccion.LastTransactionUser = ""
			Else
				E_ObjVariableAccion.LastTransactionUser = dt.Rows(0).Item("LastTransactionUser")
			End If

			If dt.Rows(0).Item("LastTransactionDate") Is DBNull.Value Then
				E_ObjVariableAccion.LastTransactionDate = "19000101"
			Else
				E_ObjVariableAccion.LastTransactionDate = dt.Rows(0).Item("LastTransactionDate")
			End If

			If dt.Rows(0).Item("isComilla") Is DBNull.Value Then
				E_ObjVariableAccion.isComilla = False
			Else
				E_ObjVariableAccion.isComilla = dt.Rows(0).Item("isComilla")
			End If



		Else
			E_ObjVariableAccion = Nothing
		End If

		Return E_ObjVariableAccion
	End Function



	Public Function LoadCbo() As DataTable
		Dim D_ObjVariableAccion As New D_ClsTblVariableAccion
		Return D_ObjVariableAccion.LoadCbo().Tables(0)
	End Function

	Public Function SelectAll(ByVal TxtBsc As String) As DataTable
		Dim D_ObjVariableAccion As New D_ClsTblVariableAccion
		Return D_ObjVariableAccion.SelectAll(TxtBsc).Tables(0)
	End Function

	Public Function Insert(ByVal E_ObjVariableAccion As E_ClsTblVariableAccion) As String
		Dim D_ObjVariableAccion As New D_ClsTblVariableAccion
		Return D_ObjVariableAccion.Insert(E_ObjVariableAccion)
	End Function

	Public Function Update(ByVal E_ObjVariableAccion As E_ClsTblVariableAccion) As String
		Dim D_ObjVariableAccion As New D_ClsTblVariableAccion
		Return D_ObjVariableAccion.Update(E_ObjVariableAccion)
	End Function

	Public Function Delete(ByVal E_ObjVariableAccion As E_ClsTblVariableAccion) As String
		Dim D_ObjVariableAccion As New D_ClsTblVariableAccion
		Return D_ObjVariableAccion.Delete(E_ObjVariableAccion)
	End Function


End Class

