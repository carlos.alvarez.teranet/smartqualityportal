Imports Capa_Dato
Imports Capa_Entidad

Public Class N_ClsTblEjecucionLogMail

	Public Function SelectRecord(ByVal E_ObjEjecucionLogMail As E_ClsTblEjecucionLogMail) As E_ClsTblEjecucionLogMail
		Dim D_ObjEjecucionLogMail As New D_ClsTblEjecucionLogMail
		Dim dt As New DataTable
		dt = D_ObjEjecucionLogMail.SelectRecord(E_ObjEjecucionLogMail).Tables(0)
		If (dt.Rows.Count > 0) Then
			If dt.Rows(0).Item("IdEjecucionLogMail") Is DBNull.Value Then
				E_ObjEjecucionLogMail.IdEjecucionLogMail = 0
			Else
				E_ObjEjecucionLogMail.IdEjecucionLogMail = dt.Rows(0).Item("IdEjecucionLogMail")
			End If

			If dt.Rows(0).Item("IdEjecucion") Is DBNull.Value Then
				E_ObjEjecucionLogMail.IdEjecucion = 0
			Else
				E_ObjEjecucionLogMail.IdEjecucion = dt.Rows(0).Item("IdEjecucion")
			End If

			If dt.Rows(0).Item("Fecha") Is DBNull.Value Then
				E_ObjEjecucionLogMail.Fecha = "19000101"
			Else
				E_ObjEjecucionLogMail.Fecha = dt.Rows(0).Item("Fecha")
			End If

			If dt.Rows(0).Item("Remitente") Is DBNull.Value Then
				E_ObjEjecucionLogMail.Remitente = ""
			Else
				E_ObjEjecucionLogMail.Remitente = dt.Rows(0).Item("Remitente")
			End If

			If dt.Rows(0).Item("Destinatario") Is DBNull.Value Then
				E_ObjEjecucionLogMail.Destinatario = ""
			Else
				E_ObjEjecucionLogMail.Destinatario = dt.Rows(0).Item("Destinatario")
			End If

			If dt.Rows(0).Item("Asunto") Is DBNull.Value Then
				E_ObjEjecucionLogMail.Asunto = ""
			Else
				E_ObjEjecucionLogMail.Asunto = dt.Rows(0).Item("Asunto")
			End If

			If dt.Rows(0).Item("Cuerpo") Is DBNull.Value Then
				E_ObjEjecucionLogMail.Cuerpo = ""
			Else
				E_ObjEjecucionLogMail.Cuerpo = dt.Rows(0).Item("Cuerpo")
			End If

			If dt.Rows(0).Item("Adjunto") Is DBNull.Value Then
				E_ObjEjecucionLogMail.Adjunto = ""
			Else
				E_ObjEjecucionLogMail.Adjunto = dt.Rows(0).Item("Adjunto")
			End If

			If dt.Rows(0).Item("LastTransactionUser") Is DBNull.Value Then
				E_ObjEjecucionLogMail.LastTransactionUser = ""
			Else
				E_ObjEjecucionLogMail.LastTransactionUser = dt.Rows(0).Item("LastTransactionUser")
			End If

			If dt.Rows(0).Item("LastTransactionDate") Is DBNull.Value Then
				E_ObjEjecucionLogMail.LastTransactionDate = "19000101"
			Else
				E_ObjEjecucionLogMail.LastTransactionDate = dt.Rows(0).Item("LastTransactionDate")
			End If

		Else
			E_ObjEjecucionLogMail = Nothing
		End If

		Return E_ObjEjecucionLogMail
	End Function

	Public Function SelectAll(ByVal TxtBsc As String) As DataTable
		Dim D_ObjEjecucionLogMail As New D_ClsTblEjecucionLogMail
		Return D_ObjEjecucionLogMail.SelectAll(TxtBsc).Tables(0)
	End Function

	Public Function Insert(ByVal E_ObjEjecucionLogMail As E_ClsTblEjecucionLogMail) As String
		Dim D_ObjEjecucionLogMail As New D_ClsTblEjecucionLogMail
		Return D_ObjEjecucionLogMail.Insert(E_ObjEjecucionLogMail)
	End Function

	Public Function Update(ByVal E_ObjEjecucionLogMail As E_ClsTblEjecucionLogMail) As String
		Dim D_ObjEjecucionLogMail As New D_ClsTblEjecucionLogMail
		Return D_ObjEjecucionLogMail.Update(E_ObjEjecucionLogMail)
	End Function

	Public Function Delete(ByVal E_ObjEjecucionLogMail As E_ClsTblEjecucionLogMail) As String
		Dim D_ObjEjecucionLogMail As New D_ClsTblEjecucionLogMail
		Return D_ObjEjecucionLogMail.Delete(E_ObjEjecucionLogMail)
	End Function


End Class

