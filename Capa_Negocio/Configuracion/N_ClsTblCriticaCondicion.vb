Imports Capa_Dato
Imports Capa_Entidad

Public Class N_ClsTblCriticaCondicion

	Public Function SelectRecord(ByVal E_ObjCriticaCondicion As E_ClsTblCriticaCondicion) As E_ClsTblCriticaCondicion
		Dim D_ObjCriticaCondicion As New D_ClsTblCriticaCondicion
		Dim dt As New DataTable
		dt = D_ObjCriticaCondicion.SelectRecord(E_ObjCriticaCondicion).Tables(0)
		If (dt.Rows.Count > 0) Then
			If dt.Rows(0).Item("IdCritica") Is DBNull.Value Then
				E_ObjCriticaCondicion.IdCritica = 0
			Else
				E_ObjCriticaCondicion.IdCritica = dt.Rows(0).Item("IdCritica")
			End If

			If dt.Rows(0).Item("Orden") Is DBNull.Value Then
				E_ObjCriticaCondicion.Orden = 0
			Else
				E_ObjCriticaCondicion.Orden = dt.Rows(0).Item("Orden")
			End If

			If dt.Rows(0).Item("IdVariableCondicion") Is DBNull.Value Then
				E_ObjCriticaCondicion.IdVariableCondicion = 0
			Else
				E_ObjCriticaCondicion.IdVariableCondicion = dt.Rows(0).Item("IdVariableCondicion")
			End If

			If dt.Rows(0).Item("OperadorLogico") Is DBNull.Value Then
				E_ObjCriticaCondicion.OperadorLogico = ""
			Else
				E_ObjCriticaCondicion.OperadorLogico = dt.Rows(0).Item("OperadorLogico")
			End If

			If dt.Rows(0).Item("Valor1") Is DBNull.Value Then
				E_ObjCriticaCondicion.Valor1 = ""
			Else
				E_ObjCriticaCondicion.Valor1 = dt.Rows(0).Item("Valor1")
			End If

			If dt.Rows(0).Item("Valor2") Is DBNull.Value Then
				E_ObjCriticaCondicion.Valor2 = ""
			Else
				E_ObjCriticaCondicion.Valor2 = dt.Rows(0).Item("Valor2")
			End If

			If dt.Rows(0).Item("AndOr") Is DBNull.Value Then
				E_ObjCriticaCondicion.AndOr = ""
			Else
				E_ObjCriticaCondicion.AndOr = dt.Rows(0).Item("AndOr")
			End If

			If dt.Rows(0).Item("LastTransactionUser") Is DBNull.Value Then
				E_ObjCriticaCondicion.LastTransactionUser = ""
			Else
				E_ObjCriticaCondicion.LastTransactionUser = dt.Rows(0).Item("LastTransactionUser")
			End If

			If dt.Rows(0).Item("LastTransactionDate") Is DBNull.Value Then
				E_ObjCriticaCondicion.LastTransactionDate = "19000101"
			Else
				E_ObjCriticaCondicion.LastTransactionDate = dt.Rows(0).Item("LastTransactionDate")
			End If

		Else
			E_ObjCriticaCondicion = Nothing
		End If

		Return E_ObjCriticaCondicion
	End Function

	Public Function SelectAll(ByVal TxtBsc As String) As DataTable
		Dim D_ObjCriticaCondicion As New D_ClsTblCriticaCondicion
		Return D_ObjCriticaCondicion.SelectAll(TxtBsc).Tables(0)
	End Function

	Public Function SelectByCritica(ByVal IdCritica As Integer) As DataTable
		Dim D_ObjCriticaCondicion As New D_ClsTblCriticaCondicion
		Return D_ObjCriticaCondicion.SelectByCritica(IdCritica).Tables(0)
	End Function



	Public Function Insert(ByVal E_ObjCriticaCondicion As E_ClsTblCriticaCondicion) As String
		Dim D_ObjCriticaCondicion As New D_ClsTblCriticaCondicion
		Return D_ObjCriticaCondicion.Insert(E_ObjCriticaCondicion)
	End Function

	Public Function Update(ByVal E_ObjCriticaCondicion As E_ClsTblCriticaCondicion) As String
		Dim D_ObjCriticaCondicion As New D_ClsTblCriticaCondicion
		Return D_ObjCriticaCondicion.Update(E_ObjCriticaCondicion)
	End Function

	Public Function Delete(ByVal E_ObjCriticaCondicion As E_ClsTblCriticaCondicion) As String
		Dim D_ObjCriticaCondicion As New D_ClsTblCriticaCondicion
		Return D_ObjCriticaCondicion.Delete(E_ObjCriticaCondicion)
	End Function


End Class

