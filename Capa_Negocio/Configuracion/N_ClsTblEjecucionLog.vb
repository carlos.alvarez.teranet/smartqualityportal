Imports Capa_Dato
Imports Capa_Entidad

Public Class N_ClsTblEjecucionLog

	Public Function SelectRecord(ByVal E_ObjEjecucionLog As E_ClsTblEjecucionLog) As E_ClsTblEjecucionLog
		Dim D_ObjEjecucionLog As New D_ClsTblEjecucionLog
		Dim dt As New DataTable
		dt = D_ObjEjecucionLog.SelectRecord(E_ObjEjecucionLog).Tables(0)
		If (dt.Rows.Count > 0) Then
			If dt.Rows(0).Item("IdEjecucionLog") Is DBNull.Value Then
				E_ObjEjecucionLog.IdEjecucionLog = 0
			Else
				E_ObjEjecucionLog.IdEjecucionLog = dt.Rows(0).Item("IdEjecucionLog")
			End If

			If dt.Rows(0).Item("IdEjecucion") Is DBNull.Value Then
				E_ObjEjecucionLog.IdEjecucion = 0
			Else
				E_ObjEjecucionLog.IdEjecucion = dt.Rows(0).Item("IdEjecucion")
			End If

			If dt.Rows(0).Item("Fecha") Is DBNull.Value Then
				E_ObjEjecucionLog.Fecha = "19000101"
			Else
				E_ObjEjecucionLog.Fecha = dt.Rows(0).Item("Fecha")
			End If

			If dt.Rows(0).Item("IdAccion") Is DBNull.Value Then
				E_ObjEjecucionLog.IdAccion = 0
			Else
				E_ObjEjecucionLog.IdAccion = dt.Rows(0).Item("IdAccion")
			End If

			If dt.Rows(0).Item("Comentario") Is DBNull.Value Then
				E_ObjEjecucionLog.Comentario = ""
			Else
				E_ObjEjecucionLog.Comentario = dt.Rows(0).Item("Comentario")
			End If

			If dt.Rows(0).Item("LastTransactionUser") Is DBNull.Value Then
				E_ObjEjecucionLog.LastTransactionUser = ""
			Else
				E_ObjEjecucionLog.LastTransactionUser = dt.Rows(0).Item("LastTransactionUser")
			End If

			If dt.Rows(0).Item("LastTransactionDate") Is DBNull.Value Then
				E_ObjEjecucionLog.LastTransactionDate = "19000101"
			Else
				E_ObjEjecucionLog.LastTransactionDate = dt.Rows(0).Item("LastTransactionDate")
			End If

		Else
			E_ObjEjecucionLog = Nothing
		End If

		Return E_ObjEjecucionLog
	End Function

	Public Function SelectAll(ByVal TxtBsc As String) As DataTable
		Dim D_ObjEjecucionLog As New D_ClsTblEjecucionLog
		Return D_ObjEjecucionLog.SelectAll(TxtBsc).Tables(0)
	End Function

	Public Function Insert(ByVal E_ObjEjecucionLog As E_ClsTblEjecucionLog) As String
		Dim D_ObjEjecucionLog As New D_ClsTblEjecucionLog
		Return D_ObjEjecucionLog.Insert(E_ObjEjecucionLog)
	End Function

	Public Function Update(ByVal E_ObjEjecucionLog As E_ClsTblEjecucionLog) As String
		Dim D_ObjEjecucionLog As New D_ClsTblEjecucionLog
		Return D_ObjEjecucionLog.Update(E_ObjEjecucionLog)
	End Function

	Public Function Delete(ByVal E_ObjEjecucionLog As E_ClsTblEjecucionLog) As String
		Dim D_ObjEjecucionLog As New D_ClsTblEjecucionLog
		Return D_ObjEjecucionLog.Delete(E_ObjEjecucionLog)
	End Function


End Class

