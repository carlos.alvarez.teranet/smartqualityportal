Imports Capa_Dato
Imports Capa_Entidad

Public Class N_ClsTblEjecucionCritica

	Public Function SelectRecord(ByVal E_ObjEjecucionCritica As E_ClsTblEjecucionCritica) As E_ClsTblEjecucionCritica
		Dim D_ObjEjecucionCritica As New D_ClsTblEjecucionCritica
		Dim dt As New DataTable
		dt = D_ObjEjecucionCritica.SelectRecord(E_ObjEjecucionCritica).Tables(0)
		If (dt.Rows.Count > 0) Then
			If dt.Rows(0).Item("IdEjecucionCritica") Is DBNull.Value Then
				E_ObjEjecucionCritica.IdEjecucionCritica = 0
			Else
				E_ObjEjecucionCritica.IdEjecucionCritica = dt.Rows(0).Item("IdEjecucionCritica")
			End If

			If dt.Rows(0).Item("IdEjecucion") Is DBNull.Value Then
				E_ObjEjecucionCritica.IdEjecucion = 0
			Else
				E_ObjEjecucionCritica.IdEjecucion = dt.Rows(0).Item("IdEjecucion")
			End If

			If dt.Rows(0).Item("KeyCritica") Is DBNull.Value Then
				E_ObjEjecucionCritica.KeyCritica = ""
			Else
				E_ObjEjecucionCritica.KeyCritica = dt.Rows(0).Item("KeyCritica")
			End If

			If dt.Rows(0).Item("TxtCritica") Is DBNull.Value Then
				E_ObjEjecucionCritica.TxtCritica = ""
			Else
				E_ObjEjecucionCritica.TxtCritica = dt.Rows(0).Item("TxtCritica")
			End If

		Else
			E_ObjEjecucionCritica = Nothing
		End If

		Return E_ObjEjecucionCritica
	End Function

	Public Function SelectAll(ByVal TxtBsc As String) As DataTable
		Dim D_ObjEjecucionCritica As New D_ClsTblEjecucionCritica
		Return D_ObjEjecucionCritica.SelectAll(TxtBsc).Tables(0)
	End Function

	Public Function Insert(ByVal E_ObjEjecucionCritica As E_ClsTblEjecucionCritica) As String
		Dim D_ObjEjecucionCritica As New D_ClsTblEjecucionCritica
		Return D_ObjEjecucionCritica.Insert(E_ObjEjecucionCritica)
	End Function

	Public Function Update(ByVal E_ObjEjecucionCritica As E_ClsTblEjecucionCritica) As String
		Dim D_ObjEjecucionCritica As New D_ClsTblEjecucionCritica
		Return D_ObjEjecucionCritica.Update(E_ObjEjecucionCritica)
	End Function

	Public Function Delete(ByVal E_ObjEjecucionCritica As E_ClsTblEjecucionCritica) As String
		Dim D_ObjEjecucionCritica As New D_ClsTblEjecucionCritica
		Return D_ObjEjecucionCritica.Delete(E_ObjEjecucionCritica)
	End Function


End Class

