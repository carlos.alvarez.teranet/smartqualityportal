Imports Capa_Dato
Imports Capa_Entidad

Public Class N_ClsTblLogCritica

	Public Function SelectRecord(ByVal E_ObjLogCritica As E_ClsTblLogCritica) As E_ClsTblLogCritica
		Dim D_ObjLogCritica As New D_ClsTblLogCritica
		Dim dt As New DataTable
		dt = D_ObjLogCritica.SelectRecord(E_ObjLogCritica).Tables(0)
		If (dt.Rows.Count > 0) Then
			If dt.Rows(0).Item("IdLogCritica") Is DBNull.Value Then
				E_ObjLogCritica.IdLogCritica = 0
			Else
				E_ObjLogCritica.IdLogCritica = dt.Rows(0).Item("IdLogCritica")
			End If

			If dt.Rows(0).Item("IdCritica") Is DBNull.Value Then
				E_ObjLogCritica.IdCritica = 0
			Else
				E_ObjLogCritica.IdCritica = dt.Rows(0).Item("IdCritica")
			End If

			If dt.Rows(0).Item("Fecha") Is DBNull.Value Then
				E_ObjLogCritica.Fecha = "19000101"
			Else
				E_ObjLogCritica.Fecha = dt.Rows(0).Item("Fecha")
			End If

			If dt.Rows(0).Item("IdAccion") Is DBNull.Value Then
				E_ObjLogCritica.IdAccion = 0
			Else
				E_ObjLogCritica.IdAccion = dt.Rows(0).Item("IdAccion")
			End If

			If dt.Rows(0).Item("Comentario") Is DBNull.Value Then
				E_ObjLogCritica.Comentario = ""
			Else
				E_ObjLogCritica.Comentario = dt.Rows(0).Item("Comentario")
			End If

			If dt.Rows(0).Item("LastTransactionUser") Is DBNull.Value Then
				E_ObjLogCritica.LastTransactionUser = ""
			Else
				E_ObjLogCritica.LastTransactionUser = dt.Rows(0).Item("LastTransactionUser")
			End If

			If dt.Rows(0).Item("LastTransactionDate") Is DBNull.Value Then
				E_ObjLogCritica.LastTransactionDate = "19000101"
			Else
				E_ObjLogCritica.LastTransactionDate = dt.Rows(0).Item("LastTransactionDate")
			End If

		Else
			E_ObjLogCritica = Nothing
		End If

		Return E_ObjLogCritica
	End Function

	Public Function SelectAll(ByVal IdCritica As Integer) As DataTable
		Dim D_ObjLogCritica As New D_ClsTblLogCritica
		Return D_ObjLogCritica.SelectAll(IdCritica).Tables(0)
	End Function

	Public Function changeStatusCritica(ByVal IdCritica As Integer) As DataTable
		Dim D_ObjLogCritica As New D_ClsTblLogCritica
		Return D_ObjLogCritica.changeStatusCritica(IdCritica).Tables(0)
	End Function



	Public Function Insert(ByVal E_ObjLogCritica As E_ClsTblLogCritica) As String
		Dim D_ObjLogCritica As New D_ClsTblLogCritica
		Return D_ObjLogCritica.Insert(E_ObjLogCritica)
	End Function

	Public Function Update(ByVal E_ObjLogCritica As E_ClsTblLogCritica) As String
		Dim D_ObjLogCritica As New D_ClsTblLogCritica
		Return D_ObjLogCritica.Update(E_ObjLogCritica)
	End Function

	Public Function Delete(ByVal E_ObjLogCritica As E_ClsTblLogCritica) As String
		Dim D_ObjLogCritica As New D_ClsTblLogCritica
		Return D_ObjLogCritica.Delete(E_ObjLogCritica)
	End Function


End Class

