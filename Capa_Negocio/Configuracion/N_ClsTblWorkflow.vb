Imports Capa_Dato
Imports Capa_Entidad

Public Class N_ClsTblWorkflow

	Public Function SelectRecord(ByVal E_ObjWorkflow As E_ClsTblWorkflow) As E_ClsTblWorkflow
		Dim D_ObjWorkflow As New D_ClsTblWorkflow
		Dim dt As New DataTable
		dt = D_ObjWorkflow.SelectRecord(E_ObjWorkflow).Tables(0)
		If (dt.Rows.Count > 0) Then
			If dt.Rows(0).Item("IdWorkflow") Is DBNull.Value Then
				E_ObjWorkflow.IdWorkflow = 0
			Else
				E_ObjWorkflow.IdWorkflow = dt.Rows(0).Item("IdWorkflow")
			End If

			If dt.Rows(0).Item("Workflow") Is DBNull.Value Then
				E_ObjWorkflow.Workflow = ""
			Else
				E_ObjWorkflow.Workflow = dt.Rows(0).Item("Workflow")
			End If

			If dt.Rows(0).Item("Descripcion") Is DBNull.Value Then
				E_ObjWorkflow.Descripcion = ""
			Else
				E_ObjWorkflow.Descripcion = dt.Rows(0).Item("Descripcion")
			End If

			If dt.Rows(0).Item("UltAct") Is DBNull.Value Then
				E_ObjWorkflow.UltAct = ""
			Else
				E_ObjWorkflow.UltAct = dt.Rows(0).Item("UltAct")
			End If

			If dt.Rows(0).Item("Estado") Is DBNull.Value Then
				E_ObjWorkflow.Estado = 0
			Else
				E_ObjWorkflow.Estado = dt.Rows(0).Item("Estado")
			End If

			If dt.Rows(0).Item("LastTransactionUser") Is DBNull.Value Then
				E_ObjWorkflow.LastTransactionUser = ""
			Else
				E_ObjWorkflow.LastTransactionUser = dt.Rows(0).Item("LastTransactionUser")
			End If

			If dt.Rows(0).Item("LastTransactionDate") Is DBNull.Value Then
				E_ObjWorkflow.LastTransactionDate = "19000101"
			Else
				E_ObjWorkflow.LastTransactionDate = dt.Rows(0).Item("LastTransactionDate")
			End If

		Else
			E_ObjWorkflow = Nothing
		End If

		Return E_ObjWorkflow
	End Function

	Public Function SelectAll(ByVal TxtBsc As String) As DataTable
		Dim D_ObjWorkflow As New D_ClsTblWorkflow
		Return D_ObjWorkflow.SelectAll(TxtBsc).Tables(0)
	End Function

	Public Function Insert(ByVal E_ObjWorkflow As E_ClsTblWorkflow) As String
		Dim D_ObjWorkflow As New D_ClsTblWorkflow
		Return D_ObjWorkflow.Insert(E_ObjWorkflow)
	End Function

	Public Function Update(ByVal E_ObjWorkflow As E_ClsTblWorkflow) As String
		Dim D_ObjWorkflow As New D_ClsTblWorkflow
		Return D_ObjWorkflow.Update(E_ObjWorkflow)
	End Function

	Public Function Delete(ByVal E_ObjWorkflow As E_ClsTblWorkflow) As String
		Dim D_ObjWorkflow As New D_ClsTblWorkflow
		Return D_ObjWorkflow.Delete(E_ObjWorkflow)
	End Function

	Public Function LoadCbo() As DataTable
		Dim D_ObjWorkflow As New D_ClsTblWorkflow
		Return D_ObjWorkflow.LoadCbo().Tables(0)
	End Function

End Class

