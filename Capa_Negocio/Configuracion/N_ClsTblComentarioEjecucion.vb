Imports Capa_Dato
Imports Capa_Entidad

Public Class N_ClsTblComentarioEjecucion

	Public Function SelectRecord(ByVal E_ObjComentarioEjecucion As E_ClsTblComentarioEjecucion) As E_ClsTblComentarioEjecucion
		Dim D_ObjComentario As New D_ClsTblComentarioEjecucion
		Dim dt As New DataTable
		dt = D_ObjComentario.SelectRecord(E_ObjComentarioEjecucion).Tables(0)
		If (dt.Rows.Count > 0) Then
			If dt.Rows(0).Item("IdComentario") Is DBNull.Value Then
				E_ObjComentarioEjecucion.IdComentario = 0
			Else
				E_ObjComentarioEjecucion.IdComentario = dt.Rows(0).Item("IdComentario")
			End If

			If dt.Rows(0).Item("IdEjecucion") Is DBNull.Value Then
				E_ObjComentarioEjecucion.IdEjecucion = 0
			Else
				E_ObjComentarioEjecucion.IdEjecucion = dt.Rows(0).Item("IdEjecucion")
			End If

			If dt.Rows(0).Item("Fecha") Is DBNull.Value Then
				E_ObjComentarioEjecucion.Fecha = "19000101"
			Else
				E_ObjComentarioEjecucion.Fecha = dt.Rows(0).Item("Fecha")
			End If

			If dt.Rows(0).Item("Comentario") Is DBNull.Value Then
				E_ObjComentarioEjecucion.Comentario = ""
			Else
				E_ObjComentarioEjecucion.Comentario = dt.Rows(0).Item("Comentario")
			End If

			If dt.Rows(0).Item("LastTransactionUser") Is DBNull.Value Then
				E_ObjComentarioEjecucion.LastTransactionUser = ""
			Else
				E_ObjComentarioEjecucion.LastTransactionUser = dt.Rows(0).Item("LastTransactionUser")
			End If

			If dt.Rows(0).Item("LastTransactionDate") Is DBNull.Value Then
				E_ObjComentarioEjecucion.LastTransactionDate = "19000101"
			Else
				E_ObjComentarioEjecucion.LastTransactionDate = dt.Rows(0).Item("LastTransactionDate")
			End If

		Else
			E_ObjComentarioEjecucion = Nothing
		End If

		Return E_ObjComentarioEjecucion
	End Function

	Public Function SelectAll(ByVal IdEjecucion As Integer) As DataTable
		Dim D_ObjComentario As New D_ClsTblComentarioEjecucion
		Return D_ObjComentario.SelectAll(IdEjecucion).Tables(0)
	End Function

	Public Function Insert(ByVal E_ObjComentarioEjecucion As E_ClsTblComentarioEjecucion) As String
		Dim D_ObjComentario As New D_ClsTblComentarioEjecucion
		Return D_ObjComentario.Insert(E_ObjComentarioEjecucion)
	End Function

	Public Function Update(ByVal E_ObjComentarioEjecucion As E_ClsTblComentarioEjecucion) As String
		Dim D_ObjComentario As New D_ClsTblComentarioEjecucion
		Return D_ObjComentario.Update(E_ObjComentarioEjecucion)
	End Function

	Public Function Delete(ByVal E_ObjComentarioEjecucion As E_ClsTblComentarioEjecucion) As String
		Dim D_ObjComentario As New D_ClsTblComentarioEjecucion
		Return D_ObjComentario.Delete(E_ObjComentarioEjecucion)
	End Function


End Class

