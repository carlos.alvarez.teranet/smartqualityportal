Imports Capa_Dato
Imports Capa_Entidad

Public Class N_ClsTblPrefijo

	Public Function SelectRecord(ByVal E_ObjPrefijo As E_ClsTblPrefijo) As E_ClsTblPrefijo
		Dim D_ObjPrefijo As New D_ClsTblPrefijo
		Dim dt As New DataTable
		dt = D_ObjPrefijo.SelectRecord(E_ObjPrefijo).Tables(0)
		If (dt.Rows.Count > 0) Then
			If dt.Rows(0).Item("IdPrefijo") Is DBNull.Value Then
				E_ObjPrefijo.IdPrefijo = 0
			Else
				E_ObjPrefijo.IdPrefijo = dt.Rows(0).Item("IdPrefijo")
			End If

			If dt.Rows(0).Item("IdCarpeta") Is DBNull.Value Then
				E_ObjPrefijo.IdCarpeta = 0
			Else
				E_ObjPrefijo.IdCarpeta = dt.Rows(0).Item("IdCarpeta")
			End If

			If dt.Rows(0).Item("Prefijo") Is DBNull.Value Then
				E_ObjPrefijo.Prefijo = ""
			Else
				E_ObjPrefijo.Prefijo = dt.Rows(0).Item("Prefijo")
			End If

			If dt.Rows(0).Item("Estado") Is DBNull.Value Then
				E_ObjPrefijo.Estado = 0
			Else
				E_ObjPrefijo.Estado = dt.Rows(0).Item("Estado")
			End If

			If dt.Rows(0).Item("LastTransactionUser") Is DBNull.Value Then
				E_ObjPrefijo.LastTransactionUser = ""
			Else
				E_ObjPrefijo.LastTransactionUser = dt.Rows(0).Item("LastTransactionUser")
			End If

			If dt.Rows(0).Item("LastTransactionDate") Is DBNull.Value Then
				E_ObjPrefijo.LastTransactionDate = "19000101"
			Else
				E_ObjPrefijo.LastTransactionDate = dt.Rows(0).Item("LastTransactionDate")
			End If

		Else
			E_ObjPrefijo = Nothing
		End If

		Return E_ObjPrefijo
	End Function

	Public Function SelectAll(ByVal TxtBsc As String) As DataTable
		Dim D_ObjPrefijo As New D_ClsTblPrefijo
		Return D_ObjPrefijo.SelectAll(TxtBsc).Tables(0)
	End Function

	Public Function SelectAllbyCarpeta(ByVal IdCarpeta As Int16) As DataTable
		Dim D_ObjPrefijo As New D_ClsTblPrefijo
		Return D_ObjPrefijo.SelectAllbyCarpeta(IdCarpeta).Tables(0)
	End Function


	Public Function LoadCbo(ByVal IdCarpeta As Int16) As DataTable
		Dim D_ObjPrefijo As New D_ClsTblPrefijo
		Return D_ObjPrefijo.LoadCbo(IdCarpeta).Tables(0)
	End Function



	Public Function Insert(ByVal E_ObjPrefijo As E_ClsTblPrefijo) As String
		Dim D_ObjPrefijo As New D_ClsTblPrefijo
		Return D_ObjPrefijo.Insert(E_ObjPrefijo)
	End Function

	Public Function Update(ByVal E_ObjPrefijo As E_ClsTblPrefijo) As String
		Dim D_ObjPrefijo As New D_ClsTblPrefijo
		Return D_ObjPrefijo.Update(E_ObjPrefijo)
	End Function

	Public Function Delete(ByVal E_ObjPrefijo As E_ClsTblPrefijo) As String
		Dim D_ObjPrefijo As New D_ClsTblPrefijo
		Return D_ObjPrefijo.Delete(E_ObjPrefijo)
	End Function


End Class

