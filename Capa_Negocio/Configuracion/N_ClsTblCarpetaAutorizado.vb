Imports Capa_Dato
Imports Capa_Entidad

Public Class N_ClsTblCarpetaAutorizado

	Public Function SelectRecord(ByVal E_ObjCarpetaAutorizado As E_ClsTblCarpetaAutorizado) As E_ClsTblCarpetaAutorizado
		Dim D_ObjCarpetaAutorizado As New D_ClsTblCarpetaAutorizado
		Dim dt As New DataTable
		dt = D_ObjCarpetaAutorizado.SelectRecord(E_ObjCarpetaAutorizado).Tables(0)
		If (dt.Rows.Count > 0) Then
			If dt.Rows(0).Item("IdCarpeta") Is DBNull.Value Then
				E_ObjCarpetaAutorizado.IdCarpeta = 0
			Else
				E_ObjCarpetaAutorizado.IdCarpeta = dt.Rows(0).Item("IdCarpeta")
			End If

			If dt.Rows(0).Item("IdAutorizado") Is DBNull.Value Then
				E_ObjCarpetaAutorizado.IdAutorizado = ""
			Else
				E_ObjCarpetaAutorizado.IdAutorizado = dt.Rows(0).Item("IdAutorizado")
			End If

			If dt.Rows(0).Item("LastTransactionUser") Is DBNull.Value Then
				E_ObjCarpetaAutorizado.LastTransactionUser = ""
			Else
				E_ObjCarpetaAutorizado.LastTransactionUser = dt.Rows(0).Item("LastTransactionUser")
			End If

			If dt.Rows(0).Item("LastTransactionDate") Is DBNull.Value Then
				E_ObjCarpetaAutorizado.LastTransactionDate = "19000101"
			Else
				E_ObjCarpetaAutorizado.LastTransactionDate = dt.Rows(0).Item("LastTransactionDate")
			End If

		Else
			E_ObjCarpetaAutorizado = Nothing
		End If

		Return E_ObjCarpetaAutorizado
	End Function

	Public Function SelectAll(ByVal TxtBsc As String) As DataTable
		Dim D_ObjCarpetaAutorizado As New D_ClsTblCarpetaAutorizado
		Return D_ObjCarpetaAutorizado.SelectAll(TxtBsc).Tables(0)
	End Function

	Public Function SelectAllbyCarpeta(ByVal IdCarpeta As Int16) As DataTable
		Dim D_ObjCarpetaAutorizado As New D_ClsTblCarpetaAutorizado
		Return D_ObjCarpetaAutorizado.SelectAllbyCarpeta(IdCarpeta).Tables(0)
	End Function

	Public Function Insert(ByVal E_ObjCarpetaAutorizado As E_ClsTblCarpetaAutorizado) As String
		Dim D_ObjCarpetaAutorizado As New D_ClsTblCarpetaAutorizado
		Return D_ObjCarpetaAutorizado.Insert(E_ObjCarpetaAutorizado)
	End Function

	Public Function Update(ByVal E_ObjCarpetaAutorizado As E_ClsTblCarpetaAutorizado) As String
		Dim D_ObjCarpetaAutorizado As New D_ClsTblCarpetaAutorizado
		Return D_ObjCarpetaAutorizado.Update(E_ObjCarpetaAutorizado)
	End Function

	Public Function Delete(ByVal E_ObjCarpetaAutorizado As E_ClsTblCarpetaAutorizado) As String
		Dim D_ObjCarpetaAutorizado As New D_ClsTblCarpetaAutorizado
		Return D_ObjCarpetaAutorizado.Delete(E_ObjCarpetaAutorizado)
	End Function

	Public Function DeleteAllCarpeta(ByVal IdCarpeta As Integer) As String
		Dim D_ObjCarpetaAutorizado As New D_ClsTblCarpetaAutorizado
		Return D_ObjCarpetaAutorizado.DeleteAllCarpeta(IdCarpeta)
	End Function



End Class

