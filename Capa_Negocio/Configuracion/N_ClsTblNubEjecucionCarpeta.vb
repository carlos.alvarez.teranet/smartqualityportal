Imports Capa_Dato
Imports Capa_Entidad

Public Class N_ClsTblNubEjecucionCarpeta

	Public Function SelectRecord(ByVal E_ObjNubEjecucionCarpeta As E_ClsTblNubEjecucionCarpeta) As E_ClsTblNubEjecucionCarpeta
		Dim D_ObjNubEjecucionCarpeta As New D_ClsTblNubEjecucionCarpeta
		Dim dt As New DataTable
		dt = D_ObjNubEjecucionCarpeta.SelectRecord(E_ObjNubEjecucionCarpeta).Tables(0)
		If (dt.Rows.Count > 0) Then
			If dt.Rows(0).Item("IdEjecucion") Is DBNull.Value Then
				E_ObjNubEjecucionCarpeta.IdEjecucion = 0
			Else
				E_ObjNubEjecucionCarpeta.IdEjecucion = dt.Rows(0).Item("IdEjecucion")
			End If

			If dt.Rows(0).Item("IdCarpeta") Is DBNull.Value Then
				E_ObjNubEjecucionCarpeta.IdCarpeta = 0
			Else
				E_ObjNubEjecucionCarpeta.IdCarpeta = dt.Rows(0).Item("IdCarpeta")
			End If

			If dt.Rows(0).Item("LastTransactionUser") Is DBNull.Value Then
				E_ObjNubEjecucionCarpeta.LastTransactionUser = ""
			Else
				E_ObjNubEjecucionCarpeta.LastTransactionUser = dt.Rows(0).Item("LastTransactionUser")
			End If

			If dt.Rows(0).Item("LastTransactionDate") Is DBNull.Value Then
				E_ObjNubEjecucionCarpeta.LastTransactionDate = "19000101"
			Else
				E_ObjNubEjecucionCarpeta.LastTransactionDate = dt.Rows(0).Item("LastTransactionDate")
			End If

		Else
			E_ObjNubEjecucionCarpeta = Nothing
		End If

		Return E_ObjNubEjecucionCarpeta
	End Function

	Public Function SelectAll(ByVal IdEjecucion As Integer) As DataTable
		Dim D_ObjNubEjecucionCarpeta As New D_ClsTblNubEjecucionCarpeta
		Return D_ObjNubEjecucionCarpeta.SelectAll(IdEjecucion).Tables(0)
	End Function

	Public Function Insert(ByVal E_ObjNubEjecucionCarpeta As E_ClsTblNubEjecucionCarpeta) As String
		Dim D_ObjNubEjecucionCarpeta As New D_ClsTblNubEjecucionCarpeta
		Return D_ObjNubEjecucionCarpeta.Insert(E_ObjNubEjecucionCarpeta)
	End Function

	Public Function Update(ByVal E_ObjNubEjecucionCarpeta As E_ClsTblNubEjecucionCarpeta) As String
		Dim D_ObjNubEjecucionCarpeta As New D_ClsTblNubEjecucionCarpeta
		Return D_ObjNubEjecucionCarpeta.Update(E_ObjNubEjecucionCarpeta)
	End Function

	Public Function Delete(ByVal E_ObjNubEjecucionCarpeta As E_ClsTblNubEjecucionCarpeta) As String
		Dim D_ObjNubEjecucionCarpeta As New D_ClsTblNubEjecucionCarpeta
		Return D_ObjNubEjecucionCarpeta.Delete(E_ObjNubEjecucionCarpeta)
	End Function


End Class

