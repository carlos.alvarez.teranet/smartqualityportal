Imports Capa_Dato
Imports Capa_Entidad

Public Class N_ClsTblJobPositionAprobador

	Public Function SelectRecord(ByVal E_ObjJobPositionAprobador As E_ClsTblJobPositionAprobador) As E_ClsTblJobPositionAprobador
		Dim D_ObjJobPositionAprobador As New D_ClsTblJobPositionAprobador
		Dim dt As New DataTable
		dt = D_ObjJobPositionAprobador.SelectRecord(E_ObjJobPositionAprobador).Tables(0)
		If (dt.Rows.Count > 0) Then
			If dt.Rows(0).Item("IdJobPosition") Is DBNull.Value Then
				E_ObjJobPositionAprobador.IdJobPosition = 0
			Else
				E_ObjJobPositionAprobador.IdJobPosition = dt.Rows(0).Item("IdJobPosition")
			End If

			If dt.Rows(0).Item("IdAprobador") Is DBNull.Value Then
				E_ObjJobPositionAprobador.IdAprobador = ""
			Else
				E_ObjJobPositionAprobador.IdAprobador = dt.Rows(0).Item("IdAprobador")
			End If

			If dt.Rows(0).Item("LastTransactionUser") Is DBNull.Value Then
				E_ObjJobPositionAprobador.LastTransactionUser = ""
			Else
				E_ObjJobPositionAprobador.LastTransactionUser = dt.Rows(0).Item("LastTransactionUser")
			End If

			If dt.Rows(0).Item("LastTransactionDate") Is DBNull.Value Then
				E_ObjJobPositionAprobador.LastTransactionDate = "19000101"
			Else
				E_ObjJobPositionAprobador.LastTransactionDate = dt.Rows(0).Item("LastTransactionDate")
			End If

		Else
			E_ObjJobPositionAprobador = Nothing
		End If

		Return E_ObjJobPositionAprobador
	End Function



	Public Function getAprobadoresDisponibles(ByVal IdJobPosition As Integer) As DataTable
		Dim D_ObjJobPositionAprobador As New D_ClsTblJobPositionAprobador
		Return D_ObjJobPositionAprobador.getAprobadoresJobPosition(IdJobPosition).Tables(0)
	End Function
	Public Function SelectAll(ByVal IdJobPosition As Integer) As DataTable
		Dim D_ObjJobPositionAprobador As New D_ClsTblJobPositionAprobador
		Return D_ObjJobPositionAprobador.SelectAll(IdJobPosition).Tables(0)
	End Function

	Public Function Insert(ByVal E_ObjJobPositionAprobador As E_ClsTblJobPositionAprobador) As String
		Dim D_ObjJobPositionAprobador As New D_ClsTblJobPositionAprobador
		Return D_ObjJobPositionAprobador.Insert(E_ObjJobPositionAprobador)
	End Function

	Public Function Update(ByVal E_ObjJobPositionAprobador As E_ClsTblJobPositionAprobador) As String
		Dim D_ObjJobPositionAprobador As New D_ClsTblJobPositionAprobador
		Return D_ObjJobPositionAprobador.Update(E_ObjJobPositionAprobador)
	End Function

	Public Function Delete(ByVal E_ObjJobPositionAprobador As E_ClsTblJobPositionAprobador) As String
		Dim D_ObjJobPositionAprobador As New D_ClsTblJobPositionAprobador
		Return D_ObjJobPositionAprobador.Delete(E_ObjJobPositionAprobador)
	End Function


End Class

