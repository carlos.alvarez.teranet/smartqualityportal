Imports Capa_Dato
Imports Capa_Entidad

Public Class N_ClsTblVariableCondicion

	Public Function SelectRecord(ByVal E_ObjVariableCondicion As E_ClsTblVariableCondicion) As E_ClsTblVariableCondicion
		Dim D_ObjVariableCondicion As New D_ClsTblVariableCondicion
		Dim dt As New DataTable
		dt = D_ObjVariableCondicion.SelectRecord(E_ObjVariableCondicion).Tables(0)
		If (dt.Rows.Count > 0) Then
			If dt.Rows(0).Item("IdVariableCondicion") Is DBNull.Value Then
				E_ObjVariableCondicion.IdVariableCondicion = 0
			Else
				E_ObjVariableCondicion.IdVariableCondicion = dt.Rows(0).Item("IdVariableCondicion")
			End If

			If dt.Rows(0).Item("VariableCondicion") Is DBNull.Value Then
				E_ObjVariableCondicion.VariableCondicion = ""
			Else
				E_ObjVariableCondicion.VariableCondicion = dt.Rows(0).Item("VariableCondicion")
			End If

			If dt.Rows(0).Item("IdTipoDato") Is DBNull.Value Then
				E_ObjVariableCondicion.IdTipoDato = 0
			Else
				E_ObjVariableCondicion.IdTipoDato = dt.Rows(0).Item("IdTipoDato")
			End If

			If dt.Rows(0).Item("Largo") Is DBNull.Value Then
				E_ObjVariableCondicion.Largo = ""
			Else
				E_ObjVariableCondicion.Largo = dt.Rows(0).Item("Largo")
			End If

			If dt.Rows(0).Item("Tabla") Is DBNull.Value Then
				E_ObjVariableCondicion.Tabla = ""
			Else
				E_ObjVariableCondicion.Tabla = dt.Rows(0).Item("Tabla")
			End If

			If dt.Rows(0).Item("Estado") Is DBNull.Value Then
				E_ObjVariableCondicion.Estado = 0
			Else
				E_ObjVariableCondicion.Estado = dt.Rows(0).Item("Estado")
			End If

			If dt.Rows(0).Item("isComilla") Is DBNull.Value Then
				E_ObjVariableCondicion.isComilla = False
			Else
				E_ObjVariableCondicion.isComilla = dt.Rows(0).Item("isComilla")
			End If

			If dt.Rows(0).Item("LastTransactionUser") Is DBNull.Value Then
				E_ObjVariableCondicion.LastTransactionUser = ""
			Else
				E_ObjVariableCondicion.LastTransactionUser = dt.Rows(0).Item("LastTransactionUser")
			End If

			If dt.Rows(0).Item("LastTransactionDate") Is DBNull.Value Then
				E_ObjVariableCondicion.LastTransactionDate = "19000101"
			Else
				E_ObjVariableCondicion.LastTransactionDate = dt.Rows(0).Item("LastTransactionDate")
			End If

		Else
			E_ObjVariableCondicion = Nothing
		End If

		Return E_ObjVariableCondicion
	End Function
	Public Function LoadCbo() As DataTable
		Dim D_ObjVariableCondicion As New D_ClsTblVariableCondicion
		Return D_ObjVariableCondicion.LoadCbo().Tables(0)
	End Function
	Public Function SelectAll(ByVal TxtBsc As String) As DataTable
		Dim D_ObjVariableCondicion As New D_ClsTblVariableCondicion
		Return D_ObjVariableCondicion.SelectAll(TxtBsc).Tables(0)
	End Function

	Public Function Insert(ByVal E_ObjVariableCondicion As E_ClsTblVariableCondicion) As String
		Dim D_ObjVariableCondicion As New D_ClsTblVariableCondicion
		Return D_ObjVariableCondicion.Insert(E_ObjVariableCondicion)
	End Function

	Public Function Update(ByVal E_ObjVariableCondicion As E_ClsTblVariableCondicion) As String
		Dim D_ObjVariableCondicion As New D_ClsTblVariableCondicion
		Return D_ObjVariableCondicion.Update(E_ObjVariableCondicion)
	End Function

	Public Function Delete(ByVal E_ObjVariableCondicion As E_ClsTblVariableCondicion) As String
		Dim D_ObjVariableCondicion As New D_ClsTblVariableCondicion
		Return D_ObjVariableCondicion.Delete(E_ObjVariableCondicion)
	End Function


End Class

